let chai = require('chai');
let chaiHttp = require('chai-http');

let server = require('../../server/server');

// Assertion Style
chai.should();

chai.use(chaiHttp);


let Token;

let userReq = {
    "email": "ysquaretechnology@gmail.com",
    "password": "Mcities12345"
}

const getToken = () => {
    return new Promise(resolve => {
        if (Token) return resolve(Token);
        return chai.request(server).post('/api/users/login').send(userReq).end((err, res) => {
            res.should.have.status(200);
            resolve(res.body.data.id);
        })
    })
}

module.exports = getToken;