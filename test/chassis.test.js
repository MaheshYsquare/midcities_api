const chai = require('chai');
const chaiHttp = require('chai-http');

const server = require('../server/server');
const getToken = require('./seed/seed'); 

// Assertion Style
chai.should();

chai.use(chaiHttp);


/** CHASSIS MODEL */
describe('Chassis Model', () => {

    let Token;

    before("USER AUTH", async () => {
        
        Token = await getToken();

    });

    /**
     * TEST THE CHASSIS GET
     */
    describe("POST /getChassisManagementData", () => {

        let req = {
            offset: 1,
            limit: 10,
            column: "",
            direction: null,
            columnFilter: false,
            columnFilterValue: null,
        }

        it('should send status 401 if token not send', (done) => {

            chai.request(server).post('/api/chassises/getChassisManagementData')
                .send(req)
                .end((err, res) => {
                    if (err) return done(err);
                    res.should.have.status(401);
                    done();
                })
        })

        it('should receive list of 10 archived records', (done) => {

            chai.request(server).post('/api/chassises/getChassisManagementData')
                .set("accesstoken", Token)
                .send(req)
                .end((err, res) => {
                    if (err) return done(err);
                    res.should.have.status(200);
                    res.body.should.be.a("object");
                    res.body.should.have.property("data");
                    res.body.should.have.property("data").should.be.a("object");
                    res.body.data.should.have.property("chassisData");
                    res.body.data.should.have.property("chassisPaginationLength");
                    res.body.data.chassisPaginationLength.should.be.a("number");
                    res.body.data.chassisData.should.be.a("array");
                    res.body.data.should.have.property("chassisData").with.lengthOf(10);
                    done();
                })
        })

    })
})
