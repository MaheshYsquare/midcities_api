const chai = require('chai');
const chaiHttp = require('chai-http');

const server = require('../server/server');
const getToken = require('./seed/seed');

// Assertion Style
chai.should();

chai.use(chaiHttp);

/** ARCHIVED MODEL */
describe('Archived Model', () => {

    let Token;

    before("USER AUTH", async () => {
        let userReq = {
            "email": "ysquaretechnology@gmail.com",
            "password": "Mcities12345"
        }

        Token = await getToken(userReq);

    });

    /**
     * TEST THE ARCHIVED ORDERS GET
     */
    describe("POST /getArchivedData", () => {

        let req = {
            offset: 1,
            limit: 10,
            column: "",
            direction: null,
            columnFilter: false,
            filterData: null,
            timeZone: "Asia/Calcutta"
        }

        it('should send status 401 if token not send', (done) => {

            chai.request(server).post('/api/archives/getArchivedData')
                .send(req)
                .end((err, res) => {
                    if (err) return done(err);
                    res.should.have.status(401);
                    done();
                })
        })

        it('should receive list of 10 archived records', (done) => {

            chai.request(server).post('/api/archives/getArchivedData')
                .set("accesstoken", Token)
                .send(req)
                .end((err, res) => {
                    if (err) return done(err);
                    res.should.have.status(200);
                    res.body.should.be.a("object");
                    res.body.should.have.property("data");
                    res.body.should.have.property("data").should.be.a("object");
                    res.body.data.should.have.property("archivedOrder");
                    res.body.data.should.have.property("archivedOrderPaginationLength");
                    res.body.data.archivedOrderPaginationLength.should.be.a("number");
                    res.body.data.archivedOrder.should.be.a("array");
                    res.body.data.should.have.property("archivedOrder").with.lengthOf(10);
                    done();
                })
        })

    })
})
