FROM node:10.14.1-alpine

WORKDIR /tms-service-api

RUN npm install -g pm2

COPY package.json /tms-service-api

RUN npm install

COPY . /tms-service-api

ENV NODE_ENV=development

# RUN chmod +x run.sh

CMD pm2-docker start pm2-config/app-dev.json

EXPOSE 3000

# ENTRYPOINT ["/tms-service-api/run.sh"]