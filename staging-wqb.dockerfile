FROM node:10.14.1-alpine
WORKDIR /tms-service-api
RUN npm config set unsafe-perm true
RUN npm install -g pm2
COPY package.json /tms-service-api

### Add phantomjs
RUN wget -qO- "https://github.com/dustinblackman/phantomized/releases/download/2.1.1a/dockerized-phantomjs.tar.gz" | tar xz -C / \
    && npm config set user 0 \
    && npm install -g phantomjs-prebuilt
### Add fonts required by phantomjs to render html correctly
RUN apk add --update ttf-dejavu ttf-droid ttf-freefont ttf-liberation ttf-ubuntu-font-family && rm -rf /var/cache/apk/*

RUN npm install
COPY . /tms-service-api
RUN chmod +x run.sh
EXPOSE 3000
ENTRYPOINT ["/tms-service-api/run.sh"]