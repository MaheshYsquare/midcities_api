FROM node:10.14.1-alpine

WORKDIR /tms-service-api

RUN npm install -g pm2

COPY package.json /tms-service-api

RUN npm install

RUN pm2 install pm2-logrotate

RUN pm2 set pm2-logrotate:retain '*'

RUN pm2 set pm2-logrotate:dateFormat YYYY-MM-DD_HH-mm-ss

RUN pm2 set pm2-logrotate:max_size 10M

RUN pm2 set pm2-logrotate:compress true

RUN pm2 set pm2-logrotate:rotateInterval '0 0 * * *'

RUN pm2 set pm2-logrotate:rotateModule true

RUN pm2 set pm2-logrotate:workerInterval 10

COPY . /tms-service-api

ENV NODE_ENV=production

CMD pm2-docker start pm2-config/app-prod.json

# RUN chmod +x run.sh
# ENTRYPOINT ["/tms-service-api/run.sh"]