'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var path = require('path');
var bodyParser = require('body-parser');
var app = module.exports = loopback();
let config = require('./boot/load-config').getConfig();
var events = require("events");
var OAuthClient = require('intuit-oauth');

app.oauthClient = new OAuthClient({
  clientId: config.quickBookClientId,
  clientSecret: config.quickBookClientSecret,
  environment: config.quickBookEnvironment,
  redirectUri: config.quickBookRedirectUri
});

// configure view handler
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(loopback.static(path.resolve(__dirname, '../client')));

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: '60mb' }));

// This is to get access_token only in the  cookies and headers not in params
app.use(loopback.token({
  cookies: ['access_token'],
  headers: ['accesstoken', 'X-Access-Token']
  //additional keys (check docs for more info)
}));

app.eventEmitter = new events.EventEmitter();

app.sockets = new Set();

app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};


// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;
  // start the server if `$ node server.js`
  if (require.main === module) {

    app.use(function updateToken(req, res, next) {
      var token = req.accessToken; // get the accessToken from the request
      if (!token) return next(); // if there's no token we use next() to delegate handling back to loopback
      var now = new Date();
      // EDIT: to make sure we don't use a token that's already expired, we don't update it
      // this line is not really needed, because the framework will catch invalid tokens already
      if (token.created.getTime() + (token.ttl * 1000) < now.getTime()) return next();
      // performance optimization, we do not update the token more often than once per five minutes
      if (now.getTime() - token.created.getTime() < 300000) return next();
      token.updateAttribute('created', now, next); // save to db and move on
    });

    // app.start()

    app.io = require('socket.io')(app.start());

    app.io.on('connection', socket => {

      socket.emit('addSocket', true);

      socket.on('login', data => {
        var AccessToken = app.models.AccessToken;
        //get credentials sent by the client
        if (data && data.userDetails) {
          AccessToken.find({
            where: {
              and: [{ userId: data.userDetails.userId }, { id: data.userDetails.id }]
            }
          }, function (tokenErr, tokenDetail) {
            if (tokenErr) throw tokenErr;
            if (tokenDetail.length) {
              if (!app.sockets.has(socket)) {
                socket.id = data.userDetails.userId;
                app.sockets.add(socket);
                console.log(new Date(), `[SOCKET] USER SOCKET ADDED ${socket.id}`, `TOTAL SOCKETS: ${app.sockets.size}`);
              }
            }
          });
        }
      });

      socket.on('logout', data => {
        app.sockets.delete(socket)
        console.log(new Date(), `[SOCKET] USER SOCKET DISCONNECTED ${socket.id}`, `REMAINING SOCKETS: ${app.sockets.size}`);
      })

      socket.on('changesInOrderDriver', data => {
        if (!app.sockets.has(socket)) {
          socket.id = data.userId;
          app.sockets.add(socket);
          console.log(new Date(), `[SOCKET] USER SOCKET ADDED ${socket.id}`, `TOTAL SOCKETS: ${app.sockets.size}`);
        }
        for (const s of app.sockets) {
          s.emit('data', { data: data.table });
        }
      })

      socket.on('sendNotification', data => {
        if (!app.sockets.has(socket)) {
          socket.id = data.userId;
          app.sockets.add(socket);
          console.log(new Date(), `[SOCKET] USER SOCKET ADDED ${socket.id}`, `TOTAL SOCKETS: ${app.sockets.size}`);
        }
        app.eventEmitter.emit('postNotification', data);
      })

      socket.on('disconnect', data => {
        app.sockets.delete(socket);
        console.log(new Date(), `[SOCKET] USER SOCKET DISCONNECTED ${socket.id}`, `REMAINING SOCKETS: ${app.sockets.size}`);
      });
    });
  }
});
