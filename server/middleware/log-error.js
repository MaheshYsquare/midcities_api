'use strict';

module.exports = function () {
    return function logError(err, req, res, next) {
        console.error(new Date(), "[MIDDLEWARE]", req.url, err);
        next(err);
    };
};