'use strict';

const OAuthClient = require('intuit-oauth');

const app = require('../../server/server.js');
const query = require('./auth');

/** To authenticate or authorize with quickbook tokens to proceed desired operation  */
const authenticateQuickbooksConnection = (model) => {

    return new Promise(async (resolve, reject) => {

        if (app.oauthClient.token.getToken().access_token) {

            checkValidityOfQuickbboksToken(resolve);

        } else {

            let quickBooksToken = await fetchQuickbooksToken(model);

            if (quickBooksToken.code !== 200) {
                return resolve(quickBooksToken)
            }

            if (quickBooksToken.data.length) {
                console.log(new Date(), "QUICKBOOKS RETRIEVED FROM DB");

                app.oauthClient.setToken(quickBooksToken.data[0]);

                checkValidityOfQuickbboksToken(resolve);

                return;
            }

            console.log(new Date(), "QUICKBOOKS CONNECTION NEEDED");

            return resolve({ code: 103, error: 'Please Connect To Quickbooks' });

        }

    })
}

const checkValidityOfQuickbboksToken = (resolve) => {

    if (app.oauthClient.isAccessTokenValid()) {
        return resolve({ code: 200, data: "quickbooks token valid" });
    }

    app.oauthClient.refresh()
        .then((authResponse) => {
            app.eventEmitter.emit('quickbooksTokenAdd', authResponse.getToken());

            return resolve({ code: 200, data: "quickbooks token refreshed" });
        })
        .catch((e) => {
            console.error(new Date(), `[QUICKBOOKS] TOKEN REFRESH ERROR`, e);

            return resolve({ code: 103, error: 'Please Connect To Quickbooks' })
        });
}

const fetchQuickbooksToken = (model) => {
    return new Promise((resolve, reject) => {
        return query.executeQuery(model, 'select * from quickbooks_token_master where id = 1;', null,
            (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[FETCH] QUICKBOOKS TOKEN DATA QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return resolve({ code: 409, error: "QUICKBOOKS TOKEN FETCH ERROR" });
                }

                resolve({ code: 200, data: queryResponse });
            })
    })
}

const fetchDataFromQuickbooks = (url) => {

    return new Promise((resolve, reject) => {

        let quickbooksUrl = app.oauthClient.environment == 'sandbox' ?
            OAuthClient.environment.sandbox : OAuthClient.environment.production;

        return app.oauthClient.makeApiCall({ url: `${quickbooksUrl}v3/company/${app.oauthClient.getToken().realmId}/${url}` })
            .then((result) => {

                if (result.statusCode < 200 || result.statusCode > 299) {

                    console.error(new Date(), `[FETCH] QUICKBOOK DATA ERROR`,
                        result && ("body" in result) ? result.body : result);

                    return resolve({ code: 409, error: 'QUICKBOOKS DATA ERROR' });
                }

                resolve({ code: 200, data: result });

            }).catch(err => {
                console.error(new Date(), `[customer] [FETCH] QUICKBOOK DATA CATCH ERROR`, err);

                resolve({ code: 409, error: 'QUICKBOOKS DATA CATCH ERROR' });
            })
    })
}

module.exports = { authenticateQuickbooksConnection, fetchDataFromQuickbooks };