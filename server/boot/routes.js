const crypto = require('crypto');

const redis = require('redis');

const config = require('./../../server/boot/load-config').getConfig();
const loopback = require('../../server/server.js');

const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);

module.exports = function (app) {

  app.get('/api/callback', (req, res) => {

    loopback.oauthClient.createToken(req.url)
      .then((authResponse) => {

        let response = app.oauthClient.getToken();

        app.eventEmitter.emit('quickbooksTokenAdd', response);

        res.send('');

      })
      .catch((e) => {

        console.error(new Date(), "[QUICKBOOKS] [REDIRECT] ROUTE CATCH ERROR", e);

        res.status(500).json(e);
      });
  });

  app.post('/keeptruckins/hooks', (req, res) => {
    for (const s of loopback.sockets) {
      s.emit('locationUpdated', req.body);
    }
    res.status(200).end();
  })

  app.post('/api/notify-railtracing', (req, res) => {

    const signature = req.headers['batch-signature'];

    // if signature is empty return 401
    if (!signature) {
      return res.status(401).json({ code: 401, error: 'FORBIDDEN' });
    }

    const payload = JSON.stringify(req.body);

    //  Validates the payload with the batch-signature hash
    const hash = crypto.createHmac('sha256', config.batchSecretKey)
      .update(payload).digest('hex');

    if (signature !== hash) {
      return res.status(401).json({ code: 401, error: 'FORBIDDEN' });
    }

    if(req.body.isUpdated) client.del('ordersPendingData');

    for (const s of loopback.sockets) {
      s.emit('railTraceUpdate', req.body);
    }

    return res.status(200).json({ code: 200, data: "Success" });

  })


  app.get('/api/confirm-user', (req, res, next) => {

    if (req.query['access_token']) {

      app.models.AccessToken.findById(req.query['access_token'], (err, token) => {

        if (err || !token) return res
          .redirect(`${config.ssl ? 'https' : 'http'}://${config.domain}/#/error/session-expired`);

        res.redirect(`${config.ssl ? 'https' : 'http'}://${config.domain}/#/authentication/new?access_token=${token.id}`);

      })

    }
  });

};
