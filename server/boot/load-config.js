'use strict';

const getConfig = () => {
    let config;

    switch (process.env.NODE_ENV) {
        case 'development':
            config = require('./../config.development.json');
            break;
        case 'staging':
            config = require('./../config.staging.json');
            break;
        case 'production':
            config = require('./../config.production.json');
            break;
        default:
            config = require('./../config.local.json');
            break;
    }

    return config;
}

module.exports = { getConfig };
