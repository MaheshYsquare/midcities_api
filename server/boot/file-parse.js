'use strict';

const fs = require('fs');

const multiparty = require('multiparty');
const async = require('async');
const AWS = require('aws-sdk');

const config = require('./load-config').getConfig();
const auth = require('./auth');
const app = require('../server');

AWS.config.update({
    secretAccessKey: config.awsSecretAccessKey,
    accessKeyId: config.awsAccessKeyId,
    region: config.awsRegion
});
const s3 = new AWS.S3({});

const getFileFromRequest = (req) => new Promise((resolve, reject) => {
    const form = new multiparty.Form();
    form.parse(req, (err, fields, files) => {
        if (err) reject(err);
        if (!files) reject('File was not found in form data.');
        else resolve({ files, fields });
    });
});

const uploadFileToS3 = (Key, model, fields, files, query) => {

    let called = false;

    const q = async.queue((task, callback) => {

        let key = [...Key];

        key.push(task.originalFilename);

        s3.upload({
            Bucket: config.awsS3BucketName,
            ACL: config.awsS3ACL,
            Key: key.join('/'),
            ContentType: task.headers['content-type'],
            Body: fs.createReadStream(task.path)
        }, (s3Err, result) => {

            if (s3Err) {
                console.error(new Date(), `[order] [UPLOAD] AWS UPLOAD ERROR`, s3Err);
                return callback(s3Err);
            }

            let params = [fields.id, result['key'], result['location'],
            result['key'], result['location']]

            auth.executeQuery(model, query, params,
                (postErr, queryResponse) => {
                    if (postErr) {
                        console.error(new Date(), `[file] [UPSERT] ${task.originalFilename} FILE LINK QUERY ERROR`,
                            "sqlMessage" in postErr ? postErr.sqlMessage : postErr);

                        return callback("sqlMessage" in postErr ? postErr.sqlMessage : postErr);
                    }

                    console.log(new Date(), `[file] [UPLOAD] FILE ${task.originalFilename} UPLOADED`);

                    callback();
                });

            // return the values of the successful AWS S3 request
        })
    }, 5);

    q.push(files, (pushErr) => {
        if (pushErr && !called) {
            console.error(new Date(), `[file] [UPLOAD] QUEUE ERROR`, pushErr);

            q.kill();

            called = true

            for (const s of app.sockets) {
                if (s.id === fields.userId) {
                    s.emit('fileUpload', { code: 409, error: "Some Files May Not be Uploaded" });
                }
            }
        }
    })

    q.error(function (error, task) {
        console.error(new Date(), '[file] [UPLOAD] Queue Error', error, task);

        for (const s of app.sockets) {
            if (s.id === fields.userId) {
                s.emit('fileUpload', { code: 409, error: "Some Files May Not be Uploaded" });
            }
        }
    });

    q.drain(() => {
        for (const s of app.sockets) {
            if (s.id === fields.userId) {
                s.emit('fileUpload', { code: 200, error: null });
            }
        }
    });
}

const deleteFileToS3 = (fileName) => {
    return new Promise((resolve, reject) => {
        let params = {
            Bucket: config.awsS3BucketName,
            Key: fileName
        };
        s3.deleteObject(params, function (err, data) {
            if (err) {
                console.error(new Date(), "[file] [DELETE] AWS DELETE ERROR", err)
                return resolve({ code: 409, error: "S3 ERROR" });
            }
            if (data) {
                console.log(new Date(), "[file] FILE DELETED SUCCESSFULLY");
                resolve({ code: 200, data });
            }
        });
    });
};

const getFileFromS3 = (fileName) => {
    return new Promise((resolve, reject) => {
        let params = {
            Bucket: config.awsS3BucketName,
            Key: fileName
        };
        return s3.getObject(params, (err, s3Response) => {
            if (err) {
                console.error(new Date(), "[file] [FETCH] AWS RETRIEVE FILE ERROR", err);

                return resolve({ code: 409, error: "S3 ERROR" });
            }

            return resolve({
                code: 200,
                data: {
                    ContentType: s3Response.ContentType,
                    buffer: s3Response.Body
                }
            })
        });
    });
};

const getFileSizeFromS3 = (fileName) => {
    return new Promise((resolve, reject) => {
        let params = {
            Bucket: config.awsS3BucketName,
            Prefix: fileName
        };
        return s3.listObjectsV2(params, (err, awsResponse) => {
            if (err) {
                console.error(new Date(), "[file] [FETCH] AWS RETRIEVE FILE SIZE ERROR", err);

                return resolve({ code: 409, error: "S3 ERROR" });
            }
            let fileSizeObject = {};

            awsResponse.Contents ? awsResponse.Contents.forEach(temp => {
                fileSizeObject[temp.Key] = temp.Size;
            }) : null;

            return resolve({
                code: 200,
                data: fileSizeObject
            })

        });
    })
}


module.exports = {
    getFileFromRequest,
    uploadFileToS3,
    deleteFileToS3,
    getFileFromS3,
    getFileSizeFromS3
};