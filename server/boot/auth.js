'use strict';

const executeQuery = (model, query, params, callback) => {
    model.dataSource.connector.query(query, params, callback);
}

const validateAccessToken = (model, access_token, callback) => {
    model.app.models.AccessToken.findById(access_token, callback)
}

const setToken = (model) => {
    model.createOptionsFromRemotingContext = function (ctx) {
        let base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }
}

module.exports = { validateAccessToken, executeQuery, setToken };