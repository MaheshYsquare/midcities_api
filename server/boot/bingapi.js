'use strict';

const config = require('./../../server/boot/load-config').getConfig();
const request = require('request');

const getMiles = (waypointFrom, waypointTo) => {
    let bingApiOptions = {
        uri: `https://dev.virtualearth.net/REST/v1/Routes/Truck?wp.0=${encodeURIComponent(waypointFrom)}&wp.1=${encodeURIComponent(waypointTo)}&distanceUnit=mile&key=${config.bingApiKey}`,
        method: "GET"
    }
    return new Promise((resolve, reject) => {
        return request(bingApiOptions, (err, result) => {
            if (err) {
                console.error(new Date(), "[API] BING API CONNECT ERROR", err);
                return resolve(0);
            } else {
                if (result.statusCode < 200 || result.statusCode > 299) {
                    console.error(new Date(), "[API] BING API ERROR", result.body);
                    return resolve(0)
                } else {
                    let bingResponse = JSON.parse(result.body)

                    let mile = 0;

                    ('resourceSets' in bingResponse) ? bingResponse.resourceSets.forEach(temp => {
                        temp.resources.forEach(element => {
                            mile = Math.ceil(+element.travelDistance)
                        })
                    }) : null

                    console.log(new Date(), "[API] DISTANCE CALCULATED", mile);

                    return resolve(mile ? mile : 0);
                }
            }
        })
    }).catch(err => {
        console.error(new Date(), "[API] BING API PROMISE CATCH ERROR", err);
    })
}

module.exports = { getMiles };