#!/bin/bash
docker build -t tms-service-api .
commithash=`git rev-parse HEAD`
commithash="${commithash//$'\r'/}"
docker tag tms-service-api us.gcr.io/tmstestenv/tms-service-api:$commithash
docker push us.gcr.io/tmstestenv/tms-service-api:$commithash
kubectl set image deployment/tms-service-api tms-service-api=us.gcr.io/tmstestenv/tms-service-api:$commithash
