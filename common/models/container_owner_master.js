'use strict';

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const _ = require('lodash');

module.exports = function (Containerownermanagement) {

    let queryParams;

    //Datasource Connector
    const executeQuery = (query, params, callback) => {
        Containerownermanagement.dataSource.connector.query(query, params, callback);
    }

    //Access Token
    const validateAccessToken = (access_token, callback) => {
        Containerownermanagement.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Containerownermanagement.createOptionsFromRemotingContext = function (ctx) {
        let base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    //post container management data
    const postContainerManagementQuery = (params) => {
        return {
            "query": "insert into container_owner_master (container_name,address,street,city,postal_code,state,email,phone_number,preferred_chassis_rental,location_name,hire_location) values (?,?,?,?,?,?,?,?,?,UCASE(?),?)",
            "params": params
        };
    }

    const postContainerManagementValidation = (request, response, options, callback) => {
        try {
            queryParams = postContainerManagementQuery([request.container_name, request.address, request.street, request.city, request.postal_code, request.state, request.email, request.phone_number, request.preferred_chassis_rental, request.location_name, request.hire_location]);
            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    if (('code' in err) && err['code'] == 'ER_DUP_ENTRY') {
                        callback(null, { code: 422, error: 'Container Name Already Exists' })
                    } else {
                        console.error(new Date(), `[container_owner] [POST] CONTAINER QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'DB ERROR' });
                    }
                } else {
                    client.del('container_master');
                    callback(null, { code: 200, data: "container created successfully" });
                }
            });
        }
        catch (e) {
            console.error(new Date(), `[container_owner] [POST] CONTAINER CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Containerownermanagement.postContainerManagement = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                postContainerManagementValidation(request, response, options, callback);
            }
        });

    }

    //get container management
    const getContainerManagementQuery = (params) => {
        return {
            "query": "select * from container_owner_master where container_name is not null",
            "params": params
        };
    }

    const getContainerManagementDataValidation = (request, response, section, options, callback) => {
        try {
            queryParams = getContainerManagementQuery();
            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                try {
                    if (err) {
                        console.error(new Date(), `[container_owner] [FETCH] CONTAINER QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: 'DB ERROR' });
                    } else {
                        queryResponse.forEach(temp => {

                            temp.hire_location = temp.hire_location !== null ?
                                JSON.parse(temp.hire_location) : null;

                            temp.preferred_chassis_rental = temp.preferred_chassis_rental !== null ?
                                temp.preferred_chassis_rental.split(',') : null;
                        })

                        client.setex("container_master", 3600, JSON.stringify(queryResponse));
                        section === 'autoSuggest' ?
                            searchContainerAutosuggestCache(request, response, section, options, callback) :
                            getContainerManagementCache(request, response, section, options, callback);
                    }
                } catch (error) {
                    console.error(new Date(), `[container_owner] [FETCH] CONTAINER CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (e) {
            console.error(new Date(), `[container_owner] [FETCH] CONTAINER CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    const getContainerManagementCache = (request, response, section, options, callback) => {
        let containerParsedData = [];
        let redisResponse;

        client.get("container_master", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[container_owner] [FETCH] CONTAINER REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' });
                } else {
                    if (cacheData) {
                        containerParsedData = JSON.parse(cacheData);

                        if (request.columnFilter) {
                            Object.keys(request.columnFilterValue).forEach(element => {
                                request.columnFilterValue[element] = request.columnFilterValue[element]
                                    .toLowerCase().replace(/[^a-zA-Z0-9@]+/ig, "")

                                containerParsedData = containerParsedData.length !== 0 ?
                                    containerParsedData.filter(temp => {
                                        return request.columnFilterValue[element] !== "" ?
                                            (
                                                (element !== 'address' && element !== 'hire_location' &&
                                                    element !== 'preferred_chassis_rental' && element !== 'email') ?
                                                    (
                                                        temp[element] ?
                                                            temp[element].toLowerCase()
                                                                .replace(/[^a-zA-Z0-9@]+/ig, "")
                                                                .indexOf(request.columnFilterValue[element]) === 0
                                                            : false
                                                    ) :
                                                    (
                                                        element === 'hire_location' ?
                                                            (
                                                                temp[element] ?
                                                                    temp[element].map(elem => elem.hire_name)
                                                                        .some(elem => elem.toString()
                                                                            .toLowerCase()
                                                                            .replace(/[^a-zA-Z0-9]+/ig, "")
                                                                            .indexOf(
                                                                                request.columnFilterValue[element]
                                                                            ) === 0)
                                                                    : false
                                                            )
                                                            : (
                                                                element === 'email' ?
                                                                    temp[element]
                                                                        .split(',')
                                                                        .some(elem => elem.toString()
                                                                            .toLowerCase()
                                                                            .replace(/[^a-zA-Z0-9@]+/ig, "")
                                                                            .indexOf(request.columnFilterValue[element]) === 0) :
                                                                    (
                                                                        temp[element] ?
                                                                            temp[element].toString().toLowerCase()
                                                                                .replace(/[^a-zA-Z0-9]+/ig, "")
                                                                                .includes(
                                                                                    request.columnFilterValue[element]
                                                                                )
                                                                            : false
                                                                    )
                                                            )
                                                    )
                                            ) : true
                                    }) : []
                            });
                        }

                        redisResponse = {
                            code: 200,
                            error: null,
                            data: formatContainerData(request, containerParsedData)
                        }

                        callback(null, redisResponse);
                    } else {
                        getContainerManagementDataValidation(request, response, section, options, callback);
                    }
                }
            } catch (error) {
                console.error(new Date(), `[container_owner] [FETCH] CONTAINER CACHE CODE ERROR`, error);

                response.status(400).json({ code: 400, error: 'CODE ERROR' });
            }
        });
    }

    const formatContainerData = (request, containerParsedData) => {
        let containerData;
        let containerPaginationLength;

        if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {
            containerData = containerParsedData.length !== 0 ? _.orderBy(containerParsedData, request.column, request.direction).slice((request.offset - 1), request.limit) : [];
        } else {
            containerData = containerParsedData.length !== 0 ? _.orderBy(containerParsedData, 'container_id', 'desc').slice((request.offset - 1), request.limit) : [];
        }

        containerPaginationLength = containerParsedData.length;

        return { containerData, containerPaginationLength }
    }

    Containerownermanagement.getContainerManagementData = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getContainerManagementCache(request, response, 'get', options, callback);
            }
        });

    }

    //update container management data
    const updateContainerManagementQueryParams = (params) => {
        return {
            "query": "update container_owner_master set container_name=?,address=?,street=?,city=?,postal_code=?,state=?,email=?,phone_number=?,preferred_chassis_rental=?,location_name = UCASE(?),hire_location = ? where container_id=?",
            "params": params
        };
    }

    const updateContainerManagementValidation = (request, response, options, callback) => {
        try {
            queryParams = updateContainerManagementQueryParams([request.container_name,
            request.address, request.street, request.city, request.postal_code, request.state,
            request.email, request.phone_number, request.preferred_chassis_rental,
            request.location_name, request.hire_location, request.container_id]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    if (('code' in err) && err['code'] == 'ER_DUP_ENTRY') {
                        callback(null, { code: 422, error: 'Container Name Already Exists' });
                    } else {
                        console.error(new Date(), `[container_owner] [UPDATE] CONTAINER QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'DB ERROR' });
                    }
                } else {
                    client.del('container_master');
                    client.del("ordersDispatchData");
                    client.del("ordersPendingData");
                    client.del("ordersDraftData");
                    client.del("dispatchData");
                    client.del("invoiceData");
                    callback(null, { code: 200, data: "container updated successfully" });
                }
            });
        } catch (e) {
            console.error(new Date(), `[container_owner] [UPDATE] CONTAINER CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Containerownermanagement.updateContainerManagement = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                updateContainerManagementValidation(request, response, options, callback);
            }
        });

    }

    //delete container management data
    const deleteContainerManagementQueryParams = (params) => {
        return {
            "query": "DELETE FROM container_owner_master WHERE container_id=?",
            "params": params
        };
    }

    const deleteContainerManagementValidation = (container_id, response, options, callback) => {
        try {
            queryParams = deleteContainerManagementQueryParams([container_id]);
            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    if (('code' in err) && err['code'] == 'ER_ROW_IS_REFERENCED_2') {
                        callback(null, { code: 409, error: 'Container is Referenced' });
                    } else {
                        console.error(new Date(), `[container_owner] [DELETE] CONTAINER QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'DB ERROR' });
                    }
                } else {
                    client.del('container_master');
                    callback(null, { code: 200, data: "container successfully deleted" });
                }
            })
        } catch (e) {
            console.error(new Date(), `[container_owner] [DELETE] CONTAINER CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Containerownermanagement.deleteContainerManagement = (container_id, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                deleteContainerManagementValidation(container_id, response, options, callback);
            }
        });

    }

    const searchContainerAutosuggestCache = (searchString, response, section, options, callback) => {
        let parsedData = [];

        client.get("container_master", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[container_owner] [FETCH] CONTAINER SEARCH REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' });
                } else {
                    if (cacheData) {
                        parsedData = JSON.parse(cacheData);

                        parsedData = parsedData.filter(temp => temp.container_name.toLowerCase()
                            .replace(/[^a-zA-Z0-9]+/ig, "")
                            .indexOf(searchString.toLowerCase()
                                .replace(/[^a-zA-Z0-9]+/ig, "")) === 0).map(temp => {
                                    return {
                                        container_name: temp.container_name,
                                        preferred_chassis_rental: temp.preferred_chassis_rental,
                                        hire_location: temp.hire_location
                                    }
                                });

                        callback(null, parsedData);
                    } else {
                        getContainerManagementDataValidation(searchString, response, section, options, callback);
                    }
                }
            } catch (error) {
                console.error(new Date(), `[container_owner] [FETCH] CONTAINER SEARCH CACHE CODE ERROR`, error);

                response.status(400).json({ code: 400, error: 'CODE ERROR' });
            }
        });
    }

    Containerownermanagement.searchContainerAutosuggest = (searchString, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                searchContainerAutosuggestCache(searchString, response, 'autoSuggest', options, callback);
            }
        });

    }

};
