'use strict';

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const _ = require('lodash');
const moment = require('moment');

module.exports = function (Archive) {

    //Datasource connector
    const executeQuery = (query, params, callback) => {
        Archive.dataSource.connector.query(query, params, callback);
    }

    //Access Token
    const validateAccessToken = (access_token, callback) => {
        Archive.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Archive.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    const setArchivedDataInRedis = (request, response, options, callback) => {

        executeQuery('call getArchivedOrders();', null,
            (err, queryResponse) => {
                try {
                    if (err) {
                        console.error(new Date(), `[archived] [FETCH] ARCHIVED ORDERS QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: "DB ERROR" })
                    } else {

                        client.setex("archived_orders", 3600, JSON.stringify(queryResponse[0]));

                        getArchivedOrderFromCache(request, response, options, callback);
                    }
                } catch (error) {
                    console.error(new Date(), `[archived] [FETCH] ARCHIVED ORDERS CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            });
    }


    const getArchivedOrderFromCache = (request, response, options, callback) => {

        let orderData = [];

        client.get("archived_orders", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[archived] [FETCH] ARCHIVED ORDERS REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: "REDIS ERROR" })
                } else {
                    if (cacheData) {

                        orderData = JSON.parse(cacheData);

                        if (request.columnFilter) {
                            orderData = filterArchivedOrderData(request, response, orderData);
                        }

                        let redisResponse = {
                            code: 200,
                            data: formatOrderData(request, orderData)
                        }

                        callback(null, redisResponse);
                    } else {
                        setArchivedDataInRedis(request, response, options, callback);
                    }
                }
            } catch (error) {
                console.error(new Date(), `[archived] [FETCH] ARCHIVED ORDERS CACHE CODE ERROR`, error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        })
    }

    const filterArchivedOrderData = (request, response, parsedData) => {
        try {
            for (let elem of request.filterData) {
                let column = elem.column;
                let values = elem.values;

                parsedData = parsedData.length !== 0 ?
                    parsedData.filter(temp => {
                        if (column === 'order_status') {

                            return temp[column] && temp[column].trim() ?
                                values.includes(temp[column]) : false;

                        } else if (column === 'date_of_payment') {

                            return temp[column] && temp[column].trim() ?
                                moment(temp[column]).tz(request.timeZone).format('M/D/YYYY')
                                    .includes(moment(values).tz(request.timeZone).format('M/D/YYYY'))
                                : false;

                        } else {

                            return temp[column] !== null ? temp[column]
                                .toString().toLowerCase().replace(/ +/g, "")
                                .indexOf(values.toLowerCase().replace(/ +/g, "")) === 0 : false;

                        }
                    })
                    : [];
            }

            return parsedData

        } catch (error) {
            response.status(400).json({ code: 400, error: 'CODE ERROR' })
        }
    }

    const formatOrderData = (request, orderParsedData) => {
        let archivedOrder;
        let archivedOrderPaginationLength;

        if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {
            archivedOrder = orderParsedData.length !== 0 ?
                _.orderBy(orderParsedData, request.column, request.direction)
                    .slice((request.offset - 1), request.limit)
                : [];
        } else {
            archivedOrder = orderParsedData.length !== 0 ?
                orderParsedData.slice((request.offset - 1), request.limit)
                : [];
        }

        archivedOrderPaginationLength = orderParsedData.length;

        return { archivedOrder, archivedOrderPaginationLength }
    }

    Archive.getArchivedData = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                getArchivedOrderFromCache(request, response, options, callback);
            }
        });

    }

};
