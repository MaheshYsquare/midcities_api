'use strict';

const multiparty = require('multiparty');
const fs = require('fs');
const fetch = require('node-fetch');

const auth = require('./../../server/boot/auth');
const config = require('./../../server/boot/load-config').getConfig();

module.exports = function (Chassisinvoice) {

    //calling internally by loopback(overriden)
    Chassisinvoice.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    const getFileFromRequest = (req) => new Promise((resolve, reject) => {
        const form = new multiparty.Form();
        form.parse(req, (err, fields, files) => {
            if (err) reject(err);
            if (!files) reject('File was not found in form data.');
            else resolve({ files, fields });
        });
    });

    const processFileAndValidateDocs = async (request, response, options, callback) => {

        try {

            const data = await getFileFromRequest(request); // retrieve file from the request

            const fileName = data.files.files[0].originalFilename.split('.'); // retreive extension of file from the file name fro validatipon

            const fileExt = fileName[fileName.length - 1];


            if (fileExt === 'xlsx' || fileExt === 'csv') { // only xlxs and csv file can able to process

                const form = new FormData();

                form.append('files', fs.createReadStream(data.files.files[0].path));

                options.reconcile ? form.append("fields", data.fields.fields[0]) : null;

                const fetchOptions = {
                    method: 'POST',
                    body: form
                }

                /* will call the chassis invoice recoincilation server for both doucument validation and reconcile */
                await fetch(`${config.chassisReconcileServer}/${options.endpoint}`, fetchOptions)
                    .then(res => res.json())
                    .then(json => {
                        return callback(null, json);
                    })
                    .catch(err => {
                        console.error(new Date(), "[chassis-invoice] [CHASSIS INVOICE] FETCH ERROR", err);

                        return response.status(400).json({ code: 400, error: "CODE ERROR" });
                    });


            } else {

                return callback(null, { code: 103, error: "File Format Must be either xlsx or csv" });

            }

        } catch (error) {
            console.error(new Date(), "[chassis-invoice] [CHASSIS INVOICE] CODE ERROR", error);

            return response.status(400).json({ code: 400, error: "CODE ERROR" });
        }

    }

    Chassisinvoice.validateDocument = (request, response, options, callback) => {

        auth.validateAccessToken(Chassisinvoice, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            }

            options.tokenModel = tokenModel;

            options.endpoint = "validateDocument";

            processFileAndValidateDocs(request, response, options, callback);

        });

    }

    Chassisinvoice.reconcileDocument = (request, response, options, callback) => {

        auth.validateAccessToken(Chassisinvoice, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            }

            options.tokenModel = tokenModel;

            options.reconcile = true;

            options.endpoint = "reconcileDocument";

            processFileAndValidateDocs(request, response, options, callback);

        });

    }
};
