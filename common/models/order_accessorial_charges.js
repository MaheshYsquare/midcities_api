'use strict';

module.exports = function (Orderaccessorialcharges) {

  //database connector
  Orderaccessorialcharges.executeQuery = function (ds, query, params, callback) {
    ds.connector.query(query, params, callback);
  }

  //Access Token
  Orderaccessorialcharges.validateAccessToken = function (access_token, callback) {
    this.app.models.AccessToken.findById(access_token, callback)
  }

  //calling internally by loopback(overriden)
  Orderaccessorialcharges.createOptionsFromRemotingContext = function (ctx) {
    var base = this.base.createOptionsFromRemotingContext(ctx);
    base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
    return base;
  }


  //update the leg management data
  Orderaccessorialcharges.updateChargesRateQuery = function (params) {
    return {
      "query": "UPDATE order_accessorial_charges set rate = ? WHERE acc_charge_id = ?",
      "params": params
    };
  }

  Orderaccessorialcharges.updateChargesRateParsing = function (data, response, options, callback) {
    var ds = Orderaccessorialcharges.dataSource;
    var queryParams;
    try {

      var updatedata = JSON.stringify(data);
      var parsedData = JSON.parse(updatedata);
      var rate = parsedData.rate;
      var acc_charge_id = parsedData.acc_charge_id;

      queryParams = Orderaccessorialcharges.updateChargesRateQuery([rate, acc_charge_id]);

      Orderaccessorialcharges.executeQuery(ds, queryParams.query, queryParams.params,
        function (err, queryResponse) {
          if (err) {
            response.status(409).json({ code: 409, error: "DB ERROR" });
          } else {
            callback(null, { code: 200, data: "Successfully Updated" });
          }
        });
    } catch (e) {
      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Orderaccessorialcharges.updateChargesRate = function (data, response, options, callback) {
    var self = this;
    this.validateAccessToken(options.access_token, function (err, tokenModel) {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        self.updateChargesRateParsing(data, response, options, callback);
      }
    });
  };
};
