'use strict';

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const _ = require('lodash')

module.exports = function (Chassismaster) {

    let queryParams;

    // Datasource connector
    const executeQuery = (query, params, callback) => {
        Chassismaster.dataSource.connector.query(query, params, callback);
    }

    //Access Token
    const validateAccessToken = (access_token, callback) => {
        Chassismaster.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Chassismaster.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    //post chassis management data
    const postChassisManagementQuery = (params) => {
        return {
            "query": "INSERT INTO chassis_master (chassis_name, address, street, city, postal_code, state, email, phone_number,location_name) VALUES (?,?,?,?,?,?,?,?,UCASE(?))",
            "params": params
        };
    }

    const postChassisManagementValidation = (request, response, options, callback) => {
        try {

            queryParams = postChassisManagementQuery([request.chassis_name, request.address,
            request.street, request.city, request.postal_code, request.state, request.email,
            request.phone_number, request.location_name]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    if (('code' in err) && err['code'] == 'ER_DUP_ENTRY') {
                        callback(null, { code: 422, error: 'Chassis Name Already Exists' })
                    } else {
                        console.error(new Date(), `[chassis] [POST] CHASSIS QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'DB ERROR' });
                    }
                } else {
                    client.del('chassis_master');
                    callback(null, { code: 200, data: "chassis created successfully" })
                }
            });

        }
        catch (e) {
            console.error(new Date(), `[chassis] [POST] CHASSIS CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Chassismaster.postChassisManagement = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                postChassisManagementValidation(request, response, options, callback);
            }
        });

    }

    //get chassis management
    const getChassisManagementQuery = () => {
        return {
            "query": "select * from chassis_master where chassis_name is not null"
        };
    }

    const getChassisManagementDataValidation = (request, response, options, callback) => {
        try {
            queryParams = getChassisManagementQuery();
            executeQuery(queryParams.query, null, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[chassis] [FETCH] CHASSIS QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: 'DB ERROR' });
                } else {

                    client.setex("chassis_master", 3600, JSON.stringify(queryResponse));

                    request ?
                        getChassisManagementCache(request, response, options, callback) :
                        getChassisNameCache(response, options, callback);
                }
            })
        } catch (e) {
            console.error(new Date(), `[chassis] [FETCH] CHASSIS CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    const getChassisManagementCache = (request, response, options, callback) => {
        let chassisParsedData = [];
        let redisResponse;

        client.get("chassis_master", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[chassis] [FETCH] CHASSIS REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' });
                } else {
                    if (cacheData) {
                        chassisParsedData = JSON.parse(cacheData);

                        if (request.columnFilter) {

                            Object.keys(request.columnFilterValue).forEach(element => {

                                request.columnFilterValue[element] = request.columnFilterValue[element]
                                    .toLowerCase().replace(/[^a-zA-Z0-9@]+/ig, "");

                                chassisParsedData = chassisParsedData.length !== 0 ?
                                    chassisParsedData.filter(temp => {
                                        return request.columnFilterValue[element] !== "" ?
                                            (
                                                element !== 'address' ?
                                                    (
                                                        temp[element] !== null ?
                                                            temp[element].toLowerCase()
                                                                .replace(/[^a-zA-Z0-9@]+/ig, "")
                                                                .indexOf(request.columnFilterValue[element]) === 0
                                                            : false
                                                    ) :
                                                    (
                                                        temp[element] !== null ?
                                                            temp[element].toLowerCase()
                                                                .replace(/[^a-zA-Z0-9]+/ig, "")
                                                                .includes(request.columnFilterValue[element])
                                                            : false
                                                    )
                                            ) : true
                                    }) : []
                            });
                        }

                        redisResponse = {
                            code: 200,
                            error: null,
                            data: formatChassisData(request, chassisParsedData)
                        }
                        callback(null, redisResponse);
                    } else {
                        getChassisManagementDataValidation(request, response, options, callback);
                    }
                }
            } catch (error) {
                console.error(new Date(), `[chassis] [FETCH] CHASSIS REDIS CODE ERROR`, error);

                response.status(400).json({ code: 400, error: 'CODE ERROR' });
            }
        });
    }

    const formatChassisData = (request, chassisParsedData) => {
        let chassisData;
        let chassisPaginationLength;

        if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {
            chassisData = chassisParsedData.length !== 0 ? _.orderBy(chassisParsedData, request.column, request.direction).slice((request.offset - 1), request.limit) : [];
        } else {
            chassisData = chassisParsedData.length !== 0 ? _.orderBy(chassisParsedData, 'chassis_id', 'desc').slice((request.offset - 1), request.limit) : [];
        }

        chassisPaginationLength = chassisParsedData.length;

        return { chassisData, chassisPaginationLength }
    }

    Chassismaster.getChassisManagementData = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getChassisManagementCache(request, response, options, callback);
            }
        });

    }

    //update chassis management data
    const updateChassisManagementQuery = (params) => {
        return {
            "query": "UPDATE chassis_master SET chassis_name=?, address=?, street=?, city=?, postal_code=?, state=?, email=?, phone_number=?,location_name=UCASE(?) WHERE chassis_id=?",
            "params": params
        };
    }

    const updateChassisManagementValidation = (request, response, options, callback) => {
        try {

            queryParams = updateChassisManagementQuery([request.chassis_name, request.address, request.street,
            request.city, request.postal_code, request.state, request.email, request.phone_number,
            request.location_name, request.chassis_id]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    if (('code' in err) && err['code'] == 'ER_DUP_ENTRY') {
                        callback(null, { code: 422, error: 'Chassis Name Already Exists' });
                    } else {
                        console.error(new Date(), `[chassis] [UPDATE] CHASSIS QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'DB ERROR' });
                    }
                } else {
                    client.del('chassis_master');
                    callback(null, { code: 200, data: "chassis updated successfully" })
                }
            });
        } catch (e) {
            console.error(new Date(), `[chassis] [UPDATE] CHASSIS CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Chassismaster.updateChassisManagement = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                updateChassisManagementValidation(request, response, options, callback);
            }
        });

    }

    //delete chassis management data
    const deleteChassisManagementQueryParams = (params) => {
        return {
            "query": "DELETE FROM chassis_master WHERE chassis_id=?",
            "params": params
        };
    }

    const deleteChassisManagementValidation = (chassis_id, response, options, callback) => {
        try {

            queryParams = deleteChassisManagementQueryParams([chassis_id]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    if (('code' in err) && err['code'] == 'ER_ROW_IS_REFERENCED_2') {
                        callback(null, { code: 422, error: 'Chassis is Referenced' })
                    } else {
                        console.error(new Date(), `[chassis] [DELETE] CHASSIS QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'DB ERROR' });
                    }
                } else {
                    client.del('chassis_master');
                    callback(null, { code: 200, data: "chassis successfully deleted" })
                }
            })
        } catch (e) {
            console.error(new Date(), `[chassis] [DELETE] CHASSIS QUERY ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Chassismaster.deleteChassisManagement = (chassis_id, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                deleteChassisManagementValidation(chassis_id, response, options, callback);
            }
        });

    }


    const getChassisNameCache = (response, options, callback) => {
        let parsedData = [];

        client.get("chassis_master", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[chassis] [FETCH] CHASSIS NAME REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' });
                } else {
                    if (cacheData) {
                        parsedData = JSON.parse(cacheData);

                        parsedData = parsedData.map(temp => temp.chassis_name);

                        callback(null, parsedData);
                    } else {
                        getChassisManagementDataValidation(undefined, response, options, callback);
                    }
                }
            } catch (error) {
                console.error(new Date(), `[chassis] [FETCH] CHASSIS NAME REDIS CODE ERROR`, error);

                response.status(400).json({ code: 400, error: 'CODE ERROR' });
            }
        })
    }

    Chassismaster.getChassisName = (response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getChassisNameCache(response, options, callback);
            }
        });

    }
};
