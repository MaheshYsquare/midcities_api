'use strict';

const auth = require('../../server/boot/auth');
const fileParse = require('../../server/boot/file-parse');

module.exports = function (Utils) {

    auth.setToken(Utils);


    Utils.getFile = (request, response, options, callback) => {

        auth.validateAccessToken(Utils, options.access_token, async (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }

            options.tokenModel = tokenModel;

            const fileContent = await fileParse.getFileFromS3(request.fileName);

            if (fileContent.code !== 200) {
                return response.status(fileContent.code).json(fileContent);
            }

            callback(null, fileContent.data);

        });
    }

    Utils.getFileSize = (request, response, options, callback) => {

        auth.validateAccessToken(Utils, options.access_token, async (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }

            options.tokenModel = tokenModel;

            const fileContent = await fileParse.getFileSizeFromS3(request.fileName);

            if (fileContent.code !== 200) {
                return response.status(fileContent.code).json(fileContent);
            }

            callback(null, fileContent);

        });
    }

    const fetchDataFromDb = (response, options, callback) => {

        let query = `SELECT
                         MAX(states) states ,
                         MAX(fixed_acc_charges) fixed_acc_charges
                     FROM
                         (
                         SELECT
                             JSON_ARRAYAGG(JSON_OBJECT('state',
                             state,
                             'code',
                             code)) states,
                             null as fixed_acc_charges
                         FROM
                             states
                     UNION
                         SELECT
                             null as states,
                             JSON_ARRAYAGG(fixed_accessorial_charge) fixed_acc_charges
                         FROM
                             fixed_accessorial_charges fac) config;`

        auth.executeQuery(Utils, query, null, (err, queryResponse) => {

            try {

                if (err) {

                    console.error(new Date(),
                        "[util.js] [FETCH] RETREIVE STATES AND CHARGES QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: "DB ERROR" });
                }

                queryResponse.forEach(element => {
                    element.states = element.states ?
                        JSON.parse(element.states) : [];

                    element.fixed_acc_charges = element.fixed_acc_charges ?
                        JSON.parse(element.fixed_acc_charges) : [];

                });

                callback(null, queryResponse[0]);

            } catch (error) {

                console.error(new Date(),
                    "[util.js] [FETCH] RETREIVE STATES AND CHARGES CODE ERROR",
                    "sqlMessage" in err ? err.sqlMessage : err);

                return response.status(401).json({ code: 401, error: "CODE ERROR" });
            }

        })

    }

    Utils.getStatesAndChargesConfig = (response, options, callback) => {

        auth.validateAccessToken(Utils, options.access_token, async (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }

            options.tokenModel = tokenModel;

            fetchDataFromDb(response, options, callback);

        });
    }

};
