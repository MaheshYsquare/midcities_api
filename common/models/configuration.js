'use strict';

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);

module.exports = function (Configurationtypemaster) {

    let queryParams;

    let tableName;

    let columnName;

    let columnId;

    //Datasource Connector
    const executeQuery = (query, params, callback) => {
        Configurationtypemaster.dataSource.connector.query(query, params, callback);
    }

    //Access Token
    const validateAccessToken = (access_token, callback) => {
        Configurationtypemaster.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Configurationtypemaster.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    //Get selected dropdown types
    const getDropdownTypeQuery = (params) => {
        return {
            "query": "select * from ?? where ?? is not null",
            "params": params
        };
    }

    const getSelectedDropdownTypesvalidation = (selectedType, response, options, callback) => {
        try {
            switch (selectedType) {
                case 'Location Types':
                    tableName = 'location_type_master';
                    columnName = 'location_type'
                    break;
                case 'Container Types':
                    tableName = 'container_type_master';
                    columnName = 'container_type'
                    break;
                case 'Order Types':
                    tableName = 'order_type_master';
                    columnName = 'order_type'
                    break;
                case 'Leg Types':
                    tableName = 'leg_type_master';
                    columnName = 'leg_type'
                    break;
                case 'Driver Types':
                    tableName = 'driver_type_master';
                    columnName = 'driver_type'
                    break;
                case 'Equipment Types':
                    tableName = 'equipment_type_master';
                    columnName = 'equipment_type'
                    break;
                case 'Equipment Locations':
                    tableName = 'equipment_location_master';
                    columnName = 'equipment_location'
                    break;
                case 'Tags':
                    tableName = 'tags_master';
                    columnName = 'tag_title';
                    break;
                case 'Order Flags':
                    tableName = 'order_flag';
                    columnName = 'range_from';
                    break;
                case 'Attendence Category':
                    tableName = 'driver_attendence_category';
                    columnName = 'title';
                    break;
            }
            queryParams = getDropdownTypeQuery([tableName, columnName]);
            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[configuration] [FETCH] DROPDOWN SINGLE QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: 'CONFIGURATION GET DB ERROR' });
                } else {
                    callback(null, { code: 200, error: null, data: queryResponse })
                }
            })
        } catch (e) {
            console.error(new Date(), `[configuration] [FETCH] DROPDOWN SINGLE CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Configurationtypemaster.getSelectedDropdownTypes = (selectedType, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getSelectedDropdownTypesvalidation(selectedType, response, options, callback);
            }
        });

    }

    //post new configuration data
    const postNewConfigurationQuery = (params) => {
        return {
            "query": "INSERT INTO ?? (??, lastupdated_on, lastupdated_by) VALUES (?, CURRENT_TIMESTAMP, ?)",
            "params": params
        };
    }

    const postNewConfigurationColorQuery = (params) => {
        return {
            "query": "INSERT INTO ?? (??, lastupdated_on, lastupdated_by, color) VALUES (?, CURRENT_TIMESTAMP, ?, ?)",
            "params": params
        };
    }

    const postNewConfigurationValidation = (request, response, options, callback) => {
        try {
            switch (request.dropdown) {
                case 'Location Types':
                    tableName = 'location_type_master';
                    columnName = 'location_type'
                    break;
                case 'Container Types':
                    tableName = 'container_type_master';
                    columnName = 'container_type'
                    break;
                case 'Order Types':
                    tableName = 'order_type_master';
                    columnName = 'order_type'
                    break;
                case 'Leg Types':
                    tableName = 'leg_type_master';
                    columnName = 'leg_type'
                    break;
                case 'Driver Types':
                    tableName = 'driver_type_master';
                    columnName = 'driver_type'
                    break;
                case 'Equipment Types':
                    tableName = 'equipment_type_master';
                    columnName = 'equipment_type'
                    break;
                case 'Equipment Locations':
                    tableName = 'equipment_location_master';
                    columnName = 'equipment_location'
                    break;
                case 'Tags':
                    tableName = 'tags_master';
                    columnName = 'tag_title';
                    break;
                case 'Attendence Category':
                    tableName = 'driver_attendence_category';
                    columnName = 'title';
                    break;
            }
            if (request.dropdown == 'Order Types' || request.dropdown == 'Leg Types' || request.dropdown == 'Tags') {
                queryParams = postNewConfigurationColorQuery([tableName, columnName, request.field_name, request.user_name, request.color]);
            } else {
                queryParams = postNewConfigurationQuery([tableName, columnName, request.field_name, request.user_name]);
            }

            executeQuery(queryParams.query, queryParams.params, (err, respdata) => {
                if (err) {
                    console.error(new Date(), `[configuration] [POST] CONFIGURATION QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: 'CONFIGURATION POST DB ERROR' });
                } else {
                    client.del('allDropdown');
                    callback(null, { code: 200, error: null, data: 'Successfully Data Inserted' });
                }
            });
        }
        catch (e) {
            console.error(new Date(), `[configuration] [POST] CONFIGURATION CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Configurationtypemaster.postNewConfiguration = (request, response, options, cb) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                postNewConfigurationValidation(request, response, options, cb);
            }
        });

    }

    //update configuration data
    const updateConfigurationQuery = (params) => {
        return {
            "query": "UPDATE ?? SET ??=?, lastupdated_on=CURRENT_TIMESTAMP, lastupdated_by=? WHERE ??=?",
            "params": params
        };
    }

    const updateConfigurationColorQuery = (params) => {
        return {
            "query": "UPDATE ?? SET ??=?, lastupdated_on=CURRENT_TIMESTAMP, lastupdated_by=?, color=? WHERE ??=?",
            "params": params
        };
    }

    const updateConfigurationValidation = (request, response, options, callback) => {
        try {
            switch (request.dropdown) {
                case 'Location Types':
                    tableName = 'location_type_master';
                    columnName = 'location_type';
                    columnId = 'location_type_id';
                    break;
                case 'Container Types':
                    tableName = 'container_type_master';
                    columnName = 'container_type';
                    columnId = 'container_type_id';
                    break;
                case 'Order Types':
                    tableName = 'order_type_master';
                    columnName = 'order_type';
                    columnId = 'order_type_id';
                    break;
                case 'Leg Types':
                    tableName = 'leg_type_master';
                    columnName = 'leg_type';
                    columnId = 'leg_type_id';
                    break;
                case 'Driver Types':
                    tableName = 'driver_type_master';
                    columnName = 'driver_type';
                    columnId = 'driver_type_id';
                    break;
                case 'Equipment Types':
                    tableName = 'equipment_type_master';
                    columnName = 'equipment_type';
                    columnId = 'equipment_type_id';
                    break;
                case 'Equipment Locations':
                    tableName = 'equipment_location_master';
                    columnName = 'equipment_location';
                    columnId = 'equipment_location_id';
                    break;
                case 'Tags':
                    tableName = 'tags_master';
                    columnName = 'tag_title';
                    columnId = 'tag_id';
                    break;
                case 'Attendence Category':
                    tableName = 'driver_attendence_category';
                    columnName = 'title';
                    columnId = 'driver_attendence_category_id';
                    break;
            }

            if (request.dropdown == 'Order Types' || request.dropdown == 'Leg Types' || request.dropdown == 'Tags') {
                queryParams = updateConfigurationColorQuery([tableName, columnName, request.fieldName, request.user_name, request.color, columnId, request.id]);
            } else {
                queryParams = updateConfigurationQuery([tableName, columnName, request.fieldName, request.user_name, columnId, request.id]);
            }
            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[configuration] [UPDATE] CONFIGURATION QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: 'CONFIGURATION UPDATE DB ERROR' });
                } else {
                    if (request.dropdown == 'Order Types' || request.dropdown == 'Tags') {
                        client.del("ordersDispatchData");
                        client.del("ordersPendingData");
                        client.del("ordersDraftData");
                        client.del("dispatchData");
                        client.del("invoiceData");
                    }
                    client.del('allDropdown');
                    callback(null, { code: 200, error: null, data: 'Successfully Data Updated' });
                }
            });
        }
        catch (e) {
            console.error(new Date(), `[configuration] [UPDATE] CONFIGURATION CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Configurationtypemaster.updateConfiguration = (request, response, options, cb) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                updateConfigurationValidation(request, response, options, cb);
            }
        });

    }

    //delete configuration data
    const deleteConfigurationQuery = (params) => {
        return {
            "query": "DELETE FROM ?? WHERE ??=?",
            "params": params
        };
    }

    const deleteConfigurationValidation = (selectedDropdown, id, response, options, callback) => {
        try {
            switch (selectedDropdown) {
                case 'Location Types':
                    tableName = 'location_type_master';
                    columnId = 'location_type_id';
                    break;
                case 'Container Types':
                    tableName = 'container_type_master';
                    columnId = 'container_type_id';
                    break;
                case 'Order Types':
                    tableName = 'order_type_master';
                    columnId = 'order_type_id';
                    break;
                case 'Leg Types':
                    tableName = 'leg_type_master';
                    columnId = 'leg_type_id';
                    break;
                case 'Driver Types':
                    tableName = 'driver_type_master';
                    columnId = 'driver_type_id';
                    break;
                case 'Equipment Types':
                    tableName = 'equipment_type_master';
                    columnId = 'equipment_type_id';
                    break;
                case 'Equipment Locations':
                    tableName = 'equipment_location_master';
                    columnId = 'equipment_location_id';
                    break;
                case 'Tags':
                    tableName = 'tags_master';
                    columnId = 'tag_id';
                    break;
                case 'Attendence Category':
                    tableName = 'driver_attendence_category';
                    columnId = 'driver_attendence_category_id';
                    break;
            }
            queryParams = deleteConfigurationQuery([tableName, columnId, id]);
            executeQuery(queryParams.query, queryParams.params, (err, deletecount) => {
                if (err) {
                    if (('code' in err) && err['code'] == 'ER_ROW_IS_REFERENCED_2') {
                        callback(null, { code: 400, error: 'type is used' })
                    } else {
                        console.error(new Date(), `[configuration] [DELETE] CONFIGURATION QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'CONFIGURATION DELETE DB ERROR' });
                    }
                } else {
                    client.del('allDropdown');

                    callback(null, { code: 200, error: null, data: 'Data Deleted Successfully' })
                }
            })
        } catch (e) {
            console.error(new Date(), `[configuration] [DELETE] CONFIGURATION CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Configurationtypemaster.deleteConfiguration = (selectedDropdown, fieldValue, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                deleteConfigurationValidation(selectedDropdown, fieldValue, response, options, callback);
            }
        });

    }

    //Get Equipment Types Dropdown
    const getDropdownQuery = (params) => {
        return {
            "query": "select distinct ?? as label,?? as value from ?? where ?? is not null;",
            "params": params
        };
    }

    const getDropdownParsing = (dropdownType, response, options, callback) => {
        try {
            switch (dropdownType) {
                case 'location':
                    tableName = 'location_type_master';
                    columnName = 'location_type';
                    columnId = 'location_type_id';
                    break;
                case 'container':
                    tableName = 'container_type_master';
                    columnName = 'container_type';
                    columnId = 'container_type_id';
                    break;
                case 'order':
                    tableName = 'order_type_master';
                    columnName = 'order_type';
                    columnId = 'order_type_id';
                    break;
                case 'leg':
                    tableName = 'leg_type_master';
                    columnName = 'leg_type';
                    columnId = 'leg_type_id';
                    break;
                case 'driver':
                    tableName = 'driver_type_master';
                    columnName = 'driver_type';
                    columnId = 'driver_type_id';
                    break;
                case 'equipmentTypes':
                    tableName = 'equipment_type_master';
                    columnName = 'equipment_type';
                    columnId = 'equipment_type_id';
                    break;
                case 'equipmentLocations':
                    tableName = 'equipment_location_master';
                    columnName = 'equipment_location';
                    columnId = 'equipment_location_id';
                    break;
                case 'tags':
                    tableName = 'tags_master';
                    columnName = 'tag_title';
                    columnId = 'tag_id';
                    break;
                case 'attendenceCategory':
                    tableName = 'driver_attendence_category';
                    columnName = 'title';
                    columnId = 'driver_attendence_category_id';
                    break;
            }

            queryParams = dropdownType == 'orderflags' ?
                getDropdownQuery(['order_flag', 'order_flag_id', 'order_flag', 'range_from']) :
                getDropdownQuery([columnName, columnId, tableName, columnName]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[configuration] [DROPDOWN] CONFIGURATION QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: 'DROPDOWN DB ERROR' });
                } else {
                    callback(null, queryResponse);
                }
            })
        } catch (e) {
            console.error(new Date(), `[configuration] [DROPDOWN] CONFIGURATION CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Configurationtypemaster.getDropdown = (dropdownType, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getDropdownParsing(dropdownType, response, options, callback);
            }
        });

    }


    const getAllDropdownParsing = (dropdownType, response, options, callback) => {
        try {
            executeQuery("call getAllDropdown();", null, (err, queryResponse) => {
                try {
                    if (err) {
                        console.error(new Date(), `[configuration] [DROPDOWN] ALL CONFIGURATION QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'DROPDOWN DB ERROR' });
                    } else {
                        let dropDowns = queryResponse[0][0];

                        Object.keys(dropDowns).forEach(temp => {
                            dropDowns[temp] = dropDowns[temp] ?
                                JSON.parse(dropDowns[temp]) : [];
                        })

                        client.setex('allDropdown', 3600, JSON.stringify(dropDowns));
                        getAllDropdownCache(dropdownType, response, options, callback);
                    }
                } catch (e) {
                    console.error(new Date(), `[configuration] [DROPDOWN] ALL CONFIGURATION CODE ERROR`, e);

                    response.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (e) {
            console.error(new Date(), `[configuration] [DROPDOWN] ALL CONFIGURATION CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    const getAllDropdownCache = (dropdownType, response, options, callback) => {
        let parsedData;

        let selectedDropdown = {};

        client.get("allDropdown", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[configuration] [DROPDOWN] ALL CONFIGURATION REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' })
                } else {
                    if (cacheData) {
                        parsedData = JSON.parse(cacheData);

                        Object.keys(parsedData).forEach(temp => {
                            if (dropdownType.split(',').some(elem => temp === elem)) {
                                selectedDropdown[temp] = parsedData[temp];
                            }
                        })

                        callback(null, selectedDropdown);
                    } else {
                        getAllDropdownParsing(dropdownType, response, options, callback);
                    }
                }
            } catch (error) {
                console.error(new Date(), `[configuration] [DROPDOWN] ALL CONFIGURATION CACHE CODE ERROR`, error);

                response.status(400).json({ code: 400, error: 'CODE ERROR' });
            }
        });
    }

    Configurationtypemaster.getAllDropdown = (dropdownType, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getAllDropdownCache(dropdownType, response, options, callback);
            }
        });

    }


    const postOrderFlagQuery = (params) => {
        return {
            "query": "INSERT INTO order_flag (order_flag, range_from, range_to, lastupdated_on, lastupdated_by) VALUES (?, ?, ?, CURRENT_TIMESTAMP, ?)",
            "params": params
        };
    }

    const updateOrderMgmtQuery = (params) => {
        return {
            "query": "update order_container_chassis set order_flag_id = ? where miles BETWEEN ? and ?",
            "params": params
        }
    }

    const validateRangeQuery = (params) => {
        return {
            "query": "SELECT * FROM order_flag where order_flag <> 'N/A' and order_flag_id <> ? and ? <= ifnull(range_to,2147483647) and ifnull(?,2147483647) >= range_from;",
            "params": params
        }
    }

    const postOrderFlagValidation = async (request, response, options, callback) => {
        try {

            if (request.range_to !== null && request.range_from >= request.range_to) {

                callback(null, { code: 402, error: 'Range from should be lesser than range to and both the values should not be the same' });

            } else {

                let validation = await validateRangeForCollision(request, true);

                if (validation.code === 200) {

                    queryParams = postOrderFlagQuery([request.order_flag, request.range_from,
                    request.range_to, request.user_name]);

                    executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                        try {

                            if (err) {
                                console.error(new Date(), `[configuration] [POST] ORDER FLAG QUERY ERROR`,
                                    "sqlMessage" in err ? err.sqlMessage : err);

                                response.status(409).json({ code: 409, error: 'DB ERROR' });
                            } else {

                                request.order_flag_id = queryResponse.insertId;

                                queryParams = updateOrderMgmtQuery([request.order_flag_id,
                                request.range_from, request.range_to !== null ? request.range_to : 2147483647]);

                                executeQuery(queryParams.query, queryParams.params,
                                    (error, orderUpdateQueryResponse) => {
                                        if (error) {
                                            console.error(new Date(), `[configuration] [UPDATE] ORDER MGMT QUERY ERROR`,
                                                "sqlMessage" in error ? error.sqlMessage : error);

                                            request.isDeleteConfiguraionOnly = true;
                                            deleteOrderFlagValidation(request, response, options, callback);
                                        } else {
                                            client.del("ordersDispatchData");
                                            client.del("ordersPendingData");
                                            client.del("ordersDraftData");
                                            client.del("dispatchData");
                                            client.del("invoiceData");
                                            callback(null, {
                                                code: 200,
                                                error: null,
                                                data: 'Successfully Data Inserted'
                                            });
                                        }
                                    });
                            }

                        } catch (error) {
                            console.error(new Date(), `[configuration] [POST] ORDER FLAG CODE ERROR`, error);

                            response.status(400).json({ code: 400, error: 'CODE ERROR' });
                        }
                    })

                } else if (validation.code === 400) {

                    callback(null, { code: 402, error: 'the given range collides with existing ranges' });

                } else if (validation.code === 500) {

                    response.status(409).json({ code: 409, error: 'DB ERROR' });

                }
            }

        } catch (e) {
            console.error(new Date(), `[configuration] [POST] ORDER FLAG CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Configurationtypemaster.postOrderFlag = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                postOrderFlagValidation(request, response, options, callback);
            }
        });

    }


    const updateOrderFlagQuery = (params) => {
        return {
            "query": "UPDATE order_flag SET order_flag = ?, range_from = ?, range_to = ?, lastupdated_on = CURRENT_TIMESTAMP, lastupdated_by = ? WHERE order_flag_id = ?",
            "params": params
        };
    }

    const updateOrderFlagValidation = async (request, response, options, callback) => {
        try {

            if (request.range_to !== null && request.range_from >= request.range_to) {

                callback(null, { code: 402, error: 'Range from should be lesser than range to and both the values should not be the same' });

            } else {

                let validation = await validateRangeForCollision(request, false);

                if (validation.code === 200) {

                    queryParams = updateOrderFlagQuery([request.order_flag,
                    request.range_from, request.range_to, request.user_name, request.order_flag_id]);

                    executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                        try {
                            if (err) {
                                console.error(new Date(), `[configuration] [UPDATE] ORDER FLAG QUERY ERROR`,
                                    "sqlMessage" in err ? err.sqlMessage : err);

                                response.status(409).json({ code: 409, error: 'DB ERROR' });
                            } else {

                                if (request.previousRangeFrom === request.range_from &&
                                    request.previousRangeTo === request.range_to) {

                                    if (request.isErrorInOrderUpdate) {

                                        response.status(409).json({
                                            code: 409,
                                            error: 'DB ERROR ORDER NOT UPDATED'
                                        });

                                    } else {

                                        callback(null, {
                                            code: 200,
                                            error: null,
                                            data: 'Successfully Data Updated'
                                        });

                                    }

                                } else {
                                    executeQuery('call orderMgmtFlagUpdate(?,?,?,?,?);', [request.order_flag_id,
                                    request.range_from, request.range_to, request.previousRangeFrom,
                                    request.previousRangeTo], (orderErr, orderQueryResponse) => {

                                        try {

                                            if (orderErr) {
                                                console.error(new Date(),
                                                    `[configuration] [UPDATE] ORDER MGMT QUERY ERROR`,
                                                    "sqlMessage" in orderErr ? orderErr.sqlMessage : orderErr);

                                                request.range_from = request.previousRangeFrom;
                                                request.range_to = request.previousRangeTo;
                                                request.isErrorInOrderUpdate = true;
                                                updateOrderFlagValidation(request, response, options, callback);

                                            } else {

                                                client.del("ordersDispatchData");
                                                client.del("ordersPendingData");
                                                client.del("ordersDraftData");
                                                client.del("dispatchData");
                                                client.del("invoiceData");
                                                callback(null, {
                                                    code: 200,
                                                    error: null,
                                                    data: 'Successfully Data Updated'
                                                });

                                            }
                                        } catch (error) {

                                            console.error(new Date(),
                                                `[configuration] [UPDATE] ORDER FLAG CODE ERROR`, error);

                                            response.status(400).json({ code: 400, error: 'CODE ERROR' });

                                        }

                                    })
                                }
                            }
                        } catch (error) {
                            console.error(new Date(), `[configuration] [UPDATE] ORDER FLAG CODE ERROR`, error);

                            response.status(400).json({ code: 400, error: 'CODE ERROR' });

                        }

                    })
                } else if (validation.code === 400) {

                    callback(null, { code: 402, error: 'the given range collides with existing ranges' });

                } else if (validation.code === 500) {

                    response.status(400).json({ code: 400, error: 'CODE ERROR' });

                }
            }


        } catch (e) {
            console.error(new Date(), `[configuration] [FETCH] ORDER FLAG CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Configurationtypemaster.updateOrderFlag = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                request.isErrorInOrderUpdate = false;
                updateOrderFlagValidation(request, response, options, callback);
            }
        });

    }

    const updateOrderMgmtOnDeleteQuery = (params) => {
        return {
            "query": "update order_container_chassis set order_flag_id = (select order_flag_id from order_flag where order_flag = 'N/A') where miles BETWEEN ? and ?",
            "params": params
        }
    }

    const deleteOrderFlagValidation = (request, response, options, callback) => {
        try {
            if (request.isDeleteConfiguraionOnly) {
                deleteOrderFlagConfiguraion(request, response, options, callback);
            } else {

                queryParams = updateOrderMgmtOnDeleteQuery([request.range_from,
                request.range_to !== null ? request.range_to : 2147483647]);

                executeQuery(queryParams.query, queryParams.params, (err, orderUpdateQueryResponse) => {
                    try {

                        if (err) {
                            console.error(new Date(), `[configuration] [UPDATE] ORDER MGMT ON DELETE QUERY ERROR`,
                                "sqlMessage" in err ? err.sqlMessage : err);

                            response.status(409).json({ code: 409, error: 'DB ERROR' });
                        } else {
                            client.del("ordersDispatchData");
                            client.del("ordersPendingData");
                            client.del("ordersDraftData");
                            client.del("dispatchData");
                            client.del("invoiceData");
                            deleteOrderFlagConfiguraion(request, response, options, callback);
                        }

                    } catch (error) {

                        console.error(new Date(), `[configuration] [UPDATE] ORDER FLAG ON DELETE CODE ERROR`, error);

                        response.status(400).json({ code: 400, error: 'CODE ERROR' });

                    }

                })
            }

        } catch (e) {
            console.error(new Date(), `[configuration] [UPDATE] ORDER FLAG CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    const deleteOrderFlagConfiguraion = (request, response, options, callback) => {

        queryParams = deleteConfigurationQuery(['order_flag', 'order_flag_id', request.order_flag_id]);

        executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
            try {
                if (err) {
                    console.error(new Date(), `[configuration] [DELETE] ORDER FLAG QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: 'DB ERROR' });
                } else {
                    if (request.isDeleteConfiguraionOnly) {

                        response.status(409).json({ code: 409, error: 'DB ERROR ORDER NOT UPDATED' });

                    } else {

                        callback(null, { code: 200, data: "configuration successfully deleted" });

                    }
                }
            } catch (error) {
                console.error(new Date(), `[configuration] [DELETE] ORDER FLAG CODE ERROR`, error);

                response.status(400).json({ code: 400, error: 'CODE ERROR' });
            }
        })
    }

    Configurationtypemaster.deleteOrderFlag = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                request.isDeleteConfiguraionOnly = false;
                deleteOrderFlagValidation(request, response, options, callback);
            }
        });

    }

    const validateRangeForCollision = (request, isPost) => {
        return new Promise((resolve, reject) => {
            queryParams = validateRangeQuery([isPost ? true : request.order_flag_id, request.range_from, request.range_to]);
            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[configuration] [VALIDATION] ORDER FLAG RANGE CHECK QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    resolve({ code: 500, error: "DB ERROR" });
                } else {
                    if (queryResponse.length === 0) {
                        resolve({ code: 200, error: null });
                    } else {
                        resolve({ code: 400, error: 'VALIDATION ERROR' })
                    }
                }
            })
        })
    }

};
