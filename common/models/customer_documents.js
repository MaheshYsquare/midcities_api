'use strict';

const auth = require('../../server/boot/auth');
const fileParse = require('../../server/boot/file-parse');

module.exports = function (Customerdocuments) {

    let queryParams;

    auth.setToken(Customerdocuments);

    const storeCustomerDocUrlQuery = () => {
        return {
            query: `insert into customer_documents 
                        (customer_id, file_name, file_link)
                    values (?,?,?)
                    on duplicate key update
                        file_name = ?,
                        file_link = ?;`
        }
    }

    const uploadFileStoreUrlInDB = async (req, res, options, callback) => {
        try {

            const data = await fileParse.getFileFromRequest(req);

            let files = data.files;

            let fields = JSON.parse(data.fields.fields[0]);

            queryParams = storeCustomerDocUrlQuery();

            let key = ['customers', fields.customer_name];

            fileParse.uploadFileToS3(key, Customerdocuments, fields, files.files, queryParams.query);

            callback(null, "File Uploading...");


        } catch (error) {
            console.error(new Date(), `[customer] [POST] CUSTOMER UPLOAD CODE ERROR`, error);

            res.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Customerdocuments.uploadCustomerFile = (request, response, options, callback) => {

        auth.validateAccessToken(Customerdocuments, options.access_token, (err, tokenModel) => {

            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            }

            options.tokenModel = tokenModel;
            uploadFileStoreUrlInDB(request, response, options, callback);

        });

    }

    const fetchCustomerUploadDocsQuery = (params) => {
        return {
            "query": "select file_name from customer_documents where customer_id = ?",
            params
        };
    }

    const fetchCustomerUploadDocsFromDb = (id, response, options, callback) => {
        try {

            queryParams = fetchCustomerUploadDocsQuery([id]);

            auth.executeQuery(Customerdocuments, queryParams.query, queryParams.params,
                (err, queryResponse) => {
                    if (err) {
                        console.error(new Date(), "[customer] [FETCH] FILE LINK QUERY ERROR",
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: "DB ERROR" });
                    }
                    queryResponse = queryResponse.map(temp => temp.file_name);

                    callback(null, { code: 200, data: queryResponse });
                });

        } catch (e) {
            console.error(new Date(), "[customer] [FETCH] FILE LINK CODE ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Customerdocuments.fetchCustomerUploadDocsList = (id, response, options, callback) => {

        auth.validateAccessToken(Customerdocuments, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }
            options.tokenModel = tokenModel;
            fetchCustomerUploadDocsFromDb(id, response, options, callback);

        });

    }


    const deleteCustomerFileFromS3AndDb = async (request, response, options, callback) => {

        const deleteResponse = await fileParse.deleteFileToS3(request.fileName);

        if (deleteResponse.code !== 200) {
            return response.status(deleteResponse.code).json(deleteResponse);
        }

        auth.executeQuery(Customerdocuments,
            "DELETE FROM customer_documents WHERE customer_id = ? AND file_name = ?",
            [request.customer_id, request.fileName], (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), "[customer] [DELETE] CUSTOMER DOC DELETE DB ERROR", err);

                    return response.status(409).json({ code: 409, error: "DB ERROR" });
                }

                callback(null, "Successfully Deleted");
            })

    }


    Customerdocuments.deleteCustomerFile = (request, response, options, callback) => {

        auth.validateAccessToken(Customerdocuments, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }
            options.tokenModel = tokenModel;
            deleteCustomerFileFromS3AndDb(request, response, options, callback);

        });

    }
};
