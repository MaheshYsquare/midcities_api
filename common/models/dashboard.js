'use strict';

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);

module.exports = function (Dashboard) {

    //Datasource connector
    const executeQuery = (query, params, callback) => {
        Dashboard.dataSource.connector.query(query, params, callback);
    }

    //Access Token
    const validateAccessToken = (access_token, callback) => {
        Dashboard.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Dashboard.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    /* To Get the Dashboard Metrics Data */
    const getDashboardMetricsValidation = (response, options, callback) => {
        try {
            executeQuery("call getDashboardMetrics();", null, (err, queryResponse) => {
                try {
                    if (err) {
                        console.error(new Date(), `[dashboard] [FETCH] DASHBOARD TOP BOARDS QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'DB ERROR' });
                    } else {
                        let data = queryResponse && queryResponse[0].length !== 0 ? queryResponse[0][0] : {};

                        callback(null, { code: 200, error: null, data });
                    }
                } catch (error) {
                    console.error(new Date(), `[dashboard] [FETCH] DASHBOARD TOP BOARDS CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (e) {
            console.error(new Date(), `[dashboard] [FETCH] DASHBOARD TOP BOARDS CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Dashboard.getDashboardMetrics = (response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getDashboardMetricsValidation(response, options, callback);
            }
        });

    }

    /* To Get the Dashboard Driver Metrics Data */
    const getDashboardDriverMetricsValidation = (driverId, response, options, callback) => {
        try {
            executeQuery("call getDashboardDriverMetrics(?);", [driverId], (err, queryResponse) => {
                try {
                    if (err) {
                        console.error(new Date(), `[dashboard] [FETCH] DASHBOARD DRIVER TOP BOARDS QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'DB ERROR' });
                    } else {
                        let data = queryResponse && queryResponse[0].length !== 0 ? queryResponse[0][0] : {};

                        callback(null, { code: 200, error: null, data });
                    }
                } catch (error) {
                    console.error(new Date(), `[dashboard] [FETCH] DASHBOARD DRIVER TOP BOARDS CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (e) {
            console.error(new Date(), `[dashboard] [FETCH] DASHBOARD DRIVER TOP BOARDS CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Dashboard.getDashboardDriverMetrics = (driverId, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getDashboardDriverMetricsValidation(driverId, response, options, callback);
            }
        });

    }

    /** To Get the Order Progress Data */
    const getOrderProgressValidation = (date, response, options, callback) => {
        try {
            executeQuery("call getOrderProgress(?)", [date], (err, queryResponse) => {
                try {
                    if (err) {
                        console.error(new Date(), `[dashboard] [FETCH] DASHBOARD ORDER PROGRESS QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'DB ERROR' });
                    } else {
                        callback(null, { code: 200, error: null, data: queryResponse[0] });
                    }
                } catch (error) {
                    console.error(new Date(), `[dashboard] [FETCH] DASHBOARD ORDER PROGRESS CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (e) {
            console.error(new Date(), `[dashboard] [FETCH] DASHBOARD ORDER PROGRESS CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Dashboard.getOrderProgress = (date, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getOrderProgressValidation(date, response, options, callback);
            }
        });

    }

    /** To Get the Invoice Metrics Data */
    const getInvoicesMetricsValidation = (response, options, callback) => {
        try {

            executeQuery("call getInvoicesMetrics();", null, (err, queryResponse) => {
                try {
                    if (err) {
                        console.error(new Date(), `[dashboard] [FETCH] DASHBOARD INVOICE METRICS QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err)

                        response.status(409).json({ code: 409, error: 'DB ERROR' });
                    } else {
                        let data = queryResponse && queryResponse[0].length !== 0 ? queryResponse[0][0] : {};

                        callback(null, { code: 200, error: null, data });
                    }
                } catch (error) {
                    console.error(new Date(), `[dashboard] [FETCH] DASHBOARD INVOICE METRICS CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (e) {
            console.error(new Date(), `[dashboard] [FETCH] DASHBOARD INVOICE METRICS CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    const fetchDataFromCache = (response, options, callback) => {
        let parsedData;
        let data;

        client.get('invoice_mgmt', (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[dashboard] [FETCH] DASHBOARD INVOICE METRICS REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' });
                } else {
                    if (cacheData) {
                        parsedData = JSON.parse(cacheData);

                        if (parsedData.type == 'mysql') {
                            getInvoicesMetricsValidation(response, options, callback)
                        } else {
                            parsedData = parsedData.data;

                            data = {
                                unpaid_invoices: parsedData
                                    .filter(temp => temp.quickbook_status == 'Unpaid').length,
                                paid_invoices: parsedData
                                    .filter(temp => temp.quickbook_status == 'Paid').length,
                                partial_invoices: parsedData
                                    .filter(temp => temp.quickbook_status == 'Partial').length
                            }

                            callback(null, { code: 200, error: null, data });
                        }

                    } else {
                        getInvoicesMetricsValidation(response, options, callback)
                    }
                }
            } catch (error) {
                console.error(new Date(), `[dashboard] [FETCH] DASHBOARD INVOICE METRICS CODE ERROR`, error);

                response.status(400).json({ code: 400, error: 'CODE ERROR' });
            }
        })
    }

    Dashboard.getInvoicesMetrics = (response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                fetchDataFromCache(response, options, callback);
            }
        });

    }

    /** fetch load type metrics from database  */
    const getLoadTypeMetricsValidation = (date, response, options, callback) => {
        try {
            executeQuery("CALL getLoadTypeDashboardMetrics(?)", [date], (err, queryResponse) => {
                try {
                    if (err) {
                        console.error(new Date(), `[dashboard] [FETCH] DASHBOARD LOAD TYPE METRICS QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'DB ERROR' });
                    } else {
                        callback(null, { code: 200, error: null, data: queryResponse[0] });
                    }
                } catch (error) {
                    console.error(new Date(), `[dashboard] [FETCH] DASHBOARD LOAD TYPE METRICS CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (e) {
            console.error(new Date(), `[dashboard] [FETCH] DASHBOARD LOAD TYPE METRICS CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Dashboard.getLoadTypeMetrics = (date, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getLoadTypeMetricsValidation(date, response, options, callback);
            }
        });

    }

    const getDriverBoardDataValidation = (driverId, filterDate, response, options, callback) => {
        try {
            executeQuery("call getDriverActiveLegs(?,?);", [driverId, filterDate], (err, queryResponse) => {
                try {
                    if (err) {
                        console.error(new Date(), `[dashboard] [FETCH] DRIVER BOARD QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: 'DB ERROR' });
                    } else {
                        callback(null, { code: 200, error: null, data: queryResponse[0] });
                    }
                } catch (error) {
                    console.error(new Date(), `[dashboard] [FETCH] DRIVER BOARD CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (e) {
            console.error(new Date(), `[dashboard] [FETCH] DRIVER BOARD CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Dashboard.getDriverBoardData = (driverId, filterDate, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getDriverBoardDataValidation(driverId, filterDate, response, options, callback);
            }
        });

    }
};
