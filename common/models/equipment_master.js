'use strict';

const auth = require('./../../server/boot/auth');

const redis = require('redis');
const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);

const _ = require('lodash');

module.exports = function (Equipmentmaster) {

    let queryParams;

    //calling internally by loopback(overriden)
    Equipmentmaster.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    //post equipment management data
    const postEquipmentManagementQuery = (params) => {
        return {
            "query": `insert
                          into
                          equipment_master(equipment_type_id,
                          equipment_name,
                          make,
                          model,
                          year,
                          vin,
                          license,
                          license_state,
                          equipment_location_id,
                          license_expiry,
                          remarks,
                          truck_no,
                          vehicle_id)
                      values (?,?,?,?,?,?,?,?,?,?,?,?,?);`,
            params
        };
    }

    const postEquipmentLocationQuery = (params) => {
        return {
            "query": `insert
                          into
                          equipment_location_history(equipment_id,
                          equipment_location_id,
                          updated_by,
                          update_date)
                      values (?,?,?,CURRENT_TIMESTAMP);`,
            params
        };
    }

    const postEquipmentManagementValidation = (request, response, options, callback) => {
        try {

            queryParams = postEquipmentManagementQuery([request.equipment_type_id, request.equipment_name,
            request.make, request.model, request.year, request.vin, request.license, request.license_state,
            request.equipment_location_id, request.license_expiry, request.remarks, request.truck_no,
            request.vehicle_id]);

            auth.executeQuery(Equipmentmaster, queryParams.query, queryParams.params, (err, queryResponse) => {
                try {

                    if (err) {
                        console.error(new Date(), "[equipment] [POST] EQUIPMENT ERROR",
                            "sqlMessage" in err ? err.sqlMessage : err)

                        return response.status(409).json({ code: 409, error: "DB ERROR" });
                    }

                    client.del('equipment_details');

                    queryParams = postEquipmentLocationQuery([queryResponse.insertId,
                    request.equipment_location_id, request.userName]);

                    auth.executeQuery(Equipmentmaster, queryParams.query, queryParams.params,
                        (postErr, equipmentLocQueryResponse) => {
                            if (postErr) {
                                console.error(new Date(), `[equipment] [POST] EQUIPMENT HISTORY QUERY ERROR`,
                                    "sqlMessage" in postErr ? postErr.sqlMessage : postErr);

                                return callback(null, { code: 103, data: "EQUIPMENT INSERTED HISTORY NOT INSERTED" });
                            }

                            callback(null, { code: 200, data: "Successfully Inserted" });
                        });

                } catch (error) {
                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            });

        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Equipmentmaster.postEquipmentManagement = (request, response, options, cb) => {

        auth.validateAccessToken(Equipmentmaster, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }

            options.tokenModel = tokenModel;

            postEquipmentManagementValidation(request, response, options, cb);

        });
    }

    //update equipment management data
    const updateEquipmentManagementQueryParams = (params) => {
        return {
            "query": "call update_equipment_data(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",
            params
        };
    }

    const updateEquipmentManagementValidation = (request, response, options, cb) => {
        try {

            queryParams = updateEquipmentManagementQueryParams([request.equipment_type_id, request.equipment_name,
            request.make, request.model, request.year, request.vin, request.license, request.license_state,
            request.equipment_location_id, request.license_expiry, request.remarks, request.userName,
            request.equipment_id, request.truck_no, request.vehicle_id]);

            auth.executeQuery(Equipmentmaster, queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {

                    if (('code' in err) && err['code'] == 'ER_ROW_IS_REFERENCED_2') {
                       return cb(null, { code: 103, error: 'This equipment is being used in the application. Please edit/delete it there and try again.' })
                    }

                    console.error(new Date(), "[equipment] [UPDATE] EQUIPMENT ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: "DB ERROR" });
                }

                client.del('equipment_details');

                cb(null, { code: 200, data: "Successfully Updated" });
            });
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Equipmentmaster.updateEquipmentManagement = (request, response, options, cb) => {

        auth.validateAccessToken(Equipmentmaster, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }

            options.tokenModel = tokenModel;

            updateEquipmentManagementValidation(request, response, options, cb);

        });
    }

    //Delete equipment management data
    const deleteEquipmentManagementQueryParams = (params) => {
        return {
            "query": "call delete_equipment_data(?)",
            params
        };
    }

    const deleteEquipmentManagementValidation = (equipmentId, response, options, callback) => {
        try {

            queryParams = deleteEquipmentManagementQueryParams([equipmentId]);

            auth.executeQuery(Equipmentmaster, queryParams.query, queryParams.params, (err, queryResponse) => {

                if (err) {
                    console.error(new Date(), "[equipment] [DELETE] EQUIPMENT ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err)

                    return response.status(409).json({ code: 409, error: "DB ERROR" });
                }

                if (Array.isArray(queryResponse) && 'MESSAGE_TEXT' in queryResponse[0][0]) {

                    console.error(new Date(), `[equipment] [DELETE] EQUIPMENT ERROR`,
                        queryResponse[0][0]);

                    return callback(null,
                        { code: 103, error: 'This equipment is being used in the application. Please edit/delete it there and try again.' });
                }

                client.del('equipment_details');

                callback(null, { code: 200, data: "Successfully Deleted" });
            })
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Equipmentmaster.deleteEquipmentManagement = (equipment_id, response, options, callback) => {

        auth.validateAccessToken(Equipmentmaster, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }
            options.tokenModel = tokenModel;

            deleteEquipmentManagementValidation(equipment_id, response, options, callback);

        });
    }

    //update equipment location
    const updateEquipmentLocationQueryParams = (params) => {
        return {
            "query": "call update_equipment_location(?,?,?,?)",
            params
        };
    }

    const updateEquipmentLocationValidation = (request, response, options, cb) => {
        try {

            queryParams = updateEquipmentLocationQueryParams([request.equipmentId,
            request.equipment_location_id, request.userName, request.date]);

            auth.executeQuery(Equipmentmaster, queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[equipment] [UPDATE] EQUIPMENT HISTORY QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: "DB ERROR" });
                }

                client.del('equipment_details');

                cb(null, { code: 200, data: "Successfully Updated" });

            });
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Equipmentmaster.updateEquipmentLocation = (request, response, options, cb) => {

        auth.validateAccessToken(Equipmentmaster, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }

            options.tokenModel = tokenModel;

            updateEquipmentLocationValidation(request, response, options, cb);

        });
    }

    //get equipment management
    const getEquipmentManagementQuery = () => {
        return {
            "query": `SELECT
                          eqp.equipment_id,
                          eqp.equipment_name,
                          eqp.license,
                          eqp.license_expiry,
                          eqp.license_state,
                          eqp.make,
                          eqp.model,
                          eqp.remarks,
                          eqp.vin,
                          eqp.year,
                          eqp.truck_no,
                          eqp.vehicle_id,
                          types.equipment_type,
                          types.equipment_type_id,
                          locations.equipment_location,
                          locations.equipment_location_id,
                          dm.driver_id,
                          elh_sub.equipment_location_history
                      FROM
                          equipment_master eqp
                      LEFT JOIN equipment_type_master types ON
                          types.equipment_type_id = eqp.equipment_type_id
                      LEFT JOIN equipment_location_master locations ON
                          locations.equipment_location_id = eqp.equipment_location_id
                      LEFT JOIN driver_master dm ON 
                          dm.vehicle_id = eqp.vehicle_id
                      LEFT JOIN (
                          SELECT
                              elh.equipment_id,
                              JSON_ARRAYAGG(JSON_OBJECT('equipment_location_history_id',
                              elh.equipment_location_history_id,
                              'equipment_location_id',
                              elh.equipment_location_id,
                              'update_date',
                              UNIX_TIMESTAMP(elh.update_date) * 1000,
                              'updated_by',
                              elh.updated_by,
                              'equipment_location',
                              eqm.equipment_location)) equipment_location_history
                          FROM
                              equipment_location_history elh
                          LEFT JOIN equipment_location_master eqm ON
                              eqm.equipment_location_id = elh.equipment_location_id
                          GROUP BY
                              elh.equipment_id) elh_sub ON
                          elh_sub.equipment_id = eqp.equipment_id;`
        };
    }

    const getEquipmentManagementValidation = (request, response, options, callback) => {
        try {

            queryParams = getEquipmentManagementQuery();

            auth.executeQuery(Equipmentmaster, queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[equipment] [FETCH] EQUIPMENT MANAGEMENT QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: "DB ERROR" });
                }

                queryResponse.forEach(temp => {

                    temp.equipment_location_history = temp.equipment_location_history ?
                        JSON.parse(temp.equipment_location_history)
                            .sort((a, b) => new Date(b.update_date).getTime() - new Date(a.update_date).getTime())
                        : [];
                });

                client.setex("equipment_details", 3600, JSON.stringify(queryResponse));

                getEquipmentManagementCache(request, response, options, callback);
            })

        } catch (e) {
            console.error(new Date(), `[equipment] [FETCH] EQUIPMENT MANAGEMENT CODE ERROR`,
                e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    const getEquipmentManagementCache = (request, response, options, callback) => {
        let equipmentParsedData;
        let redisResponse;

        let vehicleIds = [];

        client.get("equipment_details", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[equipment] [FETCH] EQUIPMENT MANAGEMENT REDIS ERROR`,
                        err);

                    return response.status(409).json({ code: 409, error: "REDIS ERROR" });
                }

                if (cacheData) {

                    equipmentParsedData = JSON.parse(cacheData);

                    if (options.mode === 'trucklist') {
                        equipmentParsedData = equipmentParsedData
                            .filter(temp => temp.equipment_type === 'Truck').map(temp => {
                                return {
                                    truck_no: temp.truck_no,
                                    vehicle_id: temp.vehicle_id,
                                    isDisabled: temp.driver_id !== null
                                }
                            });

                        return callback(null, { code: 200, data: equipmentParsedData });
                    }

                    equipmentParsedData.forEach(temp => {
                        temp.vehicle_id ? vehicleIds.push(+temp.vehicle_id) : null;
                    })

                    if (request.columnFilter) {

                        Object.keys(request.columnFilterValue).forEach(element => {

                            request.columnFilterValue[element] = element !== 'license_expiry' ?
                                request.columnFilterValue[element].toLowerCase()
                                    .replace(/[^a-zA-Z0-9-]+/ig, "") :
                                (request.columnFilterValue[element] !== '' &&
                                    request.columnFilterValue[element] !== null ?
                                    new Date(request.columnFilterValue[element])
                                        .toLocaleDateString() : '');

                            equipmentParsedData = equipmentParsedData.length !== 0 ?
                                equipmentParsedData.filter(temp => {
                                    return request.columnFilterValue[element] !== "" ?
                                        (
                                            element === 'license_expiry' ?
                                                (
                                                    temp[element] !== null ?
                                                        new Date(temp[element]).toLocaleDateString()
                                                            .includes(request.columnFilterValue[element])
                                                        : false
                                                ) :
                                                (
                                                    (element === 'equipment_type' ||
                                                        element === 'equipment_location') ?
                                                        (
                                                            temp[element] !== null ?
                                                                temp[element]
                                                                    .toLowerCase()
                                                                    .replace(/[^a-zA-Z0-9-]+/ig, "")
                                                                    .includes(
                                                                        request.columnFilterValue[element]
                                                                    )
                                                                : false
                                                        ) :
                                                        (
                                                            temp[element] !== null ?
                                                                temp[element]
                                                                    .toLowerCase()
                                                                    .replace(/[^a-zA-Z0-9-]+/ig, "")
                                                                    .indexOf(
                                                                        request.columnFilterValue[element]
                                                                    ) === 0
                                                                : false
                                                        )
                                                )
                                        ) : true
                                }) : []
                        });
                    }


                    redisResponse = {
                        code: 200,
                        error: null,
                        data: formatEquipmentData(request, equipmentParsedData),
                        disableIds: vehicleIds
                    }

                    callback(null, redisResponse);
                } else {
                    getEquipmentManagementValidation(request, response, options, callback);
                }
            } catch (error) {
                console.error(new Date(), `[equipment] [FETCH] EQUIPMENT MANAGEMENT CODE ERROR`,
                    error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        });
    }

    const formatEquipmentData = (request, equipmentParsedData) => {
        let equipmentData;
        let equipmentPaginationLength;

        if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {
            equipmentData = equipmentParsedData.length !== 0 ? _.orderBy(equipmentParsedData, request.column, request.direction).slice((request.offset - 1), request.limit) : [];
        } else {
            equipmentData = equipmentParsedData.length !== 0 ? _.orderBy(equipmentParsedData, 'equipment_id', 'desc').slice((request.offset - 1), request.limit) : [];
        }

        equipmentPaginationLength = equipmentParsedData.length;

        return { equipmentData, equipmentPaginationLength }
    }

    Equipmentmaster.getEquipmentManagement = (request, response, options, callback) => {

        auth.validateAccessToken(Equipmentmaster, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }
            options.tokenModel = tokenModel;

            options.mode = 'general';

            getEquipmentManagementCache(request, response, options, callback);
        });
    }

    Equipmentmaster.getTruckNumberList = (response, options, callback) => {

        auth.validateAccessToken(Equipmentmaster, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }
            options.tokenModel = tokenModel;

            options.mode = 'trucklist';

            getEquipmentManagementCache({ columnFilter: false }, response, options, callback);
        });
    }
};
