'use strict';

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const _ = require('lodash');
const moment = require('moment');

module.exports = function (Search) {

    //Datasource connector
    const executeQuery = (query, params, callback) => {
        Search.dataSource.connector.query(query, params, callback);
    }

    //Access Token
    const validateAccessToken = (access_token, callback) => {
        Search.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Search.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    const setOrderDataInRedis = (request, response, options, callback) => {

        executeQuery('call getAdvancedSearchData();', null,
            (err, queryResponse) => {
                try {
                    if (err) {
                        return response.status(409).json({ code: 409, error: "DB ERROR" })
                    } else {
                        queryResponse[0].forEach(temp => {
                            temp.all_legs = temp.all_legs ? JSON.parse(temp.all_legs) : [];
                        });

                        client.setex("advanced_search", 3600, JSON.stringify(queryResponse[0]));

                        advancedSearchValidation(request, response, options, callback);
                    }
                } catch (error) {
                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            });
    }


    const advancedSearchValidation = (request, response, options, callback) => {

        let orderData = [];

        let legsData = [];

        client.get("advanced_search", (err, cacheData) => {
            try {
                if (err) {
                    return response.status(409).json({ code: 409, error: "REDIS ERROR" })
                } else {
                    if (cacheData) {

                        orderData = JSON.parse(cacheData);

                        if (request.isAutoSuggest) {
                            request.search = request.search ? request.search.toLowerCase()
                                .replace(((request.searchBy === 'customer_reference' || request.searchBy === 'booking_number') ?
                                    /[^a-zA-Z0-9,'*-_.!#+"\$\&]+/g : /[^a-zA-Z0-9/]+/g), '') : null;

                            orderData = orderData.length !== 0 ?
                                orderData.filter((temp) => {
                                    (request.searchBy === "container_number" || request.searchBy === "chassis_number") ?
                                        legsData.push(temp.all_legs.find(elem => {
                                            return elem[request.searchBy] ?
                                                elem[request.searchBy]
                                                    .toLowerCase()
                                                    .replace(/[^a-zA-Z0-9/]+/g, "")
                                                    .indexOf(request.search) === 0 : false
                                        })) : null;

                                    return (
                                        temp[request.searchBy] ?
                                            (
                                                temp[request.searchBy]
                                                    .toLowerCase()
                                                    .replace(((request.searchBy === 'customer_reference' || request.searchBy === 'booking_number') ?
                                                        /[^a-zA-Z0-9,'*-_.!#+"\$\&]+/g : /[^a-zA-Z0-9/]+/g), '')
                                                    .indexOf(request.search) === 0
                                            ) : false
                                    ) ||
                                        (
                                            (request.searchBy === "container_number" || request.searchBy === "chassis_number") ?
                                                temp.all_legs.some(elem => {
                                                    return elem[request.searchBy] ?
                                                        elem[request.searchBy]
                                                            .toLowerCase()
                                                            .replace(/[^a-zA-Z0-9/]+/g, "")
                                                            .indexOf(request.search) === 0 : false
                                                }) : false
                                        )
                                }) : [];

                            (request.searchBy === "container_number" || request.searchBy === "chassis_number") ?
                                legsData.filter(temp => temp).forEach(temp => orderData.push(temp)) : null;

                            orderData = orderData.length !== 0 ?
                                _.uniq(orderData.filter(temp => temp[request.searchBy]).map(temp => temp[request.searchBy])) : [];

                            return callback(null, orderData);
                        }

                        Object.keys(request).forEach(element => {

                            request[element] = request[element].toLowerCase()
                                .replace(((element === 'customer_reference' || element === 'booking_number') ?
                                    /[^a-zA-Z0-9,'*-_.!#+"\$\&]+/g : /[^a-zA-Z0-9/]+/g), '');

                            orderData = orderData.length !== 0 ?
                                orderData.filter(temp => {

                                    if (temp.order_status === 'Cancelled') {
                                        temp.current_status = 'Order Cancelled';
                                    } else if (temp.order_status === 'No Invoice') {
                                        temp.current_status = 'Order Not Generated';
                                    } else if (temp.order_status !== 'Delivered' && temp.is_dispatch === 0 &&
                                        temp.is_draft === 0) {
                                        temp.current_status = 'Pending';
                                    } else if (temp.order_status !== 'Delivered' && temp.order_status !== 'Dispatch'
                                        && temp.is_dispatch === 1 && temp.is_draft === 0) {
                                        temp.current_status = 'Dispatch';
                                    } else if (temp.order_status === 'Dispatch' && temp.isReadyToInvoice === 0) {
                                        temp.current_status = 'Dispatch In Manual';
                                    } else if (temp.order_status === 'Dispatch' && temp.isReadyToInvoice === 1) {
                                        temp.current_status = 'Dispatch In Auto Schedule';
                                    } else if (temp.is_draft === 1) {
                                        temp.current_status = 'Draft';
                                    } else if (temp.order_status === 'Delivered' && temp.is_invoice_generated === 0) {
                                        temp.current_status = 'Ready For Invoice';
                                    } else if (temp.order_status === 'Delivered' && temp.is_invoice_generated === 1
                                        && temp.is_archived === 0) {
                                        temp.current_status = 'Invoice Generated';
                                    } else if (temp.order_status === 'Delivered' && temp.is_invoice_generated === 1
                                        && temp.is_archived === 1) {
                                        temp.current_status = 'Archived';
                                    } else if (temp.order_status === null && temp.is_archived === 0) {
                                        temp.current_status = 'Manual Invoice Generated';
                                    } else if (temp.order_status === null && temp.is_archived === 1) {
                                        temp.current_status = 'Archived';
                                    }

                                    return (temp[element] && temp[element].trim() ?
                                        temp[element].toString()
                                            .toLowerCase()
                                            .replace(((element === 'customer_reference' || element === 'booking_number') ?
                                                /[^a-zA-Z0-9,'*-_.!#+"\$\&]+/g : /[^a-zA-Z0-9/]+/g), '')
                                            .indexOf(request[element]) === 0
                                        : false) || (
                                            (element === "container_number" || element === "chassis_number") ?
                                                temp.all_legs.some(elem => {
                                                    return elem[element] ?
                                                        elem[element]
                                                            .toLowerCase()
                                                            .replace(/[^a-zA-Z0-9/]+/g, "")
                                                            .indexOf(request[element]) === 0 : false
                                                }) : false
                                        )
                                })
                                : [];

                        });

                        callback(null, Object.keys(request).length ? orderData : []);

                    } else {
                        setOrderDataInRedis(request, response, options, callback);
                    }
                }
            } catch (error) {
                console.error(new Date(), `[search] code error`, error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        })
    }

    Search.advancedSearch = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                advancedSearchValidation(request, response, options, callback);
            }
        });

    }

    Search.advancedSearchAutoSuggest = (search, searchBy, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                let request = {
                    search: search ? decodeURIComponent(search) : null,
                    searchBy,
                    isAutoSuggest: true
                }
                advancedSearchValidation(request, response, options, callback);
            }
        });

    }

    const autoSuggestOnlyOnCache = (redisKey, search, searchBy, response, options, callback) => {

        let parsedData = [];

        client.get(redisKey, (err, cacheData) => {
            try {
                if (err) {
                    return response.status(409).json({ code: 409, error: "REDIS ERROR" })
                }
                if (cacheData) {

                    parsedData = JSON.parse(cacheData);

                    parsedData = parsedData.filter(temp => {
                        return (
                            temp[searchBy] ?
                                (
                                    temp[searchBy]
                                        .toString()
                                        .toLowerCase()
                                        .replace(((searchBy === 'customer_reference' || searchBy === 'booking_number') ?
                                            /[^a-zA-Z0-9,'*-_.!#+"\$\&]+/g : /[^a-zA-Z0-9/]+/g), '')
                                        .indexOf(search) === 0
                                ) : false
                        )
                    }).map(temp => temp[searchBy]);

                    parsedData = _.uniq(parsedData);

                    return callback(null, parsedData);
                }

                return callback(null, []);


            } catch (error) {
                console.error(new Date(), `[search] code error`, error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        })

    }

    Search.filterAutoSuggest = (redisKey, search, searchBy, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }
            options.tokenModel = tokenModel;

            search = search ?
                decodeURIComponent(search)
                    .toString()
                    .toLowerCase()
                    .replace(((searchBy === 'customer_reference' || searchBy === 'booking_number') ?
                        /[^a-zA-Z0-9,'*-_.!#+"\$\&]+/g : /[^a-zA-Z0-9/]+/g), '')
                : null;

            redisKey = redisKey ? decodeURIComponent(redisKey) : null;

            searchBy = searchBy ? decodeURIComponent(searchBy) : null;

            autoSuggestOnlyOnCache(redisKey, search, searchBy, response, options, callback);

        });

    }

};
