'use strict';

const redis = require('redis');
const fetchRequest = require('request');
const async = require('async');
const _ = require('lodash');
const OAuthClient = require('intuit-oauth');

const app = require('../../server/server.js');
const auth = require('../../server/boot/auth');
const quickbooks = require('../../server/boot/quickbooks-connection');

const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);

module.exports = function (Customermanagement) {

    let queryParams;

    let quickbooksUrl = app.oauthClient.environment == 'sandbox' ?
        OAuthClient.environment.sandbox : OAuthClient.environment.production;

    auth.setToken(Customermanagement);

    /**
     * @function postCustomerData triggered to post the customer details
     * @function postCustomerMasterQuery @returns native mysql queries and it's related param
     * @function postDataInQuickbooks is to post the data in quickbooks as well as database
     */
    const postCustomerMasterQuery = (params) => {
        return {
            "query": `insert into customer_master (business_name,b_address,b_street,b_city,b_postal_code,
                b_state,p_address,p_street,p_city,p_postal_code,p_state,point_of_contact,poc_phone,
                business_phone,email,fax,preferred_invoice,customer_notes,customer_quickbooks_id,
                customer_quickbooks_sync,physical_name,billing_name,is_same_address,preferred_documents) 
                values(UCASE(?),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,UCASE(?),UCASE(?),?,?);`,
            "params": params
        };
    }

    const postDataInQuickbooksAndDb = async (req, res, options, callback) => {
        try {

            let quickbooksConnection = await quickbooks.authenticateQuickbooksConnection(Customermanagement);

            if (quickbooksConnection.code !== 200) {

                return quickbooksConnection.code === 103 ?
                    callback(null, quickbooksConnection) :
                    res.status(quickbooksConnection.code).json(quickbooksConnection);

            }

            let emails = null;
            let pointOfContact = null;
            let pocPhone = null;
            let quickbookEmails = [];
            if (req.contact_details) {
                emails = req.contact_details.map(temp => temp.email)
                quickbookEmails = emails.filter(email => email !== null);

                pointOfContact = req.contact_details.map(temp => temp.point_of_contact)
                pointOfContact = pointOfContact.length > 0 ? pointOfContact.toString() : null;

                pocPhone = req.contact_details.map(temp => temp.poc_phone)
                pocPhone = pocPhone.length > 0 ? pocPhone.toString() : null;

                for (let email of quickbookEmails) {
                    let fetchedEmail = await checkEmailDuplication(
                        "SELECT * FROM customer_master cm WHERE FIND_IN_SET(?,email);",
                        [email]
                    )

                    if (fetchedEmail.code !== 200) {
                        return res.status(fetchedEmail.code).json(fetchedEmail);
                    }

                    if (fetchedEmail.data.length) {
                        return res.status(200).json({ code: 100, error: "Duplicate Email. Please Use Different Email" });
                    }
                }

            }

            let quickbooksInput = {
                "PrimaryEmailAddr": {
                    "Address": quickbookEmails.toString()
                },
                "DisplayName": req.business_name.toUpperCase(),
                "Notes": req.customer_notes,
                "PrimaryPhone": {
                    "FreeFormNumber": req.business_phone
                },
                "PreferredDeliveryMethod": req.preferred_mode == "email" ? req.preferred_mode : "Print",
                "BillAddr": {
                    "CountrySubDivisionCode": req.b_state,
                    "City": req.b_city,
                    "PostalCode": req.b_postal_code,
                    "Line1": req.b_street,
                    "Country": "USA"
                },
                "ShipAddr": {
                    "CountrySubDivisionCode": req.p_state,
                    "City": req.p_city,
                    "PostalCode": req.p_postal_code,
                    "Line1": req.p_street,
                    "Country": "USA"
                },
            }

            let customerOptions = {
                uri: `${quickbooksUrl}/v3/company/${app.oauthClient.getToken().realmId}/customer?minorversion=40`,
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + app.oauthClient.getToken().access_token
                },
                json: quickbooksInput
            }

            fetchRequest(customerOptions, (err, result) => {
                try {
                    if (err) {
                        console.error(new Date(), `[customer] [API] QUICKBOOKS API ERROR`, err);

                        return res.status(500).json({ code: 500, error: 'QUICKBOOK API CONNECTION ERROR' })
                    }

                    if (result.statusCode < 200 || result.statusCode > 299) {

                        if (result &&
                            ('body' in result)
                            && ('Fault' in result.body)
                            && result.body.Fault.Error.length !== 0
                            && result.body.Fault.Error[0].Message == 'Duplicate Name Exists Error') {
                            return callback(null, { code: 422, error: 'Customer Name Exists' })
                        }

                        console.error(new Date(), `[customer] [API] QUICKBOOKS API ERROR`,
                            result && ('body' in result) && ('Fault' in result.body) &&
                                ('Error' in result.body.Fault) && result.body.Fault.Error &&
                                result.body.Fault.Error.length !== 0 ? result.body.Fault.Error[0]
                                : null);

                        return res.status(409).json({ code: 409, error: 'QUICKBOOKS ERROR' })

                    }

                    queryParams = postCustomerMasterQuery([req.business_name, req.b_address,
                    req.b_street, req.b_city, req.b_postal_code, req.b_state, req.p_address,
                    req.p_street, req.p_city, req.p_postal_code, req.p_state, pointOfContact,
                    pocPhone, req.business_phone, emails.toString(), req.fax, req.preferred_mode,
                    req.customer_notes, result.body.Customer.Id, result.body.Customer.SyncToken,
                    req.physical_name, req.billing_name, req.is_same_address,
                    req.preferred_documents]);

                    auth.executeQuery(Customermanagement, queryParams.query, queryParams.params, (error, queryResponse) => {
                        if (error) {
                            if (('code' in error) && error['code'] == 'ER_DUP_ENTRY') {
                                return callback(null, { code: 422, error: 'Customer Already Exists' });
                            }

                            console.error(new Date(), `[customer] [POST] CUSTOMER QUERY ERROR`,
                                "sqlMessage" in error ? error.sqlMessage : error);

                            return res.status(409).json({ code: 409, error: 'DB ERROR' });

                        }

                        client.del('customer_master');
                        callback(null, { code: 200, data: "customer created successfully" })

                    });

                } catch (error) {
                    console.error(new Date(), `[customer] [POST] CUSTOMER CODE ERROR`, error);

                    res.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (error) {
            console.error(new Date(), `[customer] [POST] CUSTOMER CODE ERROR`, error);

            res.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Customermanagement.postCustomerData = (request, response, options, callback) => {

        auth.validateAccessToken(Customermanagement, options.access_token, (err, tokenModel) => {

            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            }
            options.tokenModel = tokenModel;
            postDataInQuickbooksAndDb(request, response, options, callback);

        });

    }

    const checkEmailDuplication = (query, param) => {
        return new Promise((resolve, reject) => {
            return auth.executeQuery(Customermanagement, query, param, (err, queryResponse) => {
                if (err) {
                    return resolve({ code: 409, error: "DB ERROR" });
                }

                return resolve({ code: 200, data: queryResponse });
            })
        })
    }

    /**
     * get customer data from database and set the data in redis cache
     * send the customer data as per the request
     * request object will contain flag for pagination,sorting,filtering and etc
     **/
    const getCustomerDataFromDatabase = (request, response, options, callback) => {
        try {
            Customermanagement.find({ where: { customer_id: { gt: 0 } } }, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[customer] [FETCH] CUSTOMER QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: 'DB ERROR' })
                }
                client.setex("customer_master", 3600, JSON.stringify(queryResponse));

                options.section === 'autoSuggest' ?
                    searchCustomerAutosuggestValidation(request, response, options, callback)
                    : getCustomerDetailsCache(request, response, options, callback);
            });
        } catch (e) {
            console.error(new Date(), `[customer] [FETCH] CUSTOMER QUERY ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    const getCustomerDetailsCache = (request, response, options, callback) => {
        let customerParsedData = [];
        let finalResponse;

        client.get("customer_master", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[customer] [FETCH] CUSTOMER REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' })
                }
                if (cacheData) {
                    customerParsedData = JSON.parse(cacheData);

                    if (request.columnFilter) {

                        Object.keys(request.columnFilterValue).forEach(element => {

                            request.columnFilterValue[element] = request.columnFilterValue[element]
                                .toString().toLowerCase().replace(/[^a-zA-Z0-9@]+/ig, "");

                            customerParsedData = customerParsedData.filter(temp => {
                                return request.columnFilterValue[element] !== "" ?
                                    (
                                        temp[element] ?
                                            (
                                                element === 'email' ?
                                                    temp[element]
                                                        .split(',')
                                                        .some(elem => elem.toString()
                                                            .toLowerCase()
                                                            .replace(/[^a-zA-Z0-9@]+/ig, "")
                                                            .indexOf(request.columnFilterValue[element]) === 0) :
                                                    element === 'point_of_contact' ?
                                                        temp[element]
                                                            .split(',')
                                                            .some(elem => elem.toString()
                                                                .toLowerCase()
                                                                .replace(/[^a-zA-Z0-9@]+/ig, "")
                                                                .indexOf(request.columnFilterValue[element]) === 0) :
                                                        temp[element].toString()
                                                            .toLowerCase()
                                                            .replace(/[^a-zA-Z0-9@]+/ig, "")
                                                            .indexOf(request.columnFilterValue[element]) === 0
                                            )
                                            : false
                                    ) : true
                            })
                        });

                    }

                    finalResponse = {
                        code: 200,
                        error: null,
                        data: formatCustomerData(request, customerParsedData)
                    }

                    return callback(null, finalResponse);
                }

                getCustomerDataFromDatabase(request, response, options, callback);

            } catch (error) {
                console.error(new Date(), `[customer] [FETCH] CUSTOMER CACHE CODE ERROR`, error);

                response.status(400).json({ code: 400, error: 'CODE ERROR' });
            }
        });
    }

    const formatCustomerData = (request, customerParsedData) => {
        let customerData;
        let customerPaginationLength;

        if (request.direction && (request.direction === 'asc' || request.direction === 'desc')) {
            customerData = _.orderBy(customerParsedData, request.column, request.direction)
                .slice((request.offset - 1), request.limit);
        } else {
            customerData = _.orderBy(customerParsedData, 'customer_id', 'desc')
                .slice((request.offset - 1), request.limit);
        }

        if (customerData && customerData.length > 0) {
            let email;
            let point_of_contact;
            let poc_phone;

            for (const temp of customerData) {
                email = temp.email ? temp.email.split(',') : temp.email;
                point_of_contact = temp.point_of_contact ? temp.point_of_contact.split(',') : temp.point_of_contact;
                poc_phone = temp.poc_phone ? temp.poc_phone.split(',') : temp.poc_phone;

                if (email) {
                    temp.contact_details = email.map((emailId, index) => {
                        return {
                            email: emailId != '' ? emailId : null,
                            point_of_contact: (point_of_contact && point_of_contact[index] != '' &&
                                point_of_contact[index] != undefined) ? point_of_contact[index] : null,
                            poc_phone: (poc_phone && poc_phone[index] != '' &&
                                poc_phone[index] != undefined) ? poc_phone[index] : null
                        }
                    });
                    delete temp.email;
                    delete temp.point_of_contact;
                    delete temp.poc_phone;
                } else {
                    temp.contact_details = {
                        email: null,
                        point_of_contact: null,
                        poc_phone: null
                    }
                    delete temp.email;
                    delete temp.point_of_contact;
                    delete temp.poc_phone;
                }
            }
        }

        customerPaginationLength = customerParsedData.length;

        return { customerData, customerPaginationLength }
    }

    Customermanagement.getCustomerDetails = (request, response, options, callback) => {

        auth.validateAccessToken(Customermanagement, options.access_token, (err, tokenModel) => {

            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' })
            } else {
                options.tokenModel = tokenModel;

                options.section = 'getCustomer';

                getCustomerDetailsCache(request, response, options, callback);
            }

        });

    }

    /**
     * @function updateCustomerData triggered to update the customer details
     * @function updateCustomerDataQueryParams @returns native mysql queries and it's related param
     * @function updateDataInQuickbooks is to update the data in quickbooks as well as database
     */
    const updateCustomerDataQueryParams = (params) => {
        return {
            "query": `UPDATE
                          customer_master
                      SET
                          business_name = UCASE(?),
                          b_address =?,
                          b_street =?,
                          b_city =?,
                          b_postal_code =?,
                          b_state =?,
                          p_address =?,
                          p_street =?,
                          p_city =?,
                          p_postal_code =?,
                          p_state =?,
                          point_of_contact =?,
                          poc_phone =?,
                          business_phone =?,
                          email =?,
                          fax =?,
                          preferred_invoice =?,
                          customer_notes =?,
                          physical_name = UCASE(?),
                          billing_name = UCASE(?),
                          is_same_address = ?,
                          customer_quickbooks_sync = ?,
                          preferred_documents = ?
                      WHERE
                          customer_id = ?;`,
            "params": params
        };
    }

    const updateDataInQuickbooksAndDb = async (req, res, options, callback) => {
        try {

            let quickbooksConnection = await quickbooks.authenticateQuickbooksConnection(Customermanagement);

            if (quickbooksConnection.code !== 200) {

                return quickbooksConnection.code === 103 ?
                    callback(null, quickbooksConnection) :
                    res.status(quickbooksConnection.code).json(quickbooksConnection);

            }

            let emails = null;
            let pointOfContact = null;
            let pocPhone = null;
            let quickbookEmails = [];
            if (req.contact_details) {
                emails = req.contact_details.map(temp => temp.email)
                quickbookEmails = emails.filter(email => email !== null);

                pointOfContact = req.contact_details.map(temp => temp.point_of_contact)
                pointOfContact = pointOfContact.length > 0 ? pointOfContact.toString() : null;

                pocPhone = req.contact_details.map(temp => temp.poc_phone)
                pocPhone = pocPhone.length > 0 ? pocPhone.toString() : null;

                for (let email of quickbookEmails) {
                    let fetchedEmail = await checkEmailDuplication(
                        "SELECT * FROM customer_master cm WHERE FIND_IN_SET(?,email) and customer_id <> ?;",
                        [email, req.customer_id]
                    )

                    if (fetchedEmail.code !== 200) {
                        return res.status(fetchedEmail.code).json(fetchedEmail);
                    }

                    if (fetchedEmail.data.length) {
                        return res.status(200).json({ code: 100, error: "Duplicate Email. Please Use Different Email" });
                    }
                }

            }

            let quickbooksInput = {
                "PrimaryEmailAddr": {
                    "Address": quickbookEmails.toString()
                },
                "DisplayName": req.business_name.toUpperCase(),
                "Notes": req.customer_notes,
                "PrimaryPhone": {
                    "FreeFormNumber": req.business_phone
                },
                "PreferredDeliveryMethod": req.preferred_mode == "email" ? req.preferred_mode : "Print",
                "BillAddr": {
                    "CountrySubDivisionCode": req.b_state,
                    "City": req.b_city,
                    "PostalCode": req.b_postal_code,
                    "Line1": req.b_street,
                    "Country": "USA"
                },
                "ShipAddr": {
                    "CountrySubDivisionCode": req.p_state,
                    "City": req.p_city,
                    "PostalCode": req.p_postal_code,
                    "Line1": req.p_street,
                    "Country": "USA"
                },
                "sparse": true,
                "Id": req.customer_quickbooks_id,
                "SyncToken": ''
            }

            const URL = `customer/${req.customer_quickbooks_id}?minorversion=38`;

            let syncToken = await quickbooks.fetchDataFromQuickbooks(URL);

            if (syncToken.code !== 200) {
                return res.status(syncToken.code).json(syncToken);
            }

            quickbooksInput['SyncToken'] = syncToken.data.json.Customer.SyncToken;


            let customerOptions = {
                uri: `${quickbooksUrl}/v3/company/${app.oauthClient.getToken().realmId}/customer?minorversion=40`,
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + app.oauthClient.getToken().access_token
                },
                json: quickbooksInput
            }

            fetchRequest(customerOptions, (err, result) => {
                try {
                    if (err) {
                        return res.status(500).json({ code: 500, error: 'QUICKBOOK API CONNECTION ERROR' })
                    }

                    if (result.statusCode < 200 || result.statusCode > 299) {
                        if (result &&
                            ('body' in result)
                            && ('Fault' in result.body)
                            && result.body.Fault.Error.length !== 0
                            && result.body.Fault.Error[0].Message == 'Duplicate Name Exists Error') {
                            return callback(null, { code: 422, error: 'Customer Name Exists' })
                        }

                        console.error(new Date(), `[customer] [API] QUICKBOOKS API ERROR`,
                            result && ('body' in result) && ('Fault' in result.body) &&
                                ('Error' in result.body.Fault) && result.body.Fault.Error &&
                                result.body.Fault.Error.length !== 0 ? result.body.Fault.Error[0]
                                : null);

                        return res.status(409).json({ code: 409, error: 'QUICKBOOKS ERROR' })

                    }

                    queryParams = updateCustomerDataQueryParams([req.business_name, req.b_address,
                    req.b_street, req.b_city, req.b_postal_code, req.b_state, req.p_address,
                    req.p_street, req.p_city, req.p_postal_code, req.p_state, pointOfContact,
                    pocPhone, req.business_phone, emails.toString(), req.fax, req.preferred_mode,
                    req.customer_notes, req.physical_name, req.billing_name, req.is_same_address,
                    result.body.Customer.SyncToken, req.preferred_documents, req.customer_id]);

                    auth.executeQuery(Customermanagement, queryParams.query, queryParams.params, (error, queryResponse) => {
                        if (error) {
                            if (('code' in error) && error['code'] == 'ER_DUP_ENTRY') {
                                console.error(new Date(), `[customer] [UPDATE] CUSTOMER QUERY ERROR`,
                                    "sqlMessage" in error ? error.sqlMessage : error);

                                return callback(null, { code: 422, error: 'Customer Already Exists' })
                            }

                            return res.status(409).json({ code: 409, error: 'DB ERROR' });

                        }

                        client.del('customer_master');
                        client.del("ordersDispatchData");
                        client.del("ordersPendingData");
                        client.del("ordersDraftData");
                        client.del("dispatchData");
                        client.del("invoiceData");
                        client.del('invoice_mgmt');
                        callback(null, { code: 200, data: "Customer Updated Successfully" })

                    });

                } catch (error) {
                    console.error(new Date(), `[customer] [UPDATE] CUSTOMER CODE ERROR`, error);

                    res.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (error) {
            console.error(new Date(), `[customer] [UPDATE] CUSTOMER CODE ERROR`, error);

            res.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Customermanagement.updateCustomerData = (request, response, options, callback) => {

        auth.validateAccessToken(Customermanagement, options.access_token, (err, tokenModel) => {

            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' })
            } else {
                options.tokenModel = tokenModel;
                updateDataInQuickbooksAndDb(request, response, options, callback);
            }

        });
    }


    /**
     * To make a customer active or inactive
     * @function makeActiveOrInactive main function triggered by checking user had valid access token to proceed */
    const makeActiveOrInactiveQuery = (params) => {
        return {
            "query": "UPDATE customer_master SET customer_quickbooks_sync = ?, is_active = ? WHERE customer_id = ?",
            "params": params
        };
    }

    const makeActiveOrInactiveInQuickbooksAndDb = async (req, res, options, callback) => {
        try {
            let quickbooksConnection = await quickbooks.authenticateQuickbooksConnection(Customermanagement);

            if (quickbooksConnection.code !== 200) {

                return quickbooksConnection.code === 103 ?
                    callback(null, quickbooksConnection) :
                    res.status(quickbooksConnection.code).json(quickbooksConnection);

            }

            let quickbooksInput = {
                "Active": req.is_active,
                "sparse": true,
                "Id": req.customer_quickbooks_id,
                "SyncToken": ''
            }

            const URL = `customer/${req.customer_quickbooks_id}?minorversion=38`;

            let syncToken = await quickbooks.fetchDataFromQuickbooks(URL);

            if (syncToken.code !== 200) {
                return res.status(syncToken.code).json(syncToken);
            }

            quickbooksInput['SyncToken'] = syncToken.data.json.Customer.SyncToken;

            let customerOptions = {
                uri: `${quickbooksUrl}/v3/company/${app.oauthClient.getToken().realmId}/customer?minorversion=40`,
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + app.oauthClient.getToken().access_token
                },
                json: quickbooksInput
            }

            fetchRequest(customerOptions, (err, result) => {
                try {
                    if (err) {
                        return res.status(500).json({ code: 500, error: 'QUICKBOOK API CONNECTION ERROR' })
                    }

                    if (result.statusCode < 200 || result.statusCode > 299) {
                        if (result &&
                            ('body' in result)
                            && ('Fault' in result.body)
                            && result.body.Fault.Error.length !== 0
                            && result.body.Fault.Error[0].Message == 'Delete List Has Balance Error') {
                            return callback(null, { code: 422, error: 'Customer Having Open Invoice' })
                        }
                        console.error(new Date(), `[customer] [API] QUICKBOOKS API ERROR`,
                            result && ('body' in result) && ('Fault' in result.body) &&
                                ('Error' in result.body.Fault) && result.body.Fault.Error &&
                                result.body.Fault.Error.length !== 0 ? result.body.Fault.Error[0]
                                : null);

                        return res.status(409).json({ code: 409, error: 'QUICKBOOKS ERROR' })

                    }

                    queryParams = makeActiveOrInactiveQuery([result.body.Customer.SyncToken,
                    result.body.Customer.Active, req.customer_id]);

                    auth.executeQuery(Customermanagement, queryParams.query, queryParams.params, (error, queryResponse) => {
                        if (error) {
                            console.error(new Date(), `[customer] [UPDATE] ACTIVE/INACTIVE QUERY ERROR`,
                                "sqlMessage" in error ? error.sqlMessage : error);

                            return res.status(409).json({ code: 409, error: 'DB ERROR' });
                        }
                        client.del('customer_master');
                        client.del("ordersDispatchData");
                        client.del("ordersPendingData");
                        client.del("ordersDraftData");
                        client.del("dispatchData");
                        client.del("invoiceData");
                        callback(null, { code: 200, data: "Customer Updated Successfully" });
                    });

                } catch (error) {
                    console.error(new Date(), `[customer] [UPDATE] ACTIVE/INACTIVE CODE ERROR`, error);

                    res.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (error) {
            console.error(new Date(), `[customer] [UPDATE] ACTIVE/INACTIVE CODE ERROR`, error);

            res.status(400).json({ code: 400, error: 'CODE ERROR' })
        }
    }

    Customermanagement.makeActiveOrInactive = (request, response, options, callback) => {

        auth.validateAccessToken(Customermanagement, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                makeActiveOrInactiveInQuickbooksAndDb(request, response, options, callback);
            }
        });
    }


    /** Native Mysql Query for Customer Specific Route Rate and Customer accessories 
   * Required Input @param params */
    const getCustomerAndAccessoriesRateQuery = (params) => {
        return {
            "query": "call get_routes_accessories(?)",
            "params": params
        };
    }

    /** to fetch the data from db and process it to send to client */
    const preProcessTheRetrievedData = (customer_id, response, options, callback) => {

        try {
            queryParams = getCustomerAndAccessoriesRateQuery([customer_id]);

            auth.executeQuery(Customermanagement, queryParams.query, queryParams.params, (err, queryResponse) => {

                try {

                    if (err) {
                        console.error(new Date(), "[customer] [FETCH] CUSTOMER AND ACCESSORIES RATES DB ERROR",
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: "DB ERROR" });
                    }

                    let processedResponse = {
                        customerRouteRate: queryResponse[0][0].customer_routes !== null ?
                            _.orderBy(JSON.parse(queryResponse[0][0].customer_routes), 'route_rate_id', 'desc')
                            : [],
                        customerAccessories: queryResponse[0][0].customer_accessories !== null ?
                            _.orderBy(JSON.parse(queryResponse[0][0].customer_accessories), 'acc_charge_id', 'desc')
                            : []
                    }

                    callback(null, { code: 200, data: processedResponse });

                } catch (error) {
                    console.error(new Date(), "[customer] [FETCH] CUSTOMER AND ACCESSORIES RATES CODE ERROR",
                        error);

                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            })
        } catch (e) {
            console.error(new Date(), "[customer] [FETCH] CUSTOMER AND ACCESSORIES RATES CODE ERROR", error);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }

    }

    Customermanagement.getCustomerRateAccessories = (customer_id, response, options, callback) => {

        auth.validateAccessToken(Customermanagement, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }
            options.tokenModel = tokenModel;
            preProcessTheRetrievedData(customer_id, response, options, callback);
        });

    }

    /** GET customer autosuggest @param searchString required */
    const searchCustomerAutosuggestValidation = (searchString, response, options, callback) => {
        let customerParsedData = [];

        client.get("customer_master", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[customer] [FETCH] CUSTOMER AUTOSUGGEST REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' })
                }

                if (cacheData) {
                    customerParsedData = JSON.parse(cacheData);

                    if (options.type === 'dropdown') {
                        customerParsedData = _.orderBy(customerParsedData, 'business_name', 'asc');

                        customerParsedData = customerParsedData.map(temp => {
                            return {
                                business_name: temp.business_name,
                                customer_id: temp.customer_id
                            }
                        })
                    } else {
                        customerParsedData = customerParsedData
                            .filter(temp => temp.is_active && temp.business_name.toString()
                                .toLowerCase()
                                .replace(/[^a-zA-Z0-9]+/ig, "")
                                .indexOf(searchString.toString().toLowerCase()
                                    .replace(/[^a-zA-Z0-9]+/ig, "")) === 0);
                    }

                    return callback(null, customerParsedData);
                }

                getCustomerDataFromDatabase(searchString, response, options, callback);

            } catch (error) {
                console.error(new Date(), `[customer] [FETCH] CUSTOMER AUTOSUGGEST CODE ERROR`, error);

                response.status(400).json({ code: 400, error: 'CODE ERROR' });
            }
        });
    }

    Customermanagement.searchCustomerAutosuggest = (searchString, type, response, options, callback) => {

        auth.validateAccessToken(Customermanagement, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;

                options.type = type;

                options.section = 'autoSuggest';

                searchCustomerAutosuggestValidation(searchString, response, options, callback);
            }
        });

    }

    /** fetch selected customer from database  */
    const fetchSelectedCustomerFromDb = (customer_id, response, options, callback) => {
        try {
            auth.executeQuery(Customermanagement, "select * from customer_master where customer_id = ?", [customer_id],
                (err, queryResponse) => {
                    if (err) {

                        console.error(new Date(), `[customer] [FETCH] SELECTED CUSTOMER QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: "DB ERROR" });
                    }

                    if (queryResponse.length == 1 && queryResponse[0].email) {
                        let email = queryResponse[0].email ? queryResponse[0].email.split(',') : queryResponse[0].email;
                        let point_of_contact = queryResponse[0].point_of_contact ? queryResponse[0].point_of_contact.split(',') :
                            queryResponse[0].point_of_contact;
                        let poc_phone = queryResponse[0].poc_phone ? queryResponse[0].poc_phone.split(',') : queryResponse[0].poc_phone;

                        if (email) {
                            queryResponse[0].contact_details = email.map((emailId, index) => {
                                return {
                                    email: emailId != '' ? emailId : null,
                                    point_of_contact: (point_of_contact && point_of_contact[index] != '' &&
                                        point_of_contact[index] != undefined) ? point_of_contact[index] : null,
                                    poc_phone: (poc_phone && poc_phone[index] != '' &&
                                        poc_phone[index] != undefined) ? poc_phone[index] : null
                                }
                            });
                            delete queryResponse[0].email;
                            delete queryResponse[0].point_of_contact;
                            delete queryResponse[0].poc_phone;
                        } else {
                            queryResponse[0].contact_details = {
                                email: null,
                                point_of_contact: null,
                                poc_phone: null
                            }
                            delete queryResponse[0].email;
                            delete queryResponse[0].point_of_contact;
                            delete queryResponse[0].poc_phone;
                        }
                    }

                    callback(null, queryResponse);
                })
        } catch (e) {
            console.error(new Date(), `[customer] [FETCH] SELECTED CUSTOMER CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    /* main function api to retreive selected customer */
    Customermanagement.getSelectedCustomer = (customer_id, response, options, callback) => {

        /* to validate the user have access */
        auth.validateAccessToken(Customermanagement, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }
            options.tokenModel = tokenModel;
            fetchSelectedCustomerFromDb(customer_id, response, options, callback);
        });

    }

    /** compare both the db data and qb data  */
    const compareCustomerAndQuickbooksData = async (request, response, options, callback) => {
        try {

            let quickbooksConnection = await quickbooks.authenticateQuickbooksConnection(Customermanagement);

            if (quickbooksConnection.code !== 200) {

                return quickbooksConnection.code === 103 ?
                    callback(null, quickbooksConnection) :
                    response.status(quickbooksConnection.code).json(quickbooksConnection);

            }

            let customerData = await fetchCustomersFromDb(response); // fetch customer data from db 

            const URL = "query?query=select * from Customer MAXRESULTS 1000&minorversion=38";

            let quickbooksData = await quickbooks.fetchDataFromQuickbooks(URL);

            if (quickbooksData.code !== 200) {

                return response.status(quickbooksData.code).json(quickbooksData);

            }

            let customerQbData = quickbooksData.data.json.QueryResponse.Customer;

            let quickbookMap = new Map(); // using of map to compare in both array data

            customerQbData.forEach(temp => {
                quickbookMap.set(temp.Id, temp)
            });

            customerData.forEach(temp => {
                temp.isActive = temp.is_active;

                let customer = quickbookMap.get(temp.customer_quickbooks_id);

                if (customer) {
                    temp.is_active = customer['Active'] ? 1 : 0;
                } else {
                    temp.is_active = 0;
                }
            })

            let needToSyncData = customerData.filter(temp => (temp.isActive && !temp.is_active) ||
                (!temp.isActive && temp.is_active)); // only fetch status changed one to update in our db

            needToSyncData.length !== 0 ? syncWithMysqlDb(needToSyncData) : null;

            client.del('customer_master');
            client.del("customer_quickbooks");
            client.setex('customer_master', 3600, JSON.stringify(customerData)); //clear and set the cache to send data to frontend

            getCustomerDetailsCache(request, response, null, callback); // normal get function to processing of data

        } catch (error) {
            console.error(new Date(), `[customer] [FETCH] CUSTOMERS COMPARE CODE ERROR`, error);

            return response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    const syncWithMysqlDb = (customerData) => {
        console.log(new Date(), "[customer] [SYNC] MYSQL DB SYNC STARTED", customerData.length);

        async.mapSeries(customerData, (temp, callback) => {

            auth.executeQuery(Customermanagement, "UPDATE customer_master set is_active = ? where customer_id = ?",
                [temp.is_active, temp.customer_id], (err, queryResponse) => {
                    if (err) {
                        return callback(err);
                    } else {
                        callback(null, "UPDATED SUCCESSFULLY");
                    }
                });
        }, (err, loopResults) => {
            if (err) {
                console.error(new Date(), "[customer] [SYNC] MYSQL DB SYNC ERROR",
                    "sqlMessage" in err ? err.sqlMessage : err);
            } else {
                console.log(new Date(), "[customer] [SYNC] MYSQL DB SYNC COMPLETED",
                    `${loopResults.length} is completed`);
            }
        })
    }


    const fetchCustomersFromDb = (response) => {

        return new Promise((resolve, reject) => {
            return auth.executeQuery(Customermanagement, "select * from customer_master where customer_id <> 0", null,
                (err, queryResponse) => {
                    if (err) {

                        console.error(new Date(), `[customer] [FETCH] CUSTOMERS QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: "DB ERROR" });
                    }

                    resolve(queryResponse ? queryResponse : []);
                })
        }).catch(e => {
            console.error(new Date(), `[customer] [FETCH] CUSTOMERS PROMISE CATCH ERROR`, e);

            return response.status(400).json({ code: 400, error: "CODE ERROR" });
        })

    }

    /* main function api to sync with quickbooks */
    Customermanagement.customerQuickbookSync = (req, response, options, callback) => {

        /* to validate the user have access */
        auth.validateAccessToken(Customermanagement, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                compareCustomerAndQuickbooksData(req, response, options, callback);
            }
        });

    }

    /** compare both the db data and qb data  */
    const fetchQuickbooksCustomerList = async (request, response, options, callback) => {
        try {

            let quickbooksConnection = await quickbooks.authenticateQuickbooksConnection(Customermanagement);

            if (quickbooksConnection.code !== 200) {

                return quickbooksConnection.code === 103 ?
                    callback(null, quickbooksConnection) :
                    response.status(quickbooksConnection.code).json(quickbooksConnection);

            }

            let customerData = await fetchCustomersFromDb(response); // fetch customer data from db 

            const URL = "query?query=select * from Customer MAXRESULTS 1000&minorversion=38";

            let quickbooksData = await quickbooks.fetchDataFromQuickbooks(URL);

            if (quickbooksData.code !== 200) {

                return response.status(quickbooksData.code).json(quickbooksData);

            }

            let customerQbData = quickbooksData.data.json.QueryResponse.Customer; // fetch customer data from qb

            let quickbookMap = new Map(); // using of map to compare in both array data

            customerData.forEach(temp => {
                quickbookMap.set(temp.customer_quickbooks_id, temp)
            });

            customerQbData = customerQbData.filter(temp => {

                let customer = quickbookMap.get(temp.Id);

                return !customer;
            });

            client.setex('customer_quickbooks', 3600, JSON.stringify(customerQbData)); //clear and set the cache to send data to frontend

            quickbooksCustomerCache(request, response, callback); // normal get function to processing of data

        } catch (error) {
            console.error(new Date(), `[customer] [FETCH] CUSTOMERS COMPARE CODE ERROR`, error);

            return response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    const quickbooksCustomerCache = (request, response, callback) => {
        let parsedData = [];
        let finalResponse;

        client.get("customer_quickbooks", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[customer] [FETCH] CUSTOMER QUICKBOOKS REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' })
                }
                if (cacheData) {
                    parsedData = JSON.parse(cacheData);

                    if (request.searchFilter) {

                        parsedData = parsedData.filter(temp => {
                            return temp['DisplayName'] ?
                                temp['DisplayName'].toLowerCase().replace(/ +/g, "")
                                    .includes(request.searchFilterValue.toLowerCase().replace(/ +/g, "")) : false
                        });

                    }

                    finalResponse = {
                        code: 200,
                        error: null,
                        data: formatCustomerQuickbooks(request, parsedData)
                    }

                    return callback(null, finalResponse);
                }

                fetchQuickbooksCustomerList(request, response, null, callback);

            } catch (error) {
                console.error(new Date(), `[customer] [FETCH] CUSTOMER CACHE CODE ERROR`, error);

                response.status(400).json({ code: 400, error: 'CODE ERROR' });
            }
        });
    }

    const formatCustomerQuickbooks = (request, customerParsedData) => {
        let customerData;
        let customerPaginationLength;

        if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {
            customerData = customerParsedData.length !== 0 ?
                _.orderBy(customerParsedData, request.column, request.direction).slice((request.offset - 1), request.limit) : [];
        } else {
            customerData = customerParsedData.length !== 0 ?
                _.orderBy(customerParsedData, 'Id', 'desc').slice((request.offset - 1), request.limit) : [];
        }

        customerPaginationLength = customerParsedData.length;

        return { customerData, customerPaginationLength }
    }

    /* main function api to sync with quickbooks */
    Customermanagement.quickbooksCustomer = (req, response, options, callback) => {

        /* to validate the user have access */
        auth.validateAccessToken(Customermanagement, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            }
            options.tokenModel = tokenModel;
            quickbooksCustomerCache(req, response, callback);
        });

    }

    const postSelectedCustomerFromQuickbooks = (req, response, options, callback) => {
        try {

            let params = [];

            req.customersData.forEach(temp => {
                params.push([temp['DisplayName'], temp['Id']]);
            });

            auth.executeQuery(Customermanagement, "INSERT INTO customer_master(business_name,customer_quickbooks_id) VALUES ?", [params],
                (err, queryResponse) => {
                    if (err) {

                        console.error(new Date(), `[customer] [POST] SELECTED CUSTOMER QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: "DB ERROR" });
                    }

                    client.del("customer_quickbooks");
                    client.del("customer_master");

                    callback(null, { code: 200, data: "Successfully Inserted" });
                })
        } catch (e) {
            console.error(new Date(), `[customer] [POST] SELECTED CUSTOMER CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }


    /* main function api to sync with quickbooks */
    Customermanagement.importQuickbooksCustomer = (req, response, options, callback) => {

        /* to validate the user have access */
        auth.validateAccessToken(Customermanagement, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                postSelectedCustomerFromQuickbooks(req, response, options, callback);
            }
        });

    }
};
