'use strict';

var app = require('../../server/server.js');

var fetchRequest = require('request');

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);

const config = require('./../../server/boot/load-config').getConfig();

module.exports = function (Notification) {

    const executeQuery = (query, params, callback) => {
        Notification.dataSource.connector.query(query, params, callback);
    }

    //Access Token
    const validateAccessToken = (access_token, callback) => {
        Notification.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Notification.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    const getUserNotificationValidation = (userId, response, options, callback) => {
        var reqOptions = {
            uri: `${config.notificationServer}getNotification?userId=${userId}`,
            method: "GET"
        }

        fetchRequest.get(reqOptions, (err, result) => {
            try {
                if (err) {
                    console.error(new Date(), "[notification] [API] ERROR", err);

                    response.status(500).json({ code: 500, error: "NOTIFICATION CONNECTION ERROR" });
                } else {
                    if (result.statusCode < 200 || result.statusCode > 299) {
                        console.error(new Date(), "[notification] [API] RESPONSE ERROR",
                            "body" in result ? result.body : null);

                        return response.status(409).json({ code: 409, error: "NOTIFICATION ERROR" });
                    } else {
                        let data = result && result.body ? JSON.parse(result.body) : [];

                        callback(null, data);
                    }
                }
            } catch (error) {
                console.error(new Date(), "[notification] [FETCH] CODE ERROR", error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        })
    }

    Notification.getUserNotification = (userId, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getUserNotificationValidation(userId, response, options, callback);
            }
        });

    }

    const updateUserNotificationValidation = (userId, response, options, callback) => {
        var reqOptions = {
            uri: `${config.notificationServer}updateNotification?userId=${userId}`,
            method: "PUT",
            json: null
        }
        fetchRequest.put(reqOptions, (err, result) => {
            try {
                if (err) {
                    console.error(new Date(), "[notification] [API] ERROR", err);

                    response.status(500).json({ code: 500, error: "NOTIFICATION CONNECTION ERROR" });
                } else {
                    if (result.statusCode < 200 || result.statusCode > 299) {
                        console.error(new Date(), "[notification] [API] RESPONSE ERROR",
                            "body" in result ? result.body : null);

                        return response.status(409).json({ code: 409, error: "NOTIFICATION ERROR" });
                    } else {
                        callback(null, { code: 200, data: "successfully updated" });
                    }
                }
            } catch (error) {
                console.error(new Date(), "[notification] [FETCH] CODE ERROR", error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        })
    }

    Notification.updateUserNotification = (userId, response, options, callback) => {
        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                updateUserNotificationValidation(userId, response, options, callback);
            }
        });
    }


    const getUserRoleListFromDb = (request) => {
        try {
            executeQuery("SELECT user_id,`role` from user_master", null, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), "[notification] [FETCH] USERS LIST QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);
                } else {
                    queryResponse = queryResponse.map(element => {
                        let object = Object.assign({}, element);
                        object.msg_content = '';
                        object.is_viewed = false;
                        object.date_time = '';
                        return object
                    });
                    client.setex("user_notify", 3600, JSON.stringify(queryResponse));
                    formatUserNotifyFromCache(request);
                }
            })
        } catch (e) {
            console.error(new Date(), "[notification] [FETCH] USERS LIST CODE ERROR", e);
        }
    }

    const formatUserNotifyFromCache = (req) => {
        let parsedData;
        let formatNotification = {};
        client.get("user_notify", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), "[notification] [FETCH] USERS LIST REDIS ERROR", err);
                } else {
                    if (cacheData) {

                        parsedData = JSON.parse(cacheData);

                        parsedData = parsedData.filter(temp => temp.user_id !== req.userId);

                        if (req.isNewUserCreated) {
                            parsedData = parsedData.filter(temp => temp.user_id !== req.newUserId);
                        }

                        if (req.isMultiple) {
                            parsedData = parsedData.filter(temp => req.multipleRole.includes(temp.role))
                        } else if (!req.isMultiple && req.isDriver) {
                            parsedData = parsedData.filter(temp => temp.user_id === req.driverUserId)
                        } else {
                            parsedData = parsedData.filter(temp => temp.role == req.role)
                        }

                        formatNotification['notification'] = parsedData;

                        formatNotification['notification'].forEach(element => {
                            element.hyperlink = req.hyperlink && typeof req.hyperlink !== 'string' ?
                                req.hyperlink[element.role] : req.hyperlink;
                            delete element.role;
                            element.date_time = new Date().toISOString();
                            element.msg_content = req.msg;
                            element.icon = req.icon;
                            element.styleClass = req.styleClass;
                        });

                        formatNotification["users"] = formatNotification['notification'].map(temp => temp.user_id);

                        if (formatNotification['notification'].length !== 0) {
                            let options = {
                                uri: `${config.notificationServer}postNotification`,
                                method: "POST",
                                json: formatNotification['notification']
                            }

                            fetchRequest.post(options, (error, result) => {
                                if (error) {
                                    console.error(new Date(), '[notification] [API] CONNECT ERROR', error);
                                } else {
                                    if (result.statusCode < 200 || result.statusCode > 299) {
                                        console.error(new Date(), `[notification] [API] RESPONSE ERROR`,
                                            'body' in result ? result.body : null);
                                    } else {
                                        console.log(new Date(), '[notification] POSTED SUCCESSFULLY');
                                    }
                                    for (const s of app.sockets) {
                                        if (formatNotification["users"].includes(s.id)) {
                                            s.emit('notification', { notification: true });
                                        }
                                    }
                                }
                            })
                        }

                    } else {
                        getUserRoleListFromDb(req);
                    }
                }
            } catch (error) {
                console.error(new Date(), "[notification] [FETCH] USERS LIST CACHE CODE ERROR", error);
            }
        })
    }

    app.eventEmitter.on('postNotification', (req) => {
        formatUserNotifyFromCache(req);
    })

};
