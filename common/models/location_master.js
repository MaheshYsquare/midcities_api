'use strict';

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const _ = require('lodash');
const moment = require('moment');

module.exports = function (Locationmaster) {

    //Datasource connector
    Locationmaster.executeQuery = function (ds, query, params, callback) {
        ds.connector.query(query, params, callback);
    }

    //Access Token
    Locationmaster.validateAccessToken = function (access_token, callback) {
        this.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Locationmaster.createOptionsFromRemotingContext = function (ctx) {
        let base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    //get location management
    Locationmaster.getLocationManagementQuery = function (params) {
        return {
            "query": `SELECT 
                          lm.location_id,
                          lm.location_name,
                          lm.location_type_id,
                          lm.address,
                          lm.city,
                          lm.state,
                          lm.street,
                          lm.street_number,
                          lm.postal_code,
                          lm.phone_number,
                          lm.receiving_hours_from,
                          lm.receiving_hours_to,
                          lm.email,
                          ltm.location_type
                      FROM
                          location_master lm
                      LEFT JOIN location_type_master ltm ON
                          lm.location_type_id = ltm.location_type_id;`,
            "params": params
        };
    }

    Locationmaster.getLocationManagementDataValidation = function (request, response, options, callback) {
        let ds = Locationmaster.dataSource;
        let queryParams;
        try {
            queryParams = Locationmaster.getLocationManagementQuery();
            Locationmaster.executeQuery(ds, queryParams.query, queryParams.params,
                function (err, queryResponse) {
                    if (err) {
                        return response.status(409).json({ code: 409, error: "DB ERROR" });
                    } else {
                        client.setex("location_master", 3600, JSON.stringify(queryResponse));
                        Locationmaster.getLocationManagementCache(request, response, options, callback);
                    }
                })
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Locationmaster.getLocationManagementCache = function (request, response, options, callback) {
        let locationParsedData = [];
        let redisResponse;

        client.get("location_master", (err, cacheData) => {
            try {
                if (err) {
                    return response.status(409).json({ code: 409, error: "REDIS ERROR" });
                } else {
                    if (cacheData) {
                        locationParsedData = JSON.parse(cacheData);

                        if (request.isHubLocation) {
                            locationParsedData = locationParsedData.length !== 0 ?
                                locationParsedData.filter(temp => temp.location_type === 'Hub') : []
                        }

                        if (request.columnFilter) {

                            let receivingHoursFrom;
                            let receivingHoursTo;

                            Object.keys(request.columnFilterValue).forEach(element => {

                                request.columnFilterValue[element] = element === 'receiving_hours' ? (
                                    (request.columnFilterValue[element] !== '' && request.columnFilterValue[element] !== null) ?
                                        moment(request.columnFilterValue[element]).format('HH:mm')
                                        : ''
                                ) : request.columnFilterValue[element]
                                    .toLowerCase().replace(/[^a-zA-Z0-9@]+/ig, "");

                                locationParsedData = locationParsedData.length !== 0 ?
                                    locationParsedData.filter(temp => {

                                        receivingHoursFrom = temp.receiving_hours_from !== null ?
                                            moment(temp.receiving_hours_from).format('HH:mm') : null;

                                        receivingHoursTo = temp.receiving_hours_to !== null ?
                                            moment(temp.receiving_hours_to).format('HH:mm') : null;

                                        return request.columnFilterValue[element] !== "" ?
                                            (
                                                (element === 'location_type' || element === 'address') ?
                                                    (
                                                        temp[element] !== null ?
                                                            temp[element].toLowerCase()
                                                                .replace(/[^a-zA-Z0-9]+/ig, "")
                                                                .includes(request.columnFilterValue[element])
                                                            : false
                                                    ) :
                                                    element === 'receiving_hours' ?
                                                        (
                                                            (temp.receiving_hours_from !== null &&
                                                                temp.receiving_hours_to !== null) ?
                                                                request.columnFilterValue[element] >= receivingHoursFrom &&
                                                                request.columnFilterValue[element] <= receivingHoursTo
                                                                : false
                                                        )
                                                        : (
                                                            temp[element] !== null ?
                                                                temp[element].toLowerCase()
                                                                    .replace(/[^a-zA-Z0-9@]+/ig, "")
                                                                    .indexOf(request.columnFilterValue[element]) === 0
                                                                : false
                                                        )
                                            ) : true
                                    }) : []
                            });
                        }

                        if (request.searchFilter) {
                            locationParsedData = locationParsedData.length !== 0 ?
                                locationParsedData.filter(temp => {
                                    return temp.location_type.toLowerCase().replace(/[^a-zA-Z0-9]+/g, "").indexOf(request.searchFilterValue) === 0 ||
                                        temp.location_name.toLowerCase().replace(/[^a-zA-Z0-9]+/g, "").indexOf(request.searchFilterValue) === 0 ||
                                        temp.address.toLowerCase().replace(/[^a-zA-Z0-9]+/g, "").includes(request.searchFilterValue) ||
                                        (temp.phone_number != null ? temp.phone_number.toLowerCase().replace(/[^0-9]+/ig, "").indexOf(request.searchFilterValue) === 0 : false)
                                }) : [];
                        }

                        redisResponse = {
                            code: 200,
                            error: null,
                            data: formatLocationData(request, locationParsedData)
                        }

                        callback(null, redisResponse);
                    } else {
                        Locationmaster.getLocationManagementDataValidation(request, response, options, callback);
                    }
                }
            } catch (error) {
                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        });
    }

    const formatLocationData = (request, locationParsedData) => {
        let locationData;
        let locationPaginationLength;

        if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {
            locationData = locationParsedData.length !== 0 ? _.orderBy(locationParsedData, request.column, request.direction).slice((request.offset - 1), request.limit) : [];
        } else {
            locationData = locationParsedData.length !== 0 ? _.orderBy(locationParsedData, 'location_id', 'desc').slice((request.offset - 1), request.limit) : [];
        }

        locationPaginationLength = locationParsedData.length;

        return { locationData, locationPaginationLength }
    }

    Locationmaster.getLocationManagementData = function (request, response, options, callback) {
        let self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getLocationManagementCache(request, response, options, callback);
            }
        });
    }

    //post location management data
    Locationmaster.postLocationManagementQuery = function (params) {
        return {
            "query": `INSERT 
                          INTO
                          location_master (location_type_id,
                          location_name,
                          address,
                          street,
                          city,
                          postal_code,
                          state,
                          phone_number,
                          receiving_hours_from,
                          receiving_hours_to,
                          email)
                      VALUES (?,UCASE(?),?,?,?,?,?,?,?,?,?);`,
            "params": params
        };
    }

    Locationmaster.postLocationManagementValidation = function (data, response, options, callback) {
        let ds = Locationmaster.dataSource;
        let queryParams;
        try {
            let canceldata = JSON.stringify(data);
            let parsedData = JSON.parse(canceldata);
            let location_name = parsedData.location_name;
            let location_type_id = parsedData.location_type_id;
            let address = parsedData.address;
            let street = parsedData.street;
            let city = parsedData.city;
            let postal_code = parsedData.postal_code;
            let state = parsedData.state;
            let phone_number = parsedData.phone_number;
            let receiving_hours_from = parsedData.receiving_hours_from;
            let receiving_hours_to = parsedData.receiving_hours_to;
            let email = parsedData.email;

            queryParams = Locationmaster.postLocationManagementQuery([location_type_id, location_name,
                address, street, city, postal_code, state, phone_number, receiving_hours_from, receiving_hours_to,
                email]);

            Locationmaster.executeQuery(ds, queryParams.query, queryParams.params,
                function (err, queryResponse) {
                    if (err) {
                        if (('code' in err) && err['code'] == 'ER_DUP_ENTRY') {
                            callback(null, { code: 422, error: 'Location Name Already Exists' })
                        } else {
                            response.status(409).json({ code: 409, error: "DB ERROR" });
                        }
                    } else {
                        client.del('location_master');
                        callback(null, { code: 200, data: "location created successfully" })
                    }
                });
        }
        catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Locationmaster.postLocationManagement = function (data, response, options, callback) {
        let self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.postLocationManagementValidation(data, response, options, callback);
            }
        });
    }

    //update location management data
    Locationmaster.updateLocationManagementQuery = function (params) {
        return {
            "query": `UPDATE
                          location_master
                      SET
                          location_type_id = ?,
                          location_name = UCASE(?),
                          address = ?,
                          street = ?,
                          city = ?,
                          postal_code = ?,
                          state = ?,
                          phone_number = ?,
                          receiving_hours_from = ?,
                          receiving_hours_to = ?,
                          email = ?
                      WHERE
                          location_id = ?`,
            "params": params
        };
    }

    Locationmaster.updateLocationManagementValidation = function (data, response, options, callback) {
        let ds = Locationmaster.dataSource;
        let queryParams;
        try {

            let canceldata = JSON.stringify(data);
            let parsedData = JSON.parse(canceldata);
            let location_name = parsedData.location_name;
            let location_type_id = parsedData.location_type_id;
            let address = parsedData.address;
            let street = parsedData.street;
            let city = parsedData.city;
            let postal_code = parsedData.postal_code;
            let state = parsedData.state;
            let phone_number = parsedData.phone_number;
            let location_id = parsedData.location_id;
            let receiving_hours_from = parsedData.receiving_hours_from;
            let receiving_hours_to = parsedData.receiving_hours_to;
            let email = parsedData.email;

            queryParams = Locationmaster.updateLocationManagementQuery([location_type_id, location_name,
                address, street, city, postal_code, state, phone_number, receiving_hours_from,
                receiving_hours_to, email, location_id]);

            Locationmaster.executeQuery(ds, queryParams.query, queryParams.params,
                function (err, queryResponse) {
                    if (err) {
                        if (('code' in err) && err['code'] == 'ER_DUP_ENTRY') {
                            callback(null, { code: 422, error: 'Location Name Already Exists' })
                        } else {
                            console.error(new Date(), "[location] [UPDATE] LOCATION DATA QUERY ERROR",
                                "sqlMessage" in err ? err.sqlMessage : err);

                            response.status(409).json({ code: 409, error: "DB ERROR" });
                        }
                    } else {
                        client.del('location_master');
                        callback(null, { code: 200, data: "location updated successfully" })
                    }
                });
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Locationmaster.updateLocationManagement = function (data, response, options, callback) {
        let self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.updateLocationManagementValidation(data, response, options, callback);
            }
        });
    }

    //delete location management data
    Locationmaster.deleteLocationManagementQueryParams = function (params) {
        return {
            "query": "delete from location_master where location_id=?",
            "params": params
        };
    }

    Locationmaster.deleteLocationManagementValidation = function (location_id, response, options, callback) {
        let ds = Locationmaster.dataSource;
        let queryParams;
        try {
            queryParams = Locationmaster.deleteLocationManagementQueryParams([location_id]);
            Locationmaster.executeQuery(ds, queryParams.query, queryParams.params,
                function (err, queryResponse) {
                    if (err) {
                        if (('code' in err) && err['code'] == 'ER_ROW_IS_REFERENCED_2') {
                            callback(null, { code: 409, error: 'Location is Referenced' })
                        } else {
                            response.status(409).json({ code: 409, error: "DB ERROR" });
                        }
                    } else {
                        client.del('location_master');
                        callback(null, { code: 200, data: "location successfully deleted" })
                    }
                })
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Locationmaster.deleteLocationManagement = function (location_id, response, options, callback) {
        let self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.deleteLocationManagementValidation(location_id, response, options, callback);
            }
        });
    }

    const searchLocation = (searchString, searchBy, response, options, callback) => {
        let locationData = []

        client.get("location_master", (err, cacheData) => {
            try {
                if (err) {
                    return response.status(409).json({ code: 409, error: "REDIS ERROR" });
                } else {
                    if (cacheData) {

                        locationData = JSON.parse(cacheData);

                        locationData = locationData.length !== 0 ?
                            locationData.filter(temp => {
                                return searchBy == 'name' ?
                                    (
                                        temp.location_name.toLowerCase()
                                            .replace(/[^a-zA-Z0-9]+/g, '')
                                            .indexOf(searchString.toLowerCase()
                                                .replace(/[^a-zA-Z0-9]+/g, '')) === 0
                                    ) :
                                    (
                                        temp.address.toLowerCase()
                                            .replace(/[^a-zA-Z0-9]+/g, '')
                                            .includes(searchString.toLowerCase()
                                                .replace(/[^a-zA-Z0-9]+/g, ''))
                                    )
                            }) : [];

                        callback(null, locationData);
                    } else {
                        setDataInRedis(searchString, searchBy, response, options, callback)
                    }
                }
            } catch (error) {
                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }

        })
    }

    const setDataInRedis = (searchString, searchBy, response, options, callback) => {
        let ds = Locationmaster.dataSource;
        let queryParams;
        try {
            queryParams = Locationmaster.getLocationManagementQuery();
            Locationmaster.executeQuery(ds, queryParams.query, queryParams.params,
                function (err, queryResponse) {
                    if (err) {
                        return response.status(409).json({ code: 409, error: "DB ERROR" });
                    } else {
                        client.setex("location_master", 3600, JSON.stringify(queryResponse));
                        searchLocation(searchString, searchBy, response, options, callback);
                    }
                })
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Locationmaster.locationAutoSuggest = function (searchString, searchBy, response, options, callback) {
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                searchLocation(searchString, searchBy, response, options, callback);
            }
        });
    }
};
