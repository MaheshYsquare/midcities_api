'use strict';

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const _ = require('lodash')

module.exports = function (Accessorialchargemaster) {

    //Datasource connector
    Accessorialchargemaster.executeQuery = function (ds, query, params, callback) {
        ds.connector.query(query, params, callback);
    }

    //Access Token
    Accessorialchargemaster.validateAccessToken = function (access_token, callback) {
        this.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Accessorialchargemaster.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    //Get general Accessories rate
    Accessorialchargemaster.getGeneralAccessoriesRateValidation = function (request, response, options, callback) {
        try {
            Accessorialchargemaster.find({ where: { customer_id: 0 } }, function (err, queryResponse) {
                if (err) {
                    console.error(new Date(), `[accessorial_charge] [FETCH] GENERAL ACCESSORIAL RATE QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {
                    client.setex("general_accessories_rates", 3600, JSON.stringify(queryResponse));
                    Accessorialchargemaster.getGeneralAccessoriesRateCache(request, response, options, callback);
                }
            });
        } catch (e) {
            console.error(new Date(), `[accessorial_charge] [FETCH] GENERAL ACCESSORIAL RATE CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Accessorialchargemaster.getGeneralAccessoriesRateCache = function (request, response, options, callback) {
        let parsedData = [];
        let charges = ["Driver Wait Time", "Regular 20' Chassis Fee", "Regular 40' Chassis Fee",
            "Triaxle Fee", "Chassis Positions", "Yardpulls", "Yard Storage", "Scale Charges", "HAZMAT Surcharge",
            "Overweight", "Reefer Surcharge", "High Value Surcharge", "Chassis Rental", "Fuel Surcharge"];
        let redisResponse;

        client.get("general_accessories_rates", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[accessorial_charge] [FETCH] GENERAL ACCESSORIAL RATE REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: "REDIS ERROR" });
                } else {
                    if (cacheData) {
                        parsedData = JSON.parse(cacheData);

                        charges = charges.filter(temp => parsedData.some(elem => elem.accessories_name === temp));

                        if (request.columnFilter) {
                            Object.keys(request.columnFilterValue).forEach(element => {
                                request.columnFilterValue[element] = request.columnFilterValue[element]
                                    .toLowerCase().replace(/[^a-zA-Z0-9.\%]+/ig, "")

                                parsedData = parsedData.length !== 0 ?
                                    parsedData.filter(temp => {
                                        return request.columnFilterValue[element] !== "" ?
                                            (
                                                element === 'accessories_interval' ?
                                                    (
                                                        temp[element] !== null ?
                                                            temp[element].toString().toLowerCase()
                                                                .replace(/[^a-zA-Z0-9\%]+/ig, "")
                                                                .includes(request.columnFilterValue[element])
                                                            : false
                                                    ) :
                                                    (
                                                        temp[element] !== null ?
                                                            temp[element].toString().toLowerCase()
                                                                .replace(/[^a-zA-Z0-9.]+/ig, "")
                                                                .indexOf(request.columnFilterValue[element]) === 0
                                                            : false
                                                    )
                                            ) : true
                                    }) : []
                            });
                        }

                        redisResponse = {
                            code: 200,
                            error: null,
                            data: formatGeneralAccessoriesData(request, parsedData),
                            disableCharges: charges
                        }

                        callback(null, redisResponse)
                    } else {
                        Accessorialchargemaster.getGeneralAccessoriesRateValidation(request, response, options, callback);
                    }
                }
            } catch (error) {
                console.error(new Date(), `[accessorial_charge] [FETCH] GENERAL ACCESSORIAL RATE CACHE CODE ERROR`, error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        });
    }

    const formatGeneralAccessoriesData = (request, generalAccessoriesParsedData) => {
        let generalAccessoriesData;
        let generalAccessoriesPaginationLength;

        if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {
            generalAccessoriesData = generalAccessoriesParsedData.length !== 0 ?
                _.orderBy(generalAccessoriesParsedData, request.column, request.direction)
                    .slice((request.offset - 1), request.limit) : [];
        } else {
            generalAccessoriesData = generalAccessoriesParsedData.length !== 0 ?
                _.orderBy(generalAccessoriesParsedData, 'acc_charge_id', 'desc')
                    .slice((request.offset - 1), request.limit) : [];
        }

        generalAccessoriesPaginationLength = generalAccessoriesParsedData.length;

        return { generalAccessoriesData, generalAccessoriesPaginationLength }
    }

    Accessorialchargemaster.getGeneralAccessoriesRates = function (request, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getGeneralAccessoriesRateCache(request, response, options, callback);
            }
        });
    }

    //Get general Accessories names
    Accessorialchargemaster.getAccessoriesNamesQuery = function () {
        return {
            "query": "select distinct accessories_name from accessorial_charge_master WHERE customer_id = 0;"
        };
    }

    Accessorialchargemaster.getAccessoriesNamesValidation = function (response, options, callback) {
        var ds = Accessorialchargemaster.dataSource;
        var queryParams;
        try {
            queryParams = Accessorialchargemaster.getAccessoriesNamesQuery();
            Accessorialchargemaster.executeQuery(ds, queryParams.query, null, function (err, queryResponse) {
                try {
                    if (err) {
                        console.error(new Date(), `[accessorial_charge] [FETCH] ACCESSORIAL NAMES QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: "DB ERROR" });
                    } else {
                        queryResponse = queryResponse.map(temp => temp.accessories_name);

                        callback(null, queryResponse)
                    }
                } catch (error) {
                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            })
        } catch (e) {
            console.error(new Date(), `[accessorial_charge] [FETCH] ACCESSORIAL NAMES CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }

    }

    Accessorialchargemaster.getAccessoriesNames = function (response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getAccessoriesNamesValidation(response, options, callback);
            }
        });
    }

    //Get accessory charges
    Accessorialchargemaster.getAccessoriesChargesQuery = function (params) {
        return {
            "query": "SELECT * FROM accessorial_charge_master WHERE (customer_id = ? or customer_id = 0)",
            "params": params
        };
    }

    Accessorialchargemaster.getAccessoriesChargesValidation = function (customer_id, response, options, callback) {
        var ds = Accessorialchargemaster.dataSource;
        var queryParams;
        try {
            queryParams = Accessorialchargemaster.getAccessoriesChargesQuery([customer_id]);
            Accessorialchargemaster.executeQuery(ds, queryParams.query, queryParams.params, function (err, queryResponse) {
                if (err) {
                    console.error(new Date(), `[accessorial_charge] [FETCH] ACCESSORIAL CHARGES QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {
                    callback(null, queryResponse);
                }
            })
        } catch (e) {
            console.error(new Date(), `[accessorial_charge] [FETCH] ACCESSORIAL CHARGES CODE ERROR`, e);
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Accessorialchargemaster.getAccessoriesCharges = function (customer_id, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getAccessoriesChargesValidation(customer_id, response, options, callback);
            }
        });
    }

    //post general accessories rates
    Accessorialchargemaster.postGeneralAccessoriesRatesQuery = function (params) {
        return {
            "query": "INSERT INTO accessorial_charge_master (accessories_name, accessories_interval, accessories_value, rate, description) VALUES (?,?,?,?,?)",
            "params": params
        };
    }

    Accessorialchargemaster.postGeneralAccessoriesRatesValidation = function (data, response, options, callback) {
        var ds = Accessorialchargemaster.dataSource;
        var queryParams;

        try {

            var canceldata = JSON.stringify(data);
            var parsedData = JSON.parse(canceldata);
            var accessories_name = parsedData.accessories_name;
            var accessories_interval = parsedData.accessories_interval;
            var accessories_value = parsedData.accessories_value;
            var description = parsedData.description;
            var rate = parsedData.rate;

            queryParams = Accessorialchargemaster.postGeneralAccessoriesRatesQuery([accessories_name,
                accessories_interval, accessories_value, rate, description]);

            Accessorialchargemaster.executeQuery(ds, queryParams.query, queryParams.params, function (err, queryResponse) {
                if (err) {
                    if (('code' in err) && err['code'] == 'ER_DUP_ENTRY') {
                        callback(null, { code: 422, error: 'Accessories Already Exists' });
                    } else {
                        console.error(new Date(), `[accessorial_charge] [POST] ACCESSORIAL RATE QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: "DB ERROR" });
                    }
                } else {
                    client.del('general_accessories_rates');
                    callback(null, { code: 200, data: "Accessories created successfully" });
                }
            });
        }
        catch (e) {
            console.error(new Date(), `[accessorial_charge] [POST] ACCESSORIAL RATE CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Accessorialchargemaster.postGeneralAccessoriesRates = function (data, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.postGeneralAccessoriesRatesValidation(data, response, options, callback);
            }
        });
    }

    //update general accessories rates
    Accessorialchargemaster.updateChassisManagementQuery = function (params) {
        return {
            "query": "UPDATE accessorial_charge_master SET accessories_name=?, accessories_interval=?, accessories_value=?, rate=?, description=? WHERE acc_charge_id=? and customer_id=?",
            "params": params
        };
    }

    Accessorialchargemaster.updateGeneralAccessoriesRatesValidation = function (data, response, options, callback) {
        var ds = Accessorialchargemaster.dataSource;
        var queryParams;

        try {

            var canceldata = JSON.stringify(data);
            var parsedData = JSON.parse(canceldata);
            var accessories_name = parsedData.accessories_name;
            var accessories_interval = parsedData.accessories_interval;
            var accessories_value = parsedData.accessories_value;
            var description = parsedData.description;
            var rate = parsedData.rate;
            var customer_id = parsedData.customer_id;
            var acc_charge_id = parsedData.acc_charge_id;

            queryParams = Accessorialchargemaster.updateChassisManagementQuery([accessories_name,
                accessories_interval, accessories_value, rate, description, acc_charge_id, customer_id]);

            Accessorialchargemaster.executeQuery(ds, queryParams.query, queryParams.params, function (err, queryResponse) {
                if (err) {
                    if (('code' in err) && err['code'] == 'ER_DUP_ENTRY') {
                        callback(null, { code: 422, error: 'Accessories Already Exists' })
                    } else {
                        console.error(new Date(), `[accessorial_charge] [UPDATE] ACCESSORIAL RATE QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: "DB ERROR" });
                    }
                } else {
                    client.del('general_accessories_rates');
                    callback(null, { code: 200, data: "accessories updated successfully" })
                }
            });
        } catch (e) {
            console.error(new Date(), `[accessorial_charge] [UPDATE] ACCESSORIAL RATE CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Accessorialchargemaster.updateGeneralAccessoriesRates = function (data, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.updateGeneralAccessoriesRatesValidation(data, response, options, callback);
            }
        });
    }

    //delete general accessories rates
    Accessorialchargemaster.deleteGeneralAccessoriesRatesQuery = function (params) {
        return {
            "query": "DELETE FROM accessorial_charge_master WHERE acc_charge_id=?",
            "params": params
        };
    }

    Accessorialchargemaster.deleteGeneralAccessoriesRatesValidation = function (acc_charge_id, response, options, callback) {
        var ds = Accessorialchargemaster.dataSource;
        var queryParams;
        try {
            queryParams = Accessorialchargemaster.deleteGeneralAccessoriesRatesQuery([acc_charge_id]);
            Accessorialchargemaster.executeQuery(ds, queryParams.query, queryParams.params, function (err, queryResponse) {
                if (err) {
                    console.error(new Date(), `[accessorial_charge] [DELETE] ACCESSORIAL RATE QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {
                    client.del('general_accessories_rates');
                    callback(null, { code: 200, data: "Successfully Deleted" });
                }
            })
        } catch (e) {
            console.error(new Date(), `[accessorial_charge] [DELETE] ACCESSORIAL RATE CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Accessorialchargemaster.deleteGeneralAccessoriesRates = function (acc_charge_id, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.deleteGeneralAccessoriesRatesValidation(acc_charge_id, response, options, callback);
            }
        });
    }
};
