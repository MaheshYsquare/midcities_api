'use strict';

const async = require('async');
const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const config = require('./../../server/boot/load-config').getConfig();

const AWS = require('aws-sdk');
const s3 = new AWS.S3({});
AWS.config.update({
  secretAccessKey: config.awsSecretAccessKey,
  accessKeyId: config.awsAccessKeyId,
  region: config.awsRegion
});
const fetchRequest = require('request');
const app = require('../../server/server.js');
const OAuthClient = require('intuit-oauth');
const moment = require('moment');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const Jimp = require('jimp');


const pdf = require('pdfjs');

module.exports = function (Invoice) {

  let queryParams;

  let quickbooksUrl = app.oauthClient.environment == 'sandbox' ? OAuthClient.environment.sandbox : OAuthClient.environment.production;

  let invoiceAdded;

  //database connector
  const executeQuery = (query, params, callback) => {
    Invoice.dataSource.connector.query(query, params, callback);
  }

  //Access Token
  const validateAccessToken = (access_token, callback) => {
    Invoice.app.models.AccessToken.findById(access_token, callback)
  }

  //calling internally by loopback(overriden)
  Invoice.createOptionsFromRemotingContext = function (ctx) {
    var base = this.base.createOptionsFromRemotingContext(ctx);
    base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
    return base;
  }

  /**
   * Post and update of auto invoice for single order
   * @function postAutoInvoice is used to trigger the remote method requires @argument {request,response,options,callback}
   * @function authenticateQuickbooksConnection is to validate the quickbooks token if not return for connection prompt
   * @function checkPostOrUpdate is to check the invoice already exist or not, if not post or update in quickbooks
   * @function postOrUpdateDataInQuickbooks to post or update data in quickbooks
   * @function postAutoInvoiceQuery,updateAutoInvoiceQuery,checkInvoiceQuery return the mysql native query and its related params
   */
  const postAutoInvoiceQuery = (params) => {
    return {
      "query": "call postQuickbooksDataInInvoiceMgmt(?,?,?,?,?,?,?,?,?)",
      "params": params
    };
  }

  const updateAutoInvoiceQuery = (params) => {
    return {
      "query": `UPDATE
                          invoice_mgmt
                      set
                          total_amount = ?,
                          outstanding = ?,
                          paid = ?,
                          write_off = 0,
                          date = CURDATE(),
                          due_date = ?,
                          lastupdated_by = ?,
                          invoice_quickbooks_sync = ?,
                          lastupdated_on = CURRENT_TIMESTAMP
                      WHERE
                          invoice_id = ?;`,
      "params": params
    };
  }

  const checkInvoiceQuery = (params) => {
    return {
      "query": "SELECT * FROM invoice_mgmt WHERE order_container_chassis_id = ? and order_id = ? and customer_id = ?",
      "params": params
    };
  }

  const authenticateQuickbooksConnection = async (request, response, options, section, callback) => {

    try {
      if (app.oauthClient.token.getToken().access_token) {

        if (app.oauthClient.isAccessTokenValid()) {

          if (section == 'single') {
            checkForPostOrUpdate(request, response, callback)
          } else if (section == 'multiple') {
            multipleCheckForPostOrUpdate(request, response, callback);
          } else if (section == 'delete') {
            deleteInvoiceParsing(request, response, options, callback);
          }
        } else {
          app.oauthClient.refresh()
            .then((authResponse) => {

              app.eventEmitter.emit('quickbooksTokenAdd', authResponse.getToken());

              if (section == 'single') {
                checkForPostOrUpdate(request, response, callback)
              } else if (section == 'multiple') {
                multipleCheckForPostOrUpdate(request, response, callback);
              } else if (section == 'delete') {
                deleteInvoiceParsing(request, response, options, callback);
              }

            })
            .catch((e) => {
              console.log(new Date(), "[invoice_mgmt] QUICKBOOKS CONNECTION NEEDED", e);

              callback(null, { code: 103, error: 'Please Connect To Quickbooks' });
            });
        }

      } else {
        let quickBooksToken = await fetchQuickbooksToken(response);

        if (quickBooksToken.length !== 0) {
          console.log(new Date(), "[invoice_mgmt] QUICKBOOKS RETRIEVED FROM DB");

          app.oauthClient.setToken(quickBooksToken[0]);

          authenticateQuickbooksConnection(request, response, options, section, callback);
        } else {
          console.log(new Date(), "[invoice_mgmt] QUICKBOOKS CONNECTION NEEDED");

          callback(null, { code: 103, error: 'Please Connect To Quickbooks' });
        }
      }
    } catch (e) {
      response.status(400).json({ code: 400, error: 'CODE ERROR' });
    }
  }

  const checkForPostOrUpdate = (request, response, callback) => {
    queryParams = checkInvoiceQuery([request.order_container_chassis_id, request.order_id, request.customer_id]);
    executeQuery(queryParams.query, queryParams.params, (err, checkQueryResponse) => {
      if (err) {
        return response.status(409).json({ code: 409, error: 'CHECK QUERY DB ERROR' });
      } else {
        if (request.customer_quickbooks_id !== null) {
          if (checkQueryResponse.length === 0) {
            postOrUpdateDataInQuickbooks(request, response, null, callback);
          } else {
            postOrUpdateDataInQuickbooks(request, response, checkQueryResponse[0], callback);
          }
        } else {
          callback(null, {
            code: 422,
            error: 'Customer Not Present In Quickbooks'
          })
        }
      }
    });
  }

  const postOrUpdateDataInQuickbooks = async (req, res, checkQueryResponse, callback) => {
    try {
      let itemId = await fetchItemIdFromQuickbooks();

      req.invoiceNumber = await fetchInvoiceNumber();

      if (itemId.code === 200) {
        req.quickbook_line.forEach(temp => {
          temp.SalesItemLineDetail.ItemRef.value = itemId.Id
        })
      } else if (itemId.code === 409) {
        return res.status(409).json(itemId)
      }

      let quickbooksInput = {
        "Line": req.quickbook_line,
        "CustomerRef": {
          "value": req.customer_quickbooks_id
        },
        "DueDate": moment().add(7, 'days').format('YYYY-MM-DD')
      }
      if (checkQueryResponse !== null) {
        let syncToken = await fetchInvoiceSyncTokenFromQuickbooks(checkQueryResponse.invoice_quickbooks_id);

        if (syncToken.code === 200) {
          quickbooksInput['SyncToken'] = syncToken.syncToken
        } else if (syncToken.code === 409) {
          return res.status(409).json(syncToken)
        }
        quickbooksInput['Id'] = checkQueryResponse.invoice_quickbooks_id
        quickbooksInput['sparse'] = true
      } else {
        quickbooksInput['DocNumber'] = req.invoiceNumber;
      }

      let invoiceOptions = {
        uri: `${quickbooksUrl}/v3/company/${app.oauthClient.getToken().realmId}/invoice?minorversion=38`,
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + app.oauthClient.getToken().access_token
        },
        json: quickbooksInput
      }
      fetchRequest(invoiceOptions, async (err, result) => {
        try {

          if (err) {
            return res.status(500).json({ code: 500, error: 'QUICKBOOK API CONNECTION ERROR' });
          }

          if (result.statusCode < 200 || result.statusCode > 299) {

            if (result &&
              ('body' in result)
              && ('Fault' in result.body)
              && result.body.Fault.Error.length
              && 'Message' in result.body.Fault.Error[0]
              && (result.body.Fault.Error[0].Message == 'Invalid Customer'
                || result.body.Fault.Error[0].Message == 'Invalid Reference Id')) {

              console.error(new Date(), "[invoice_mgmt] [QUICKBOOKS] API ERROR",
                result.body.Fault.Error[0]);

              return callback(null, {
                code: 422,
                error: 'Invoice cannot be created for inactive customer'
              })
            }

            if (result &&
              ('body' in result)
              && ('Fault' in result.body)
              && result.body.Fault.Error.length
              && 'Message' in result.body.Fault.Error[0]
              && result.body.Fault.Error[0].Message.includes("Duplicate Document Number Error")) {

              console.error(new Date(), "[invoice_mgmt] [QUICKBOOKS] API ERROR",
                result.body.Fault.Error[0]);

              const updateSequence = await updateSequenceDocNumber();

              if (updateSequence.code !== 200) {
                return res.status(409).json(updateSequence);
              }

              postOrUpdateDataInQuickbooks(req, res, checkQueryResponse, callback);

              return;
            }

            console.error(new Date(), `[invoice_mgmt] [API] QUICKBOOKS API ERROR`,
              result && ('body' in result) && ('Fault' in result.body) &&
                ('Error' in result.body.Fault) && result.body.Fault.Error &&
                result.body.Fault.Error.length !== 0 ? result.body.Fault.Error[0]
                : null);

            return res.status(409).json({ code: 409, error: 'QUICKBOOKS ERROR' })

          }

          if (checkQueryResponse !== null) {

            queryParams = updateAutoInvoiceQuery([result.body.Invoice.TotalAmt,
            result.body.Invoice.TotalAmt,
            (+result.body.Invoice.TotalAmt - +result.body.Invoice.Balance),
            result.body.Invoice.DueDate, req.created_by,
            result.body.Invoice.SyncToken, checkQueryResponse.invoice_id]);

          } else {

            queryParams = postAutoInvoiceQuery([req.invoiceNumber,
            result.body.Invoice.TotalAmt, req.customer_id, result.body.Invoice.DueDate,
            req.created_by, result.body.Invoice.Id, result.body.Invoice.SyncToken,
            req.order_container_chassis_id, req.order_id]);

          }

          executeQuery(queryParams.query, queryParams.params, (postErr, queryResponse) => {
            if (postErr) {
              console.error(new Date(), "[invoice_mgmt] [POST/UPDATE] AUTO INVOICE",
                "sqlMessage" in postErr ? postErr.sqlMessage : postErr);

              res.status(409).json({ code: 409, error: 'DB ERROR' });
            } else {
              client.del("invoiceData");
              client.del('invoice_mgmt');
              client.del('advanced_search');
              callback(null, { code: 200, data: "Invoice Generated Successfully" });
            }
          });

        } catch (error) {
          res.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
      })
    } catch (error) {
      res.status(400).json({ code: 400, error: 'CODE ERROR' });
    }
  }

  const updateSequenceDocNumber = () => {
    return new Promise((resolve, reject) => {
      return executeQuery("UPDATE sequences SET invoice_number = invoice_number + 1 WHERE sequence_name = 'orders';",
        null, (err, checkQueryResponse) => {
          if (err) {
            return resolve({ code: 409, error: "UPDATE SEQUENCE ERROR" });
          }

          resolve({ code: 200, data: "Successfully Updated" });

        })
    })
  }

  Invoice.postAutoInvoice = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {

      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
      } else {
        options.tokenModel = tokenModel;
        authenticateQuickbooksConnection(request, response, options, 'single', callback);
      }

    });

  };

  /**
   * Post or Update multiple invoice generation in quickbooks
   * @function postMultipleAutoInvoice main route check for valid access token and make call to check for quickbooks authorization
   * @function multipleCheckForPostOrUpdate used to fetch rate of leg and accessories and do multiple post or update in quickbooks
   */
  const getLegAccessoriesRateQuery = (params) => {
    return {
      "query": "call getLegAccessoriesRate(?)",
      "params": params
    };
  }

  const multipleCheckForPostOrUpdate = async (request, response, callback) => {
    try {
      let postOrUpdateResponse;
      let legAccessoriesRate;
      let checkInvoice;

      if (request.customer_quickbooks_id !== null) {
        for (const temp of request.containerToBeInvoiced) {
          legAccessoriesRate = await fetchLegAccessoriesRateFromDB(temp.order_container_chassis_id);

          if (legAccessoriesRate.code !== 200) {
            return response.status(409).json(legAccessoriesRate);
          }

          if (legAccessoriesRate.data.length === 0) {
            return response.status(200).json({ code: 422, error: "No Charges In Container" });
          }

          checkInvoice = await checkInvoicePromise(temp.order_container_chassis_id,
            temp.order_id, temp.customer_id);

          if (checkInvoice.code !== 200) {
            return response.status(409).json(checkInvoice);
          } else if (checkInvoice.code === 200) {

            if (checkInvoice.data.length === 0) {
              postOrUpdateResponse = await multiplePostOrUpdateDataInQuickbooks(temp,
                legAccessoriesRate.data, null);
            } else {
              postOrUpdateResponse = await multiplePostOrUpdateDataInQuickbooks(temp,
                legAccessoriesRate.data, checkInvoice.data[0]);
            }

            if (postOrUpdateResponse.code !== 200 && postOrUpdateResponse.code !== 422) {
              return response.status(postOrUpdateResponse.code).json(postOrUpdateResponse);
            } else if (postOrUpdateResponse.code === 422) {
              return callback(null, postOrUpdateResponse);
            }
          }
        }

        client.del("invoiceData");
        client.del('invoice_mgmt');
        client.del('advanced_search');

        callback(null, { code: 200, data: "successfully generated" });
      } else {
        callback(null, {
          code: 422,
          error: 'Customer Not Present In Quickbooks'
        })
      }

    } catch (error) {

      response.status(400).json({ code: 400, error: 'CODE ERROR' });

    }
  }

  const checkInvoicePromise = (containerLevelId, orderLevelId, customerId) => {
    return new Promise((resolve, reject) => {
      queryParams = checkInvoiceQuery([containerLevelId, orderLevelId, customerId]);
      return executeQuery(queryParams.query, queryParams.params, async (err, checkQueryResponse) => {
        if (err) {
          resolve({ code: 409, error: "CHECK QUERY DB ERROR" });
        } else {
          resolve({ code: 200, data: checkQueryResponse });
        }
      })
    })
  }

  const fetchLegAccessoriesRateFromDB = (id) => {
    return new Promise((resolve, reject) => {
      queryParams = getLegAccessoriesRateQuery([id]);
      return executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          resolve({ code: 409, error: 'DB ERROR' })
        } else {
          let LineData = [];
          let legs = [];
          let accessories = [];

          queryResponse[0].forEach(temp => {
            legs = temp['legs'] != null ? JSON.parse(temp['legs']) : [];
            accessories = temp['accessories'] != null ? JSON.parse(temp['accessories']) : [];
          })

          legs.forEach(temp => {
            LineData.push({
              "DetailType": "SalesItemLineDetail",
              "Description": temp.leg_type,
              "Amount": +temp.rate,
              "SalesItemLineDetail": {
                "ItemRef": {
                  "name": "Services",
                  "value": "19"
                }
              }
            })
          })

          accessories.forEach(temp => {
            LineData.push({
              "DetailType": "SalesItemLineDetail",
              "Description": temp.description,
              "Amount": +temp.rate,
              "SalesItemLineDetail": {
                "ItemRef": {
                  "name": "Services",
                  "value": "19"
                }
              }
            })
          })

          resolve({ code: 200, data: LineData })
        }
      });
    });
  }

  const multiplePostOrUpdateDataInQuickbooks = async (temp, quickbook_line, checkQueryResponse) => {
    try {
      let itemId = await fetchItemIdFromQuickbooks();

      temp.invoiceNumber = await fetchInvoiceNumber();

      if (itemId.code === 200) {
        quickbook_line.forEach(elem => {
          elem.SalesItemLineDetail.ItemRef.value = itemId.Id
        })
      } else if (itemId.code === 409) {
        return res.status(409).json(itemId)
      }

      let quickbooksInput = {
        "Line": quickbook_line,
        "CustomerRef": {
          "value": temp.customer_quickbooks_id
        },
        "DueDate": moment().add(7, 'days').format('YYYY-MM-DD')
      }
      if (checkQueryResponse !== null) {
        let syncToken = await fetchInvoiceSyncTokenFromQuickbooks(checkQueryResponse.invoice_quickbooks_id);

        if (syncToken.code === 200) {
          quickbooksInput['SyncToken'] = syncToken.syncToken
        } else if (syncToken.code === 409) {
          return syncToken
        }

        quickbooksInput['Id'] = checkQueryResponse.invoice_quickbooks_id
        quickbooksInput['sparse'] = true
      } else {
        quickbooksInput['DocNumber'] = temp.invoiceNumber;
      }

      let invoiceOptions = {
        uri: `${quickbooksUrl}/v3/company/${app.oauthClient.getToken().realmId}/invoice?minorversion=38`,
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + app.oauthClient.getToken().access_token
        },
        json: quickbooksInput
      }

      return new Promise((resolve, reject) => {
        fetchRequest(invoiceOptions, (err, result) => {
          if (err) {
            resolve({ code: 500, error: 'QUICKBOOK API CONNECTION ERROR' });
          } else {
            if (result.statusCode < 200 || result.statusCode > 299) {

              if (result &&
                ('body' in result)
                && ('Fault' in result.body)
                && result.body.Fault.Error.length
                && 'Message' in result.body.Fault.Error[0]
                && (result.body.Fault.Error[0].Message == 'Invalid Customer'
                  || result.body.Fault.Error[0].Message == 'Invalid Reference Id')) {
                return resolve({
                  code: 422,
                  error: 'Invoice cannot be created for inactive customer'
                })
              }

              console.error(new Date(), `[invoice_mgmt] [API] QUICKBOOKS API ERROR`,
                result && ('body' in result) && ('Fault' in result.body) &&
                  ('Error' in result.body.Fault) && result.body.Fault.Error &&
                  result.body.Fault.Error.length !== 0 ? result.body.Fault.Error[0]
                  : null);

              return resolve({ code: 409, error: 'QUICKBOOKS ERROR' })

            } else {
              if (checkQueryResponse !== null) {

                queryParams = updateAutoInvoiceQuery([result.body.Invoice.TotalAmt, result.body.Invoice.TotalAmt,
                (+result.body.Invoice.TotalAmt - +result.body.Invoice.Balance), result.body.Invoice.DueDate,
                temp.created_by, result.body.Invoice.SyncToken, checkQueryResponse.invoice_id]);

              } else {

                queryParams = postAutoInvoiceQuery([temp.invoiceNumber, result.body.Invoice.TotalAmt,
                temp.customer_id, result.body.Invoice.DueDate, temp.created_by, result.body.Invoice.Id,
                result.body.Invoice.SyncToken, temp.order_container_chassis_id, temp.order_id]);

              }

              return executeQuery(queryParams.query, queryParams.params, (postErr, queryResponse) => {
                if (postErr) {
                  console.error(new Date(), "[invoice_mgmt] [POST/UPDATE] AUTO INVOICE",
                    "sqlMessage" in postErr ? postErr.sqlMessage : postErr);
                  resolve({ code: 409, error: 'DB ERROR' })
                } else {
                  resolve({ code: 200, data: "Invoice Generated Successfully" });
                }
              });
            }
          }
        })
      });
    } catch (error) {
      return { code: 400, error: 'CODE ERROR' };
    }
  }

  const fetchInvoiceNumber = () => {
    return new Promise((resolve, reject) => {
      return executeQuery("select invoice_number from sequences where sequence_name = 'orders';",
        null, (err, queryResponse) => {
          if (err) {
            console.error(new Date(), `[invoice_mgmt] [FETCH] INVOICE NUMBER QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            resolve(Math.floor(Math.random() * 100000) + 1);
          } else {
            resolve(queryResponse[0].length !== 0 ?
              queryResponse[0].invoice_number : Math.floor(Math.random() * 100000) + 1);
          }
        })
    })
  }

  Invoice.postMultipleAutoInvoice = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {

      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
      } else {
        options.tokenModel = tokenModel;
        authenticateQuickbooksConnection(request, response, options, 'multiple', callback);
      }

    });

  };

  //Get invoice management data
  const getInvoiceManagementDataQuery = () => {
    return {
      "query": "CALL getInvoiceManagementData();"
    };
  }

  //Get invoice management data
  const updateInvoiceDataQuery = (params) => {
    return {
      "query": "update ?? set invoice_status = ?,is_archived = ?,paid = ?,total_amount = ?,outstanding = ?,date_of_payment = ? where ?? = ?;",
      "params": params
    };
  }

  const getInvoiceManagementDataParsing = async (request, response, options, callback) => {
    try {
      let invoiceData;
      let cacheInput;

      if (request.isQuickbooks) {
        if (app.oauthClient.token.getToken().access_token) {
          if (app.oauthClient.isAccessTokenValid()) {
            invoiceData = await fetchDataFromDBAndQuickbooks();

            if (invoiceData.code == 200) {
              cacheInput = {
                data: invoiceData.data,
                type: 'quickbooks'
              }
              client.setex('invoice_mgmt', 3600, JSON.stringify(cacheInput));
              getInvoiceManagementDataCache(request, response, options, callback);
            } else {
              return response.status(409).json(invoiceData)
            }

          } else {
            app.oauthClient.refresh()
              .then(async (authResponse) => {
                app.eventEmitter.emit('quickbooksTokenAdd', authResponse.getToken());

                invoiceData = await fetchDataFromDBAndQuickbooks();

                if (invoiceData.code == 200) {
                  cacheInput = {
                    data: invoiceData.data,
                    type: 'quickbooks'
                  }
                  client.setex('invoice_mgmt', 3600, JSON.stringify(cacheInput));
                  getInvoiceManagementDataCache(request, response, options, callback);
                } else {
                  return response.status(409).json(invoiceData)
                }

              })
              .catch((e) => {
                console.log(new Date(), "[invoice_mgmt] QUICKBOOKS CONNECTION NEEDED", e);

                return callback(null, { code: 103, error: 'Please Connect To Quickbooks' });
              });
          }
        } else {
          let quickBooksToken = await fetchQuickbooksToken(response);

          if (quickBooksToken.length !== 0) {
            console.log(new Date(), "[invoice_mgmt] QUICKBOOKS RETRIEVED FROM DB");

            app.oauthClient.setToken(quickBooksToken[0]);

            getInvoiceManagementDataParsing(request, response, options, callback);
          } else {
            console.log(new Date(), "[invoice_mgmt] QUICKBOOKS CONNECTION NEEDED");

            return callback(null, { code: 103, error: 'Please Connect To Quickbooks' });
          }
        }
      } else {
        queryParams = getInvoiceManagementDataQuery();

        executeQuery(queryParams.query, null, (err, queryResponse) => {
          try {
            if (err) {
              response.status(409).json({ code: 409, error: 'DB ERROR' })
            } else {
              queryResponse[0].forEach(temp => {
                temp.quickbook_status = temp.invoice_status;

                if (temp.order_type === 'One Way Empty - Intermodal' ||
                  temp.order_type === 'One Way Loaded - Intermodal') {
                  temp.format_order_type = temp.order_type.split('-')[0]
                    .split(' ').map(elem => elem.charAt(0)).join('');
                }
              })

              invoiceData = queryResponse[0];

              cacheInput = {
                data: invoiceData,
                type: 'mysql'
              }

              client.setex('invoice_mgmt', 3600, JSON.stringify(cacheInput));

              getInvoiceManagementDataCache(request, response, options, callback);
            }
          } catch (error) {
            response.status(400).json({ code: 400, error: 'CODE ERROR' });
          }
        })
      }
    } catch (e) {
      response.status(400).json({ code: 400, error: 'CODE ERROR' });
    }
  }

  const fetchQuickbooksToken = (response) => {
    return new Promise((resolve, reject) => {
      return executeQuery('select * from quickbooks_token_master where id = 1;', null,
        (err, queryResponse) => {
          if (err) {
            return response.status(409).json({ code: 409, error: "QUICKBOOKS TOKEN FETCH ERROR" });
          } else {
            resolve(queryResponse);
          }
        })
    })
  }

  const fetchDataFromDBAndQuickbooks = () => {
    return new Promise((resolve, reject) => {
      queryParams = getInvoiceManagementDataQuery();
      return executeQuery(queryParams.query, null, (err, queryResponse) => {
        if (err) {
          response.status(409).json({ code: 409, error: 'DB ERROR' })
        } else {
          app.oauthClient.makeApiCall({ url: `${quickbooksUrl}v3/company/${app.oauthClient.getToken().realmId}/query?query=select * from Invoice ORDERBY MetaData.CreateTime DESC MAXRESULTS 1000&minorversion=38` })
            .then((result) => {
              if (result.statusCode < 200 || result.statusCode > 299) {
                console.error(new Date(), "[invoice_mgmt] [FETCH] QUICKBOOKS INVOICE FETCH ERROR")
                resolve({ code: 409, error: 'QUICKBOOKS ERROR' });
              } else {
                let quickbooksData = result.json.QueryResponse.Invoice;

                let quickbookMap = new Map();

                quickbooksData.forEach(temp => {
                  quickbookMap.set(temp.Id, temp)
                });

                queryResponse[0].forEach(temp => {
                  let invoiceNumber = quickbookMap.get(temp.invoice_quickbooks_id)
                  if (invoiceNumber) {
                    temp.previousPaid = temp.paid;

                    temp.quickbook_status = (invoiceNumber.TotalAmt === invoiceNumber.Balance) ?
                      'Unpaid' :
                      (invoiceNumber.Balance !== 0 ? 'Partial' : 'Paid');

                    temp.total_amount = invoiceNumber.TotalAmt;
                    temp.outstanding = invoiceNumber.Balance;
                    temp.paid = +invoiceNumber.TotalAmt - +invoiceNumber.Balance;

                    temp.date_of_payment = temp.quickbook_status === 'Paid' ?
                      invoiceNumber.MetaData.LastUpdatedTime : null;

                    temp.is_archived = temp.quickbook_status === 'Paid' ? 1 : 0;

                    temp.invoice_status = (temp.quickbook_status === 'Partial'
                      && temp.previousPaid !== temp.paid) ?
                      'Sync Needed' : temp.invoice_status;
                  } else {
                    temp.quickbook_status = "Not In Quickbooks";
                  }
                })

                let needToSyncData = queryResponse[0].filter(temp => (temp.is_archived === 1
                  && temp.invoice_status !== 'Paid') || temp.invoice_status === 'Sync Needed');

                needToSyncData.length !== 0 ? syncWithMysqlDb(needToSyncData) : null;

                resolve({ code: 200, data: queryResponse[0] });
              }
            })
            .catch((error) => {
              console.error(new Date(), "[invoice_mgmt] [FETCH] QUICKBOOKS INVOICE FETCH ERROR",
                "body" in error ? error.body : error);
              resolve({ code: 409, error: 'QUICKBOOK API CALL ERROR' });
            })
        }
      })
    })
  }

  const getInvoiceManagementDataCache = (request, response, options, callback) => {
    let paidInvoicesParsedData; // variable to store parsed paidInvoices data
    let unpaidInvoicesParsedData; // variable to store parsed unpaidInvoices data
    let parsedData;
    let redisResponse;

    /* to get the data from redis db if no data present, set the data to redis db*/
    client.get('invoice_mgmt', async (err, cacheData) => {
      try {
        if (err) {
          return response.status(409).json({ code: 409, error: 'REDIS ERROR' });
        } else {
          if (cacheData) {
            parsedData = JSON.parse(cacheData).data;

            if (request.isUniqueByCustomer) {
              parsedData = parsedData.length !== 0 ?
                parsedData.filter(temp => temp.customer_id == request.customerId) : [];

              if (request.columnFilter) {
                parsedData = filterInvoiceData(parsedData, request.columnFilterValue, request.timeZone, response);
              }

              redisResponse = {
                code: 200,
                error: null,
                data: formatInvoiceData(parsedData, request.offset,
                  request.limit, request.direction, request.column)
              }

              callback(null, redisResponse);
            } else {
              paidInvoicesParsedData = parsedData.length !== 0 ?
                parsedData.filter(temp => {
                  return ('quickbook_status' in temp) ?
                    (
                      (temp.quickbook_status !== 'Not In Quickbooks'
                        && temp.quickbook_status !== 'Sync Needed') ?
                        temp.quickbook_status == 'Paid' : temp.outstanding <= 0
                    ) : temp.outstanding <= 0;
                }) : []

              unpaidInvoicesParsedData = parsedData.length !== 0 ?
                parsedData.filter(temp => {
                  return ('quickbook_status' in temp) ?
                    (
                      (temp.quickbook_status !== 'Not In Quickbooks'
                        && temp.quickbook_status !== 'Sync Needed') ?
                        (temp.quickbook_status == 'Unpaid' ||
                          temp.quickbook_status == 'Partial') : temp.outstanding > 0
                    ) : temp.outstanding > 0;
                }) : []

              if (request.paidColumnFilter) {
                paidInvoicesParsedData = filterInvoiceData(paidInvoicesParsedData,
                  request.paidColumnFilterValue, request.timeZone, response);
              }

              if (request.unpaidColumnFilter) {
                unpaidInvoicesParsedData = filterInvoiceData(unpaidInvoicesParsedData,
                  request.unpaidColumnFilterValue, request.timeZone, response);
              }

              let paidInvoices = formatInvoiceData(paidInvoicesParsedData,
                request.paidOffset, request.paidLimit, request.paidDirection,
                request.paidColumn);

              let unpaidInvoices = formatInvoiceData(unpaidInvoicesParsedData,
                request.unpaidOffset, request.unpaidLimit, request.unpaidDirection,
                request.unpaidColumn);

              if (request.isQuickbooks) {

                if (app.oauthClient.token.getToken().access_token) {
                  if (request.table === 'paid') {
                    let paidSync = paidInvoices.invoiceData.length !== 0 ?
                      await fetchPageWiseQbData(paidInvoices.invoiceData.map(temp => temp.invoice_quickbooks_id).join("','"), paidInvoices.invoiceData) :
                      paidInvoices.invoiceData;

                    paidInvoices.invoiceData = paidSync.code === 200 ? paidSync.data : [];
                  } else if (request.table === 'unpaid') {
                    let unpaidSync = unpaidInvoices.invoiceData.length !== 0 ?
                      await fetchPageWiseQbData(unpaidInvoices.invoiceData.map(temp => temp.invoice_quickbooks_id).join("','"), unpaidInvoices.invoiceData) :
                      unpaidInvoices.invoiceData;

                    unpaidInvoices.invoiceData = unpaidSync.code === 200 ? unpaidSync.data : [];
                  }
                } else {
                  return getInvoiceManagementDataParsing(request, response, options, callback);
                }

              }

              redisResponse = {
                paidInvoices,
                unpaidInvoices
              }

              callback(null, { code: 200, error: null, data: redisResponse });
            }
          } else {
            getInvoiceManagementDataParsing(request, response, options, callback);
          }
        }
      } catch (error) {
        response.status(400).json({ code: 400, error: "CODE ERROR" });
      }
    });
  }

  const fetchPageWiseQbData = (ids, formatedData) => {

    return new Promise((resolve, reject) => {
      return app.oauthClient.makeApiCall({ url: `${quickbooksUrl}v3/company/${app.oauthClient.getToken().realmId}/query?query=select * from Invoice WHERE id IN ('${ids}')&minorversion=38` })
        .then((result) => {
          if (result.statusCode < 200 || result.statusCode > 299) {
            console.error(new Date(), "[invoice_mgmt] [FETCH] QUICKBOOKS INVOICE FETCH ERROR")
            resolve({ code: 409, error: 'QUICKBOOKS ERROR' });
          } else {
            let quickbooksData = result.json.QueryResponse.Invoice;

            let quickbookMap = new Map();

            quickbooksData.forEach(temp => {
              quickbookMap.set(temp.Id, temp)
            });

            formatedData.forEach(temp => {
              let invoiceNumber = quickbookMap.get(temp.invoice_quickbooks_id);

              if (invoiceNumber) {
                temp.previousPaid = temp.paid;

                temp.quickbook_status = (invoiceNumber.TotalAmt === invoiceNumber.Balance) ?
                  'Unpaid' :
                  (invoiceNumber.Balance !== 0 ? 'Partial' : 'Paid');

                temp.total_amount = invoiceNumber.TotalAmt;
                temp.outstanding = invoiceNumber.Balance;
                temp.paid = +invoiceNumber.TotalAmt - +invoiceNumber.Balance;

                temp.date_of_payment = temp.quickbook_status === 'Paid' ?
                  invoiceNumber.MetaData.LastUpdatedTime : null;

                temp.is_archived = temp.quickbook_status === 'Paid' ? 1 : 0;

                temp.invoice_status = (temp.quickbook_status === 'Partial'
                  && temp.previousPaid !== temp.paid) ?
                  'Sync Needed' : temp.invoice_status;
              } else {
                temp.quickbook_status = "Not In Quickbooks";
              }
            })

            let needToSyncData = formatedData.filter(temp => (temp.is_archived === 1
              && temp.invoice_status !== 'Paid') || temp.invoice_status === 'Sync Needed');

            needToSyncData.length !== 0 ? syncWithMysqlDb(needToSyncData) : null;

            resolve({ code: 200, data: formatedData });
          }
        })
        .catch((error) => {
          console.error(new Date(), "[invoice_mgmt] [FETCH] QUICKBOOKS INVOICE FETCH ERROR",
            "body" in error ? error.body : error);
          resolve({ code: 409, error: 'QUICKBOOK API CALL ERROR' });
        })
    })
  }

  const filterInvoiceData = (parsedData, filterData, timeZone, response) => {
    try {
      for (let elem of filterData) {

        let column = elem.column;
        let values = elem.values;

        parsedData = parsedData.filter(temp => {
          if (column === 'date_of_payment' || column === 'date') {

            return temp[column] && temp[column].trim() ?
              moment(temp[column]).tz(timeZone).format('M/D/YYYY')
                .includes(moment(values).tz(timeZone).format('M/D/YYYY'))
              : false;

          } else {

            return temp[column] ? temp[column]
              .toString().toLowerCase().replace(/ +/g, "")
              .indexOf(values.toLowerCase().replace(/ +/g, "")) === 0 : false

          }
        });
      }

      return parsedData

    } catch (error) {
      response.status(400).json({ code: 400, error: 'CODE ERROR' })
    }
  }

  const formatInvoiceData = (invoiceParsedData, offset, limit, direction, column) => {
    let invoiceData;
    let invoicePaginationLength;

    if (direction !== null && (direction === 'asc' || direction === 'desc')) {
      invoiceData = invoiceParsedData.length !== 0 ? _.orderBy(invoiceParsedData, column, direction).slice((offset - 1), limit) : [];
    } else {
      invoiceData = invoiceParsedData.length !== 0 ? _.orderBy(invoiceParsedData, 'created_on', 'desc').slice((offset - 1), limit) : [];
    }

    invoicePaginationLength = invoiceParsedData.length;

    return { invoiceData, invoicePaginationLength }
  }

  Invoice.getInvoiceManagementData = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {

      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
      } else {
        options.tokenModel = tokenModel;
        request.isQuickbooksInitial ? client.del('invoice_mgmt') : null;
        getInvoiceManagementDataCache(request, response, options, callback);
      }

    });

  }

  const syncWithMysqlDb = (invoiceData) => {
    console.log(new Date(), "[invoice_mgmt] [SYNC] MYSQL DB SYNC STARTED", invoiceData.length);

    let isArchived = invoiceData.some(temp => temp.is_archived === 1);

    async.mapSeries(invoiceData, (temp, callback) => {
      queryParams = updateInvoiceDataQuery([(temp.invoice_type === 'auto' ?
        'invoice_mgmt' : 'manual_invoice'), temp.quickbook_status, temp.is_archived, temp.paid,
      temp.total_amount, temp.outstanding, temp.date_of_payment, (temp.invoice_type === 'auto' ?
        'invoice_id' : 'manual_invoice_id'), (temp.invoice_type === 'auto' ?
          temp.invoice_id : temp.manual_invoice_id)]);

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          return callback(err);
        } else {
          callback(null, "UPDATED SUCCESSFULLY");
        }
      });
    }, (err, loopResults) => {
      if (err) {
        console.error(new Date(), "[invoice_mgmt] [SYNC] MYSQL DB SYNC ERROR", "sqlMessage" in err ? err.sqlMessage : err);
      } else {
        console.log(new Date(), "[invoice_mgmt] [SYNC] MYSQL DB SYNC COMPLETED", `${loopResults.length} is updated`);
        if (isArchived) {
          console.log(new Date(), "[invoice_mgmt] [SYNC] ARCHIVED ORDER CACHE CLEAR",
            `${invoiceData.filter(temp => temp.is_archived === 1).length} is updated`);
          client.del('archived_orders');
        }
      }
    })
  }


  const deleteAutoInvoiceQuery = (params) => {
    return {
      "query": "call deleteInvoiceManagementData(?,?)",
      "params": params
    };
  }

  const deleteInvoiceParsing = async (req, response, options, callback) => {
    let quickbooksInput;

    let syncToken;

    let invoiceOptions = {
      uri: `${quickbooksUrl}/v3/company/${app.oauthClient.getToken().realmId}/invoice?operation=delete&minorversion=38`,
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + app.oauthClient.getToken().access_token
      },
      json: quickbooksInput
    }
    try {

      for (const temp of req.containerToBeDeleted) {
        syncToken = await fetchInvoiceSyncTokenFromQuickbooks(temp.invoice_quickbooks_id);

        if (syncToken.code === 409) {
          temp.syncToken = null
        } else if (syncToken.code === 200) {
          temp.syncToken = syncToken.syncToken
        }
      }

      async.mapSeries(req.containerToBeDeleted, (temp, cb) => {

        if (temp.syncToken === null) {
          return cb({ code: 409, error: "QUICKBOOKS SYNC ERROR" })
        }

        quickbooksInput = {
          "SyncToken": temp.syncToken,
          "Id": temp.invoice_quickbooks_id
        }
        invoiceOptions['json'] = quickbooksInput

        fetchRequest(invoiceOptions, (err, result) => {
          if (err) {
            return cb({ code: 500, error: 'QUICKBOOK API CONNECTION ERROR' });
          } else {
            if (result.statusCode < 200 || result.statusCode > 299) {
              console.error(new Date(), `[invoice_mgmt] [API] QUICKBOOKS API ERROR`,
                result && ('body' in result) && ('Fault' in result.body) &&
                  ('Error' in result.body.Fault) && result.body.Fault.Error &&
                  result.body.Fault.Error.length !== 0 ? result.body.Fault.Error[0]
                  : null);

              return cb({ code: 409, error: 'QUICKBOOKS ERROR' });
            } else {
              queryParams = deleteAutoInvoiceQuery([temp.order_container_chassis_id, temp.order_id]);
              executeQuery(queryParams.query, queryParams.params, (deleteErr, queryResponse) => {
                if (deleteErr) {
                  console.error(new Date(), "[invoice_mgmt] [DELETE] MYSQL ERROR",
                    "sqlMessage" in deleteErr ? deleteErr.sqlMessage : deleteErr);

                  return cb({ code: 409, error: 'DB ERROR' });
                } else {
                  cb(null, queryResponse);
                }
              });
            }
          }
        })
      }, (err, results) => {
        if (err) {
          response.status(409).json({ code: 409, error: 'LOOP ERROR', Data: err });
        } else {

          client.del("invoiceData");
          client.del('invoice_mgmt');
          client.del('advanced_search');
          callback(null, { code: 200, data: "successfully deleted" })
        }
      })
    } catch (e) {
      response.status(400).json({ code: 400, error: 'CODE ERROR' });
    }
  }

  Invoice.deleteAutoInvoice = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
      } else {
        options.tokenModel = tokenModel;
        authenticateQuickbooksConnection(request, response, options, 'delete', callback);
      }
    });

  };

  const fetchInvoiceSyncTokenFromQuickbooks = (id) => {
    return new Promise((resolve, reject) => {
      return app.oauthClient.makeApiCall({ url: `${quickbooksUrl}v3/company/${app.oauthClient.getToken().realmId}/invoice/${id}?minorversion=38` })
        .then((result) => {
          if (result.statusCode < 200 || result.statusCode > 299) {
            console.error(new Date(), `[invoice_mgmt] [FETCH] QUICKBOOK SYNC TOKEN ERROR`,
              result && ("body" in result) ? result.body : null);

            resolve({ code: 409, error: 'QUICKBOOKS SYNC API ERROR' });
          } else {
            let syncToken = result.json.Invoice.SyncToken;

            resolve({ code: 200, syncToken })
          }
        }).catch(err => {
          console.error(new Date(), `[invoice_mgmt] [FETCH] QUICKBOOK SYNC TOKEN ERROR`, err);

          resolve({ code: 409, error: 'QUICKBOOKS SYNC CATCH ERROR' });
        })
    })
  }

  const fetchItemIdFromQuickbooks = () => {
    return new Promise((resolve, reject) => {
      return app.oauthClient.makeApiCall({ url: `${quickbooksUrl}v3/company/${app.oauthClient.getToken().realmId}/query?query=select * from Item where Name='Services'&minorversion=38` })
        .then((result) => {
          if (result.statusCode < 200 || result.statusCode > 299) {
            console.error(new Date(), `[invoice_mgmt] [FETCH] QUICKBOOKS ITEM ID ERROR`,
              result && ("body" in result) ? result.body : null);

            resolve({ code: 409, error: 'QUICKBOOKS ITEM API ERROR' });
          } else {
            let Id = result.json.QueryResponse.Item[0].Id;

            resolve({ code: 200, Id })
          }
        }).catch(err => {
          console.error(new Date(), `[invoice_mgmt] [FETCH] QUICKBOOK ITEM ID ERROR`, err);

          resolve({ code: 409, error: 'QUICKBOOKS ITEM CATCH ERROR' });
        })
    })
  }

  const fetchListOfUploadedFileNamesQuery = (params) => {
    return {
      "query": "SELECT * from ?? WHERE FIND_IN_SET(??,?);",
      "params": params
    };
  }

  const fetchLegAndAccessoriesFromDbQuery = (params) => {
    return {
      "query": "call getOrderLegAccessories(?)",
      "params": params
    };
  }

  const fetchAccessorialChargesFromDbQuery = (params) => {
    return {
      "query": "SELECT * from manual_invoice_accessorial_charges miac WHERE manual_invoice_id = ?;",
      "params": params
    };
  }

  const fetchListOfUploadedFileNames = (table, column, id) => {
    return new Promise((resolve, reject) => {
      queryParams = fetchListOfUploadedFileNamesQuery([table, column, id]);
      return executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[invoice_mgmt] [FETCH] UPLOAD FILE LIST QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          resolve({ code: 409, data: [], error: "DB ERROR" })
        } else {
          resolve({ code: 200, data: queryResponse, error: null })
        }
      })
    })
  }

  const fetchReportFromReportingServer = (endpoint, invoiceGenerationData, multiple, isMergeDocs) => {
    let options = {
      uri: `${config.reportingServer}${endpoint}`,
      method: "POST",
      encoding: null,
      json: invoiceGenerationData
    }
    return new Promise((resolve, reject) => {
      return fetchRequest.post(options, (err, result) => {
        if (err) {
          console.error(new Date(), "[invoice_mgmt] [API] REPORTING CONNECTION ERROR", err);

          resolve({ code: 500, error: "REPORTING CONNECTION ERROR" })
        } else {
          if (result.statusCode < 200 || result.statusCode > 299) {
            console.error(new Date(), "[invoice_mgmt] [API] REPORTING RESPONSE ERROR",
              "body" in result ? result.body : null);

            resolve({ code: 409, error: "REPORTING DOC ERROR" });
          } else {
            let data = {
              filename: `${multiple ? invoiceGenerationData.order_number : invoiceGenerationData.invoice_number}.pdf`,
              content: isMergeDocs ? result.body : new Uint8Array(result.body)
            }
            resolve({ code: 200, data })
          }
        }
      })
    })
  }

  const getFilesFromAws = (Key, isMergeDocs) => {
    return new Promise((resolve, reject) => {
      return s3.getObject(
        {
          Bucket: config.awsS3BucketName,
          Key
        }, (err, s3Response) => {
          if (err) {
            console.error(new Date(), "[invoice_mgmt] [FETCH] AWS FILE RETRIEVE ERROR", err);

            resolve({ code: 409, error: "S3 Fetch File Error" })
          } else {
            let data = {
              filename: isMergeDocs ? Key : Key.split('/')[Key.split('/').length - 1],
              content: isMergeDocs ? s3Response.Body : new Uint8Array(s3Response.Body)
            }
            resolve({ code: 200, data });
          }
        });
    })
  }

  const fetchLegAndAccessoriesFromDb = (id) => {
    return new Promise((resolve, reject) => {
      queryParams = fetchLegAndAccessoriesFromDbQuery([id]);
      return executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[invoice_mgmt] [FETCH] LEG AND ACCESSORIES QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          resolve({ code: 409, data: [], error: "DB ERROR" })
        } else {
          queryResponse[0].forEach(temp => {
            temp.legs = temp.legs !== null ? JSON.parse(temp.legs) : [];
            temp.accessories = temp.accessories !== null ? JSON.parse(temp.accessories) : [];
          })
          let data = {
            leg: queryResponse[0][0].legs,
            charges: queryResponse[0][0].accessories
          }
          resolve({ code: 200, data, error: null })
        }
      })
    })
  }

  const fetchAccessorialChargesFromDb = (id) => {
    return new Promise((resolve, reject) => {
      queryParams = fetchAccessorialChargesFromDbQuery([id]);
      return executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[invoice_mgmt] [FETCH] ACCESSORIAL CHARGES QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          resolve({ code: 409, data: [], error: "DB ERROR" })
        } else {
          resolve({ code: 200, data: queryResponse, error: null })
        }
      })
    })
  }

  const sendEmailWithAttachment = async (request) => {

    let emailAttachment = [];

    if (request.invoice_type === 'auto') {

      if (request.isNeedToFetchLeg) {
        let fetchedLegAndAccessories = await fetchLegAndAccessoriesFromDb(request.id);

        request.invoiceGenerationData.leg_mgmt = fetchedLegAndAccessories.data.leg;

        request.invoiceGenerationData.accessoriesCharge = fetchedLegAndAccessories.data.charges;
      }

      let fetchedUploadedFileNames = await fetchListOfUploadedFileNames('upload_document_links', 'order_container_chassis_id', request.id);

      let usersListToAttach = Object.keys(request.attachmentList)
        .filter(temp => request.attachmentList[temp] === true);

      let fileNamesToAttach = fetchedUploadedFileNames.data
        .filter(temp => usersListToAttach
          .some(elem => temp.file_name.includes(elem))).map(temp => temp.file_name);

      if (fileNamesToAttach.length !== 0) {
        for (const temp of fileNamesToAttach) {
          let fileBuffer = await getFilesFromAws(temp, false);

          if (fileBuffer.code === 200) {
            emailAttachment.push(fileBuffer.data)
          } else {
            console.error(new Date(), "[invoice_mgmt] [FETCH] AWS FILE RETRIEVE ERROR")
          }
        }
      }

      if (request.attachmentList.invoice_pdf && !invoiceAdded) {
        let fetchedReport = await fetchReportFromReportingServer(request.multiple ?
          'multipleInvoice' : 'autoInvoice', request.invoiceGenerationData, request.multiple, false);

        if (fetchedReport.code === 200) {
          if (emailAttachment.length != 0 ||
            request.count === request.totalLength - 1 ||
            fileNamesToAttach.length === 0) {

            emailAttachment.unshift(fetchedReport.data);
            invoiceAdded = true;

          }
        } else {
          console.error(new Date(), "[invoice_mgmt] [FETCH] REPORTING SERVER ERROR");
        }
      }

    } else {
      let fetchedUploadedFileNames;

      if (request.isNeedToFetch) {
        let fetchedAccessorialCharges = await fetchAccessorialChargesFromDb(request.id);

        request.invoiceGenerationData.accessoriesCharge = fetchedAccessorialCharges.data;

        fetchedUploadedFileNames = await fetchListOfUploadedFileNames('manual_invoice_docs', 'manual_invoice_id', request.id);

        fetchedUploadedFileNames.data = fetchedUploadedFileNames.data.map(temp => temp.file_name);
      } else {
        fetchedUploadedFileNames = { data: request.file_names };
      }

      let usersListToAttach = Object.keys(request.attachmentList)
        .filter(temp => request.attachmentList[temp] === true);

      let fileNamesToAttach = fetchedUploadedFileNames.data
        .filter(temp => usersListToAttach.some(elem => temp.includes(elem)));

      if (fileNamesToAttach.length !== 0) {
        for (const temp of fileNamesToAttach) {
          let fileBuffer = await getFilesFromAws(temp, false);

          if (fileBuffer.code === 200) {
            emailAttachment.push(fileBuffer.data)
          } else {
            console.error(new Date(), "[invoice_mgmt] [FETCH] AWS FILE RETRIEVE ERROR")
          }
        }
      }

      if (request.attachmentList.invoice_pdf) {
        let fetchedReport = await fetchReportFromReportingServer('manualInvoice',
          request.invoiceGenerationData, request.multiple, false);

        if (fetchedReport.code === 200) {
          emailAttachment.unshift(fetchedReport.data);
        }
      }
    }

    console.log(new Date(), "[invoice_mgmt] [SEND] TOTAL FILE ATTACHED :", emailAttachment.length);

    if (emailAttachment.length !== 0) {
      Invoice.app.models.Email.send({
        to: request.to,
        from: config.mail,
        cc: request.cc,
        subject: request.subject,
        text: request.body,
        attachments: emailAttachment
      })

      return "Successfully Mail Sent"
    } else {
      return "Mail Not Sent"
    }
  }

  Invoice.emailInvoiceWithAttachment = (request, response, options, callback) => {

    validateAccessToken(options.access_token, async (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
      } else {
        try {
          options.tokenModel = tokenModel;
          invoiceAdded = false;

          if (request.multiple) {

            let orderContainerChassisId = request.id.split(',');

            request.totalLength = orderContainerChassisId.length

            request.count = 0;

            for await (const id of orderContainerChassisId) {
              request.id = id;

              await sendEmailWithAttachment(request);

              request.count = request.count + 1;
            }

            callback(null, { code: 200, data: "Successfully Mail Sent With Attachment" });
          } else {
            sendEmailWithAttachment(request).then((data) => {
              if (data === "Successfully Mail Sent") {
                callback(null, { code: 200, data: "Successfully Mail Sent With Attachment" });
              } else {
                callback(null, { code: 422, data: "Mail Not Sent" });
              }
            }).catch((e) => {
              console.error(new Date, "[invoice] [EMAIL] SEND ERROR", e);

              response.status(409).json({ code: 409, error: 'Mail Sending Error' });
            })
          }
        } catch (error) {
          response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
      }
    });

  }

  // authorize quick books
  Invoice.authorizeQuickBooksConnectionValidation = function (response, options, callback) {
    try {
      var authUri = app.oauthClient.authorizeUri({
        scope: [OAuthClient.scopes.Accounting,
        OAuthClient.scopes.OpenId], state: 'intuit-test'
      })

      callback(null, authUri);

    } catch (e) {
      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Invoice.authorizeQuickBooksConnection = function (response, options, callback) {
    var self = this;
    validateAccessToken(options.access_token, function (err, tokenModel) {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        self.authorizeQuickBooksConnectionValidation(response, options, callback);
      }
    });
  }


  // reconnect quick books
  Invoice.reconnectQuickBooksConnectionValidation = function (response, options, callback) {
    try {

      if (app.oauthClient.token.getToken().access_token) {
        app.oauthClient.revoke().then((authResponse) => {
          deleteAndSendAuthorizeUri(response, options, callback);
        }).catch((e) => {
          console.error(new Date(), `[invoice_mgmt] [DELETE] QUICKBOOKS REVOKE TOKEN ERROR`, e);

          return response.status(409).json({ code: 409, error: "TOKEN REVOKE ERROR" });
        });

      } else {
        executeQuery('select * from quickbooks_token_master where id = 1;', null,
          (err, queryResponse) => {
            if (err) {
              console.error(new Date(), `[invoice_mgmt] [FETCH] QUICKBOOKS TOKEN QUERY ERROR`,
                "sqlMessage" in err ? err.sqlMessage : err);

              response.status(409).json({ code: 409, error: "QUICKBOOKS TOKEN RETRIEVE ERROR" });
            } else {
              if (queryResponse.length !== 0) {
                app.oauthClient.setToken(queryResponse[0]);

                app.oauthClient.revoke().then((authResponse) => {
                  deleteAndSendAuthorizeUri(response, options, callback);
                }).catch((e) => {
                  console.error(new Date(),
                    `[invoice_mgmt] [DELETE] QUICKBOOKS REVOKE TOKEN ERROR`, e);

                  return response.status(409).json({ code: 409, error: "TOKEN REVOKE ERROR" });
                });
              } else {
                deleteAndSendAuthorizeUri(response, options, callback);
              }
            }
          })
      }

    } catch (e) {
      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  const deleteAndSendAuthorizeUri = (response, options, callback) => {
    executeQuery('delete from quickbooks_token_master where id = 1;', null,
      (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[invoice_mgmt] [DELETE] QUICKBOOKS TOKEN REVOKE QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          return response.status(409).json({ code: 409, error: "QUICKBOOKS TOKEN REVOKE ERROR" });
        } else {
          console.log(new Date(), "[invoice_mgmt] [DELETE] QUICKBOOKS TOKEN DELETED IN DB")
          var authUri = app.oauthClient.authorizeUri({
            scope: [OAuthClient.scopes.Accounting,
            OAuthClient.scopes.OpenId], state: 'intuit-test'
          })

          callback(null, authUri);
        }
      })
  }

  Invoice.reconnectQuickBooksConnection = function (response, options, callback) {
    var self = this;
    validateAccessToken(options.access_token, function (err, tokenModel) {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        self.reconnectQuickBooksConnectionValidation(response, options, callback);
      }
    });
  }

  const sendEmailWithMergeDocsProcess = async (request, response, options, callback) => {

    try {

      const docPdf = new pdf.Document({
        font: require('pdfjs/font/Helvetica'),
        padding: 10
      });

      docPdf.pipe(
        fs.createWriteStream(
          path.resolve(__dirname, `../${request.id}-${request.loggedInUser}.pdf`)
        )
      );

      if (request.invoice_type === 'auto') {

        if (request.isNeedToFetchLeg) {

          let fetchedLegAndAccessories = await fetchLegAndAccessoriesFromDb(request.id);

          request.invoiceGenerationData.leg_mgmt = fetchedLegAndAccessories.data.leg;

          request.invoiceGenerationData.accessoriesCharge = fetchedLegAndAccessories.data.charges;

        }

        let fetchedUploadedFileNames = await fetchListOfUploadedFileNames('upload_document_links', 'order_container_chassis_id', request.id);

        let usersListToAttach = Object.keys(request.attachmentList)
          .filter(temp => request.attachmentList[temp] === true);

        let fileNamesToAttach = fetchedUploadedFileNames.data
          .filter(temp => usersListToAttach
            .some(elem => temp.file_name.includes(elem))).map(temp => temp.file_name);

        if (fileNamesToAttach.length) {

          for (const temp of fileNamesToAttach) {

            let fileBuffer = await getFilesFromAws(temp, true);

            if (fileBuffer.code === 200) {

              let ext = temp.split('.')[temp.split('.').length - 1].toLowerCase();

              if (ext === 'png') {

                Jimp.read(fileBuffer.data.content).then(image => {
                  image.getBufferAsync(Jimp.MIME_JPEG)
                    .then(buffer => {
                      docPdf.image(new pdf.Image(buffer));
                    })
                });

              } else if (ext === 'jpg' || ext === 'jpeg') {

                docPdf.image(new pdf.Image(fileBuffer.data.content));

              } else if (ext === 'pdf') {

                docPdf.addPagesOf(new pdf.ExternalDocument(fileBuffer.data.content));

              }

            } else {
              console.error(new Date(), "[invoice_mgmt] [FETCH] AWS FILE RETRIEVE ERROR")
            }
          }

        }

        if (request.attachmentList.invoice_pdf) {

          let fetchedReport = await fetchReportFromReportingServer('autoInvoice', request.invoiceGenerationData, request.multiple, true);

          if (fetchedReport.code === 200) {
            docPdf.addPagesOf(new pdf.ExternalDocument(fetchedReport.data.content));
          } else {
            console.error(new Date(), "[invoice_mgmt] [FETCH] REPORTING SERVER ERROR");
          }
        }

      } else {
        let fetchedUploadedFileNames;

        if (request.isNeedToFetch) {

          let fetchedAccessorialCharges = await fetchAccessorialChargesFromDb(request.id);

          request.invoiceGenerationData.accessoriesCharge = fetchedAccessorialCharges.data;

          fetchedUploadedFileNames = await fetchListOfUploadedFileNames('manual_invoice_docs', 'manual_invoice_id', request.id);

          fetchedUploadedFileNames.data = fetchedUploadedFileNames.data.map(temp => temp.file_name);

        } else {

          fetchedUploadedFileNames = { data: request.file_names };

        }

        let usersListToAttach = Object.keys(request.attachmentList)
          .filter(temp => request.attachmentList[temp] === true);

        let fileNamesToAttach = fetchedUploadedFileNames.data
          .filter(temp => usersListToAttach.some(elem => temp.includes(elem)));

        if (fileNamesToAttach.length !== 0) {
          for (const temp of fileNamesToAttach) {
            let fileBuffer = await getFilesFromAws(temp, true);

            if (fileBuffer.code === 200) {
              let ext = temp.split('.')[temp.split('.').length - 1].toLowerCase();

              if (ext === 'png') {

                Jimp.read(fileBuffer.data.content).then(image => {
                  image.getBufferAsync(Jimp.MIME_JPEG).then(buffer => {
                    docPdf.image(new pdf.Image(buffer));
                  })
                });

              } else if (ext === 'jpg' || ext === 'jpeg') {

                docPdf.image(new pdf.Image(fileBuffer.data.content));

              } else if (ext === 'pdf') {

                docPdf.addPagesOf(new pdf.ExternalDocument(fileBuffer.data.content));

              }
            } else {
              console.error(new Date(), "[invoice_mgmt] [FETCH] AWS FILE RETRIEVE ERROR")
            }
          }
        }

        if (request.attachmentList.invoice_pdf) {
          let fetchedReport = await fetchReportFromReportingServer('manualInvoice',
            request.invoiceGenerationData, request.multiple, true);

          if (fetchedReport.code === 200) {
            docPdf.addPagesOf(new pdf.ExternalDocument(fetchedReport.data.content));
          }
        }
      }

      await docPdf.end();

      let stats = fs.statSync(path.resolve(__dirname, `../${request.id}-${request.loggedInUser}.pdf`));

      if (stats.size <= 25000000) {

        Invoice.app.models.Email.send({
          to: request.to,
          from: config.mail,
          cc: request.cc,
          subject: request.subject,
          text: request.body,
          attachments: [
            {
              filename: `${request.invoiceGenerationData.order_number}.pdf`,
              content: fs.createReadStream(path.resolve(__dirname, `../${request.id}-${request.loggedInUser}.pdf`))
            }
          ]
        }, (err, result) => {
          if (err) {
            console.error(new Date(), "[invoice] [SEND] MAIL ERROR", err);

            fs.unlinkSync(path.resolve(__dirname, `../${request.id}-${request.loggedInUser}.pdf`));

            return response.status(409).json({ code: 409, error: "Error in sending mail" });
          }

          fs.unlinkSync(path.resolve(__dirname, `../${request.id}-${request.loggedInUser}.pdf`));

          callback(null, { code: 200, data: "Successfully Mail Sent" });

        })

      } else {
        fs.unlinkSync(path.resolve(__dirname, `../${request.id}-${request.loggedInUser}.pdf`));

        callback(null, { code: 103, data: "Attatchment is too large" });
      }

    } catch (error) {
      console.error(new Date(), "[invoice] SEND MAIL WITH MERGE DOCS CODE ERROR", error);

      response.status(400).json({ code: 400, error: 'CODE ERROR' });
    }
  }

  Invoice.sendEmailWithMergeDocs = (request, response, options, callback) => {

    validateAccessToken(options.access_token, async (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
      } else {
        options.tokenModel = tokenModel;
        sendEmailWithMergeDocsProcess(request, response, options, callback);
      }
    });

  }

  const viewWithMergeDocsProcess = async (request, response, options, callback) => {

    try {

      console.log("initiate process [1788]");
      const docPdf = new pdf.Document({
        font: require('pdfjs/font/Helvetica'),
        padding: 10
      });

      docPdf.pipe(
        fs.createWriteStream(
          path.resolve(__dirname, `../${request.id}-${request.loggedInUser}-view.pdf`)
        )
      );

      console.log("initiate pdfjs [1800]");

      if (request.invoice_type === 'auto') {

        let fetchedUploadedFileNames = await fetchListOfUploadedFileNames('upload_document_links', 'order_container_chassis_id', request.id);

        let fileNamesToAttach = fetchedUploadedFileNames.data.map(temp => temp.file_name);

        if (fileNamesToAttach.length) {

          for (const temp of fileNamesToAttach) {

            let fileBuffer = await getFilesFromAws(temp, true);

            if (fileBuffer.code === 200) {

              let ext = temp.split('.')[temp.split('.').length - 1].toLowerCase();

              if (ext === 'png') {

                Jimp.read(fileBuffer.data.content).then(image => {
                  image.getBufferAsync(Jimp.MIME_JPEG)
                    .then(buffer => {
                      docPdf.image(new pdf.Image(buffer));
                    })
                });

              } else if (ext === 'jpg' || ext === 'jpeg') {

                docPdf.image(new pdf.Image(fileBuffer.data.content));

              } else if (ext === 'pdf') {

                docPdf.addPagesOf(new pdf.ExternalDocument(fileBuffer.data.content));

              }

            } else {
              console.error(new Date(), "[invoice_mgmt] [FETCH] AWS FILE RETRIEVE ERROR")
            }
          }

        }


        let fetchedReport = await fetchReportFromReportingServer('autoInvoice', request.invoiceGenerationData, request.multiple, true);

        if (fetchedReport.code === 200) {
          docPdf.addPagesOf(new pdf.ExternalDocument(fetchedReport.data.content));
        } else {
          console.error(new Date(), "[invoice_mgmt] [FETCH] REPORTING SERVER ERROR");
        }

      } else {
        let fetchedUploadedFileNames = { data: request.file_names };

        let fileNamesToAttach = fetchedUploadedFileNames.data;

        if (fileNamesToAttach.length) {

          for (const temp of fileNamesToAttach) {

            let fileBuffer = await getFilesFromAws(temp, true);

            if (fileBuffer.code === 200) {

              let ext = temp.split('.')[temp.split('.').length - 1].toLowerCase();

              if (ext === 'png') {

                Jimp.read(fileBuffer.data.content).then(image => {
                  image.getBufferAsync(Jimp.MIME_JPEG).then(buffer => {
                    docPdf.image(new pdf.Image(buffer));
                  })
                });

              } else if (ext === 'jpg' || ext === 'jpeg') {

                docPdf.image(new pdf.Image(fileBuffer.data.content));

              } else if (ext === 'pdf') {

                docPdf.addPagesOf(new pdf.ExternalDocument(fileBuffer.data.content));

              }
            } else {
              console.error(new Date(), "[invoice_mgmt] [FETCH] AWS FILE RETRIEVE ERROR")
            }
          }
        }

        let fetchedReport = await fetchReportFromReportingServer('manualInvoice',
          request.invoiceGenerationData, request.multiple, true);

        if (fetchedReport.code === 200) {
          docPdf.addPagesOf(new pdf.ExternalDocument(fetchedReport.data.content));
        }
      }

      await docPdf.end();

      console.log("pdfjs end [1901]");

      // Use fs.createReadStream() method
      // to read the file
      let reader = fs.createReadStream(path.resolve(__dirname, `../${request.id}-${request.loggedInUser}-view.pdf`));

      let file = [];

      // Read and store chunk in file array
      reader.on('data', function (chunk) {
        file.push(chunk);
      });

      reader.on('end', function () {
        let parsedFile = Buffer.concat(file).toString();

        fs.unlinkSync(path.resolve(__dirname, `../${request.id}-${request.loggedInUser}-view.pdf`));

        callback(null, { code: 200, data: parsedFile });
      });

    } catch (error) {
      console.error(new Date(), "[invoice] VIEW WITH MERGE DOCS CODE ERROR", error);

      response.status(400).json({ code: 400, error: 'CODE ERROR' });
    }
  }

  Invoice.viewWithMergeDocs = (request, response, options, callback) => {

    validateAccessToken(options.access_token, async (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
      } else {
        options.tokenModel = tokenModel;
        viewWithMergeDocsProcess(request, response, options, callback);
      }
    });

  }

  const fetchInvoiceDetailQuery = (params) => {
    return {
      "query": `SELECT
                          invoice_type,
                          order_number,
                          container_sequence,
                          invoice_quickbooks_id
                      FROM
                          (
                          SELECT
                              im.invoice_type,
                              oc.order_number,
                              oc.container_sequence,
                              im.invoice_quickbooks_id
                          FROM
                              invoice_mgmt im
                          INNER JOIN (
                              SELECT
                                  om.order_id,
                                  om.order_number,
                                  occ.order_container_chassis_id,
                                  occ.container_sequence
                              FROM
                                  order_mgmt om
                              INNER JOIN order_container_chassis occ on
                                  occ.order_id = om.order_id) oc on
                              oc.order_container_chassis_id = im.order_container_chassis_id
                      UNION
                          SELECT
                              mi.invoice_type,
                              mi.order_number,
                              null as container_sequence,
                              mi.manual_invoice_quickbooks_id as invoice_quickbooks_id
                          FROM
                              manual_invoice mi) auto_manual
                      WHERE
                          invoice_quickbooks_id = ?;`,
      "params": params
    };
  }

  const fetchInvoiceDetailFromDb = (id, response, options, callback) => {
    try {
      queryParams = fetchInvoiceDetailQuery([id]);

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[invoice] [FETCH] INVOICE DETAIL QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          return response.status(409).json({ code: 409, error: "DB ERROR" });
        }

        callback(null, { code: 200, data: queryResponse });

      });
    } catch (e) {
      console.error(new Date(), `[invoice] [FETCH] INVOICE DETAIL CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }


  Invoice.fetchInvoiceDetail = (id, response, options, callback) => {

    validateAccessToken(options.access_token, async (err, tokenModel) => {
      if (err || !tokenModel) {
        return response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
      }
      options.tokenModel = tokenModel;
      fetchInvoiceDetailFromDb(id, response, options, callback);

    });

  }
};
