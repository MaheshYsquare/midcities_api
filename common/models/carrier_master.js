'use strict';

const auth = require('../../server/boot/auth');


module.exports = function (Carriermaster) {

    let queryParams;

    auth.setToken(Carriermaster);

    const fetchCarrierListQuery = () => {
        return {
            "query": `SELECT
                          cm.carrier_id,
                          cm.carrier_name,
                          cm.email,
                          cm.location_name,
                          cm.address,
                          cm.mc_no,
                          cm.phone_no,
                          cd.carrier_documents
                      FROM
                          carrier_master cm
                      LEFT JOIN (
                          SELECT
                              carrier_id,
                              JSON_ARRAYAGG(JSON_OBJECT('carrier_document_id',
                              carrier_document_id,
                              'file_name',
                              file_name)) carrier_documents
                          FROM
                              carrier_documents
                          GROUP BY
                              carrier_id) cd ON
                          cm.carrier_id = cd.carrier_id
                      WHERE cm.is_deleted = 0;`
        };
    }

    const fetchCarrierListFromDb = (response, options, callback) => {
        try {
            queryParams = fetchCarrierListQuery();

            auth.executeQuery(Carriermaster, queryParams.query, null, (err, queryResponse) => {

                if (err) {
                    console.error(new Date(), `[carrier] [FETCH] CARRIER QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: 'DB ERROR' });
                }

                queryResponse.forEach(temp => {
                    temp.carrier_documents = temp.carrier_documents ?
                        JSON.parse(temp.carrier_documents) : [];

                    temp.carrier_documents = temp.carrier_documents.map(elem => elem.file_name);
                })

                callback(null, queryResponse);
            })
        } catch (e) {
            console.error(new Date(), `[carrier] [FETCH] CARRIER CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Carriermaster.getCarriersList = (response, options, callback) => {

        auth.validateAccessToken(Carriermaster, options.access_token, (err, tokenModel) => {

            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            }

            options.tokenModel = tokenModel;

            fetchCarrierListFromDb(response, options, callback);

        });

    }

    const softDeleteCarrierFromDb = (id, response, options, callback) => {
        try {
            auth.executeQuery(Carriermaster,
                "UPDATE carrier_master SET is_deleted = 1 WHERE carrier_id = ?",
                [id], (err, queryResponse) => {

                    if (err) {
                        console.error(new Date(), `[carrier] [DELETE] CARRIER QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: 'DB ERROR' });
                    }

                    callback(null, "Successfully Deleted");
                })
        } catch (e) {
            console.error(new Date(), `[carrier] [DELETE] CARRIER CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Carriermaster.deleteCarrier = (id, response, options, callback) => {

        auth.validateAccessToken(Carriermaster, options.access_token, (err, tokenModel) => {

            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            }

            options.tokenModel = tokenModel;

            softDeleteCarrierFromDb(id, response, options, callback);

        });

    }
};
