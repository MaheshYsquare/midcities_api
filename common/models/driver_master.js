'use strict';

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const async = require('async');
const multiparty = require('multiparty');
const { readFileSync } = require('fs');
const fetchRequest = require('request');
const AWS = require('aws-sdk');
const config = require('./../../server/boot/load-config').getConfig();
const crypto = require('../../server/boot/encryption.js');
const bing = require('../../server/boot/bingapi');
const moment = require('moment');

let app = require('../../server/server.js');

AWS.config.update({
    secretAccessKey: config.awsSecretAccessKey,
    accessKeyId: config.awsAccessKeyId,
    region: config.awsRegion
});
const s3 = new AWS.S3({});

const _ = require('lodash')


module.exports = function (Drivermaster) {

    let queryParams;

    //Datasource connector
    const executeQuery = (query, params, callback) => {
        Drivermaster.dataSource.connector.query(query, params, callback);
    }

    //Access Token
    const validateAccessToken = (access_token, callback) => {
        Drivermaster.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Drivermaster.createOptionsFromRemotingContext = function (ctx) {
        let base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    /** Fetch Driver Data From Database and set in redis cache */
    const fetchDriverDataFromDb = (request, response, options, section, callback) => {

        try {

            executeQuery("call getDriverDetails();", null, (err, queryResponse) => {
                try {
                    if (err) {
                        console.error(new Date(), `[driver] [FETCH] DRIVER DETAILS QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: 'DB ERROR' });
                    }

                    queryResponse[0].forEach(temp => {

                        temp.est_pickup_to_time = temp.est_pickup_to_time !== null ?
                            (temp.est_pickup_to_time.toString().indexOf('0000-00-00 00:00:00') == 0 ?
                                null : temp.est_pickup_to_time) : null;

                        temp.est_pickup_from_time = temp.est_pickup_from_time !== null ?
                            (temp.est_pickup_from_time.toString().indexOf('0000-00-00 00:00:00') == 0 ?
                                null : temp.est_pickup_from_time) : null;

                        temp.est_delivery_to_time = temp.est_delivery_to_time !== null ?
                            (temp.est_delivery_to_time.toString().indexOf('0000-00-00 00:00:00') == 0 ?
                                null : temp.est_delivery_to_time) : null;

                        temp.est_delivery_from_time = temp.est_delivery_from_time !== null ?
                            (temp.est_delivery_from_time.toString().indexOf('0000-00-00 00:00:00') == 0 ?
                                null : temp.est_delivery_from_time) : null;

                        temp.dl_time = temp.dl_time !== null ?
                            (
                                temp.dl_time.toString().indexOf('0000-00-00 00:00:00') == 0 ?
                                    null : temp.dl_time
                            ) : null;

                        temp.pu_time = temp.pu_time !== null ?
                            (
                                temp.pu_time.toString().indexOf('0000-00-00 00:00:00') == 0 ?
                                    null : temp.pu_time
                            ) : null;

                        temp.selected_drivers = temp.selected_drivers !== null ?
                            JSON.parse(temp.selected_drivers) : [];

                        temp.selected_drivers.forEach(element => {
                            element.dl_time = (element.dl_time !== null ?
                                (
                                    element.dl_time == 0 || element.dl_time == 1000 ?
                                        null : element.dl_time
                                ) : null);

                            element.pu_time = (element.pu_time !== null ?
                                (
                                    element.pu_time == 0 || element.pu_time == 1000 ?
                                        null : element.pu_time
                                ) : null)
                        });

                        temp.phone_number = temp.phone_number && temp.phone_number.trim() ?
                            crypto.decrypt(temp.phone_number) : null;
                    });

                    client.setex('driverDetailsData', 3600, JSON.stringify(queryResponse[0]));

                    if (section === "get") {
                        fetchDriverDataFromCache(request, response, options, section, callback);
                    } else if (section === "search") {
                        searchDriver(request, response, options, section, callback);
                    }

                } catch (error) {
                    console.error(new Date(), `[driver] [FETCH] DRIVER DETAILS CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (e) {
            console.error(new Date(), `[driver] [FETCH] DRIVER DETAILS CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    /** fetch data from redis cache  */
    const fetchDriverDataFromCache = (request, response, options, section, callback) => {
        let redisResponse; // main response json
        let parsedData; // letiable to store parsed dispatch driver data

        /* to get the data from redis db if no data present, set the data to redis db*/
        client.get('driverDetailsData', (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[driver] [FETCH] DRIVER DETAILS REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' });
                }

                if (cacheData) {

                    parsedData = JSON.parse(cacheData);

                    if (request.role == 'Driver') {

                        parsedData = parsedData.length !== 0 ?
                            parsedData.filter(temp => temp.user_id == request.userId) : [];

                    }

                    if (request.isNotAllDriver) {
                        parsedData = parsedData.length !== 0 ?
                            parsedData.filter(temp => temp.driver_id !== request.driverId) : [];
                    }

                    if (!request.isInactiveIncluded) {
                        parsedData = parsedData.length !== 0 ?
                            parsedData.filter(temp => temp.user_status ?
                                temp.user_status !== 'inactive' : true)
                            : [];
                    }

                    if (request.isDriverBoard) {

                        parsedData = parsedData.length !== 0 ?
                            parsedData.filter(temp => {
                                return request.filterDate !== null ? (
                                    temp.pu_time !== null ?
                                        new Date(request.filterDate).getTime() === new Date(new Date(temp.pu_time).toDateString()).getTime()
                                        : false
                                ) : false
                            })
                            : [];
                    }

                    if (request.columnFilter) {
                        parsedData = formatFilteredDriverData(request, response, parsedData);
                    }

                    if (request.searchFilter) {
                        parsedData = parsedData.length !== 0 ?
                            parsedData.filter(temp => {
                                return temp.driver_name !== null ?
                                    temp.driver_name.toLowerCase().replace(/ +/g, "")
                                        .includes(request.searchFilterValue.toLowerCase().replace(/ +/g, "")) : false
                            }) : [];
                    }

                    redisResponse = {
                        code: 200,
                        error: null,
                        data: formatDriverData(request, parsedData)
                    }

                    callback(null, redisResponse);
                } else {
                    fetchDriverDataFromDb(request, response, options, section, callback);
                }
            } catch (error) {
                console.error(new Date(), `[driver] [FETCH] DRIVER DETAILS CACHE CODE ERROR`, error);

                response.status(400).json({ code: 400, error: 'CODE ERROR' });
            }
        });
    }

    const formatDriverData = (request, driverParsedData) => {
        let driverData;
        let driverPaginationLength;

        if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {
            driverData = driverParsedData.length !== 0 ?
                _.orderBy(driverParsedData, (request.column === 'driver_name' ?
                    [driver => driver.driver_name.toLowerCase()] :
                    request.column), request.direction)
                    .slice((request.offset - 1), request.limit)
                : [];
        } else {
            driverData = driverParsedData.length !== 0 ?
                _.orderBy(driverParsedData, ['user_status', driver => driver.driver_name.toLowerCase()], ['asc', 'asc'])
                    .slice((request.offset - 1), request.limit)
                : [];
        }

        driverPaginationLength = driverParsedData.length;

        return { driverData, driverPaginationLength }
    }

    const formatFilteredDriverData = (request, response, parsedData) => {
        try {

            for (let elem of request.filterData) {

                let column = elem.column;
                let values = elem.values;

                parsedData = parsedData.length !== 0 ?
                    parsedData.filter(temp => {
                        if (column === 'phone_number') {

                            return temp[column] ? temp[column]
                                .toString().toLowerCase().replace(/[^a-zA-Z0-9]+/ig, "")
                                .indexOf(values.toLowerCase().replace(/[^a-zA-Z0-9]+/ig, "")) === 0 : false;

                        } else if (column === 'pu_time' || column === 'dl_time') {

                            return temp[column] && temp[column].trim() ?
                                moment(temp[column]).tz(request.timeZone).format('M/D/YYYY')
                                    .includes(moment(values).tz(request.timeZone).format('M/D/YYYY'))
                                : false;

                        } else {

                            return temp[column] ? temp[column]
                                .toString().toLowerCase().replace(/ +/g, "")
                                .indexOf(values.toLowerCase().replace(/ +/g, "")) === 0 : false;

                        }
                    })
                    : [];
            }

            return parsedData

        } catch (error) {
            response.status(400).json({ code: 400, error: 'CODE ERROR' })
        }
    }

    Drivermaster.getDriverDetails = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                fetchDriverDataFromCache(request, response, options, "get", callback);
            }
        });

    }

    const searchDriver = (searchString, response, options, section, callback) => {
        let driverData = [];

        client.get("driverDetailsData", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[driver] [FETCH] DRIVER AUTOSUGGEST REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' });
                } else {
                    if (cacheData) {

                        driverData = JSON.parse(cacheData);

                        driverData = driverData.length !== 0 ?
                            driverData.filter(temp => {
                                return (temp.user_status ? temp.user_status === 'active' : true) &&
                                    temp.driver_name
                                        .toLowerCase()
                                        .replace(/[^a-zA-Z0-9]+/g, '')
                                        .includes(searchString.toLowerCase().replace(/[^a-zA-Z0-9]+/g, ''))
                            }) : [];

                        driverData = driverData.length !== 0 ? driverData.map(temp => {
                            return {
                                searchValue: temp['driver_name'],
                                hazardousConfirm: temp['hazmat_certified'],
                                id: temp['driver_id'],
                                legStatus: temp['leg_status']
                            }
                        }) : [];

                        callback(null, driverData);
                    } else {
                        fetchDriverDataFromDb(searchString, response, options, section, callback);
                    }
                }
            } catch (error) {
                console.error(new Date(), `[driver] [FETCH] DRIVER AUTOSUGGEST CODE ERROR`, error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        })
    }

    Drivermaster.driverAutoSuggest = (search, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                search = search ? decodeURIComponent(search) : null;
                searchDriver(search, response, options, 'search', callback);
            }
        });
    }

    /** Get the Driver Pay Data From Database and Set in Redis Cache */
    const fetchDriverPayDataFromDb = (request, response, options, callback) => {
        try {

            executeQuery("CALL getDriverPayData();", null, (err, queryResponse) => {
                try {
                    if (err) {
                        console.error(new Date(), `[driver] [FETCH] DRIVER PAY QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: 'DB ERROR' });
                    }

                    queryResponse[0].forEach(temp => {

                        temp.pu_time = temp.pu_time !== null ?
                            (
                                temp.pu_time.toString().indexOf('0000-00-00 00:00:00') == 0 ?
                                    null : temp.pu_time
                            ) : null;

                        temp.dl_time = temp.dl_time !== null ?
                            (
                                temp.dl_time.toString().indexOf('0000-00-00 00:00:00') == 0 ?
                                    null : temp.dl_time
                            ) : null;

                        if (request.timeZone) {
                            temp.pickupDate = temp.pu_time ? moment(temp.pu_time).tz(request.timeZone).format('M/D/YYYY') : null;

                            temp.deliveryDate = temp.dl_time ? moment(temp.dl_time).tz(request.timeZone).format('M/D/YYYY') : null;
                        } else {
                            temp.pickupDate = temp.pu_time ? new Date(temp.pu_time).toDateString() : null;

                            temp.deliveryDate = temp.dl_time ? new Date(temp.dl_time).toDateString() : null;
                        }

                        temp.rates_per_mile = temp.rates_per_mile !== null ?
                            JSON.parse(temp.rates_per_mile) : [];

                        temp.rates_per_mile.forEach(elem => {
                            elem.range_from = +elem.range_from;
                            elem.range_to = elem.range_to !== null ? +elem.range_to : null;
                        })

                        temp.rates_per_mile = _.orderBy(temp.rates_per_mile,
                            ['range_from', 'range_to'], ['asc', 'asc']);

                        if (temp.is_rate_edited === 0) {

                            temp.isRateCalculated = false;

                            temp.rates_per_mile.forEach(element => {
                                if (!temp.isRateCalculated) {

                                    if ((temp.miles >= element.range_from &&
                                        temp.miles <= element.range_to) || element.range_to === null) {

                                        temp.driver_rate = element.fixed_rate === 1 ?
                                            +(
                                                +element.rate
                                            ).toFixed(2)
                                            : +(
                                                +temp.miles * +element.rate
                                            ).toFixed(2);

                                        temp.isRateCalculated = true;
                                    }

                                }
                            })

                        } else {
                            temp.driver_rate = +temp.rate;
                        }

                        temp.driver_extra_charge = 0;

                        if (temp.is_charges_added) {

                            temp.driver_charges = temp.driver_charges !== null ?
                                JSON.parse(temp.driver_charges) : [];

                            temp.driver_extra_charge = temp.driver_charges
                                .reduce((prev, curr) => +prev + +curr.rate, 0);

                        }

                        temp.total_driver_rate = +(+temp.driver_rate + +temp.driver_extra_charge).toFixed(2);
                    })

                    client.setex(`driverPayData`, 3600, JSON.stringify(queryResponse[0]));

                    fetchDriverPayDataFromCache(request, response, options, callback);

                } catch (error) {
                    console.error(new Date(), `[driver] [FETCH] DRIVER PAY CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (e) {
            console.error(new Date(), `[driver] [FETCH] DRIVER PAY CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    const fetchDriverPayDataFromCache = (request, response, options, callback) => {
        let redisResponse; // main response json
        let parsedData; // letiable to store parsed dispatch driver data

        /* to get the data from redis db if no data present, set the data to redis db*/
        client.get('driverPayData', (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), `[driver] [FETCH] DRIVER PAY REDIS ERROR`, err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' });
                }
                if (cacheData) {
                    parsedData = JSON.parse(cacheData);

                    parsedData = parsedData.filter(temp => {
                        return (
                            request.filterDate ?
                                (
                                    (
                                        temp.pickupDate ?
                                            (new Date(request.filterDate).getTime() == new Date(temp.pickupDate).getTime()) :
                                            false
                                    ) ||
                                    (
                                        temp.deliveryDate ?
                                            (new Date(request.filterDate).getTime() == new Date(temp.deliveryDate).getTime()) :
                                            false
                                    ) ||
                                    (
                                        (temp.pickupDate && temp.deliveryDate) ?
                                            (
                                                new Date(request.filterDate).getTime() >= new Date(temp.pickupDate).getTime() &&
                                                new Date(request.filterDate).getTime() <= new Date(temp.deliveryDate).getTime()
                                            ) :
                                            false
                                    )
                                )
                                : true
                        )
                    });

                    if (request.role == 'Driver') {
                        parsedData = parsedData.length !== 0 ?
                            parsedData.filter(temp => temp.user_id == request.user_id) : [];
                    }

                    if (request.columnFilter) {
                        parsedData = formatFilteredDriverData(request, response, parsedData)
                    }

                    if (request.searchFilter) {
                        parsedData = parsedData.length !== 0 ?
                            parsedData.filter(temp =>
                            (
                                temp.driver_name !== null ?
                                    temp.driver_name.toLowerCase().replace(/ +/g, "")
                                        .includes(request.searchFilterValue
                                            .toLowerCase().replace(/ +/g, ""))
                                    : false
                            )) : [];
                    }

                    if (request.isListById) {

                        parsedData = parsedData.filter(temp => {
                            return temp.driver_id === request.driver_id &&
                                temp.is_paid === request.is_paid && (
                                    request.filterDate ?
                                        (
                                            (
                                                temp.pickupDate ?
                                                    (new Date(request.filterDate).getTime() == new Date(temp.pickupDate).getTime()) :
                                                    false
                                            ) ||
                                            (
                                                temp.deliveryDate ?
                                                    (new Date(request.filterDate).getTime() == new Date(temp.deliveryDate).getTime()) :
                                                    false
                                            ) ||
                                            (
                                                (temp.pickupDate && temp.deliveryDate) ?
                                                    (
                                                        new Date(request.filterDate).getTime() >= new Date(temp.pickupDate).getTime() &&
                                                        new Date(request.filterDate).getTime() <= new Date(temp.deliveryDate).getTime()
                                                    ) :
                                                    false
                                            )
                                        )
                                        : true
                                )
                        });
                    }

                    redisResponse = {
                        code: 200,
                        error: null,
                        data: formatDriverPayData(request, parsedData)
                    }

                    callback(null, redisResponse);
                } else {
                    fetchDriverPayDataFromDb(request, response, options, callback);
                }

            } catch (error) {
                console.error(new Date(), `[driver] [FETCH] DRIVER PAY CACHE CODE ERROR`, error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        });
    }

    const formatDriverPayData = (request, driverParsedData) => {
        let driverPayData;
        let driverPayPaginationLength;
        let driverPendingAmount = 0;

        driverParsedData.forEach(temp => {
            if (temp.is_paid === 0) {
                driverPendingAmount = driverPendingAmount + +temp.total_driver_rate;
            }
        })

        if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {

            driverPayData = driverParsedData.length !== 0 ?
                _.orderBy(driverParsedData, request.column, request.direction)
                    .slice((request.offset - 1), request.limit)
                : [];

        } else {

            driverPayData = driverParsedData.length !== 0 ?
                _.orderBy(driverParsedData, 'dl_time', request.roleDirection)
                    .slice((request.offset - 1), request.limit)
                : [];

        }

        driverPayPaginationLength = driverParsedData.length;

        return { driverPayData, driverPayPaginationLength, driverPendingAmount }
    }

    Drivermaster.getDriverPayData = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                fetchDriverPayDataFromCache(request, response, options, callback);
            }
        });

    }


    /** Native Mysql Query of Driver Update
    * Required Input @param params */
    const updateDriverDataQuery = (params) => {
        return {
            "query": "update driver_master set truck_number = ?,vehicle_id = ?,phone_number = ? where driver_id = ?",
            "params": params
        };
    }

    const updateDriverDataParsing = (request, response, options, callback) => {
        try {

            request.phone_number = request.phone_number && request.phone_number.trim() ?
                crypto.encrypt(request.phone_number) : null;

            queryParams = updateDriverDataQuery([request.truck_number, request.vehicle_id, request.phone_number, request.driver_id]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[driver] [UPDATE] DRIVER DATA QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {
                    client.del("driverDetailsData");
                    client.del("equipment_details");
                    client.del("driverPayData");
                    callback(null, { code: 200, error: null, data: 'Data Updated Successfully' });
                }
            });
        } catch (e) {
            console.error(new Date(), `[driver] [UPDATE] DRIVER DATA CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Drivermaster.updateDriverData = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                updateDriverDataParsing(request, response, options, callback);
            }
        });

    };


    /** Native Mysql Query of Driver Delete
    * Required Input @param params */
    const deleteDriverQuery = (params) => {
        return {
            "query": "delete from ?? where ?? = ?",
            "params": params
        };
    }

    const deleteDriverFromDb = (driver_id, user_id, response, options, callback) => {

        try {

            queryParams = deleteDriverQuery(['driver_master', 'driver_id', driver_id]);

            executeQuery(queryParams.query, queryParams.params, (err, driverQueryResponse) => {
                try {
                    if (err) {
                        if (('code' in err) && err['code'] == 'ER_ROW_IS_REFERENCED_2') {
                            return callback(null, { code: 400, error: 'Driver is Referenced' })
                        } else {
                            console.error(new Date(), `[driver] [DELETE] DRIVER DATA QUERY ERROR`,
                                "sqlMessage" in err ? err.sqlMessage : err);

                            return response.status(409).json({ code: 409, error: "DB ERROR" });
                        }
                    }
                    if (user_id) {

                        executeQuery("DELETE FROM users_ui_settings WHERE user_id=?", [user_id], (userUiErr, queryResponse) => {
                            if (userUiErr) {
                                console.error(new Date(), "[driver] [DELETE] USER UI SETTINGS QUERY ERROR",
                                    "sqlMessage" in userUiErr ? userUiErr.sqlMessage : userUiErr);

                                return response.status(409).json({ code: 409, error: "DB ERROR" });
                            }

                            queryParams = deleteDriverQuery(['user_master', 'user_id', user_id]);

                            executeQuery(queryParams.query, queryParams.params, (deleteErr, userQueryResponse) => {
                                if (deleteErr) {
                                    console.error(new Date(), `[driver] [DELETE] USER DATA QUERY ERROR`,
                                        "sqlMessage" in deleteErr ? deleteErr.sqlMessage : deleteErr);

                                    return response.status(409).json({ code: 409, error: "DB ERROR USER" });
                                }

                                client.del("driverDetailsData");
                                client.del("driverPayData");
                                client.del('user_master');
                                client.del('user_notify');
                                callback(null, { code: 200, error: null, data: 'Driver Successfully Deleted' });

                            });

                        });

                    } else {

                        client.del("driverDetailsData");
                        client.del("driverPayData");
                        client.del('user_master');
                        client.del('user_notify');
                        callback(null, { code: 200, error: null, data: 'Driver Successfully Deleted' });
                    }

                } catch (error) {
                    console.error(new Date(), `[driver] [DELETE] DRIVER DATA CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            });
        } catch (e) {
            console.error(new Date(), `[driver] [DELETE] DRIVER DATA CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Drivermaster.deleteDriver = (driver_id, user_id, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                deleteDriverFromDb(driver_id, user_id, response, options, callback);
            }
        });

    };


    const checkDeactivateDriverQuery = (params) => {
        return {
            "query": "select * from leg_mgmt where driver_id = ? and leg_status <> 'Delivered'",
            "params": params
        };
    }

    const updateDeactivateDriverQuery = (params) => {
        return {
            "query": "call deactivateDriver(?,?,?,?,?,?,?);",
            "params": params
        };
    }

    const deactivateDriverParsing = (request, response, options, callback) => {
        try {

            queryParams = checkDeactivateDriverQuery([request.driver_id]);

            executeQuery(queryParams.query, queryParams.params, (err, checkQueryResponse) => {

                try {

                    if (err) {
                        console.error(new Date(), `[driver] [UPDATE] DRIVER DEACTIVATE CHECK QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: "DB ERROR" });
                    }

                    if (checkQueryResponse.length === 0) {

                        queryParams = updateDeactivateDriverQuery([request.user_id, request.reason,
                        request.rehirable, request.drug_testing_result, request.accident_summary,
                        request.driver_id, request.termination_date]);

                        executeQuery(queryParams.query, queryParams.params, (error, updateQUeryResponse) => {
                            if (error) {
                                console.error(new Date(), `[driver] [UPDATE] DRIVER DEACTIVATE QUERY ERROR`,
                                    "sqlMessage" in error ? error.sqlMessage : error);

                                return response.status(409).json({ code: 409, error: "DB ERROR" });
                            }

                            client.del("driverDetailsData");
                            client.del('user_master');
                            app.eventEmitter.emit('revokeToken', user_id);
                            callback(null, { code: 200, data: "Successfully Updated" });
                        });

                    } else {
                        callback(null, { code: 422, error: "Driver Is Assigned" });
                    }
                } catch (error) {
                    console.error(new Date(), `[driver] [UPDATE] DRIVER DEACTIVATE CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            })
        } catch (e) {
            console.error(new Date(), `[driver] [UPDATE] DRIVER DEACTIVATE CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Drivermaster.deactivateDriver = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                deactivateDriverParsing(request, response, options, callback);
            }
        });

    };


    const fetchSelectedDriverDataFromDb = (driver_id, response, options, callback) => {
        try {

            executeQuery("CALL getSelectedDriver(?);", [driver_id], (err, queryResponse) => {
                try {

                    if (err) {
                        console.error(new Date(), `[driver] [FETCH] SELECTED DRIVER DATA QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: "DB ERROR" });
                    }

                    queryResponse[0].forEach(temp => {

                        temp.driver_special_endorsement = temp.driver_special_endorsement ?
                            JSON.parse(temp.driver_special_endorsement) : [];

                        temp.driver_attendence = temp.driver_attendence ?
                            JSON.parse(temp.driver_attendence) : [];

                        temp.driver_attendence = _.orderBy(temp.driver_attendence, 'driver_attendence_id', 'desc');

                        temp.driver_incident = temp.driver_incident ?
                            JSON.parse(temp.driver_incident) : [];

                        temp.phone_number = temp.phone_number && temp.phone_number.trim() ?
                            crypto.decrypt(temp.phone_number) : null;

                        temp.license_no = temp.license_no && temp.license_no.trim() ?
                            crypto.decrypt(temp.license_no) : null;

                        temp.ssn = temp.ssn && temp.ssn.trim() ?
                            crypto.decrypt(temp.ssn) : null;

                        temp.fuel_card_number = temp.fuel_card_number ?
                            crypto.decrypt(temp.fuel_card_number) : null;
                    })

                    callback(null, queryResponse[0]);

                } catch (error) {
                    console.error(new Date(), `[driver] [FETCH] SELECTED DRIVER DATA CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            })
        } catch (e) {
            console.error(new Date(), `[driver] [FETCH] SELECTED DRIVER DATA CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Drivermaster.getSelectedDriverInfoData = (driver_id, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                fetchSelectedDriverDataFromDb(driver_id, response, options, callback);
            }
        });
    }

    Drivermaster.sendEmailValidation = function (data, response, options, callback) {
        try {
            let updatedata = JSON.stringify(data);
            let parsedData = JSON.parse(updatedata);
            let senderAddress = parsedData.senderAddress;
            let driverLoadsheet = parsedData.driverLoadsheet;

            let requestOptions = {
                uri: `${config.reportingServer}driverLoadSheet`,
                method: "POST",
                encoding: null,
                json: driverLoadsheet
            }

            fetchRequest.post(requestOptions, (err, result) => {
                try {

                    if (err) {
                        console.error(new Date(), `[driver] [API] REPORTING CONNECT ERROR`, err);

                        return response.status(500).json({ code: 500, error: "REPORTING SERVER CONNECTION ERROR" });
                    }

                    if (result.statusCode < 200 || result.statusCode > 299) {

                        console.error(new Date(), `[driver] [API] REPORTING SERVER ERROR`,
                            "body" in result ? result.body : result);

                        return response.status(409).json({ code: 409, error: "REPORTING DOC ERROR" });
                    }

                    Drivermaster.app.models.Email.send({
                        to: [senderAddress],
                        from: config.mail,
                        subject: `LoadSheet ${driverLoadsheet.order_number}.pdf`,
                        attachments: [
                            {   // binary buffer as an attachment
                                filename: `LoadSheet ${driverLoadsheet.order_number}.pdf`,
                                content: new Uint8Array(result.body)
                            }
                        ]
                    });

                    callback(null, `Successfully mail sent to ${senderAddress}`);


                } catch (error) {
                    console.error(new Date(), `[driver] [API] SEND EMAIL CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            })
        } catch (e) {
            console.error(new Date(), `[driver] [API] SEND EMAIL CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Drivermaster.sendEmail = function (data, response, options, callback) {
        let self = this;
        validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.sendEmailValidation(data, response, options, callback);
            }
        });
    }

    /* posr driver */
    Drivermaster.updateDriverMasterQuery = function (params) {
        return {
            "query": `UPDATE
                          driver_master
                      set
                          truck_number = ?,
                          vehicle_id = ?,
                          driver_notes = ?,
                          fuel_card_number = ?,
                          ssn = ?,
                          street = ?,
                          state = ?,
                          zip = ?,
                          city = ?,
                          phone_number = ?,
                          elog_no = ?,
                          license_no = ?,
                          email = ?,
                          lic_exp_date = ?,
                          hazmat_certified = ?,
                          hazmat_exp_date = ?,
                          med_exp_date = ?
                      WHERE
                          driver_id = ?`,
            params
        };
    }

    Drivermaster.postSpecialEndorsementQuery = function (params) {
        return {
            "query": "INSERT into driver_special_endorsement(driver_id,special_endorsement_name,special_endorsement_exp_date) VALUES(?,?,?)",
            "params": params
        };
    }

    Drivermaster.postDriverIncidentQuery = function (params) {
        return {
            "query": "INSERT into driver_incident(driver_id,name,description,action_taken,file_name) VALUES(?,?,?,?,?)",
            "params": params
        };
    }

    Drivermaster.postMoreInfoParsing = async (request, response, options, callback) => {
        try {
            let postdata = JSON.stringify(request);
            let parsedData = JSON.parse(postdata);
            let truck_number = parsedData.truck_number;
            let vehicle_id = parsedData.vehicle_id;
            let driver_notes = parsedData.driver_notes;
            let street = parsedData.street;
            let state = parsedData.state;
            let zip = parsedData.zip;
            let city = parsedData.city;
            let email = parsedData.email;
            let lic_exp_date = parsedData.lic_exp_date;
            let hazmat_certified = parsedData.hazmat_certified;
            let hazmat_exp_date = parsedData.hazmat_exp_date;
            let med_exp_date = parsedData.med_exp_date;
            let driver_id = parsedData.driver_id;
            let specialEndorsement = parsedData.specialEndorsement;
            let incident = parsedData.incident;
            let event = parsedData.event;
            let elog_no = parsedData.elog_no;
            let fuel_card_number = event === 'general' ?
                (parsedData.fuel_card_number && parsedData.fuel_card_number.trim() ?
                    crypto.encrypt(parsedData.fuel_card_number) : null)
                : null;
            let ssn = event === 'general' ?
                (parsedData.ssn && parsedData.ssn.trim() ?
                    crypto.encrypt(parsedData.ssn) : null)
                : null;
            let phone_number = event === 'general' ?
                (parsedData.phone_number && parsedData.phone_number.trim() ?
                    crypto.encrypt(parsedData.phone_number) : null)
                : null;
            let license_no = event === 'general' ?
                (parsedData.license_no && parsedData.license_no.trim() ?
                    crypto.encrypt(parsedData.license_no) : null)
                : null;

            let input = {
                tableName: '',
                columns: [],
                value: ''
            }

            if (event == 'general') {

                queryParams = Drivermaster.updateDriverMasterQuery([truck_number, vehicle_id, driver_notes,
                    fuel_card_number, ssn, street, state, zip, city, phone_number, elog_no, license_no, email, lic_exp_date,
                    hazmat_certified, hazmat_exp_date, med_exp_date, driver_id]);

                executeQuery(queryParams.query, queryParams.params, async (err, driverUpdateResponse) => {
                    try {
                        if (err) {
                            console.error(new Date(), `[driver] [UPDATE] DRIVER MORE INFO POST QUERY ERROR`,
                                "sqlMessage" in err ? err.sqlMessage : err);

                            response.status(409).json({ code: 409, error: 'DB ERROR' });
                        } else {
                            if (specialEndorsement.length) {
                                await actionFunction('special', specialEndorsement,
                                    input, driver_id, response);
                            }
                            client.del("driverDetailsData");
                            client.del("equipment_details");
                            client.del("driverPayData");
                            callback(null, "success");
                        }
                    } catch (error) {
                        console.error(new Date(), `[driver] [UPDATE] DRIVER MORE INFO POST CODE ERROR`, error);

                        response.status(400).json({ code: 400, error: 'CODE ERROR' });
                    }
                });

            } else {

                if (incident.length) {
                    await actionFunction('incident', incident, input, driver_id, response);
                }

                callback(null, "success");

            }

        } catch (e) {
            console.error(new Date(), `[driver] [UPDATE] DRIVER MORE INFO POST CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Drivermaster.postMoreInfo = function (request, response, options, callback) {
        let self = this;
        validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.postMoreInfoParsing(request, response, options, callback);
            }
        });
    };


    /* update more info */
    Drivermaster.updateSpecialEndorsementQuery = function (params) {
        return {
            "query": "UPDATE driver_special_endorsement set special_endorsement_name = ?,special_endorsement_exp_date = ? WHERE driver_special_endorsement_id = ?",
            "params": params
        };
    }

    Drivermaster.updateDriverIncidentQuery = function (params) {
        return {
            "query": "UPDATE driver_incident set name = ?,description = ?,action_taken = ?,file_name = ? WHERE driver_incident_id = ?",
            "params": params
        };
    }

    Drivermaster.deleteDriverMoreInfoDataQuery = function (params) {
        return {
            "query": "delete from ?? where ?? = ?",
            "params": params
        };
    }

    Drivermaster.updateDriverMoreInfoParsing = function (request, response, options, callback) {
        try {
            let postdata = JSON.stringify(request);
            let parsedData = JSON.parse(postdata);
            let truck_number = parsedData.truck_number;
            let vehicle_id = parsedData.vehicle_id;
            let driver_notes = parsedData.driver_notes;

            let fuel_card_number = parsedData.fuel_card_number && parsedData.fuel_card_number.trim() ?
                crypto.encrypt(parsedData.fuel_card_number) : null;

            let ssn = parsedData.ssn && parsedData.ssn.trim() ?
                crypto.encrypt(parsedData.ssn) : null;

            let street = parsedData.street;
            let state = parsedData.state;
            let zip = parsedData.zip;
            let city = parsedData.city;

            let phone_number = parsedData.phone_number && parsedData.phone_number.trim() ?
                crypto.encrypt(parsedData.phone_number) : null;

            let license_no = parsedData.license_no && parsedData.license_no.trim() ?
                crypto.encrypt(parsedData.license_no) : null;

            let email = parsedData.email;
            let lic_exp_date = parsedData.lic_exp_date;
            let hazmat_certified = parsedData.hazmat_certified;
            let hazmat_exp_date = parsedData.hazmat_exp_date;
            let med_exp_date = parsedData.med_exp_date;
            let driver_id = parsedData.driver_id;
            let specialEndorsement = parsedData.specialEndorsement;
            let incident = parsedData.incident;
            let elog_no = parsedData.elog_no;
            let input = {
                tableName: '',
                columns: [],
                value: ''
            }

            queryParams = Drivermaster.updateDriverMasterQuery([truck_number, vehicle_id, driver_notes, fuel_card_number,
                ssn, street, state, zip, city, phone_number, elog_no, license_no, email, lic_exp_date, hazmat_certified,
                hazmat_exp_date, med_exp_date, driver_id]);

            executeQuery(queryParams.query, queryParams.params, async (err, driverUpdateResponse) => {
                if (err) {
                    console.error(new Date(), `[driver] [UPDATE] DRIVER MORE INFO UPDATE QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: 'DB ERROR' });
                }

                if (specialEndorsement.length) {
                    input.tableName = 'driver_special_endorsement'
                    input.columns = ['special_endorsement_name', 'special_endorsement_exp_date',
                        null, 'driver_special_endorsement_id']
                    await actionFunction('special', specialEndorsement,
                        input, driver_id, response);
                }

                if (incident.length) {
                    input.tableName = 'driver_incident'
                    input.columns = [null, null, null, 'driver_incident_id']
                    await actionFunction('incident', incident, input, driver_id, response);
                }

                client.del("driverDetailsData");
                client.del("equipment_details");
                client.del("driverPayData");
                callback(null, "success");
            });
        } catch (e) {
            console.error(new Date(), `[driver] [UPDATE] DRIVER MORE INFO UPDATE CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    const actionFunction = (event, data, input, driver_id, response) => {
        return new Promise((resolve, reject) => {
            return async.mapSeries(data, function (temp, callback) {
                if (temp.isUpdate) {
                    if (event == 'special') {
                        queryParams = Drivermaster.updateSpecialEndorsementQuery([temp.special_endorsement_name, temp.special_endorsement_exp_date, temp.driver_special_endorsement_id]);
                        executeQuery(queryParams.query, queryParams.params, function (err, specialEndorsementResponse) {
                            if (err) {
                                console.error(new Date(), `[driver] [UPDATE] SPECIAL ENDORESEMENT QUERY ERROR`,
                                    "sqlMessage" in err ? err.sqlMessage : err);
                            }
                        })
                    } else if (event == 'incident') {
                        queryParams = Drivermaster.updateDriverIncidentQuery([temp.name, temp.description, temp.action_taken, temp.file_name, temp.driver_incident_id]);
                        executeQuery(queryParams.query, queryParams.params, function (err, incidentResponse) {
                            if (err) {
                                console.error(new Date(), `[driver] [UPDATE] INCIDENT QUERY ERROR`,
                                    "sqlMessage" in err ? err.sqlMessage : err);
                            }
                        })
                    }
                } else if (temp.isDelete) {
                    queryParams = Drivermaster.deleteDriverMoreInfoDataQuery([input.tableName, input.columns[3], temp[input.columns[3]]]);
                    executeQuery(queryParams.query, queryParams.params, function (err, deleteResponse) {
                        if (err) {
                            console.error(new Date(), `[driver] [DELETE] DRIVER INFO DATA QUERY ERROR`,
                                "sqlMessage" in err ? err.sqlMessage : err);
                        }
                    })
                } else if (temp.isInsert) {
                    if (event == 'special') {
                        queryParams = Drivermaster.postSpecialEndorsementQuery([driver_id, temp.special_endorsement_name, temp.special_endorsement_exp_date]);
                        executeQuery(queryParams.query, queryParams.params, function (err, specialEndorsementResponse) {
                            if (err) {
                                console.error(new Date(), `[driver] [POST] SPECIAL ENDORESEMENT QUERY ERROR`,
                                    "sqlMessage" in err ? err.sqlMessage : err);
                            }
                        })
                    } else if (event == 'incident') {
                        queryParams = Drivermaster.postDriverIncidentQuery([driver_id, temp.name, temp.description, temp.action_taken, temp.file_name]);
                        executeQuery(queryParams.query, queryParams.params, function (err, incidentResponse) {
                            if (err) {
                                console.error(new Date(), `[driver] [POST] INCIDENT QUERY ERROR`,
                                    "sqlMessage" in err ? err.sqlMessage : err);
                            }
                        })
                    }
                }
                callback()
            }, (err, result) => {
                if (err) {
                    console.error(new Date(), `[driver] [UPDATE] DRIVER INFO LOOP ERROR`, err);

                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }).catch(err => {
            console.error(new Date(), `[driver] [UPDATE] DRIVER INFO DATA PROMISE CACHE ERROR`, err);

            response.status(400).json({ code: 400, error: 'PROMISE ERROR' });
        })
    }


    Drivermaster.updateDriverMoreInfo = function (request, response, options, callback) {
        let self = this;
        validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.updateDriverMoreInfoParsing(request, response, options, callback);
            }
        });
    };


    /* upload file */
    Drivermaster.updateFileDetails = function (params) {
        return {
            "query": "update ?? set ?? = ? where ?? = ?",
            "params": params
        };
    }

    Drivermaster.postFileDetails = function (params) {
        return {
            "query": "insert into driver_incident(driver_id,file_name) values(?,?)",
            "params": params
        };
    }

    const getFileFromRequest = (req) => new Promise((resolve, reject) => {
        const form = new multiparty.Form();
        form.parse(req, (err, fields, files) => {
            if (err) reject(err);
            if (!files) reject('File was not found in form data.');
            else resolve({ files, fields });
        });
    });

    const uploadFileToS3 = (file, Key, options = [{}]) => {

        // turn the file into a buffer for uploading
        const buffer = readFileSync(file.path);

        // generate a new random file name
        const fileName = file.originalFilename;

        let extension = fileName.split('.')[1]
        let metadata = ""
        if (extension === 'png') {
            metadata = "image/png"
        } else if (extension === 'jpg' || extension === 'jpeg') {
            metadata = "image/jpeg"
        } else if (extension === 'pdf') {
            metadata = "application/pdf"
        }

        // return a promise
        return new Promise((resolve, reject) => {
            return s3.upload({
                Bucket: config.awsS3BucketName,
                ACL: config.awsS3ACL,
                Key,
                ContentType: metadata,
                Body: buffer
            }, (err, result) => {
                if (err) reject(err);
                else resolve(result); // return the values of the successful AWS S3 request
            });
        });
    };


    Drivermaster.uploadDriverFile = (req, res, options) => {
        Drivermaster.app.models.AccessToken.findById(options.access_token, async (err, token) => {
            if (err || !token) {
                return res.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                try {
                    // extract the file from the request object
                    const data = await getFileFromRequest(req);

                    let files = data.files

                    let fields = JSON.parse(data.fields.fields[0]);

                    let filePath;

                    if (fields.fileRelatedTo == 'driver') {
                        filePath = ['drivers', 'profile_pic', files.files[0].originalFilename].join('/')

                        let { Location, ETag, Bucket, Key } = await uploadFileToS3(files.files[0], filePath);

                        if (Key) {
                            queryParams = Drivermaster.updateFileDetails(['driver_master', 'profile_link',
                                Key, 'driver_id', fields.driverId]);
                            executeQuery(queryParams.query, queryParams.params,
                                function (error, queryResponse) { })
                        }

                        res.status(200).json({ Key, Location, ETag, Bucket })
                    } else {
                        filePath = ['drivers', fields.driverName, 'incident',
                            files.files[0].originalFilename].join('/')

                        if (fields.driverIncidentId != null) {
                            let { Location, ETag, Bucket, Key } = await uploadFileToS3(files.files[0], filePath);

                            if (Key) {
                                queryParams = Drivermaster.updateFileDetails(['driver_incident', 'file_name',
                                    Key, 'driver_incident_id', fields.driverIncidentId]);
                                executeQuery(queryParams.query, queryParams.params,
                                    function (error, queryResponse) { })
                            }

                            res.status(200).json({ Key, Location, ETag, Bucket })
                        } else {
                            let { Location, ETag, Bucket, Key } = await uploadFileToS3(files.files[0], filePath);

                            if (Key) {
                                queryParams = Drivermaster.postFileDetails([fields.driverId, Key]);
                                executeQuery(queryParams.query, queryParams.params,
                                    function (postErr, queryResponse) {
                                        try {
                                            if (postErr) {
                                                console.error(new Date(),
                                                    `[driver] [UPLOAD] FILE DETAILS UPDATE QUERY ERROR`,
                                                    "sqlMessage" in postErr ? postErr.sqlMessage : postErr);

                                                res.status(409).json({ code: 409, error: 'POST DB ERROR' })
                                            } else {
                                                res.status(200).json({ id: queryResponse.insertId, Key, Location, ETag, Bucket })
                                            }
                                        } catch (error) {
                                            console.error(new Date(),
                                                `[driver] [UPLOAD] FILE DETAILS UPDATE CODE ERROR`, error);

                                            res.status(400).json({ code: 400, error: 'CODE ERROR' });
                                        }
                                    })
                            }
                        }
                    }
                } catch (error) {
                    console.error(new Date(), `[driver] [UPLOAD] FILE UPLOAD CODE ERROR`, error);

                    res.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            }
        })
    };

    /* get file */
    Drivermaster.getDriverFileValidation = function (request, response, options, callback) {
        let fileName = request.fileName;
        s3.getObject(
            {
                Bucket: config.awsS3BucketName,
                Key: fileName
            }, function (err, s3Response) {
                if (err) {
                    console.error(new Date(), `[driver] [FETCH] AWS FILE RETERIVE ERROR`, err);

                    response.status(409).json({ code: 409, error: 'S3 ERROR' })
                }
                else {
                    callback(null, {
                        ContentType: s3Response.ContentType,
                        buffer: s3Response.Body
                    })
                }
            });
    }

    Drivermaster.getDriverFile = function (request, response, options, callback) {
        let self = this;
        validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getDriverFileValidation(request, response, options, callback);
            }
        });
    }

    /* delete file */
    const deleteFileToS3 = (fileName, options = [{}]) => {
        // return a promise
        return new Promise((resolve, reject) => {
            let params = {
                Bucket: config.awsS3BucketName,
                Key: fileName
            };
            s3.deleteObject(params, function (err, data) {
                if (err) {
                    console.error(new Date(), `[driver] [DELETE] AWS FILE DELETE ERROR`, err);

                    return reject(err);
                }
                if (data) {
                    console.log(new Date(), "[driver] [DELETE] FILE DELETED SUCCESSFULLY");
                    resolve(data);
                }
            });
        });
    };

    Drivermaster.deleteDriverFile = async (request, res, options) => {
        Drivermaster.app.models.AccessToken.findById(options.access_token, async (err, token) => {
            if (err || !token) {
                return res.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                try {
                    let fileName = request.fileName;
                    let incident_id = request.incident_id;

                    await deleteFileToS3(fileName);

                    queryParams = Drivermaster.updateFileDetails(['driver_incident', 'file_name', null,
                        'driver_incident_id', incident_id]);

                    executeQuery(queryParams.query, queryParams.params,
                        function (updateErr, queryResponse) {
                            if (updateErr) {
                                console.error(new Date(),
                                    `[driver] [UPDATE] DELETED FILE DETAILS UPDATE QUERY ERROR`,
                                    "sqlMessage" in updateErr ? updateErr.sqlMessage : updateErr);

                                res.status(409).json({ code: 409, error: 'DB ERROR' });
                            } else {
                                return 'Deleted Successfully'
                            }
                        })
                } catch (error) {
                    console.error(new Date(), `[driver] [DELETE] FILE DELETE CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            }
        })
    };

    Drivermaster.postDriverPayDataQuery = function (params) {
        return {
            "query": `INSERT into driver_pay(driver_id,order_container_chassis_id,order_id,leg_number,leg_type_id,pu_name,pu_loc,dl_name,dl_loc,container_number,chassis_number,miles,pu_time,dl_time,is_paid,rate,is_charges_added)
                      values (?,?,?,?,?,UCASE(?),?,UCASE(?),?,UCASE(?),UCASE(?),?,?,?,?,?,?)`,
            "params": params
        };
    }

    Drivermaster.postDriverPayChargesDataQuery = function (params) {
        return {
            "query": `INSERT into driver_accessorial_charges(driver_pay_id,accessories_name,rate)
                      values ?`,
            "params": params
        };
    }

    Drivermaster.postDriverPayDataValidation = async function (request, response, options, callback) {
        try {
            let calculatedMiles = await bing.getMiles(request.pu_loc, request.dl_loc);

            queryParams = Drivermaster.postDriverPayDataQuery([request.driver_id,
            request.order_container_chassis_id, request.order_id, request.leg_number, request.leg_type_id,
            request.pu_name, request.pu_loc, request.dl_name, request.dl_loc, request.container_number,
            request.chassis_number, calculatedMiles, request.pu_time, request.dl_time, request.is_paid,
                0, request.is_charges_added]);

            executeQuery(queryParams.query, queryParams.params, function (err, queryResponse) {
                try {

                    if (err) {
                        console.error(new Date(), `[driver] [POST] DRIVER PAY QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: 'DB ERROR' });
                    }

                    request.driverChargesArray = [];

                    request.driver_charges ? request.driver_charges.forEach(temp => {
                        if (temp.accessories_name) {
                            request.driverChargesArray.push([queryResponse.insertId, temp.accessories_name,
                            temp.rate]);
                        }
                    }) : null;

                    if (request.driverChargesArray.length) {

                        queryParams = Drivermaster.postDriverPayChargesDataQuery([
                            request.driverChargesArray]);

                        executeQuery(queryParams.query, queryParams.params,
                            (driverChargesErr, driverChargesResponse) => {

                                if (driverChargesErr) {

                                    console.error(new Date(), "[driver] [POST] DRIVER CHARGES QUERY ERROR",
                                        "sqlMessage" in driverChargesErr ?
                                            driverChargesErr.sqlMessage : driverChargesErr);

                                    client.del('driverPayData');
                                    client.del('orderSearchData');
                                    response.status(409).json({ code: 409, error: 'CHARGE DB ERROR' });

                                } else {
                                    console.log(new Date(),
                                        `[driver] [POST] DRIVER PAY ${request.order_container_chassis_id} ${request.leg_number}`);

                                    client.del('driverPayData');
                                    client.del('orderSearchData');
                                    callback(null, 'Successfully Inserted');
                                }

                            })
                    } else {
                        console.log(new Date(),
                            `[driver] [POST] DRIVER PAY ${request.order_container_chassis_id} ${request.leg_number}`);

                        client.del('driverPayData');
                        client.del('orderSearchData');
                        callback(null, 'Successfully Updated');
                    }


                } catch (error) {
                    console.error(new Date(), `[driver] [POST] DRIVER PAY DATA CODE ERROR`, error);

                    response.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (e) {
            console.error(new Date(), `[driver] [POST] DRIVER PAY DATA CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Drivermaster.postDriverPayData = function (request, response, options, callback) {
        let self = this;
        validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                self.postDriverPayDataValidation(request, response, options, callback);
            }
        });
    }

    Drivermaster.updateDriverPayDataQuery = function (params) {
        return {
            "query": "UPDATE driver_pay set leg_type_id = ?,pu_name = UCASE(?) ,pu_loc = ?,dl_name = UCASE(?),dl_loc = ?,container_number = UCASE(?),chassis_number = UCASE(?),miles = ?,pu_time = ?,dl_time = ?,is_paid = ?,rate = ?,is_rate_edited = ?,order_id = ?,order_container_chassis_id = ?,leg_number = ?,is_charges_added = ? WHERE driver_pay_id = ?;",
            "params": params
        };
    }

    Drivermaster.deleteDriverChargesQuery = function (params) {
        return {
            "query": "Delete from driver_accessorial_charges WHERE driver_pay_id = ?;",
            "params": params
        };
    }

    Drivermaster.updateDriverPayDataValidation = async function (request, response, options, callback) {
        try {

            let calculatedMiles;

            if (request.isMilesEditable) {
                calculatedMiles = request.miles;
            } else {
                if (request.previousPickupAddress == request.pu_loc &&
                    request.previousDeliveryAddress == request.dl_loc) {
                    calculatedMiles = request.miles === 0 ? await bing.getMiles(request.pu_loc, request.dl_loc) : request.miles;
                } else {
                    calculatedMiles = await bing.getMiles(request.pu_loc, request.dl_loc);
                }
            }

            queryParams = Drivermaster.updateDriverPayDataQuery([request.leg_type_id, request.pu_name,
            request.pu_loc, request.dl_name, request.dl_loc, request.container_number,
            request.chassis_number, calculatedMiles, request.pu_time, request.dl_time, request.is_paid,
            request.rate, request.isRateEdited, request.order_id, request.order_container_chassis_id,
            request.leg_number, request.is_charges_added, request.id]);

            executeQuery(queryParams.query, queryParams.params, function (err, queryResponse) {
                if (err) {
                    console.error(new Date(), `[driver] [UPDATE] DRIVER PAY DATA QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: 'DB ERROR' });
                }

                queryParams = Drivermaster.deleteDriverChargesQuery([request.id]);

                executeQuery(queryParams.query, queryParams.params, (deleteChargesErr, deleteQueryResponse) => {
                    if (deleteChargesErr) {

                        console.error(new Date(), "[driver] [DELETE] DRIVER CHARGES QUERY ERROR",
                            "sqlMessage" in deleteChargesErr ? deleteChargesErr.sqlMessage : deleteChargesErr);

                        client.del('driverPayData');
                        return response.status(409).json({ code: 409, error: 'DB DELETE ERROR' });
                    }

                    request.driverChargesArray = [];

                    request.driver_charges ? request.driver_charges.forEach(temp => {
                        if (temp.accessories_name) {
                            request.driverChargesArray.push([request.id, temp.accessories_name,
                            temp.rate]);
                        }
                    }) : null;

                    if (request.driverChargesArray.length) {

                        queryParams = Drivermaster.postDriverPayChargesDataQuery([
                            request.driverChargesArray]);

                        executeQuery(queryParams.query, queryParams.params,
                            (driverChargesErr, driverChargesResponse) => {

                                if (driverChargesErr) {

                                    console.error(new Date(), "[driver] [POST] DRIVER CHARGES QUERY ERROR",
                                        "sqlMessage" in driverChargesErr ? driverChargesErr.sqlMessage :
                                            driverChargesErr);

                                    client.del('driverPayData');
                                    client.del('orderSearchData');
                                    response.status(409).json({ code: 409, error: 'CHARGE DB ERROR' });

                                } else {
                                    client.del('driverPayData');
                                    client.del('orderSearchData');
                                    callback(null, 'Successfully Updated');
                                }

                            })
                    } else {
                        client.del('driverPayData');
                        client.del('orderSearchData');
                        callback(null, 'Successfully Updated');
                    }

                })

            })
        } catch (e) {
            console.error(new Date(), `[driver] [UPDATE] DRIVER PAY DATA CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Drivermaster.updateDriverPayData = function (request, response, options, callback) {
        let self = this;
        validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                self.updateDriverPayDataValidation(request, response, options, callback);
            }
        });
    }


    Drivermaster.deleteDriverPayDataQuery = function (params) {
        return {
            "query": "call deleteDriverPay(?,?,?);",
            "params": params
        };
    }

    Drivermaster.deleteDriverPayDataValidation = function (request, response, options, callback) {
        try {
            queryParams = Drivermaster.deleteDriverPayDataQuery([request.legNumber, request.containerLevelId,
            request.driverPayId]);

            executeQuery(queryParams.query, queryParams.params, function (err, queryResponse) {
                if (err) {
                    console.error(new Date(), `[driver] [DELETE] DRIVER PAY DATA QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: 'DB ERROR' });
                } else {

                    console.log(new Date(),
                        `[driver] [DELETE] DRIVER PAY ${request.containerLevelId} ${request.legNumber}`);

                    client.del('driverPayData');
                    client.del('orderSearchData');
                    callback(null, 'Successfully Deleted');
                }
            })
        } catch (e) {
            console.error(new Date(), `[driver] [DELETE] DRIVER PAY DATA CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Drivermaster.deleteDriverPayData = function (request, response, options, callback) {
        let self = this;
        validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                self.deleteDriverPayDataValidation(request, response, options, callback);
            }
        });
    }


    const postAttendenceDataQuery = (params) => {
        return {
            "query": "INSERT INTO driver_attendence(name,date_from,date_to,driver_attendence_category_id,driver_id) values(?,?,?,?,?)",
            "params": params
        };
    }


    const postAttendenceDataValidation = (request, response, options, callback) => {

        try {

            queryParams = postAttendenceDataQuery([request.name, request.date_from,
            request.date_to, request.attendence_category, request.driver_id]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[driver] [POST] DRIVER ATTENDENCE DATA QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: 'DB ERROR' });
                }

                callback(null, 'Successfully Inserted');
            })

        } catch (e) {
            console.error(new Date(), `[driver] [POST] DRIVER ATTENDENCE DATA CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Drivermaster.postAttendenceData = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                postAttendenceDataValidation(request, response, options, callback);
            }
        });
    }

    const updateAttendenceDataQuery = (params) => {
        return {
            "query": "UPDATE driver_attendence SET name = ?, date_from = ?, date_to = ?,driver_attendence_category_id = ? WHERE driver_attendence_id = ?",
            "params": params
        };
    }


    const updateAttendenceDataValidation = (request, response, options, callback) => {

        try {

            queryParams = updateAttendenceDataQuery([request.name, request.date_from,
            request.date_to, request.attendence_category, request.attendence_id]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[driver] [UPDATE] DRIVER ATTENDENCE DATA QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: 'DB ERROR' });
                }

                callback(null, 'Successfully Updated');
            })

        } catch (e) {
            console.error(new Date(), `[driver] [UPDATE] DRIVER ATTENDENCE DATA CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Drivermaster.updateAttendenceData = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                updateAttendenceDataValidation(request, response, options, callback);
            }
        });
    }

    const deleteAttendenceDataValidation = (request, response, options, callback) => {

        try {

            executeQuery("DELETE FROM driver_attendence WHERE driver_attendence_id = ?", [request.attendence_id],
                (err, queryResponse) => {
                    if (err) {
                        console.error(new Date(), `[driver] [DELETE] DRIVER ATTENDENCE DATA QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: 'DB ERROR' });
                    }

                    callback(null, 'Successfully Deleted');
                })

        } catch (e) {
            console.error(new Date(), `[driver] [DELETE] DRIVER ATTENDENCE DATA CODE ERROR`, e);

            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Drivermaster.deleteAttendenceData = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                deleteAttendenceDataValidation(request, response, options, callback);
            }
        });
    }

};
