'use strict';

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const moment = require('moment');
const bing = require('../../server/boot/bingapi');

const config = require('./../../server/boot/load-config').getConfig();

module.exports = function (Legmgmt) {

  let queryParams;

  //database connector
  const executeQuery = (query, params, callback) => {
    Legmgmt.dataSource.connector.query(query, params, callback);
  }

  //Access Token
  const validateAccessToken = (access_token, callback) => {
    Legmgmt.app.models.AccessToken.findById(access_token, callback)
  }

  //calling internally by loopback(overriden)
  Legmgmt.createOptionsFromRemotingContext = function (ctx) {
    var base = this.base.createOptionsFromRemotingContext(ctx);
    base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
    return base;
  }

  /** POST LEG DATA
   * @function postLegQuery used to return query string and desired params 
   * @function postLegManagementParsing perform native query to insert leg data and bing api operation and rate based on route 
   * @function postLegManagement root function to trigger the remote method and validate access token */

  const postLegQuery = (params) => {
    return {
      "query": `insert into leg_mgmt(chassis_number,container_number,dl_loc,dl_time,dmg,driver_id,est_miles,
        leg_number,leg_status,leg_type_id,order_id,pu_loc,pu_time,created_by,created_on,order_container_chassis_id,
        pu_name,dl_name,rate,pu_time_to,dl_time_to,is_brokered,carrier_id) 
        values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,current_timestamp,?,UCASE(?),UCASE(?),?,?,?,?,?)`,
      "params": params
    };
  }

  const postInbetweenLegQuery = (params) => {
    return {
      "query": `call postInbetweenLeg(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      "params": params
    };
  }

  const postLegManagementParsing = async (request, response, options, callback) => {

    try {

      let statusAndStreetTurnFlag = await fetchStatusAndStreenTurnFlag(response, request.order_id,
        request.order_container_chassis_id);

      if (statusAndStreetTurnFlag.order_status !== null &&
        statusAndStreetTurnFlag.order_status !== 'Delivered') {

        let calculatedMiles = await bing.getMiles(request.pu_loc, request.dl_loc);

        if (request.isAddLegInbetween) {
          let nextLegCalculatedMiles;

          if (request.dl_loc !== null && request.nextLegDeliveryLocation !== null) {
            nextLegCalculatedMiles = await bing.getMiles(request.dl_loc, request.nextLegDeliveryLocation);
          }

          queryParams = postInbetweenLegQuery([request.chassis_number, request.container_number, request.dl_loc,
          request.dl_time, request.driver_id, calculatedMiles, request.leg_number,
          request.is_brokered ? 'Assigned' : (!request.driver_id ? 'Open' : 'Assigned'), request.leg_type_id, request.order_id,
          request.pu_loc, request.pu_time, request.loggedInUser, request.order_container_chassis_id,
          request.pu_name, request.dl_name, 0, nextLegCalculatedMiles, 0,
          request.pu_time_to, request.dl_time_to, request.is_brokered, request.carrier_id]);

          executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
            if (err) {
              console.error(new Date(), `[leg] [POST] INBETWEEN LEG DATA QUERY ERROR`,
                "sqlMessage" in err ? err.sqlMessage : err);

              return response.status(409).json({ code: 409, error: "DB ERROR" });
            }

            client.del("driverDetailsData");
            if (request.isDispatch === 1) {
              client.del("ordersDispatchData");
              client.del("dispatchData");
            } else if (request.isDispatch === 0) {
              client.del("ordersPendingData");
            } else {
              client.del("invoiceData");
            }
            client.del("advanced_search");
            callback(null, { code: 200, data: "Successfully Leg Inserted Inbetween" });

          });

        } else {
          queryParams = postLegQuery([request.chassis_number, request.container_number, request.dl_loc,
          request.dl_time, request.dmg, request.driver_id, calculatedMiles, request.leg_number,
          request.is_brokered ? 'Assigned' : (!request.driver_id ? 'Open' : 'Assigned'), request.leg_type_id, request.order_id,
          request.pu_loc, request.pu_time, request.loggedInUser, request.order_container_chassis_id,
          request.pu_name, request.dl_name, 0, request.pu_time_to, request.dl_time_to,
          request.is_brokered, request.carrier_id]);

          executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
            if (err) {
              console.error(new Date(), `[leg] [POST] LEG DATA QUERY ERROR`,
                "sqlMessage" in err ? err.sqlMessage : err);

              return response.status(409).json({ code: 409, error: "DB ERROR" });
            }
            client.del("driverDetailsData");
            if (request.isDispatch === 1) {
              client.del("ordersDispatchData");
              client.del("dispatchData");
            } else if (request.isDispatch === 0) {
              client.del("ordersPendingData");
            } else {
              client.del("invoiceData");
            }
            client.del("advanced_search");
            callback(null, { code: 200, data: "Successfully Leg Inserted" });

          });
        }


      } else {
        callback(null, { code: 103, error: "Order Closed. Cannot Add New Leg" });
      }
    } catch (e) {
      console.error(new Date(), `[leg] [POST] LEG DATA CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Legmgmt.postLegManagement = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        postLegManagementParsing(request, response, options, callback);
      }
    });

  };

  /** FETCH LEG AND ACCESSORIES DATA
   * @function getLegAccessoriesQuery used to return query string and desired params 
   * @function getLegAccessoriesValidation perform native query and process the retrived data to send to client 
   * @function getLegAccessories root function to trigger the remote method and validate access token */

  const getLegAccessoriesQuery = (params) => {
    return {
      "query": "call getLegAccessories(?)",
      "params": params
    };
  }

  const getLegAccessoriesValidation = (containerLevelId, response, options, callback) => {
    let processedResponse = {};
    try {
      queryParams = getLegAccessoriesQuery([containerLevelId]);
      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        try {
          if (err) {
            console.error(new Date(), `[leg] [FETCH] LEG DATA QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            return response.status(409).json({ code: 409, error: "DB ERROR" });
          }

          queryResponse[0].forEach(temp => {
            processedResponse.legs = temp.legs !== null ?
              JSON.parse(temp.legs).sort((a, b) => a.leg_number - b.leg_number) : []; // parse leg string to json
            processedResponse.charges = temp.accessories !== null ? JSON.parse(temp.accessories) : [];
          });

          /** logic for the leg deadline based on status 
           * open or assigned - leg pickup time minus currrent time
           * picked up - delivery time minus pickup time */
          processedResponse.legs.forEach(temp => {

            temp.pu_time = temp.pu_time && new Date(temp.pu_time).getFullYear() !== 1970 ? temp.pu_time : null;

            temp.dl_time = temp.dl_time && new Date(temp.dl_time).getFullYear() !== 1970 ? temp.dl_time : null;

            temp.pu_time_to = temp.pu_time_to && new Date(temp.pu_time_to).getFullYear() !== 1970 ? temp.pu_time_to : null;

            temp.dl_time_to = temp.dl_time_to && new Date(temp.dl_time_to).getFullYear() !== 1970 ? temp.dl_time_to : null;

            if (temp.pu_time === temp.pu_time_to) {
              temp.pickup_time = temp.pu_time;
            } else {
              temp.pickup_time = null;
            }

            if (temp.dl_time === temp.dl_time_to) {
              temp.delivery_time = temp.dl_time;
            } else {
              temp.delivery_time = null;
            }

            if ((temp.leg_status === 'Open' || temp.leg_status === 'Assigned') && temp.pu_time !== null) {

              temp.timeDifference = moment(temp.pu_time).diff(moment(), 'seconds', true) > 0 ?
                moment(temp.pu_time).diff(moment(), 'seconds', true) : null;

            } else if (temp.leg_status === 'Picked Up' && temp.pu_time !== null && temp.dl_time !== null) {

              temp.timeDifference = moment(temp.dl_time).diff(moment(temp.pu_time), 'seconds', true) > 0 ?
                moment(temp.dl_time).diff(moment(temp.pu_time), 'seconds', true) : null;

            } else {

              temp.timeDifference = null;

            }

            if (temp.timeDifference !== null && temp.timeDifference > 0) {

              temp.days = Math.floor(temp.timeDifference / (24 * 60 * 60)).toString();

              temp.hours = Math.round(temp.timeDifference % (24 * 60 * 60) / 3600).toString();

              temp.isLeastTime = +temp.days === 0 && +temp.hours <= 3 ? true : false;

              temp.days = temp.days.length > 1 ? temp.days : 0 + temp.days;

              temp.hours = temp.hours.length > 1 ? temp.hours : 0 + temp.hours;

              temp.deadline = temp.days + "d " + temp.hours + "hr";

            } else {
              temp.deadline = null;
            }
          })


          callback(null, { code: 200, data: processedResponse });

        } catch (error) {
          console.error(new Date(), `[leg] [FETCH] LEG DATA CODE ERROR`, error);

          response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
      })
    } catch (e) {
      console.error(new Date(), `[leg] [FETCH] LEG DATA CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Legmgmt.getLegAccessories = (containerLevelId, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        getLegAccessoriesValidation(containerLevelId, response, options, callback);
      }
    });

  }

  /** UPDATE LEG DATA
  * @function updateLegManagementQuery used to return query string and desired params 
  * @function updateLegManagementParsing perform native query to update leg data and bing api operation and rate based on route 
  * @function updateLegManagement root function to trigger the remote method and validate access token */

  const updateLegManagementQuery = (params) => {
    return {
      "query": `call updateLegManagement(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);`,
      "params": params
    };
  }

  const updateLegManagementParsing = async (request, response, options, callback) => {

    try {

      let calculatedMiles;

      /* checking the flag to update user given miles 
      otherwise check for previous location changes and hit for the bing api to get miles */
      if (request.isMilesEditable) {
        calculatedMiles = request.miles;
      } else {
        if (request.previousPickupAddress == request.pu_loc && request.previousDeliveryAddress == request.dl_loc) {
          calculatedMiles = request.miles === 0 ? await bing.getMiles(request.pu_loc, request.dl_loc) : request.miles;
        } else {
          calculatedMiles = await bing.getMiles(request.pu_loc, request.dl_loc);
        }
      }

      queryParams = updateLegManagementQuery([request.chassis_number, request.container_number, request.dl_loc,
      request.dl_time, request.dmg, request.driver_id, calculatedMiles, request.loggedInUser,
      request.leg_number, request.leg_type_id, request.pu_loc, request.pu_time, request.pu_name,
      request.dl_name, 0, request.order_id, request.id, request.order_container_chassis_id,
      request.leg_status, request.pu_time_to, request.dl_time_to]);

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[leg] [UPDATE] LEG DATA QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          return response.status(409).json({ code: 409, error: "DB ERROR" })
        }
        client.del("driverDetailsData");
        client.del("driverPayData");
        if (request.isDispatch === 1) {
          client.del("ordersDispatchData");
          client.del("dispatchData");
        } else if (request.isDispatch === 0) {
          client.del("ordersPendingData");
        } else {
          client.del("invoiceData");
        }
        client.del("advanced_search");
        callback(null, { code: 200, data: "Successfully Updated" });

      });
    } catch (e) {
      console.error(new Date(), `[leg] [UPDATE] LEG DATA CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Legmgmt.updateLegManagement = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        updateLegManagementParsing(request, response, options, callback);
      }
    });

  };

  /** DELETE LEG DATA
  * @function deleteLegDataQuery used to return query string and desired params 
  * @function deleteLegDataParsing perform native query to delete leg data and reaarrange the leg number to sort 
  * @function deleteLegData root function to trigger the remote method and validate access token */

  const deleteLegDataQuery = (params) => {
    return {
      "query": "call delete_leg_sort(?,?,?,?)",
      "params": params
    };
  }

  const deleteLegDataParsing = async (request, response, options, callback) => {
    try {
      let statusAndStreetTurnFlag = await fetchStatusAndStreenTurnFlag(response, request.order_id,
        request.containerLevelId);

      if (statusAndStreetTurnFlag.order_status !== null &&
        statusAndStreetTurnFlag.order_status !== 'Delivered') {

        queryParams = deleteLegDataQuery([request.order_id, request.leg_number, request.containerLevelId,
        request.leg_id]);

        executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
          if (err) {
            console.error(new Date(), `[leg] [DELETE] LEG DATA QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            return response.status(409).json({ code: 409, error: "DB ERROR" });
          }
          console.log(new Date(), `[leg] [DELETE] LEG DATA DELETED ${request.containerLevelId} ${request.leg_number}`);

          client.del("driverDetailsData");
          client.del("driverPayData");
          if (request.isDispatch === 1) {
            client.del("ordersDispatchData");
            client.del("dispatchData");
          } else if (request.isDispatch === 0) {
            client.del("ordersPendingData");
          } else {
            client.del("invoiceData");
          }
          client.del("advanced_search");
          callback(null, { code: 200, data: "Deleted Successfully" });

        });

      } else {
        callback(null, { code: 103, error: "Order Closed. Cannot Delete Leg" });
      }
    } catch (e) {
      console.error(new Date(), `[leg] [DELETE] LEG DATA CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Legmgmt.deleteLegData = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        deleteLegDataParsing(request, response, options, callback);
      }
    });

  };

  /** POST HUB LEG DATA
   * @function postHubLegManagementQuery used to return query string and desired params 
   * @function postHubLegManagementParsing perform native query to insert hub leg after the leg data  
   * @function postHubLegManagement root function to trigger the remote method and validate access token */

  const postHubLegManagementQuery = (params) => {
    return {
      "query": "call postHubLegInbetweenOrAfterData(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
      "params": params
    };
  }

  const postHubLegManagementParsing = async (request, response, options, callback) => {
    try {
      let statusAndStreetTurnFlag = await fetchStatusAndStreenTurnFlag(response, request.order_id,
        request.order_container_chassis_id);

      if (statusAndStreetTurnFlag.order_status !== null &&
        statusAndStreetTurnFlag.order_status !== 'Delivered') {

        let calculatedMiles = await bing.getMiles(request.pu_loc, request.dl_loc);

        let calculatedPrevMiles = 0;

        if (request.isInbetween) {
          calculatedPrevMiles = await bing.getMiles(request.prev_loc, request.hub_loc);
        }

        queryParams = postHubLegManagementQuery([request.chassis_number, request.container_number,
        request.leg_number, request.previousStatus, request.leg_status, request.leg_type_id, request.pu_name,
        request.pu_loc, request.dl_name, request.dl_loc, request.hub_name, request.hub_loc, request.order_id,
        request.order_container_chassis_id, request.driver_id, calculatedMiles, 0, request.loggedInUser,
        request.isInbetween ? 1 : 0, request.leg_id, calculatedPrevMiles, 0, request.prev_loc,
        request.prev_name, request.prev_rate, request.prev_miles, request.pu_time, request.dl_time,
        request.dl_time_to, request.is_brokered, request.carrier_id]);

        executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
          if (err) {
            console.error(new Date(), `[leg] [POST] HUB LEG DATA QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            return response.status(409).json({ code: 409, error: "DB ERROR" });
          }

          console.log(new Date(),
            `[leg] [POST] HUB PERFORMED SUCCESSFULLY ON ${request.order_container_chassis_id} ${request.leg_number} ${request.previousStatus} ${request.leg_status}`);

          client.del("driverDetailsData");
          client.del("driverPayData");
          client.del("ordersDispatchData");
          client.del("dispatchData");
          client.del("advanced_search");
          callback(null, { code: 200, data: "Successfully Hub Data Inserted" });

        });

      } else {
        callback(null, { code: 103, error: "Order Closed. Cannot Make To Hub" });
      }
    } catch (e) {
      console.error(new Date(), `[leg] [POST] HUB LEG DATA CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Legmgmt.postHubLegManagement = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        postHubLegManagementParsing(request, response, options, callback);
      }
    });

  };

  /** POST BOBTAIL LEG DATA
   * @function postBobtailLegManagementQuery used to return query string and desired params 
   * @function postBobtailLegManagementParsing perform native query to insert bobtail leg before the leg data  
   * @function postBobtailLegManagement root function to trigger the remote method and validate access token */

  const postBobtailLegManagementQuery = (params) => {
    return {
      "query": "call postBobtailLegData(?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
      "params": params
    };
  }

  const postBobtailLegManagementParsing = async (request, response, options, callback) => {

    try {

      let calculatedMiles = await bing.getMiles(request.pu_loc, request.dl_loc);

      queryParams = postBobtailLegManagementQuery([request.chassis_number, request.container_number,
      request.driver_id, calculatedMiles, 0, request.leg_number, request.order_id,
      request.loggedInUser, request.pu_name, request.pu_loc, request.dl_name, request.dl_loc,
      request.order_container_chassis_id, request.leg_id]);

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[leg] [POST] BOBTAIL LEG DATA QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          return response.status(409).json({ code: 409, error: "DB ERROR" });
        }
        client.del("driverDetailsData");
        client.del("driverPayData");
        client.del("ordersDispatchData");
        client.del("dispatchData");
        client.del("advanced_search");
        callback(null, { code: 200, data: "successfully data inserted" });

      });
    } catch (e) {
      console.error(new Date(), `[leg] [POST] BOBTAIL LEG DATA QUERY ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Legmgmt.postBobtailLegManagement = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        postBobtailLegManagementParsing(request, response, options, callback);
      }
    });
  };

  /** REARRANGE LEG DATA
   * @function rearrangeLegNumberQuery used to return query string and desired params 
   * @function rearrangeLegNumberParsing perform native query to sort the leg number based on upward or downward 
   * @function rearrangeLegNumber root function to trigger the remote method and validate access token */

  const rearrangeLegNumberQuery = (params) => {
    return {
      "query": "call rearrangeLegNumber(?,?,?,?)",
      "params": params
    };
  }

  const rearrangeLegNumberParsing = (request, response, options, callback) => {
    try {
      queryParams = rearrangeLegNumberQuery([request.leg_number,
      request.isUp ? request.leg_number - 1 : request.leg_number + 1, request.order_id,
      request.order_container_chassis_id]);

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[leg] [UPDATE] REARRANGE LEG DATA QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          return response.status(409).json({ code: 409, error: "DB ERROR" });
        }
        client.del("driverDetailsData");

        if (request.isDispatch === 1) {
          client.del("ordersDispatchData");
          client.del("dispatchData");
        } else if (request.isDispatch === 0) {
          client.del("ordersPendingData");
        } else {
          client.del("invoiceData");
        }

        client.del("advanced_search");
        callback(null, { code: 200, data: "Successfully Rearranged" })

      });
    } catch (e) {
      console.error(new Date(), `[leg] [UPDATE] REARRANGE LEG DATA CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Legmgmt.rearrangeLegNumber = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        rearrangeLegNumberParsing(request, response, options, callback);
      }
    });

  };


  const checkPreviousLegStatusQuery = (params) => {
    return {
      "query": `SELECT lm.leg_status,lm.driver_id,concat(dm.first_name ,' ',dm.last_name) driver_name from leg_mgmt lm
                left join driver_master dm on 
                dm.driver_id = lm.driver_id 
                WHERE order_container_chassis_id = ? and leg_number = ?;`,
      "params": params
    };
  }

  const updateStatusAssignedQuery = (params) => {
    return {
      "query": "call updateLegStatusAssigned(?,?,?)",
      "params": params
    };
  }

  const updateStatusPickedUpQuery = (params) => {
    return {
      "query": "call update_leg_status_inprogress(?,?,?)",
      "params": params
    };
  }

  const updateStatusDeliveredLegQuery = (params) => {
    return {
      "query": "call updateLegStatusDelivered(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
      "params": params
    };
  }

  const checkDriverAvailabilityQuery = (params) => {
    return {
      "query": `SELECT
                    is_picked_up
                from
                    (
                        SELECT group_concat(DISTINCT driver_id) all_drivers,
                        GROUP_concat(DISTINCT leg_status) all_status,
                        LOCATE('Picked Up', GROUP_concat(DISTINCT leg_status)) is_picked_up,
                        order_container_chassis_id
                from
                    leg_mgmt
                WHERE
                    driver_id = ?
                GROUP by
                    order_container_chassis_id) checkDriver
                WHERE
                    is_picked_up <> 0;`,
      "params": params
    };
  }

  const checkDriverLastKnownLocationQuery = (params) => {
    return {
      "query": `select
                    driver_id,
                    SUBSTRING_INDEX(group_concat(dl_loc order by dl_time DESC), ',', 1) as last_known_location,
                    SUBSTRING_INDEX(group_concat(dl_name order by dl_time DESC), ',', 1) as last_known_location_name
                from
                    leg_mgmt
                where
                    leg_status = 'Delivered'
                    and driver_id = ?
                GROUP BY
                    driver_id;`,
      "params": params
    };
  }

  const checkDriverIsActiveQuery = (params) => {
    return {
      "query": `select is_deactivated from driver_master where driver_id = ?`,
      "params": params
    };
  }

  const fetchLegDataQuery = (params) => {
    return {
      "query": "select * from leg_mgmt where leg_id = ?",
      "params": params
    };
  }

  const updateLegStatusTopBottomParsing = async (request, response, options, callback) => {

    try {

      let statusAndStreetTurnFlag = await fetchStatusAndStreenTurnFlag(response, request.order_id,
        request.order_container_chassis_id);

      if (statusAndStreetTurnFlag.order_status &&
        statusAndStreetTurnFlag.order_status !== 'Delivered') { // to check whether order is delivered

        let fetchedPreviousData = await fetchLegStatusAndDriver(request, response); // to check the status of previous leg

        /* to change the status based on its previous status
        `Assigned` status don't required check for previous leg status */
        if (request.previousStatus === "Open" && request.currentStatus === "Assigned") {

          setOpenToAssignedStatus(request, response, options, callback);

          return;

        }

        /* in order to make the leg status `Picked Up` or `Delivered`
        system needs to check the previous leg status `Delivered` */
        if (request.leg_number === 1 || (fetchedPreviousData.length &&
          fetchedPreviousData[0].leg_status === 'Delivered')) { 

          if (request.previousStatus === "Assigned" && request.currentStatus === "Picked Up") {

            request.is_brokered ?
              setStatusPickedUp(request, response, callback) :
              setAssignedToPickedUpStatus(request, fetchedPreviousData[0], response, options, callback);

            return;

          }

          if (request.previousStatus === "Picked Up" && request.currentStatus === "Delivered") {

            setPickedUpToDeliveredStatus(request, response, options, callback);

            return;

          }

        }

        return callback(null, { code: 103, error: "Previous Leg Not Closed Yet" });

      }

      return callback(null, { code: 103, error: "Order Closed. Cannot Change Status" });

    } catch (e) {
      console.error(new Date(), `[leg] [UPDATE] CHANGE LEG STATUS CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  const setOpenToAssignedStatus = (request, response, options, callback) => {

    if (request.driver_id) {

      queryParams = checkDriverIsActiveQuery([request.driver_id]);

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[leg] [FETCH] CHECK DRIVER QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          return response.status(409).json({ code: 409, error: "DB ERROR ON CHECK" });
        }

        if (queryResponse[0].is_deactivated === 0) {

          queryParams = updateStatusAssignedQuery([request.order_id, request.leg_id,
          request.order_container_chassis_id]);

          executeQuery(queryParams.query, queryParams.params, (updateErr, updateQueryResponse) => {
            if (updateErr) {
              console.error(new Date(), `[leg] [UPDATE] LEG STATUS ASSIGNED QUERY ERROR`,
                "sqlMessage" in updateErr ? updateErr.sqlMessage : updateErr);

              return response.status(409).json({ code: 409, error: "DB ERROR" });
            }

            client.del("driverDetailsData");
            client.del("ordersDispatchData");
            client.del("dispatchData");
            client.del("advanced_search");
            callback(null, {
              code: 200,
              error: null,
              data: { currentLegStatus: null, containerStatus: null, nextLegStatus: null },
              legStatus: "Assigned"
            });

          })

        } else {
          callback(null, { code: 103, error: "Driver Is Inactive Please Try With Different Driver" });
        }
      })
    } else {
      callback(null, { code: 104, error: "Please Assign Driver" });
    }
  }

  const setAssignedToPickedUpStatus = async (request, previousData, response, options, callback) => {

    let checkDriverAvailability = await checkDriverStatus(request, response);

    if (!checkDriverAvailability.length) {

      if (request.leg_number === 1) {

        checkDriverLocationAndUpdate(request, response, callback);

        return;

      }

      previousData.driver_id === request.driver_id ?

        setStatusPickedUp(request, response, callback) :

        checkDriverLocationAndUpdate(request, response, callback);

      return;

    }

    return callback(null, { code: 103, error: 'Driver is on another order' });

  }

  const checkDriverLocationAndUpdate = async (request, response, callback) => {

    let driverLastKnownLocation = await checkDriverLastKnownLocation(request, response);

    if (!driverLastKnownLocation.length) {

      setStatusPickedUp(request, response, callback);

    } else if (driverLastKnownLocation.length &&
      (driverLastKnownLocation[0].last_known_location === request.pu_loc ||
        driverLastKnownLocation[0].last_known_location_name === request.pu_name)) {

      setStatusPickedUp(request, response, callback);

    } else {

      callback(null, { code: 105, error: "Open Confirm For Bobtail", data: driverLastKnownLocation });

    }

  }

  const checkDriverStatus = (request, response) => {
    return new Promise((resolve, reject) => {
      queryParams = checkDriverAvailabilityQuery([request.driver_id]);
      return executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[leg] [FETCH] DRIVER AVAILABILTY QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          response.status(409).json({ code: 409, error: "DB ERROR" })
        } else {
          resolve(queryResponse);
        }
      })
    })
  }

  const checkDriverLastKnownLocation = (request, response) => {
    return new Promise((resolve, reject) => {
      queryParams = checkDriverLastKnownLocationQuery([request.driver_id]);
      return executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[leg] [FETCH] DRIVER LAST LOCATION QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          response.status(409).json({ code: 409, error: "DB ERROR" })
        } else {
          resolve(queryResponse);
        }
      })
    })
  }

  const setStatusPickedUp = (request, response, callback) => {
    queryParams = updateStatusPickedUpQuery([request.order_id, request.leg_id, request.order_container_chassis_id]);

    executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
      if (err) {
        console.error(new Date(), `[leg] [UPDATE] LEG STATUS PICKEDUP QUERY ERROR`,
          "sqlMessage" in err ? err.sqlMessage : err);

        return response.status(409).json({ code: 409, error: "DB ERROR" });
      }
      client.del("driverDetailsData");
      client.del("ordersDispatchData");
      client.del("dispatchData");
      client.del("advanced_search");
      callback(null, {
        code: 200,
        error: null,
        data: { currentLegStatus: null, containerStatus: null, nextLegStatus: null },
        legStatus: "Picked Up"
      });
    })
  }

  const setPickedUpToDeliveredStatus = async (request, response, options, callback) => {
    let fetchRequest = await fetchLegData(request, response);

    let newLegMiles;

    if (request.chassis_address) {
      newLegMiles = await bing.getMiles(fetchRequest.dl_loc, request.chassis_address);
    }

    queryParams = updateStatusDeliveredLegQuery([fetchRequest.driver_id, fetchRequest.order_container_chassis_id,
    fetchRequest.order_id, fetchRequest.leg_number, fetchRequest.leg_type_id, fetchRequest.pu_name,
    fetchRequest.pu_loc, fetchRequest.dl_name, fetchRequest.dl_loc, fetchRequest.container_number,
    fetchRequest.chassis_number, fetchRequest.est_miles, fetchRequest.pu_time, fetchRequest.dl_time,
    fetchRequest.paid, fetchRequest.rate, request.leg_id, request.chassis_name, request.chassis_address,
      newLegMiles, request.chassis_address ? 1 : 0, fetchRequest.is_brokered, fetchRequest.carrier_id]);

    executeQuery(queryParams.query, queryParams.params, (err, currentLegResponse) => {
      try {

        if (err) {
          console.error(new Date(), `[leg] [UPDATE] LEG STATUS DELIVERED QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          return response.status(409).json({ code: 409, error: "DB ERROR" });
        }

        if (Array.isArray(currentLegResponse) && 'MESSAGE_TEXT' in currentLegResponse[0][0]) {

          console.error(new Date(), `[leg] [UPDATE] LEG STATUS DELIVERED QUERY ERROR`,
            currentLegResponse[0][0]);

          return response.status(409).json({ code: 409, error: "DB ERROR" });
        }

        console.log(new Date(),
          `[leg] [UPDATE] [DELIVERED] ON LEG ${fetchRequest.leg_number} ${fetchRequest.order_id}`,
          currentLegResponse);

        client.del("driverDetailsData");
        client.del("driverPayData");
        client.del("ordersDispatchData");
        client.del("dispatchData");
        client.del("advanced_search");

        callback(null, {
          code: 200,
          data: {
            currentLegStatus: null,
            containerStatus: currentLegResponse[0][0].containerStatus,
            nextLegStatus: "Not Updated"
          },
          legStatus: "Delivered"
        })

      } catch (error) {
        console.error(new Date(), `[leg] [UPDATE] LEG STATUS CHANGE CODE ERROR`, error);

        response.status(400).json({ code: 400, error: "CODE ERROR" });
      }
    })
  }

  const fetchLegData = (request, response) => {
    return new Promise((resolve, reject) => {
      queryParams = fetchLegDataQuery([request.leg_id]);
      return executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[leg] [FETCH] LEG DATA QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          response.status(409).json({ code: 409, error: "DB ERROR" });
        } else {
          resolve(queryResponse[0]);
        }
      })
    })
  }

  Legmgmt.updateLegStatusTopBottom = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        updateLegStatusTopBottomParsing(request, response, options, callback);
      }
    });

  };

  const undoDeliveredStatusUpdateLegQuery = (params) => {
    return {
      "query": "call undoDeliveredStatusUpdate(?,?,?,?,?,?)",
      "params": params
    };
  }

  const undoPickedUpStatusUpdateLegQuery = (params) => {
    return {
      "query": "call undoPickedUpStatusUpdate(?,?,?,?)",
      "params": params
    };
  }

  const undoAssignedStatusUpdateLegQuery = (params) => {
    return {
      "query": "call undoAssignedStatusUpdate(?,?,?)",
      "params": params
    };
  }

  const updateLegStatusBottomTopParsing = async (request, response, options, callback) => {

    try {

      let statusAndStreetTurnFlag = await fetchStatusAndStreenTurnFlag(response, request.order_id,
        request.order_container_chassis_id);

      if (statusAndStreetTurnFlag.order_status !== null &&
        statusAndStreetTurnFlag.order_status !== 'Delivered') {

        let fetchedNextLeg = await fetchLegStatusAndDriver(request, response);

        if (request.leg_number === request.lastLeg ||
          (fetchedNextLeg.length &&
            (fetchedNextLeg[0].leg_status !== 'Delivered' &&
              fetchedNextLeg[0].leg_status !== 'Picked Up'))) {

          if (request.previousStatus === "Delivered" && request.currentStatus === "Picked Up") {
            setDeliveredToPickedUpStatus(request, response, options, callback);
          } else if (request.previousStatus === "Picked Up" && request.currentStatus === "Assigned") {
            setPickedUpToAssignedStatus(request, response, options, callback);
          } else if (request.previousStatus === "Assigned" && request.currentStatus === "Open") {
            setAssignedToOpenStatus(request, response, options, callback);
          }

        } else {
          callback(null, { code: 103, error: "Next Leg Should Not Picked Up or Delivered to Perform Undo" })
        }

      } else {
        callback(null, { code: 103, error: "Order Closed. Cannot Change Status" })
      }
    } catch (e) {
      console.error(new Date(), `[leg] [UPDATE] LEG STATUS CHANGE BOTTOM TOP QUERY ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  const fetchLegStatusAndDriver = (request, response) => {
    return new Promise((resolve, reject) => {

      queryParams = checkPreviousLegStatusQuery([request.order_container_chassis_id,
      request.isTopBottom ? (request.leg_number - 1) : (request.leg_number + 1)]);

      return executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[leg] [FETCH] PREVIOUS LEG DATA QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          response.status(409).json({ code: 409, error: "DB ERROR" });
        } else {
          resolve(queryResponse);
        }
      })
    })
  }

  const setDeliveredToPickedUpStatus = (request, response, options, callback) => {
    queryParams = checkDriverIsActiveQuery([request.driver_id]);

    executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
      if (err) {
        console.error(new Date(), `[leg] [FETCH] CHECK DRIVER ACTIVE QUERY ERROR`,
          "sqlMessage" in err ? err.sqlMessage : err);

        return response.status(409).json({ code: 409, error: "DB ERROR ON CHECK" });
      }

      if ((queryResponse.length && 'is_deactivated' in queryResponse[0] &&
        queryResponse[0].is_deactivated === 0) || request.is_brokered) {

        queryParams = undoDeliveredStatusUpdateLegQuery([request.driver_id, request.order_container_chassis_id,
        request.order_id, request.leg_number, request.leg_id, request.is_brokered]);

        executeQuery(queryParams.query, queryParams.params, (updateErr, updateQueryResponse) => {
          if (updateErr) {
            console.error(new Date(), `[leg] [UPDATE] UNDO LEG STATUS DELIVERED QUERY ERROR`,
              "sqlMessage" in updateErr ? updateErr.sqlMessage : updateErr);

            return response.status(409).json({ code: 409, error: "DB ERROR" });
          }

          try {

            if (Array.isArray(updateQueryResponse) && 'MESSAGE_TEXT' in updateQueryResponse[0][0]) {
              console.error(new Date(), `[leg] [UPDATE] UNDO LEG STATUS DELIVERED QUERY ERROR`,
                updateQueryResponse[0][0]);

              return response.status(409).json({ code: 409, error: "DB ERROR" });
            }

            client.del("driverDetailsData");
            client.del("driverPayData");
            client.del("ordersDispatchData");
            client.del("dispatchData");
            client.del("advanced_search");

            if (updateQueryResponse[0][0].currentLegStatus.includes('DRIVER IN ROUTE')) {
              return callback(null, { code: 103, error: "Driver In Route in Another Order", legStatus: "Delivered" });
            }

            return callback(null, { code: 200, data: updateQueryResponse[0][0], legStatus: "Picked Up" });

          } catch (error) {

            return response.status(400).json({ code: 400, error: "CODE ERROR" });

          }

        })

      } else {
        callback(null, { code: 103, error: "Driver Is Inactive Please Try With Different Driver" });
      }
    })
  }

  const setPickedUpToAssignedStatus = (request, response, options, callback) => {
    queryParams = undoPickedUpStatusUpdateLegQuery([request.order_id, request.leg_id,
    request.order_container_chassis_id, request.isBobtailRemoved]);

    executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
      if (err) {
        console.error(new Date(), `[leg] [UPDATE] UNDO LEG STATUS PICKEDUP QUERY ERROR`,
          "sqlMessage" in err ? err.sqlMessage : err);

        return response.status(409).json({ code: 409, error: "DB ERROR" });
      }
      client.del("driverDetailsData");
      client.del("ordersDispatchData");
      client.del("dispatchData");
      client.del("advanced_search");
      callback(null, {
        code: 200,
        data: { currentLegStatus: null, containerStatus: null, nextLegStatus: null },
        legStatus: "Assigned"
      })

    })
  }

  const setAssignedToOpenStatus = (request, response, options, callback) => {
    queryParams = undoAssignedStatusUpdateLegQuery([request.order_id, request.leg_id,
    request.order_container_chassis_id]);

    executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
      if (err) {
        console.error(new Date(), `[leg] [UPDATE] UNDO LEG STATUS ASSIGNED QUERY ERROR`,
          "sqlMessage" in err ? err.sqlMessage : err);

        return response.status(409).json({ code: 409, error: "DB ERROR" });
      }

      client.del("driverDetailsData");
      client.del("ordersDispatchData");
      client.del("dispatchData");
      client.del("advanced_search");
      callback(null, {
        code: 200,
        data: { currentLegStatus: null, containerStatus: null, nextLegStatus: null },
        legStatus: "Open"
      })

    })
  }

  Legmgmt.updateLegStatusBottomTop = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        updateLegStatusBottomTopParsing(request, response, options, callback);
      }
    });

  };


  /** SPARSE UPDATE LEG DATA
   * @function sparseUpdateLegDataQuery used to return query string and desired params 
   * @function sparseUpdateLegDataParsing perform native query to update only certain column based on request
   * @function sparseUpdateLegData root function to trigger the remote method and validate access token */

  const sparseUpdateLegDriverDataQuery = (params) => {
    return {
      "query": "update leg_mgmt set driver_id = ? where order_container_chassis_id = ? and leg_id = ?",
      "params": params
    };
  }

  const sparseUpdateLegMilesAndRateDataQuery = (params) => {
    return {
      "query": "update leg_mgmt set est_miles = ?,rate = ? where order_container_chassis_id = ? and leg_id = ?",
      "params": params
    };
  }

  const sparseUpdateLegDataParsing = async (request, response, options, callback) => {

    try {

      if (request.sparseTitle === 'driver') {

        let statusAndStreetTurnFlag = await fetchStatusAndStreenTurnFlag(response, request.order_id,
          request.order_container_chassis_id);

        if (statusAndStreetTurnFlag.order_status !== null &&
          statusAndStreetTurnFlag.order_status !== 'Delivered') {

          queryParams = checkDriverIsActiveQuery([request.driver_id]);

          executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
            try {

              if (err) {
                return response.status(409).json({ code: 409, error: "DB ERROR ON CHECK" });
              }

              if (queryResponse[0].is_deactivated === 0) {

                driverSparseUpdate(request, response, callback);

              } else {

                callback(null, { code: 103, error: "Driver Is Inactive Please Try With Different Driver" });

              }

            } catch (error) {
              response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
          })

        } else {
          callback(null, { code: 103, error: "Order Closed. Cannot Assign Driver" })
        }
      } else {
        queryParams = sparseUpdateLegMilesAndRateDataQuery([request.miles, request.rate,
        request.order_container_chassis_id, request.leg_id]);

        executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
          if (err) {
            response.status(409).json({ code: 409, error: "DB ERROR" });
          } else {
            callback(null, { code: 200, error: null, data: "Updated Successfully" });
          }
        })
      }

    } catch (e) {
      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  const driverSparseUpdate = (request, response, callback) => {
    queryParams = sparseUpdateLegDriverDataQuery([request.driver_id, request.order_container_chassis_id,
    request.leg_id]);

    executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
      if (err) {
        response.status(409).json({ code: 409, error: "DB ERROR" });
      } else {
        queryParams = updateStatusAssignedQuery([request.order_id, request.leg_id,
        request.order_container_chassis_id]);

        executeQuery(queryParams.query, queryParams.params, (error, sparseQueryResponse) => {
          if (error) {
            client.del("driverDetailsData");
            client.del("ordersDispatchData");
            client.del("dispatchData");
            client.del("advanced_search");
            response.status(409).json({ code: 409, error: "DB ERROR STATUS NOT UPDATED" });
          } else {
            client.del("driverDetailsData");
            client.del("ordersDispatchData");
            client.del("dispatchData");
            client.del("advanced_search");
            callback(null, { code: 200, error: null, data: "Updated Successfully" });
          }
        })
      }
    });
  }

  Legmgmt.sparseUpdateLegData = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        sparseUpdateLegDataParsing(request, response, options, callback);
      }
    });

  };

  const getDriverRelatedLegQuery = (params) => {
    return {
      "query": "call driverRelatedLegs(?)",
      "params": params
    };
  }

  const getDriverRelatedLegDataParse = (driverId, response, options, callback) => {

    try {
      queryParams = getDriverRelatedLegQuery([driverId]);
      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        try {
          if (err) {
            response.status(409).json({ code: 409, error: 'DB ERROR' })
          } else {
            queryResponse[0].forEach(temp => {

              temp.pu_time = temp.pu_time !== null ?
                (temp.pu_time.toString().indexOf('0000-00-00 00:00:00') == 0 ?
                  null : temp.pu_time) : null;

              temp.dl_time = temp.dl_time !== null ?
                (temp.dl_time.toString().indexOf('0000-00-00 00:00:00') == 0 ?
                  null : temp.dl_time) : null;
            })

            callback(null, queryResponse[0]);
          }
        } catch (error) {
          response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
      })
    } catch (e) {
      response.status(400).json({ code: 400, error: 'CODE ERROR' });
    }
  }

  Legmgmt.getDriverRelatedLeg = (driverId, response, options, callback) => {
    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' })
      } else {
        options.tokenModel = tokenModel;
        getDriverRelatedLegDataParse(driverId, response, options, callback);
      }
    });
  }

  // get selected order data
  const performStreetTurnQuery = (params) => {
    return {
      "query": `call performStreetTurn(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      "params": params
    };
  }

  const performStreetTurnValidation = async (request, response, options, callback) => {
    try {
      request.order_status = "Delivered";
      request.isReadyToInvoice = 1;
      request.time = null;

      switch (request.flag) {
        case "Yes, end of business day":
          request.order_status = "Dispatch";
          request.time = new Date();
          break;
        case "No":
          request.order_status = "Dispatch";
          request.isReadyToInvoice = 0;
          break;
        default:
          break;
      }

      let calculatedMiles = await bing.getMiles(request.currentDeliveryLocation, request.newPickupLocation);

      let statusAndStreetTurnFlag = await fetchStatusAndStreenTurnFlag(response, request.curentOrderId,
        request.currentOrderContainerId);

      if (statusAndStreetTurnFlag.order_status !== null &&
        statusAndStreetTurnFlag.order_status !== 'Delivered' &&
        statusAndStreetTurnFlag.street_turned_to === null) {

        queryParams = performStreetTurnQuery([request.currentOrderContainerId,
        request.curentOrderId, request.currentLegNumber, request.order_status, request.isReadyToInvoice,
        request.currentDeliveryLocation, request.currentDeliveryLocName, request.newPickupLocation,
        request.newPickupLocName, request.currentContainerNumber, request.currentChassisNumber,
        request.currentDriverId, calculatedMiles, request.loggedInUser, request.newOrderContainerId,
        request.newOrderId, request.oldNotes, request.newNotes, request.notesDescription,
        request.loggedInUserId, request.time]);

        executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
          try {
            if (err) {
              console.error(new Date(), `[leg] [UPDATE] PERFORM STREET TURN QUERY ERROR`,
                "sqlMessage" in err ? err.sqlMessage : err);

              response.status(409).json({ code: 409, error: "DB ERROR" });
            } else {
              if (Array.isArray(queryResponse) && 'MESSAGE_TEXT' in queryResponse[0][0]) {

                console.error(new Date(), `[leg] [UPDATE] PERFORM STREET TURN QUERY ERROR`,
                  queryResponse[0][0]);

                return response.status(409).json({ code: 409, error: "DB ERROR" });
              }
              client.del("ordersDispatchData");
              client.del("dispatchData");
              client.del("orderSearchData");
              client.del("driverDetailsData");
              client.del("advanced_search");
              callback(null, { code: 200, data: "Street Turn Performed" });
            }
          } catch (error) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
          }
        })

      } else {
        callback(null, {
          code: 103,
          error: statusAndStreetTurnFlag.street_turned_to !== null ?
            "Order Already Street Turned" : (statusAndStreetTurnFlag.order_status === 'Delivered' ?
              "Order Closed. Cannot Street Turn" : "Something Went Wrong")
        })
      }
    } catch (e) {
      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Legmgmt.performStreetTurn = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        performStreetTurnValidation(request, response, options, callback);
      }
    });

  }


  // get selected order data
  const performBobtailToQuery = (params) => {
    return {
      "query": `call performBobtailTo(?,?,?,?,?,?,?,?,?,?,?)`,
      "params": params
    };
  }

  const performBobtailToValidation = async (request, response, options, callback) => {
    try {

      let calculatedMiles = await bing.getMiles(request.dl_loc, request.bobtail_to_loc);

      let statusAndStreetTurnFlag = await fetchStatusAndStreenTurnFlag(response, request.order_id,
        request.order_container_chassis_id);

      if (statusAndStreetTurnFlag.order_status !== null &&
        statusAndStreetTurnFlag.order_status !== 'Delivered' &&
        statusAndStreetTurnFlag.street_turned_to === null) {

        queryParams = performBobtailToQuery([request.driver_id, calculatedMiles, 0, request.leg_number,
        request.order_id, request.loggedInUser, request.dl_name, request.dl_loc, request.bobtail_to_name,
        request.bobtail_to_loc, request.order_container_chassis_id]);

        executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
          try {
            if (err) {
              console.error(new Date(), `[leg] [UPDATE] PERFORM BOBTAIL TO QUERY ERROR`,
                "sqlMessage" in err ? err.sqlMessage : err);

              response.status(409).json({ code: 409, error: "DB ERROR" });
            } else {
              if (Array.isArray(queryResponse) && 'MESSAGE_TEXT' in queryResponse[0][0]) {

                console.error(new Date(), `[leg] [UPDATE] PERFORM BOBTAIL TO QUERY ERROR`,
                  queryResponse[0][0]);

                return response.status(409).json({ code: 409, error: "DB ERROR" });
              }
              client.del("ordersDispatchData");
              client.del("dispatchData");
              client.del("orderSearchData");
              client.del("driverDetailsData");
              client.del("advanced_search");
              callback(null, { code: 200, data: "Bobtail To Performed" });
            }
          } catch (error) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
          }
        })

      } else {
        callback(null, {
          code: 103,
          error: statusAndStreetTurnFlag.street_turned_to !== null ?
            "Order Already Street Turned" : (statusAndStreetTurnFlag.order_status === 'Delivered' ?
              "Order Closed. Cannot Street Turn" : "Something Went Wrong")
        })
      }
    } catch (e) {
      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Legmgmt.performBobtailTo = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        performBobtailToValidation(request, response, options, callback);
      }
    });

  }

  const makeLegBrokeredQuery = (params) => {
    return {
      "query": `UPDATE leg_mgmt SET leg_status = 'Assigned',carrier_id = ?, carrier_rate = ?, is_brokered = 1 WHERE FIND_IN_SET(leg_id,?)`,
      "params": params
    };
  }

  const makeLegBrokeredProcess = async (request, response, options, callback) => {

    try {

      queryParams = makeLegBrokeredQuery([request.carrier_id, request.carrier_rate, request.leg_id]);

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[leg] [UPDATE] MAKE BROKERED QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          return response.status(409).json({ code: 409, error: "DB ERROR" })
        }

        executeQuery("UPDATE order_container_chassis SET order_status = ? WHERE order_container_chassis_id = ?",
          [request.order_status, request.id], (err, queryResponse) => {
            if (err) {
              console.error(new Date(), `[leg] [UPDATE] MAKE BROKERED QUERY ERROR`,
                "sqlMessage" in err ? err.sqlMessage : err);

              return response.status(409).json({ code: 409, error: "DB ERROR" })
            }

            client.del("ordersDispatchData");
            client.del("dispatchData");
            callback(null, { code: 200, data: "Successfully Updated" });
          });

      });
    } catch (e) {
      console.error(new Date(), `[leg] [UPDATE] MAKE BROKERED CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Legmgmt.makeLegBrokered = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      }

      options.tokenModel = tokenModel;
      makeLegBrokeredProcess(request, response, options, callback);

    });

  };

  Legmgmt.sendEmailToContainerOwner = (request, response, options, cb) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;

        const html = "<span>" + (request.container_number ? request.container_number.split("-").join("") : "--") + "</span> <br><br> <span>PCS " + (request.piece ? request.piece : "--") + "</span> <br> <span>WGH " + request.weight + " LBS</span> <br> <span>SEAL " + (request.seal_no ? request.seal_no : "--") + "</span> <br><br> <p>Thank you,<br />Mid Cities Motor Freight, Inc.<br />powered by Lotus TMS</p>";

        Legmgmt.app.models.Email.send({
          to: request.senderAddress,
          from: config.mail,
          subject: request.subject,
          html
        }, (mailErr, result) => {
          if (mailErr) {
            console.error(new Date(), "[leg] [SEND] MAIL ERROR", mailErr);

            response.status(409).json({ code: 409, error: "Error in sending mail" });
          } else {
            cb(null, { code: 200, data: "Successfully Mail Sent" });
          }
        })
      }
    });

  };

  const fetchStatusAndStreenTurnFlag = (response, orderLevelId, containerLevelId) => {
    return new Promise((resolve, reject) => {
      return executeQuery(`SELECT order_status,street_turned_to from
       order_container_chassis occ WHERE order_id = ? and order_container_chassis_id = ?;`,
        [orderLevelId, containerLevelId], (err, queryResponse) => {
          if (err) {
            console.error(new Date(), `[leg] [FETCH] STREET TURN LEG QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            response.status(409).json({ code: 409, error: "DB CHECK ERROR" });
          } else {
            resolve(queryResponse.length !== 0 ? queryResponse[0] :
              { order_status: null, street_turned_to: 0 })
          }
        })
    }).catch(err => {
      response.status(409).json({ code: 409, error: "DB FETCH PROMISE ERROR" });
    })
  }
};
