'use strict';

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const _ = require('lodash');
const bing = require('../../server/boot/bingapi');

module.exports = function (Routeratemaster) {

  let queryParams;

  /** EXECUTE NATIVE MYSQL QUERY
  * @param query 
  * @param params
  * @function callback  */
  const executeQuery = (query, params, callback) => {
    Routeratemaster.dataSource.connector.query(query, params, callback);
  }

  /** TO VALIDATE ACCESS TOKEN
   * @param access_token 
   * @function callback  */
  const validateAccessToken = (access_token, callback) => {
    Routeratemaster.app.models.AccessToken.findById(access_token, callback)
  }

  //calling internally by loopback(overriden)
  Routeratemaster.createOptionsFromRemotingContext = function (ctx) {
    var base = this.base.createOptionsFromRemotingContext(ctx);
    base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
    return base;
  }

  /** Native Mysql Query POST ROUTE RATE  
   * Required Input @param params */
  const postRouteRateQuery = (params) => {
    return {
      "query": "insert into route_rate_master (customer_id, start_loc, des_loc, rate, distance, order_type_id,pu_name,dl_name,format_address) values (?,?,?,?,?,?,UCASE(?),UCASE(?),?)",
      "params": params
    };
  }

  /** to post route on db */
  const postRouteRateOnDb = async (request, response, options, callback) => {
    try {
      let calculatedMiles = await bing.getMiles(request.start_loc, request.des_loc); // fetch miles from bing api

      queryParams = postRouteRateQuery([request.customer_id, request.start_loc,
      request.des_loc, request.rate, calculatedMiles, request.order_type_id, request.pu_name,
      request.dl_name, request.format_address]); // form query and param

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => { // post data to db
        if (err) {
          if (('code' in err) && err['code'] == 'ER_DUP_ENTRY') {
            callback(null, { code: 422, error: 'Route Already Exists' });
          } else {
            console.error(new Date(), `[route_rate] [POST] ROUTE RATE QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            response.status(409).json({ code: 409, error: "DB ERROR" });
          }
        } else {
          client.del('route_rate');
          callback(null, { code: 200, data: "route created successfully" });
        }
      });
    }
    catch (e) {
      console.error(new Date(), `[route_rate] [POST] ROUTE RATE CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  /* main api to trigger the post route rate */
  Routeratemaster.postRouteRate = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => { // validate the user access
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        postRouteRateOnDb(request, response, options, callback);
      }
    });

  }

  /** Native Mysql Query fetch General Route Data */
  const getGeneralRouteRateQuery = () => {
    return {
      "query": `SELECT
      rrm.customer_id,
      rrm.des_loc,
      rrm.distance,
      rrm.rate,
      rrm.route_rate_id,
      rrm.start_loc,
      rrm.dl_name,
      rrm.pu_name,
      rrm.format_address,
      otm.order_type,
      otm.order_type_id
    FROM
      route_rate_master rrm
    inner join order_type_master otm ON
      otm.order_type_id = rrm.order_type_id
    WHERE
      rrm.customer_id = 0;`
    };
  }

  /** fetch general route rate data from db and set in redis cache */
  const fetchGeneralRouteRateData = (request, res, section, options, callback) => {
    try {

      queryParams = getGeneralRouteRateQuery();

      executeQuery(queryParams.query, null, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), `[route_rate] [FETCH] GENERAL ROUTE RATE QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          res.status(409).json({ code: 409, error: 'DB ERROR' });
        } else {
          client.setex("route_rate", 3600, JSON.stringify(queryResponse)); // setting of data on redis cache

          section === 'getRouteId' ? performBulkUpdateOnDb(request, res, options, callback) :
            fetchRouteDataFromCache(request, res, options, callback);
        }
      });
    } catch (e) {
      console.error(new Date(), `[route_rate] [FETCH] GENERAL ROUTE RATE CODE ERROR`, e);

      res.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  /** retrieve data from cache and preprocessing of data like pagination,filter and sorting */
  const fetchRouteDataFromCache = (request, res, options, callback) => {
    let routeParsedData = [];
    let response;

    client.get("route_rate", (err, cacheData) => {
      try {
        if (err) {
          console.error(new Date(), `[route_rate] [FETCH] GENERAL ROUTE RATE REDIS ERROR`, err);

          return res.status(409).json({ code: 409, error: 'REDIS ERROR' });
        } else {
          if (cacheData) {
            routeParsedData = JSON.parse(cacheData); // parse the retrieve

            if (request.columnFilter) { // to perform column wise filter

              Object.keys(request.columnFilterValue).forEach(element => {

                request.columnFilterValue[element] = request.columnFilterValue[element]
                  .toLowerCase().replace(/[^a-zA-Z0-9]+/ig, "");

                routeParsedData = routeParsedData.length !== 0 ?
                  routeParsedData.filter(temp => {
                    return request.columnFilterValue[element] !== "" ?
                      (
                        element === 'start_loc' || element === 'des_loc' ?
                          (
                            temp[element] !== null ?
                              temp[element].toString().toLowerCase().replace(/[^a-zA-Z0-9]+/ig, "")
                                .includes(request.columnFilterValue[element]) : false
                          ) :
                          (
                            temp[element] !== null ?
                              temp[element].toString().toLowerCase().replace(/[^a-zA-Z0-9]+/ig, "")
                                .indexOf(request.columnFilterValue[element]) === 0 : false
                          )
                      ) : true
                  }) : []
              });

            }

            response = {
              code: 200,
              error: null,
              data: formatRouteData(request, routeParsedData) // format the route data for pagination, sorting
            }

            callback(null, response);
          } else {
            fetchGeneralRouteRateData(request, res, 'getRouteData', options, callback);
          }
        }
      } catch (error) {
        console.error(new Date(), `[route_rate] [FETCH] GENERAL ROUTE RATE CODE ERROR`, error);

        res.status(400).json({ code: 400, error: "CODE ERROR" });
      }
    });
  }

  /** format the route data for pagination, sorting  */
  const formatRouteData = (request, routeParsedData) => {
    let routeData;
    let routePaginationLength;

    if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {
      routeData = routeParsedData.length !== 0 ?
        _.orderBy(routeParsedData, request.column, request.direction).slice((request.offset - 1), request.limit)
        : [];
    } else {
      routeData = routeParsedData.length !== 0 ?
        _.orderBy(routeParsedData, 'route_rate_id', 'desc').slice((request.offset - 1), request.limit)
        : [];
    }

    routePaginationLength = routeParsedData.length;

    return { routeData, routePaginationLength }
  }

  Routeratemaster.getGeneralRouteRateDetails = (request, res, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        res.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchRouteDataFromCache(request, res, options, callback);
      }
    });

  }

  /** Native Mysql Query UPDATE ROUTE RATE  
   * Required Input @param params */
  const updateRouteRateQuery = (params) => {
    return {
      "query": "update route_rate_master set customer_id=?, start_loc=?, des_loc=?, rate=?, distance=?, order_type_id=? ,pu_name = UCASE(?),dl_name=UCASE(?),format_address = ? where route_rate_id=?",
      "params": params
    };
  }

  /** to update route data on database */
  const updateRouteRateOnDb = async (request, response, options, callback) => {
    try {
      let calculatedMiles;

      if (request.isMilesEditable) { // if the user directly edit the miles
        calculatedMiles = request.distance;
      } else {
        if (request.previousPickupAddress == request.start_loc &&
          request.previousDeliveryAddress == request.des_loc) { //fetch miles from bing only if the address is not edited

          calculatedMiles = request.distance === 0 ?
            await bing.getMiles(request.start_loc, request.des_loc) : request.distance;
        } else {

          calculatedMiles = await bing.getMiles(request.start_loc, request.des_loc);
        }
      }

      queryParams = updateRouteRateQuery([request.customer_id, request.start_loc,
      request.des_loc, request.rate, calculatedMiles, request.order_type_id, request.pu_name, request.dl_name,
      request.format_address, request.route_rate_id]); // form query and params

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => { // update route data
        if (err) {
          if (('code' in err) && err['code'] == 'ER_DUP_ENTRY') {
            callback(null, { code: 422, error: 'Route Already Exists' })
          } else {
            console.error(new Date(), `[route_rate] [UPDATE] ROUTE RATE QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            response.status(409).json({ code: 409, error: "DB ERROR" });
          }
        } else {
          client.del('route_rate');
          callback(null, { code: 200, data: "route updated successfully" });
        }
      });
    } catch (e) {
      console.error(new Date(), `[route_rate] [UPDATE] ROUTE RATE CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  /* main function to trigger the api */
  Routeratemaster.updateRouteRate = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        updateRouteRateOnDb(request, response, options, callback);
      }
    });

  }

  /** to delete route rate data on db */
  const deleteRouteRateOnDb = (route_rate_id, response, options, callback) => {
    try {

      executeQuery("delete from route_rate_master where route_rate_id=?", [route_rate_id],
        (err, queryResponse) => {
          if (err) {
            console.error(new Date(), `[route_rate] [DELETE] ROUTE RATE QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            response.status(409).json({ code: 409, error: "DB ERROR" });
          } else {
            client.del('route_rate');
            callback(null, { code: 200, data: "successfully deleted" });
          }
        });
    } catch (e) {
      console.error(new Date(), `[route_rate] [DELETE] ROUTE RATE CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  /* main function to trigger the delete route rate api */
  Routeratemaster.deleteGeneralRouteRate = (route_rate_id, response, options, callback) => {
    /* validate the user access */
    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        deleteRouteRateOnDb(route_rate_id, response, options, callback);
      }
    });
  }

  /** Native Mysql Query BULK UPDATE  
   * Required Input @param params */
  const updateBulkRateQuery = (params) => {
    return {
      "query": "call rate_bulk_update(?,?,?,?,?)",
      "params": params
    };
  }

  /** to Peroform bulk update on route rate */
  const performBulkUpdateOnDb = async (request, response, options, callback) => {
    try {

      /* server side selection of all routes id for bulk update */
      if (request.isAllRouteSelected) {
        request.route_rate = await fetchAllRouteRateId(request, response, callback);
      }

      queryParams = updateBulkRateQuery([request.is_increase, request.increase_type, request.amount,
      request.customer_id, request.route_rate]); // form query and params

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => { // execute native query
        if (err) {
          console.error(new Date(), `[route_rate] [UPDATE] BULK RATE QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          response.status(409).json({ code: 409, error: "DB ERROR" });
        } else {
          client.del('route_rate');
          callback(null, queryResponse.length !== 0 ? queryResponse[0] : [])
        }
      });
    } catch (e) {
      console.error(new Date(), `[route_rate] [UPDATE] BULK RATE CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  /** to retreive all route rate id from database for selected customer id */
  const fetchAllRouteRateId = (request, response, callback) => {
    return new Promise((resolve, reject) => {
      let routeParsedData = [];

      return client.get("route_rate", (err, cacheData) => {
        try {
          if (err) {
            console.error(new Date(), `[route_rate] [FETCH] ROUTE RATE ID REDIS ERROR`, err);

            return resolve(null);
          }
          if (cacheData) {
            routeParsedData = JSON.parse(cacheData); // parse the retrieve

            if (request.columnFilter) { // to perform column wise filter

              Object.keys(request.columnFilterValue).forEach(element => {

                request.columnFilterValue[element] = request.columnFilterValue[element]
                  .toLowerCase().replace(/[^a-zA-Z0-9]+/ig, "");

                routeParsedData = routeParsedData.length !== 0 ?
                  routeParsedData.filter(temp => {
                    return request.columnFilterValue[element] !== "" ?
                      (
                        element === 'start_loc' || element === 'des_loc' ?
                          (
                            temp[element] !== null ?
                              temp[element].toString().toLowerCase().replace(/[^a-zA-Z0-9]+/ig, "")
                                .includes(request.columnFilterValue[element]) : false
                          ) :
                          (
                            temp[element] !== null ?
                              temp[element].toString().toLowerCase().replace(/[^a-zA-Z0-9]+/ig, "")
                                .indexOf(request.columnFilterValue[element]) === 0 : false
                          )
                      ) : true
                  }) : []
              });

            }

            let routeRateIds = routeParsedData.map(temp => temp.route_rate_id)
              .filter(temp => !request.unSelectedId.includes(temp)).join(',');

            resolve(routeRateIds);

          } else {
            fetchGeneralRouteRateData(request, response, 'getRouteId', null, callback);
          }
        } catch (error) {
          console.error(new Date(), `[route_rate] [FETCH] ROUTE RATE ID CODE ERROR`, error);

          resolve(null);
        }
      });
    }).catch(err => {
      console.error(new Date(), `[route_rate] [CATCH] ROUTE RATE ID PROMISE ERROR`, err);

      response.status(409).json({ code: 409, error: "PROMISE ERROR" });
    })
  }

  /* main function to trigger api call */
  Routeratemaster.updateBulkRate = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        performBulkUpdateOnDb(request, response, options, callback);
      }
    });

  };
};
