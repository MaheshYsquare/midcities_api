'use strict';

const _ = require('lodash')

module.exports = function (Noteshistories) {

    //Datasource connector
    Noteshistories.executeQuery = function (ds, query, params, callback) {
        ds.connector.query(query, params, callback);
    }

    //Access Token
    Noteshistories.validateAccessToken = function (access_token, callback) {
        this.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Noteshistories.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    //Get general Accessories names
    Noteshistories.getNotesQuery = function (params) {
        return {
            "query": "select * from notes_history where order_container_chassis_id=?",
            "params": params

        };
    }

    Noteshistories.getNotesValidation = function (order_container_chassis_id, response, options, callback) {
        var ds = Noteshistories.dataSource;
        var queryParams;
        try {
            queryParams = Noteshistories.getNotesQuery([order_container_chassis_id]);

            Noteshistories.executeQuery(ds, queryParams.query, queryParams.params,
                function (err, queryResponse) {
                    try {
                        if (err) {
                            console.error(new Date(), '[notes] [FETCH] DB ERROR', err);

                            return response.status(409).json({ code: 409, error: "DB ERROR" });
                        }

                        let finalResponse = {
                            "A": queryResponse.filter(temp => temp.notes_description === 'order_notes'),
                            "B": queryResponse.filter(temp => temp.notes_description === 'dispatch_notes'),
                            "C": queryResponse.filter(temp => temp.notes_description === 'invoice_notes')
                        }

                        callback(null, finalResponse);
                    } catch (e) {
                        console.error(new Date(), '[notes] [FETCH] CB CODE ERROR', e);

                        return response.status(400).json({ code: 400, error: "CODE ERROR" });
                    }
                })
        } catch (e) {
            console.error(new Date(), '[notes] [FETCH] CODE ERROR', e);

            return response.status(400).json({ code: 400, error: "CODE ERROR" });
        }

    }

    Noteshistories.getNotes = function (order_container_chassis_id, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getNotesValidation(order_container_chassis_id, response, options, callback);
            }
        });
    }

    Noteshistories.postNotesQuery = function (params) {
        return {
            "query": "INSERT INTO notes_history(notes,notes_description,created_by,created_on,order_container_chassis_id,user_id) VALUES(?,?,?,CURRENT_TIMESTAMP,?,?);",
            "params": params
        };
    }

    Noteshistories.postNotesParsing = function (data, response, options, callback) {
        var ds = Noteshistories.dataSource;
        var queryParams;
        try {
            var postdata = JSON.stringify(data);
            var parsedData = JSON.parse(postdata);
            var notes = parsedData.notes;
            var notes_description = parsedData.notes_description;
            var created_by = parsedData.created_by;
            var order_container_chassis_id = parsedData.order_container_chassis_id;
            var user_id = parsedData.user_id;

            queryParams = Noteshistories.postNotesQuery([notes, notes_description, created_by,
                order_container_chassis_id, user_id]);

            Noteshistories.executeQuery(ds, queryParams.query, queryParams.params,
                function (err, queryResponse) {
                    if (err) {
                        response.status(409).json({ code: 409, error: "DB ERROR" });
                    } else {
                        callback(null, { code: 200, data: "Successfully Inserted" });
                    }
                });

        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Noteshistories.postNotes = function (data, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.postNotesParsing(data, response, options, callback);
            }
        });
    };

    Noteshistories.putNotesQuery = function (params) {
        return {
            "query": "UPDATE notes_history SET notes = ?,notes_description = ?,lastupdated_by = ?,lastupdated_on = CURRENT_TIMESTAMP WHERE notes_id = ? and order_container_chassis_id = ?;",
            "params": params
        };
    }

    Noteshistories.putNotesParsing = function (data, response, options, callback) {
        var ds = Noteshistories.dataSource;
        var queryParams;
        try {
            var postdata = JSON.stringify(data);
            var parsedData = JSON.parse(postdata);
            var notes = parsedData.notes;
            var notes_description = parsedData.notes_description;
            var lastupdated_by = parsedData.lastupdated_by;
            var notes_id = parsedData.notes_id;
            var order_container_chassis_id = parsedData.order_container_chassis_id;

            queryParams = Noteshistories.putNotesQuery([notes, notes_description,
                lastupdated_by, notes_id, order_container_chassis_id]);

            Noteshistories.executeQuery(ds, queryParams.query, queryParams.params,
                function (err, queryResponse) {
                    if (err) {
                        response.status(409).json({ code: 409, error: "DB ERROR" });
                    } else {
                        callback(null, { code: 200, data: "Successfully Updated" });
                    }
                });
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Noteshistories.putNotes = function (data, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.putNotesParsing(data, response, options, callback);
            }
        });
    };

    //Delete Notes Histories
    Noteshistories.deleteNotesQuery = function (params) {
        return {
            "query": "DELETE FROM notes_history WHERE notes_id = ? AND order_container_chassis_id = ?;",
            "params": params
        };
    }

    Noteshistories.deleteNotesValidation = function (notes_id, order_container_chassis_id, response, options, callback) {
        var ds = Noteshistories.dataSource;
        var queryParams;
        try {
            queryParams = Noteshistories.deleteNotesQuery([notes_id, order_container_chassis_id]);

            Noteshistories.executeQuery(ds, queryParams.query, queryParams.params,
                function (err, queryResponse) {
                    if (err) {
                        response.status(409).json({ code: 409, error: "DB ERROR" });
                    } else {
                        callback(null, { code: 200, data: "Successfully Deleted" });
                    }
                })

        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Noteshistories.deleteNotes = function (notes_id, order_container_chassis_id, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.deleteNotesValidation(notes_id, order_container_chassis_id, response, options, callback);
            }
        });
    }
}