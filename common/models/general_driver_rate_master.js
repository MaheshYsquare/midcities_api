'use strict';

const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const _ = require('lodash')

module.exports = function (Generaldriverratemaster) {

    //Datasource connector
    Generaldriverratemaster.executeQuery = function (ds, query, params, callback) {
        ds.connector.query(query, params, callback);
    }

    //Access Token
    Generaldriverratemaster.validateAccessToken = function (access_token, callback) {
        this.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Generaldriverratemaster.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    //Get general driver rates
    Generaldriverratemaster.getGeneralDriverRatesQuery = function () {
        return {
            "query": "call getGeneralDriverRates();"
        };
    }

    Generaldriverratemaster.getGeneralDriverRatesValidation = function (request, response, options, callback) {
        var ds = Generaldriverratemaster.dataSource;
        var queryParams;
        try {
            queryParams = Generaldriverratemaster.getGeneralDriverRatesQuery();
            Generaldriverratemaster.executeQuery(ds, queryParams.query, null,
                function (err, queryResponse) {
                    try {
                        if (err) {
                            return response.status(409).json({ code: 409, error: "DB ERROR" });
                        } else {
                            queryResponse[0].forEach(temp => {
                                temp.rates_per_mile = temp.rates_per_mile !== null ?
                                    JSON.parse(temp.rates_per_mile) : [];

                                temp.rates_per_mile.forEach(elem => {
                                    elem.range_from = +elem.range_from;
                                    elem.range_to = elem.range_to !== null ? +elem.range_to : null;
                                })

                                temp.rates_per_mile = _.orderBy(temp.rates_per_mile,
                                    ['range_from', 'range_to'], ['asc', 'asc'])
                            })

                            client.setex("general_driver_rates", 3600, JSON.stringify(queryResponse[0]));

                            Generaldriverratemaster.getGeneralDriverRatesCache(request, response, options, callback);
                        }
                    } catch (error) {
                        response.status(400).json({ code: 400, error: "CODE ERROR" });
                    }
                });
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Generaldriverratemaster.getGeneralDriverRatesCache = function (request, response, options, callback) {
        let generalDriverRatesParsedData = [];
        let redisResponse;

        client.get("general_driver_rates", (err, cacheData) => {
            try {
                if (err) {
                    return response.status(409).json({ code: 409, error: "REDIS ERROR" });
                } else {
                    if (cacheData) {
                        generalDriverRatesParsedData = JSON.parse(cacheData);

                        if (request.columnFilter) {
                            Object.keys(request.columnFilterValue).forEach(element => {
                                request.columnFilterValue[element] = request.columnFilterValue[element]
                                    .toLowerCase().replace(/[^a-zA-Z0-9.]+/ig, "")

                                generalDriverRatesParsedData = generalDriverRatesParsedData.length !== 0 ?
                                    generalDriverRatesParsedData.filter(temp => {
                                        return request.columnFilterValue[element] !== "" ?
                                            (
                                                temp[element] !== null ?
                                                    temp[element].toString()
                                                        .toLowerCase()
                                                        .replace(/[^a-zA-Z0-9.]+/ig, "")
                                                        .indexOf(
                                                            request.columnFilterValue[element]
                                                        ) === 0
                                                    : false
                                            )
                                            : true
                                    }) : []
                            });
                        }

                        redisResponse = {
                            code: 200,
                            error: null,
                            data: formatGeneralDriverRatesData(request, generalDriverRatesParsedData)
                        }

                        callback(null, redisResponse);
                    } else {
                        Generaldriverratemaster.getGeneralDriverRatesValidation(request, response,
                            options, callback);
                    }
                }
            } catch (error) {
                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        });
    }

    const formatGeneralDriverRatesData = (request, generalDriverRatesParsedData) => {
        let generalDriverRatesData;
        let generalDriverRatesPaginationLength;

        if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {
            generalDriverRatesData = generalDriverRatesParsedData.length !== 0 ? _.orderBy(generalDriverRatesParsedData, request.column, request.direction).slice((request.offset - 1), request.limit) : [];
        } else {
            generalDriverRatesData = generalDriverRatesParsedData.length !== 0 ? _.orderBy(generalDriverRatesParsedData, 'driver_rate_id', 'desc').slice((request.offset - 1), request.limit) : [];
        }

        generalDriverRatesPaginationLength = generalDriverRatesParsedData.length;

        return { generalDriverRatesData, generalDriverRatesPaginationLength }
    }

    Generaldriverratemaster.getGeneralDriverRates = function (request, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getGeneralDriverRatesCache(request, response, options, callback);
            }
        });
    }

    //post general driver rates
    Generaldriverratemaster.postGeneralDriverRatesQuery = function (params) {
        return {
            "query": "INSERT INTO general_driver_rate_master (driver_type_id, leg_type_id) VALUES (?,?)",
            "params": params
        };
    }

    //post general driver rates
    Generaldriverratemaster.postGeneralDriverRatesPerMileQuery = function (params) {
        return {
            "query": "INSERT INTO general_driver_rates_per_mile (driver_rate_id, range_from, range_to, rate,fixed_rate) VALUES ?",
            "params": params
        };
    }

    Generaldriverratemaster.postGeneralDriverRatesValidation = function (data, response, options, callback) {
        var ds = Generaldriverratemaster.dataSource;
        var queryParams;
        try {
            var canceldata = JSON.stringify(data);
            var parsedData = JSON.parse(canceldata);
            var driver_type_id = parsedData.driver_type_id;
            var leg_type_id = parsedData.leg_type_id;
            var ratesPerMiles = parsedData.rates_per_mile;
            let ratesArray = [];

            ds.connector.beginTransaction(Generaldriverratemaster.Transaction.REPEATABLE_READ,
                (transactionErr, transaction) => {

                    if (transactionErr) {
                        console.error(new Date(), `[driver_rate] [TRANSACTION] ERROR`, transactionErr);

                        response.status(409).json({ code: 409, error: "DB TRANSACTION ERROR" });
                    } else {
                        queryParams = Generaldriverratemaster.postGeneralDriverRatesQuery([driver_type_id,
                            leg_type_id]);

                        transaction.query(queryParams.query, queryParams.params,
                            function (postErr, postQueryResponse) {

                                if (postErr) {

                                    transaction.rollback(() => {
                                        transaction.release();
                                        if (('code' in postErr) && postErr['code'] == 'ER_DUP_ENTRY') {
                                            callback(null, {
                                                code: 409,
                                                error: 'Rate Already Given For this Driver Type and Leg Type'
                                            })
                                        } else {
                                            response.status(409).json({ code: 409, error: "DB POST ERROR" });
                                        }
                                    })

                                } else {

                                    ratesPerMiles.forEach(temp => {
                                        ratesArray.push([postQueryResponse.insertId, temp.range_from,
                                        temp.range_to, temp.rate, temp.fixed_rate]);
                                    });

                                    queryParams = Generaldriverratemaster.postGeneralDriverRatesPerMileQuery([
                                        ratesArray]);

                                    transaction.query(queryParams.query, queryParams.params,
                                        function (postRatesErr, postRatesQueryResponse) {
                                            if (postRatesErr) {
                                                transaction.rollback((rollbackErr) => {
                                                    transaction.release();
                                                    response.status(409).json({ code: 409, error: "DB POST RATES ERROR" });
                                                })
                                            } else {
                                                transaction.commit((commitErr) => {
                                                    if (commitErr) {
                                                        transaction.rollback(() => {
                                                            console.error(new Date(), `[driver_rate] [COMMIT] ERROR`,
                                                                commitErr);

                                                            transaction.release();

                                                            response.status(409).json({
                                                                code: 409,
                                                                error: "DB COMMIT ERROR"
                                                            });
                                                        })
                                                    } else {
                                                        transaction.release();
                                                        client.del('general_driver_rates');
                                                        client.del('driverPayData');
                                                        callback(null, {
                                                            code: 200,
                                                            data: "successfully inserted"
                                                        });
                                                    }
                                                })
                                            }
                                        })
                                }
                            });
                    }

                })

        }
        catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Generaldriverratemaster.postGeneralDriverRates = function (data, response, options, cb) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.postGeneralDriverRatesValidation(data, response, options, cb);
            }
        });
    }

    //update general driver rates
    Generaldriverratemaster.updateGeneralDriverRatesQuery = function (params) {
        return {
            "query": "UPDATE general_driver_rate_master SET driver_type_id = ?, leg_type_id = ? WHERE driver_rate_id = ?",
            "params": params
        };
    }

    Generaldriverratemaster.deleteGeneralDriverRatesPerMileQuery = function (params) {
        return {
            "query": "DELETE FROM general_driver_rates_per_mile WHERE driver_rate_id = ?",
            "params": params
        };
    }

    Generaldriverratemaster.updateGeneralDriverRatesValidation = function (data, response, options, cb) {
        var ds = Generaldriverratemaster.dataSource;
        var queryParams;
        try {
            var canceldata = JSON.stringify(data);
            var parsedData = JSON.parse(canceldata);
            var driver_type_id = parsedData.driver_type_id;
            var leg_type_id = parsedData.leg_type_id;
            var ratesPerMiles = parsedData.rates_per_mile;
            var driverRateId = parsedData.driver_rate_id;
            var ratesArray = [];

            ds.connector.beginTransaction(Generaldriverratemaster.Transaction.REPEATABLE_READ,
                (transactionErr, transaction) => {

                    if (transactionErr) {
                        console.error(new Date(), `[driver_rate] [TRANSACTION] ERROR`, transactionErr);

                        response.status(409).json({ code: 409, error: "DB TRANSACTION ERROR" });
                    } else {
                        queryParams = Generaldriverratemaster.updateGeneralDriverRatesQuery([driver_type_id,
                            leg_type_id, driverRateId]);

                        transaction.query(queryParams.query, queryParams.params,
                            function (putErr, putQueryResponse) {

                                if (putErr) {

                                    transaction.rollback(() => {
                                        transaction.release();
                                        if (('code' in putErr) && putErr['code'] == 'ER_DUP_ENTRY') {
                                            cb(null, {
                                                code: 409,
                                                error: 'Rate Already Given For this Driver Type and Leg Type'
                                            })
                                        } else {
                                            response.status(409).json({ code: 409, error: "DB PUT ERROR" });
                                        }
                                    })

                                } else {

                                    queryParams = Generaldriverratemaster.deleteGeneralDriverRatesPerMileQuery([
                                        driverRateId]);

                                    transaction.query(queryParams.query, queryParams.params,
                                        function (deleteRatesErr, deleteRatesQueryResponse) {

                                            if (deleteRatesErr) {
                                                transaction.rollback((rollbackErr) => {
                                                    transaction.release();
                                                    response.status(409).json({
                                                        code: 409,
                                                        error: "DB DELETE ERROR"
                                                    });
                                                })
                                            } else {
                                                ratesPerMiles.forEach(temp => {
                                                    ratesArray.push([driverRateId, temp.range_from,
                                                        temp.range_to, temp.rate, temp.fixed_rate]);
                                                });

                                                queryParams = Generaldriverratemaster.postGeneralDriverRatesPerMileQuery([
                                                    ratesArray]);

                                                transaction.query(queryParams.query, queryParams.params,
                                                    function (putRatesErr, putRatesQueryResponse) {
                                                        if (putRatesErr) {
                                                            transaction.rollback((rollbackErr) => {
                                                                transaction.release();
                                                                response.status(409).json({
                                                                    code: 409,
                                                                    error: "DB PUT RATES ERROR"
                                                                });
                                                            })
                                                        } else {
                                                            transaction.commit((commitErr) => {
                                                                if (commitErr) {
                                                                    transaction.rollback(() => {
                                                                        console.error(new Date(), `[driver_rate] [COMMIT] ERROR`,
                                                                            commitErr);

                                                                        transaction.release();
                                                                        response.status(409).json({
                                                                            code: 409,
                                                                            error: "DB COMMIT ERROR"
                                                                        });
                                                                    })
                                                                } else {
                                                                    transaction.release();
                                                                    client.del('general_driver_rates');
                                                                    client.del('driverPayData');
                                                                    cb(null, {
                                                                        code: 200,
                                                                        data: "successfully updated"
                                                                    });
                                                                }
                                                            })
                                                        }
                                                    })
                                            }
                                        })
                                }
                            });
                    }

                })
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Generaldriverratemaster.updateGeneralDriverRates = function (data, response, options, cb) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.updateGeneralDriverRatesValidation(data, response, options, cb);
            }
        });
    }

    //delete general driver rates
    Generaldriverratemaster.deleteGeneralDriverRatesQuery = function (params) {
        return {
            "query": "call deleteDriverRates(?);",
            "params": params
        };
    }

    Generaldriverratemaster.deleteGeneralDriverRatesValidation = function (driverRateId, response, options, callback) {
        var ds = Generaldriverratemaster.dataSource;
        var queryParams;
        try {
            queryParams = Generaldriverratemaster.deleteGeneralDriverRatesQuery([driverRateId]);
            Generaldriverratemaster.executeQuery(ds, queryParams.query, queryParams.params,
                function (err, queryResponse) {
                    if (err) {
                        response.status(409).json({ code: 409, error: "DB ERROR" });
                    } else {
                        client.del('general_driver_rates');
                        client.del('driverPayData');
                        callback(null, { code: 200, data: "successfully deleted" });
                    }
                })
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Generaldriverratemaster.deleteGeneralDriverRates = function (driver_rate_id, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.deleteGeneralDriverRatesValidation(driver_rate_id, response, options, callback);
            }
        });
    }
};
