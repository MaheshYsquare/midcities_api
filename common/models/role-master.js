'use strict';

module.exports = function (Rolemaster) {

    //Datasource connector
    Rolemaster.executeQuery = function (ds, query, params, callback) {
        ds.connector.query(query, params, callback);
    }

    //Access Token
    Rolemaster.validateAccessToken = function (access_token, callback) {
        this.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Rolemaster.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    //Get role type
    Rolemaster.getRoleTypeQuery = function () {
        return {
            //"query": "select distinct GROUP_CONCAT(name) as role_type from role_master"
            "query": "select rm.name, GROUP_CONCAT(pm.permission_type) as permissions from permission_master pm inner join role_master rm on pm.role_id = rm.id GROUP BY rm.name"
        };
    }

    Rolemaster.getRoleTypevalidation = function (response, options, callback) {
        var ds = Rolemaster.dataSource;
        var queryParams;
        try {
            queryParams = Rolemaster.getRoleTypeQuery();
            Rolemaster.executeQuery(ds, queryParams.query, null, function (err, queryResponse) {
                if (err) {
                    response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {
                    callback(null, queryResponse);
                }
            })
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Rolemaster.getRoleType = function (response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getRoleTypevalidation(response, options, callback);
            }
        });
    }
};
