'use strict';

const fetch = require('node-fetch');

const auth = require('./../../server/boot/auth');
const config = require('./../../server/boot/load-config').getConfig();

module.exports = function (KeepTruckin) {

    //calling internally by loopback(overriden)
    KeepTruckin.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    const getVehiclesListValidation = (response, callback) => {

        let url = `${config.keeptruckinServer}getVehiclesList`;

        let options = {
            method: 'GET',
            headers: {}
        };

        fetch(url, options)
            .then(result => result.json())
            .then(json => {
                if (json.code !== 200) {
                    console.error(new Date(), '[KEEPTRUCKIN] [VEHICLES LIST] ERROR', json);

                    return response.status(409).json({ code: 409, error: "KEEPTRUCKIN ERROR" });
                }

                let data = json.data.formatedVehicles ? json.data.formatedVehicles : [];

                return callback(null, { code: 200, data });
            })
            .catch(err => {
                console.error(new Date(), '[KEEPTRUCKIN] [VEHICLES LIST] CATCH ERROR', err);

                response.status(409).json({ code: 409, error: "KEEPTRUCKIN ERROR" });
            });
    }

    KeepTruckin.getVehiclesList = (response, options, callback) => {

        auth.validateAccessToken(KeepTruckin, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getVehiclesListValidation(response, callback);
            }
        });

    }


    const getVehicleLocationValidation = (id, date, response, callback) => {

        let url = `${config.keeptruckinServer}getVehicleLocation?vehicle_id=${id}&date=${date}`;

        let options = {
            method: 'GET',
            headers: {}
        };

        fetch(url, options)
            .then(result => result.json())
            .then(json => {
                if (json.code !== 200) {
                    console.error(new Date(), '[KEEPTRUCKIN] [VEHICLE LOCATION] ERROR', json);

                    return response.status(409).json({ code: 409, error: "KEEPTRUCKIN ERROR" });
                }

                let data = json.data ? json.data : null;

                return callback(null, { code: 200, data });
            })
            .catch(err => {
                console.error(new Date(), '[KEEPTRUCKIN] [VEHICLE LOCATION] ERROR', err);

                response.status(409).json({ code: 409, error: "KEEPTRUCKIN ERROR" });
            });
    }

    KeepTruckin.getVehicleLocation = (id, date, response, options, callback) => {

        auth.validateAccessToken(KeepTruckin, options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                getVehicleLocationValidation(id, date, response, callback);
            }
        });

    }

};
