'use strict';

const fetchRequest = require('request');
const _ = require('lodash');
const moment = require('moment');

const config = require('./../../server/boot/load-config').getConfig();
const quickbooks = require('../../server/boot/quickbooks-connection');

module.exports = function (Report) {

  //Datasource Connector
  const executeQuery = (query, params, callback) => {
    Report.dataSource.connector.query(query, params, callback);
  }

  //Access Token
  const validateAccessToken = (access_token, callback) => {
    Report.app.models.AccessToken.findById(access_token, callback)
  }

  //calling internally by loopback(overriden)
  Report.createOptionsFromRemotingContext = function (ctx) {
    var base = this.base.createOptionsFromRemotingContext(ctx);
    base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
    return base;
  }


  const fetchCompletedOrdersFromDb = (date_from, date_to, response, options, callback) => {
    try {
      executeQuery("CALL getCompletedOrders(?,?);", [date_from, date_to], (err, queryResponse) => {
        if (err) {

          console.error(new Date(), `[report] [FETCH] COMPLETED ORDERS QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          response.status(409).json({ code: 409, error: "DB ERROR" });
        } else {
          callback(null, queryResponse.length !== 0 ? queryResponse[0] : [])
        }
      })
    } catch (e) {
      console.error(new Date(), `[report] [FETCH] COMPLETED ORDERS CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }

  }

  Report.getCompletedOrder = (date_from, date_to, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchCompletedOrdersFromDb(date_from, date_to, response, options, callback);
      }
    });

  }

  const fetchDriverManfestReportFromDb = (driverId, date_from, date_to, timezone, response, options, callback) => {
    try {
      let timeOffset = moment.tz(timezone).format("Z");

      executeQuery("CALL getDriverManifestReport(?,?,?,?);", [driverId, date_from, date_to, timeOffset],
        (err, queryResponse) => {
          try {

            if (err) {
              console.error(new Date(), `[report] [FETCH] DRIVER MANIFEST QUERY ERROR`,
                "sqlMessage" in err ? err.sqlMessage : err);

              return response.status(409).json({ code: 409, error: "DB ERROR" });
            }

            queryResponse[0].forEach(temp => {
              temp.legs = temp.legs ? JSON.parse(temp.legs) : [];

              temp.dl_date = new Date(temp.dl_time).toLocaleDateString();

              temp.legs.forEach(elem => {

                elem.pu_time = elem.pu_time === 0 ? null : elem.pu_time;

                elem.dl_time = elem.dl_time === 0 ? null : elem.dl_time;

                elem.rates_per_mile = elem.rates_per_mile !== null ?
                  elem.rates_per_mile : [];

                elem.rates_per_mile.forEach(item => {
                  item.range_from = +item.range_from;
                  item.range_to = item.range_to ? +item.range_to : null;
                })

                elem.rates_per_mile = _.orderBy(elem.rates_per_mile,
                  ['range_from', 'range_to'], ['asc', 'asc']);

                elem.accessories_rate = +elem.accessories_rate;

                if (elem.is_rate_edited === 0) {

                  elem.isRateCalculated = false;

                  elem.rates_per_mile.forEach(element => {
                    if (!elem.isRateCalculated) {

                      if ((elem.miles >= element.range_from &&
                        elem.miles <= element.range_to) || element.range_to === null) {

                        elem.driver_rate = element.fixed_rate === 1 ?
                          +(
                            +element.rate
                          ).toFixed(2)
                          : +(
                            +elem.miles * +element.rate
                          ).toFixed(2);

                        elem.isRateCalculated = true;
                      }

                    }
                  })

                } else {
                  elem.driver_rate = +elem.rate;
                }
              })

              temp.paid = temp.legs.map(elem => {
                return {
                  rate: elem.driver_rate,
                  accessories_rate: +elem.accessories_rate,
                  is_paid: elem.is_paid
                }
              }).reduce((prev, curr) => {
                return curr.is_paid ? (prev + curr.rate + curr.accessories_rate) : prev
              }, 0);

              temp.paid = +temp.paid.toFixed(2);

              temp.unpaid = temp.legs.map(elem => {
                return {
                  rate: elem.driver_rate,
                  accessories_rate: +elem.accessories_rate,
                  is_paid: elem.is_paid
                }
              }).reduce((prev, curr) => {
                return curr.is_paid ? prev : (prev + curr.rate + curr.accessories_rate)
              }, 0);

              temp.unpaid = +temp.unpaid.toFixed(2);
            })

            let paid = queryResponse[0].map(temp => temp.paid).reduce((a, b) => a + b, 0);

            let unpaid = queryResponse[0].map(temp => temp.unpaid).reduce((a, b) => a + b, 0);

            let reportResponse = {
              driver_name: queryResponse[0].length !== 0 ? queryResponse[0][0].driver_name : null,
              date_from: new Date(date_from).toLocaleDateString(),
              date_to: new Date(date_to).toLocaleDateString(),
              driverRates: queryResponse[0],
              total_paid: +paid.toFixed(2),
              total_unpaid: +unpaid.toFixed(2),
            }

            callback(null, reportResponse);

          } catch (error) {
            console.error(new Date(), `[report] [FETCH] DRIVER MANIFEST CODE ERROR`, error);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
          }
        })
    } catch (e) {
      console.error(new Date(), `[report] [FETCH] DRIVER MANIFEST CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }

  }

  Report.getDriverManifestReport = (driverId, date_from, date_to, timezone, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchDriverManfestReportFromDb(driverId, date_from, date_to, timezone, response, options, callback);
      }
    });
  }


  const fetchDriverListFromDb = (response, options, callback) => {
    try {
      executeQuery("SELECT driver_id,concat(first_name,' ',last_name) driver_name FROM driver_master;", null,
        (err, queryResponse) => {
          if (err) {

            console.error(new Date(), `[report] [FETCH] DRIVERS LIST QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            response.status(409).json({ code: 409, error: "DB ERROR" });
          } else {

            queryResponse = _.orderBy(queryResponse, [driver => driver.driver_name.toLowerCase()], ['asc']);

            callback(null, queryResponse)
          }
        })
    } catch (e) {
      console.error(new Date(), `[report] [FETCH] DRIVERS LIST CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }

  }

  Report.getDriverList = (response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchDriverListFromDb(response, options, callback);
      }
    });

  }


  const fetchIncomeGeneratedReportFromDb = (date_from, date_to, timezone, response, options, callback) => {
    try {
      let timeOffset = moment.tz(timezone).format("Z");

      executeQuery("CALL getCustomerIncomeReport(?,?,?);", [timeOffset, date_from, date_to],
        (err, queryResponse) => {
          try {

            if (err) {
              console.error(new Date(), `[report] [FETCH] INCOME GENERATED QUERY ERROR`,
                "sqlMessage" in err ? err.sqlMessage : err);

              return response.status(409).json({ code: 409, error: "DB ERROR" });
            }

            queryResponse[0].forEach((element, i) => {

              element.serial_no = i + 1;

              element.lastPayDate = element.lastPayDate ?
                moment(element.lastPayDate).tz(timezone).format('MM/DD/YYYY')
                : null;
            });

            callback(null, queryResponse[0]);

          } catch (error) {
            console.error(new Date(), `[report] [FETCH] INCOME GENERATED CODE ERROR`, error);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
          }
        })
    } catch (e) {
      console.error(new Date(), `[report] [FETCH] INCOME GENERATED CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Report.getIncomeGeneratedReport = (date_from, date_to, timezone, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchIncomeGeneratedReportFromDb(date_from, date_to, timezone, response, options, callback);
      }
    });
  }

  const fetchChassisReconcileFromDb = (chassis, date_from, date_to, timezone, response, options, callback) => {
    try {

      chassis = decodeURIComponent(chassis);

      date_from = decodeURIComponent(date_from);

      date_to = decodeURIComponent(date_to);

      timezone = decodeURIComponent(timezone);

      let timeOffset = moment.tz(timezone).format("Z");

      executeQuery("CALL getChassisReconcileReport(?,?,?,?);",
        [chassis, date_from, date_to, timeOffset], (err, queryResponse) => {
          if (err) {

            console.error(new Date(), `[report] [FETCH] CHASSIS RECONCILE REPORT QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            return response.status(409).json({ code: 409, error: "DB ERROR" });
          }

          try {
            let result = [];

            queryResponse[0].forEach(elem => {
              elem.legs = elem.legs ? JSON.parse(elem.legs) : [];

              elem.legs = _.orderBy(elem.legs,
                ['order_sequence', 'leg_number'], ['asc', 'asc']);

              elem.legs.forEach(temp => {
                temp.pu_time = temp.pu_time && new Date(temp.pu_time).getFullYear() !== 1970 ?
                  temp.pu_time : null;

                temp.dl_time = temp.dl_time && new Date(temp.dl_time).getFullYear() !== 1970 ?
                  temp.dl_time : null;

                temp.pu_time = temp.pu_time ?
                  moment(new Date(temp.pu_time)).tz(timezone).format('MM/DD/YYYY HH:mm') : null;

                temp.dl_time = temp.dl_time ?
                  moment(new Date(temp.dl_time)).tz(timezone).format('MM/DD/YYYY HH:mm') : null;

                temp.pickup_date = temp.pu_time ?
                  moment(new Date(temp.pu_time)).tz(timezone).format('MM/DD/YYYY') : null;

                temp.delivery_date = temp.dl_time ?
                  moment(new Date(temp.dl_time)).tz(timezone).format('MM/DD/YYYY') : null;

                temp.pickup_time = temp.pu_time ?
                  moment(new Date(temp.pu_time)).tz(timezone).format('HH:mm') : null;

                temp.delivery_time = temp.dl_time ?
                  moment(new Date(temp.dl_time)).tz(timezone).format('HH:mm') : null;

                result.push(temp);
              })
            })

            callback(null, { code: 200, data: result });

          } catch (error) {

            console.error(new Date(), `[report] [FETCH] CHASSIS RECONCILE REPORT CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
          }

        })
    } catch (e) {
      console.error(new Date(), `[report] [FETCH] CHASSIS RECONCILE REPORT CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }

  }

  Report.getChassisReconcileReport = (chassis, date_from, date_to, timezone, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchChassisReconcileFromDb(chassis, date_from, date_to, timezone, response, options, callback);
      }
    });

  }

  const fetchRevenueFromDb = (date_from, date_to, timezone, response, options, callback) => {
    try {

      date_from = decodeURIComponent(date_from);

      date_to = decodeURIComponent(date_to);

      timezone = decodeURIComponent(timezone);

      let timeOffset = moment.tz(timezone).format("Z");

      executeQuery("CALL getRevenueGenratedReport(?,?,?);",
        [date_from, date_to, timeOffset], (err, queryResponse) => {
          if (err) {

            console.error(new Date(), `[report] [FETCH] REVENUE GENERATED REPORT QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            return response.status(409).json({ code: 409, error: "DB ERROR" });
          }

          try {
            let result = {
              total_amount: +(queryResponse[0].map(temp => +temp.total_amount)
                .reduce((prev, curr) => prev + curr, 0)).toFixed(2),
              total_invoice: queryResponse[0].length,
              list: queryResponse[0]
            }

            result.list.forEach((temp, i) => {
              temp.serial_no = i + 1;

              temp.created_on = temp.created_on ?
                moment(temp.created_on).tz(timezone).format('MM/DD/YYYY') : null;

              temp.total_amount = +temp.total_amount;
            });

            callback(null, { code: 200, data: result });

          } catch (error) {

            console.error(new Date(), `[report] [FETCH] REVENUE GENERATED REPORT CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
          }

        })
    } catch (e) {
      console.error(new Date(), `[report] [FETCH] CHASSIS RECONCILE REPORT CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }

  }

  Report.getRevenueGeneratedReport = (date_from, date_to, timezone, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchRevenueFromDb(date_from, date_to, timezone, response, options, callback);
      }
    });

  }

  const fetchCompletedMovesFromDb = (customerId, date_from, date_to, timezone, response, options, callback) => {
    try {

      customerId = decodeURIComponent(customerId);

      date_from = decodeURIComponent(date_from);

      date_to = decodeURIComponent(date_to);

      timezone = decodeURIComponent(timezone);

      let timeOffset = moment.tz(timezone).format("Z");

      executeQuery("CALL getCustomerListOfMoves(?,?,?,?);",
        [customerId, date_from, date_to, timeOffset], (err, queryResponse) => {
          if (err) {

            console.error(new Date(), `[report] [FETCH] CUSTOMER LIST OF MOVES QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            return response.status(409).json({ code: 409, error: "DB ERROR" });
          }

          queryResponse[0].forEach((temp, i) => {
            temp.pu_time = temp.pu_time && new Date(temp.pu_time).getYear() &&
              new Date(temp.pu_time).getYear() !== 1970 ? temp.pu_time : null;

            temp.dl_time = temp.dl_time && new Date(temp.dl_time).getYear() &&
              new Date(temp.dl_time).getYear() !== 1970 ? temp.dl_time : null;

            temp.serial_no = i + 1;

            temp.pu_time = temp.pu_time ?
              moment(new Date(temp.pu_time)).tz(timezone).format('MM/DD/YYYY HH:mm') : null;

            temp.dl_time = temp.dl_time ?
              moment(new Date(temp.dl_time)).tz(timezone).format('MM/DD/YYYY HH:mm') : null;

            temp.pickup_date = temp.pu_time ?
              moment(new Date(temp.pu_time)).tz(timezone).format('MM/DD/YYYY') : null;

            temp.delivery_date = temp.dl_time ?
              moment(new Date(temp.dl_time)).tz(timezone).format('MM/DD/YYYY') : null;

            temp.pickup_time = temp.pu_time ?
              moment(new Date(temp.pu_time)).tz(timezone).format('HH:mm') : null;

            temp.delivery_time = temp.dl_time ?
              moment(new Date(temp.dl_time)).tz(timezone).format('HH:mm') : null;

          })

          callback(null, { code: 200, data: queryResponse[0] });

        })
    } catch (e) {
      console.error(new Date(), `[report] [FETCH] CUSTOMER LIST OF MOVES CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }

  }

  Report.getCompletedMovesReport = (customerId, date_from, date_to, timezone, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchCompletedMovesFromDb(customerId, date_from, date_to, timezone, response, options, callback);
      }
    });

  }

  const fetchMilesFromDb = (driverTypeId, date_from, date_to, timezone, response, options, callback) => {
    try {

      driverTypeId = decodeURIComponent(driverTypeId);

      date_from = decodeURIComponent(date_from);

      date_to = decodeURIComponent(date_to);

      timezone = decodeURIComponent(timezone);

      let timeOffset = moment.tz(timezone).format("Z");

      executeQuery("CALL getMilesReport(?,?,?,?);",
        [driverTypeId, date_from, date_to, timeOffset], (err, queryResponse) => {
          if (err) {

            console.error(new Date(), `[report] [FETCH] CUSTOMER LIST OF MOVES QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            return response.status(409).json({ code: 409, error: "DB ERROR" });
          }

          queryResponse[0].forEach(temp => {
            temp.pu_time = temp.pu_time && new Date(temp.pu_time).getYear() &&
              new Date(temp.pu_time).getYear() !== 1970 ?
              moment(new Date(temp.pu_time)).tz(timezone).format('MM/DD/YYYY HH:mm') : null;

            temp.dl_time = temp.dl_time && new Date(temp.dl_time).getYear() &&
              new Date(temp.dl_time).getYear() !== 1970 ?
              moment(new Date(temp.dl_time)).tz(timezone).format('MM/DD/YYYY HH:mm') : null;

            temp.pickup_date = temp.pu_time && new Date(temp.pu_time).getYear() &&
              new Date(temp.pu_time).getYear() !== 1970 ?
              moment(new Date(temp.pu_time)).tz(timezone).format('MM/DD/YYYY') : null;

            temp.delivery_date = temp.dl_time && new Date(temp.dl_time).getYear() &&
              new Date(temp.dl_time).getYear() !== 1970 ?
              moment(new Date(temp.dl_time)).tz(timezone).format('MM/DD/YYYY') : null;
          });

          queryResponse[0] = _.orderBy(queryResponse[0], 'order_sequence', 'asc');

          callback(null, { code: 200, data: queryResponse[0] });

        })
    } catch (e) {
      console.error(new Date(), `[report] [FETCH] CUSTOMER LIST OF MOVES CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }

  }

  Report.getMilesReport = (driverTypeId, date_from, date_to, timezone, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchMilesFromDb(driverTypeId, date_from, date_to, timezone, response, options, callback);
      }
    });

  }

  const fetchPastDueFromDb = (customerId, timezone, response, options, callback) => {
    try {

      customerId = decodeURIComponent(customerId);

      timezone = decodeURIComponent(timezone);

      executeQuery("CALL getPastDueReport(?);", [customerId],
        async (err, queryResponse) => {
          if (err) {

            console.error(new Date(), `[report] [FETCH] CUSTOMER LIST OF MOVES QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            return response.status(409).json({ code: 409, error: "DB ERROR" });
          }

          queryResponse[0].forEach(temp => {
            temp.date = temp.date && new Date(temp.date).getYear() &&
              new Date(temp.date).getYear() !== 1970 ?
              moment(new Date(temp.date)).tz(timezone).format('MM/DD/YYYY') : null;
          });

          let quickbooksConnection = await quickbooks.authenticateQuickbooksConnection(Report);

          if (quickbooksConnection.code !== 200) {

            return quickbooksConnection.code === 103 ?
              callback(null, quickbooksConnection) :
              response.status(quickbooksConnection.code).json(quickbooksConnection);

          }

          let customerDetails;

          let actualData = [];

          if (queryResponse[0].length) {
            customerDetails = {
              customer_quickbooks_id: +queryResponse[0][0].customer_quickbooks_id,
              business_name: queryResponse[0][0].business_name,
              email: queryResponse[0][0].email,
              business_phone: queryResponse[0][0].business_phone,
              b_address: queryResponse[0][0].b_address,
              billing_name: queryResponse[0][0].billing_name,
              b_street: queryResponse[0][0].b_street,
              b_city: queryResponse[0][0].b_city,
              b_state: queryResponse[0][0].b_state,
              b_postal_code: queryResponse[0][0].b_postal_code
            }

            const URL = `query?query=select * from Invoice Where Balance > '0' AND CustomerRef = '${customerDetails.customer_quickbooks_id}' MAXRESULTS 1000&minorversion=55`;

            let quickbooksData = await quickbooks.fetchDataFromQuickbooks(URL);

            if (quickbooksData.code !== 200) {

              return response.status(quickbooksData.code).json(quickbooksData);

            }

            let fetchedQuickbooksData = quickbooksData.data.json.QueryResponse.Invoice;

            let quickbooksMap = new Map();

            queryResponse[0].forEach(elem => {
              quickbooksMap.set(elem.invoice_number, elem);
            })

            fetchedQuickbooksData.forEach(temp => {
              let invoice = quickbooksMap.get(temp.DocNumber);

              if (invoice) {
                actualData.push({
                  invoice_number: temp.DocNumber,
                  date: invoice.date,
                  customer_reference: invoice.customer_reference,
                  container_number: invoice.container_number,
                  total_amount: temp.TotalAmt,
                  paid: +(+temp.TotalAmt - +temp.Balance).toFixed(2),
                  outstanding: temp.Balance
                })
              }
            })
          }

          let finalResponse = {
            list: actualData,
            customerDetails,
            balance: +(actualData.map(temp => temp.outstanding).reduce((a, b) => a + b, 0)).toFixed(2)
          }

          callback(null, { code: 200, data: finalResponse });

        })
    } catch (e) {
      console.error(new Date(), `[report] [FETCH] CUSTOMER LIST OF MOVES CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }

  }

  Report.getPastDueReport = (customerId, timezone, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchPastDueFromDb(customerId, timezone, response, options, callback);
      }
    });

  }

  const fetchAgingSummaryFromQb = async (response, options, callback) => {
    try {

      let quickbooksConnection = await quickbooks.authenticateQuickbooksConnection(Report);

      if (quickbooksConnection.code !== 200) {

        return quickbooksConnection.code === 103 ?
          callback(null, quickbooksConnection) :
          response.status(quickbooksConnection.code).json(quickbooksConnection);

      }

      const URL = `reports/AgedReceivables?report_date=${moment(new Date()).format('YYYY-MM-DD')}&minorversion=55`;

      let quickbooksData = await quickbooks.fetchDataFromQuickbooks(URL);

      if (quickbooksData.code !== 200) {

        return response.status(quickbooksData.code).json(quickbooksData);

      }

      let agingReportQbData = quickbooksData.data.json;

      let columns = [];

      let formatData = [];

      agingReportQbData.Columns.Column.forEach((element, i) => {
        columns.push(i === 0 ? element["ColType"] : element["ColTitle"])
      });

      agingReportQbData.Rows.Row.forEach((element) => {
        let object = {};

        if (Object.keys(element)[0] === "Summary") {

          element.Summary.ColData.forEach((temp, i) => {
            object[columns[i]] = temp.value;
          })

        } else if (Object.keys(element)[0] === "ColData") {

          element[Object.keys(element)[0]].forEach((temp, i) => {
            object[columns[i]] = temp.value;
            i === 0 ? object["customer_quickbooks_id"] = temp.id : null;
          });

        } else if (Object.keys(element)[0] === "Header") {

          element.Rows.Row.forEach(elem => {
            let secondaryObject = {};

            elem[Object.keys(elem)[0]].forEach((temp, i) => {
              secondaryObject[columns[i]] = temp.value;
              i === 0 ? secondaryObject["customer_quickbooks_id"] = temp.id : null;
            })

            formatData.push(secondaryObject);
          })

        }

        Object.keys(object).length ? formatData.push(object) : null;
      });

      formatData.forEach(temp => {
        temp.one_thirty = +temp["1 - 30"];
        temp.thirty_sixty = +temp["31 - 60"];
        temp.sixty_ninety = +temp["61 - 90"];
        temp.ninety_over = +temp["91 and over"];
        temp.Current = +temp.Current;
        temp.Total = +temp.Total
      })

      callback(null, { code: 200, data: formatData });


    } catch (e) {
      console.error(new Date(), `[report] [FETCH] AGING SUMMARY REPORT CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }

  }

  Report.getAgingSummaryReport = (response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchAgingSummaryFromQb(response, options, callback);
      }
    });

  }

  const fetchAgingDetailFromQb = async (customerId, response, options, callback) => {
    try {

      let quickbooksConnection = await quickbooks.authenticateQuickbooksConnection(Report);

      if (quickbooksConnection.code !== 200) {

        return quickbooksConnection.code === 103 ?
          callback(null, quickbooksConnection) :
          response.status(quickbooksConnection.code).json(quickbooksConnection);

      }

      const URL = `reports/AgedReceivableDetail?report_date=${moment(new Date()).format('YYYY-MM-DD')}&customer=${customerId}&minorversion=55`;

      let quickbooksData = await quickbooks.fetchDataFromQuickbooks(URL);

      if (quickbooksData.code !== 200) {

        return response.status(quickbooksData.code).json(quickbooksData);

      }

      let agingReportQbData = quickbooksData.data.json;

      let columns = [];

      let formatData = [];

      agingReportQbData.Columns.Column.forEach((element, i) => {
        columns.push(element["ColTitle"] === 'Num' ? 'Invoice #' : element["ColTitle"])
      });

      console.log(new Date(), "[report.js] [AGING DETAIL]", JSON.stringify(agingReportQbData.Rows.Row))

      agingReportQbData.Rows.Row.forEach((element, j) => {

        Object.keys(element).forEach(item => {

          let object = {};

          if (item === 'Header') {
            element.Header[Object.keys(element.Header)[0]].forEach((temp, i) => {
              i === 0 ? object["header"] = temp.value : object["isgrouped"] = true;
            })

            formatData.push(object);
          }

          if (item === 'Rows') {

            element.Rows.Row.forEach(elem => {
              object = {};

              elem[Object.keys(elem)[0]].forEach((temp, i) => {
                object[columns[i]] = temp.value;
                i === 1 ?
                  object["invoice_quickbooks_id"] = temp.id :
                  (i === 3 ?
                    object["customer_quickbooks_id"] = temp.id :
                    null);
              })

              formatData.push(object);
            })
          }

          if (item === 'Summary' && agingReportQbData.Rows.Row.length === (j + 1)) {
            object = {};

            element.Summary.ColData.forEach((temp, i) => {
              object[columns[i]] = temp.value;
            })

            formatData.push(object);
          }

        })
      });

      formatData.forEach(temp => {
        temp.Amount = +temp.Amount;
        temp.open_balance = +temp["Open Balance"];
        temp.due_date = temp["Due Date"];
        temp.invoice_number = temp["Invoice #"];
      })


      callback(null, { code: 200, data: formatData });


    } catch (e) {
      console.error(new Date(), `[report] [FETCH] AGING DETAIL REPORT CODE ERROR`, e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }

  }

  Report.getAgingDetailReport = (customerId, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchAgingDetailFromQb(customerId, response, options, callback);
      }
    });

  }

  const fetchNotesFromDb = (id, type, response, options, callback) => {

    let query;

    if (type === "customer") {
      query = `SELECT
                         customer_notes notes,
                         customer_quickbooks_id id
                     FROM
                         customer_master cm
                     WHERE
                         customer_quickbooks_id = ?;`
    } else if (type === 'invoice') {
      query = `SELECT
                         nh2.notes,
                         im.order_container_chassis_id id
                     FROM
                         invoice_mgmt im
                     LEFT JOIN (
                         SELECT
                             nh.order_container_chassis_id,
                             JSON_ARRAYAGG(JSON_OBJECT('order_container_chassis_id',
                             nh.order_container_chassis_id,
                             'notes',
                             nh.notes,
                             'created_by',
                             nh.created_by,
                             'created_on',
                             UNIX_TIMESTAMP(nh.created_on) * 1000,
                             'lastupdated_by',
                             nh.lastupdated_by,
                             'lastupdated_on',
                             UNIX_TIMESTAMP(nh.lastupdated_on) * 1000,
                             'user_id',
                             nh.user_id,
                             'notes_id',
                             nh.notes_id)) notes
                         FROM
                             notes_history nh
                         WHERE
                             notes_description = 'invoice_notes'
                         GROUP BY
                             nh.order_container_chassis_id ) nh2 ON
                         nh2.order_container_chassis_id = im.order_container_chassis_id
                     WHERE
                         im.invoice_quickbooks_id = ?;`
    } else {
      query = `SELECT
                         notes,
                         manual_invoice_quickbooks_id id,
                         created_by,
                         created_on
                     FROM
                         manual_invoice mi
                     WHERE
                         manual_invoice_quickbooks_id = ?;`
    }

    executeQuery(query, [id], (err, queryResponse) => {

      try {

        if (err) {

          console.error(new Date(), `[report] [FETCH] CUSTOMER LIST OF MOVES QUERY ERROR`,
            "sqlMessage" in err ? err.sqlMessage : err);

          return response.status(409).json({ code: 409, error: "DB ERROR" });
        }

        if (type === "customer") {
          return callback(null, { code: 200, data: queryResponse });
        }

        if (type === "invoice" && queryResponse.length) {

          queryResponse.forEach(temp => {

            temp.notes = temp.notes ? JSON.parse(temp.notes) : [];

            temp.notes.forEach(elem => {
              temp.created_on = temp.created_on && new Date(temp.created_on).getYear() &&
                new Date(temp.created_on).getYear() !== 1970 ?
                temp.created_on : null;

              temp.lastupdated_on = temp.lastupdated_on && new Date(temp.lastupdated_on).getYear() &&
                new Date(temp.lastupdated_on).getYear() !== 1970 ?
                temp.lastupdated_on : null;
            });
          });

          return callback(null, { code: 200, data: queryResponse, isInvoice: true });

        }

        if (type === "invoice" && !queryResponse.length) {

          type = "manualInvoice";

          fetchNotesFromDb(id, type, response, options, callback);

          return;
        }

        callback(null, { code: 200, data: queryResponse, isInvoice: false });

      } catch (e) {
        console.error(new Date(), `[report] [FETCH] CUSTOMER LIST OF MOVES CODE ERROR`, e);

        return response.status(400).json({ code: 400, error: "CODE ERROR" });
      }

    })

  }

  Report.getReportNotes = (id, type, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchNotesFromDb(id, type, response, options, callback);
      }
    });

  }

  const updateNotesInDb = (request, response, options, callback) => {

    let query;

    if (request.type === "customer") {
      query = "UPDATE customer_master SET customer_notes = ? WHERE customer_quickbooks_id = ?;"
    } else {
      query = "UPDATE manual_invoice SET notes = ? WHERE manual_invoice_quickbooks_id = ?;"
    }

    executeQuery(query, [request.notes, request.id], (err, queryResponse) => {

      if (err) {

        console.error(new Date(), `[report] [UPDATE] REPORT NOTES QUERY ERROR`,
          "sqlMessage" in err ? err.sqlMessage : err);

        return response.status(409).json({ code: 409, error: "DB ERROR" });
      }

      return callback(null, { code: 200, data: "Successfully Updated" });
    })
  }

  Report.updateReportNotes = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        updateNotesInDb(request, response, options, callback);
      }
    });

  }

  const generateReportFromReportingServer = (req, response, callback) => {
    try {

      if (req.reportData.agingFooter && req.reportData.format === 'xlsx') {
        req.reportData.data.push(req.reportData.agingFooter);
      }

      let options = {
        uri: `${config.reportingServer}${req.reportType}`,
        method: "POST",
        encoding: null,
        json: req.reportData
      }
      fetchRequest.post(options, (err, result) => {
        try {
          if (err) {
            response.status(500).json({ code: 500, error: "REPORTING CONNECTION ERROR" });
          } else {
            if (result.statusCode < 200 || result.statusCode > 299) {
              return response.status(409).json({ code: 409, error: "REPORTING DOC ERROR" });
            } else {
              callback(null, result.body);
            }
          }
        } catch (error) {
          response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
      })
    } catch (e) {
      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Report.generateReport = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        generateReportFromReportingServer(request, response, callback);
      }
    });
  }

  const generateAndSendEmail = (req, response, callback) => {

    if (req.reportData.agingFooter && req.reportData.format === 'xlsx') {
      req.reportData.data.push(req.reportData.agingFooter);
    }

    let options = {
      uri: `${config.reportingServer}${req.reportType}`,
      method: "POST",
      encoding: null,
      json: req.reportData
    }

    fetchRequest(options, (err, result) => {
      try {
        if (err) {
          return response.status(500).json({ code: 500, error: "REPORTING CONNECTION ERROR" });
        }

        if (result.statusCode < 200 || result.statusCode > 299) {
          return response.status(409).json({ code: 409, error: "REPORTING DOC ERROR" });
        }

        Report.app.models.Email.send({
          to: req.senderAddress,
          from: config.mail,
          subject: req.subject,
          attachments: [
            {   // binary buffer as an attachment
              filename: req.fileName,
              content: result.body
            }
          ]
        }, (mailErr, resp) => {
          if (mailErr) {
            console.error(new Date(), "[report] [SEND] MAIL ERROR", mailErr);

            return response.status(409).json({ code: 409, error: "Error in sending mail" });
          }

          callback(null, "Successfully Mail Sent");
        });

      } catch (error) {
        response.status(400).json({ code: 400, error: "CODE ERROR" });
      }
    })
  }

  Report.sendEmail = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        generateAndSendEmail(request, response, callback);
      }
    });
  }
};
