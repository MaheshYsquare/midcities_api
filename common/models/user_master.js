'use strict';

const path = require('path');
const async = require("async");
const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const _ = require('lodash');

const app = require('../../server/server.js');

const DEFAULT_RESET_DAY_TTL = 3600 * 24 * 2; // 2 days in seconds

let config = require('./../../server/boot/load-config').getConfig();

module.exports = function (Usermaster) {

  /* to set reset password ttl */
  // Usermaster.settings.resetPasswordTokenTTL = DEFAULT_RESET_PW_TTL;

  let queryParams; // variable to store query and param on each function

  /** EXECUTE NATIVE MYSQL QUERY
   * @param query 
   * @param params
   * @function callback  */
  const executeQuery = (query, params, callback) => {
    Usermaster.dataSource.connector.query(query, params, callback);
  }

  /** TO VALIDATE ACCESS TOKEN
   * @param access_token 
   * @function callback  */
  const validateAccessToken = (access_token, callback) => {
    Usermaster.app.models.AccessToken.findById(access_token, callback)
  }

  /* calling internally by loopback(overriden) */
  Usermaster.createOptionsFromRemotingContext = function (ctx) {
    var base = this.base.createOptionsFromRemotingContext(ctx);
    base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
    return base;
  }

  /* Before the create method generate a random password using before remote operational hooks to 
  override loopback base api */
  Usermaster.beforeRemote('create', (context, userInstance, next) => {

    const token = context.req.headers.accesstoken ? context.req.headers.accesstoken : "";

    validateAccessToken(token, (err, tokenModel) => {
      if (err || !tokenModel) {
        context.res.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        context.args.data.password = Math.random().toString(36).slice(-8);
        next();
      }
    });

  });

  /** Native Mysql Query of Driver Post  
   * Required Input @param params */
  const postDriverDataQuery = (params) => {
    return {
      "query": "insert into driver_master(first_name,last_name,email,user_id,driver_type_id) values (?,?,?,?,?)",
      "params": params
    };
  }

  /** Native Mysql Query of UI SETTINGS POST  
   * Required Input @param params */
  const postUiSettingsQuery = (params) => {
    return {
      "query": "insert into users_ui_settings(user_id,orders_dispatch,orders_pending,orders_draft,dispatch_view,invoice_view,invoice_to_generated_view) values (?,?,?,?,?,?,?)",
      "params": params
    };
  }

  /* After the create method generate a short lived token for the user to set the password */
  Usermaster.afterRemote('create', (context, userInstance, next) => {

    let where = {
      email: userInstance.email,
    };

    Usermaster.findOne({ where: where }, (userError, user) => { // fetch the user details from db

      if (userError) {

        console.error(new Date(), "[user] [REMOTE] USER AFTER CREATE REMOTE METHOD USER FETCH ERROR",
          "sqlMessage" in userError ? userError.sqlMessage : userError);

        return context.res.status(409).json({ code: 409, error: 'DB ERROR' });
      }

      if (!user) {

        console.error(new Date(), "[user] [REMOTE] USER AFTER CREATE REMOTE METHOD USER NOT FOUND ERROR", "EMAIL_NOT_FOUND");

        return context.res.status(404).json({ code: 404, error: 'EMAIL_NOT_FOUND' });
      }

      /* create access token with 2 days ttl for the verification mail */
      user.createAccessToken(DEFAULT_RESET_DAY_TTL, (accessTokenErr, accessToken) => {

        if (accessTokenErr) {
          console.error(new Date(), "[user] [ACCESS] USER AFTER CREATE REMOTE METHOD CREATE ACCESS TOKEN ERROR",
            "sqlMessage" in accessTokenErr ? accessTokenErr.sqlMessage : accessTokenErr);

          return context.res.status(409).json({ code: 409, error: 'DB ERROR' });
        }

        Usermaster.passwordreset = false;

        let options = {
          type: 'email',
          to: userInstance.email,
          from: config.mail,
          subject: 'Thanks for registering.',
          template: path.resolve(__dirname, '../../server/views/verify.ejs'),
          host: config.domain,
          // host: 'localhost:3000',
          port: '80',
          redirect: `${config.ssl ? 'https' : 'http'}://${config.domain}/#/authentication/new?access_token=${accessToken.id}`,
          // redirect: `${config.ssl ? 'https' : 'http'}://localhost:4200/#/authentication/new?access_token=${accessToken.id}`,
          user: Usermaster,
        };

        /* use the verify option to send email to created user */
        user.verify(options, (err, response, next1) => {

          if (err) {
            console.error(new Date(), "[user] [VERIFY] USER AFTER CREATE REMOTE METHOD USER VERIFY ERROR", err);

            return context.res.status(409).json({ code: 409, error: 'USER VERIFY ERROR' });
          }

          client.del('user_master');
          client.del('user_notify');

          queryParams = postUiSettingsQuery([context.result.user_id, context.result.orders_dispatch, context.result.orders_pending,
          context.result.orders_draft, context.result.dispatch_view, context.result.invoice_view,
          context.result.invoice_to_generated_view]);

          executeQuery(queryParams.query, queryParams.params, (uiErr, postUiResponse) => {
            if (uiErr) {
              console.error(new Date(), "[user] [POST] UI SETTINGS POST ERROR",
                "sqlMessage" in uiErr ? uiErr.sqlMessage : uiErr);
            }
          });

          /* if created user role is driver then user also needs to get created on driver table */
          if (context.result.role == 'Driver') {

            queryParams = postDriverDataQuery([context.result.first_name, context.result.last_name,
            context.result.email, context.result.user_id, context.result.driver_type_id]);

            executeQuery(queryParams.query, queryParams.params, (driverErr, postDriverResponse) => {
              if (driverErr) {
                console.error(new Date(), "[user] [POST] USER AFTER CREATE REMOTE METHOD DRIVER POST ERROR",
                  "sqlMessage" in driverErr ? driverErr.sqlMessage : driverErr);

                return context.res.send({ code: 200, data: context.result });
              } else {
                client.del('driverDetailsData');

                return context.res.send({ code: 200, data: context.result });
              }

            });

          } else {
            return context.res.send({ code: 200, data: context.result });
          }

        });

      });

    });

  });

  /** Native Mysql Query Of GET User Details - No input is required */
  const getUserDetailsQuery = () => {
    return {
      "query": `SELECT 
                    users.user_id,
                    users.username,
                    users.first_name,
                    users.last_name,
                    users.role,
                    users.permissions,
                    users.emailVerified,
                    users.email,
                    types.driver_type,
                    types.driver_type_id,
                    users.user_status,
                    users.role_permissions,
                    users.is_signed_up
                FROM
                    user_master users
                INNER JOIN driver_type_master types ON
                    types.driver_type_id = users.driver_type_id`
    };
  }

  /** To Retrieve Data From Db and Set it to the Redis Cache */
  const fetchUserDataFromDb = (request, response, options, callback) => {

    try {

      queryParams = getUserDetailsQuery();

      executeQuery(queryParams.query, null, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), "[user] [FETCH] GET USER DB ERROR",
            "sqlMessage" in err ? err.sqlMessage : err);

          return response.status(409).json({ code: 409, error: "DB ERROR" });
        } else {

          client.setex("user_master", 3600, JSON.stringify(queryResponse)); // set data in redis

          getUserDetailsCache(request, response, options, callback);
        }
      });

    } catch (e) {
      console.error(new Date(), "[user] [FETCH] GET USER CODE ERROR ON DB FETCH", e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  /** retrieve data from redis and preprocess the data for server pagination,sort and filter  */
  const getUserDetailsCache = (request, response, options, callback) => {
    let userParsedData = [];
    let redisResponse;

    let subMenu = ["usermanagement", "customermanagement", "locationmanagement", "chassismanagement", "containerownermanagement", "equipmentmanagement"
      , "generalrouterate", "accessorialcharges", "driverrates",
      "driverdetails", "driverpay"]

    client.get("user_master", (err, cacheData) => { // to retreive data from redis
      try {

        if (err) {
          console.error(new Date(), "[user] [FETCH] GET USER REDIS ERROR",
            "sqlMessage" in err ? err.sqlMessage : err);

          return response.status(409).json({ code: 409, error: "REDIS ERROR" });
        } else {

          /* if data present done preprocessing otherwise set the data to redis by fetching from db */
          if (cacheData) {

            userParsedData = JSON.parse(cacheData); // parse the string json retrieved from redis

            userParsedData = userParsedData.length !== 0 ?
              userParsedData.filter(temp => temp.user_id !== request.userId) : []; // filter the logged in user

            if (request.searchFilter) { // to filter based on the search string if user is searched 
              userParsedData = userParsedData.filter(temp => {
                return (
                  temp.first_name.toLowerCase().replace(/ +/g, "").indexOf(request.searchFilterValue) === 0 ||
                  temp.last_name.toLowerCase().replace(/ +/g, "").indexOf(request.searchFilterValue) === 0 ||
                  temp.email.toLowerCase().replace(/ +/g, "").indexOf(request.searchFilterValue) === 0 ||
                  temp.role.toLowerCase().replace(/ +/g, "").indexOf(request.searchFilterValue) === 0 ||
                  (temp.is_signed_up ? temp.user_status.toLowerCase().replace(/ +/g, "")
                    .indexOf(request.searchFilterValue) === 0 : null) ||
                  temp.permissions.toLowerCase().replace(/ +/g, "").split(',')
                    .filter(item => item.indexOf(request.searchFilterValue) === 0).length !== 0 ||
                  (subMenu.filter(element => {
                    if (element.indexOf(request.searchFilterValue) === 0) {
                      return temp.role_permissions.toLowerCase()
                        .replace(/[^a-zA-Z0-9,:]+/ig, "")
                        .replace(/:+/g, ",").split(',')
                        .filter(elem => elem !== 'drivermanagement' &&
                          elem.indexOf(request.searchFilterValue) === 0).length !== 0
                    }
                  }).length !== 0)
                )
              });
            }

            redisResponse = {
              code: 200,
              error: null,
              data: formatUserData(request, userParsedData) // format the user data with pagination and sorting
            }

            callback(null, redisResponse);
          } else {
            fetchUserDataFromDb(request, response, options, callback);
          }
        }
      } catch (error) {
        console.error(new Date(), "[user] [FETCH] GET USER CODE ERROR ON REDIS CACHE FETCH", error);

        response.status(400).json({ code: 400, error: "CODE ERROR" });
      }
    });
  }

  /** to format the data and done pagination and sorting based on inputs */
  const formatUserData = (request, userParsedData) => {
    let userData;
    let userPaginationLength;

    if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {
      userData = userParsedData.length !== 0 ? _.orderBy(userParsedData, request.column, request.direction).slice((request.offset - 1), request.limit) : [];
    } else {
      userData = userParsedData.length !== 0 ? _.orderBy(userParsedData, 'user_id', 'desc').slice((request.offset - 1), request.limit) : [];
    }

    userPaginationLength = userParsedData.length;

    return { userData, userPaginationLength }
  }

  Usermaster.getUserDetails = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        getUserDetailsCache(request, response, options, callback);
      }
    });

  }

  /** Native Mysql Query of Check Driver is assigned or picked up  
   * Required Input @param params */
  const checkDeactivateDriverQuery = (params) => {
    return {
      "query": "SELECT * from leg_mgmt lm WHERE driver_id = (SELECT driver_id from driver_master dm WHERE user_id = ?) and leg_status <> 'Delivered';",
      "params": params
    };
  }

  /** Native Mysql Query of User Update  
   * Required Input @param params */
  const updateUserDetailsQueryParams = (params) => {
    return {
      "query": "call updateUserDetails(?,?,?,?,?,?,?,?)",
      "params": params
    };
  }

  /** Native Mysql Query of Driver Update  
   * Required Input @param params */
  const updateDriverDataQuery = (params) => {
    return {
      "query": "update driver_master set first_name = ?,last_name = ?,driver_type_id=?,is_deactivated = ? where user_id = ?",
      "params": params
    };
  }

  /** Native Mysql Query of Driver Delete  
   * Required Input @param params */
  const deleteDriverDataQuery = (params) => {
    return {
      "query": "delete from driver_master where user_id = ?",
      "params": params
    };
  }

  /** To perform the update on user and driver table */
  const updateUserDetailsValidation = (request, response, options, callback) => {

    try {

      /* to check with previous driver data to delete, post or update driver based on condtion */
      if (request.previousRole == 'Driver' && request.role !== 'Driver') { // previous role driver new role not driver then delete driver

        queryParams = deleteDriverDataQuery([request.user_id]);

        executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
          if (err) {

            if (('code' in err) && err['code'] == 'ER_ROW_IS_REFERENCED_2') {
              callback(null, { code: 400, error: 'Driver is Referenced' })
            } else {
              console.error(new Date(), "[user] [DELETE] DELETE DRIVER DB ERROR", "sqlMessage" in err ? err.sqlMessage : err);

              response.status(409).json({ code: 409, error: "DB ERROR" });
            }
          } else {
            client.del('driverDetailsData');

            userUpdate(request, response, callback);
          }
        })

      } else {

        /* previous role driver and new role driver with inactive status then check driver assigned or picked up
        if assigned or picked up do nothing otherwise update the status */
        if (request.previousRole == 'Driver' && request.role == 'Driver' && request.user_status == 'inactive') {

          queryParams = checkDeactivateDriverQuery([request.user_id]);

          executeQuery(queryParams.query, queryParams.params, (err, userResponse) => {
            if (err) {
              console.error(new Date(), "[user] [FETCH] CHECK DRIVER ASSIGNED DB ERROR",
                "sqlMessage" in err ? err.sqlMessage : err);

              response.status(409).json({ code: 409, error: "DB ERROR" });
            } else {
              if (userResponse.length === 0) {
                userUpdate(request, response, callback);
              } else {
                callback(null, { code: 409, error: 'Driver is Referenced' })
              }
            }

          });

        } else {

          userUpdate(request, response, callback); // to perform user update

        }
      }

    } catch (e) {
      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  /** Perform user update on user table  */
  const userUpdate = (request, response, callback) => {

    queryParams = updateUserDetailsQueryParams([request.first_name, request.last_name,
    request.role, request.permissions, request.user_id, request.role_permissions,
    request.driver_type_id, request.user_status]);

    executeQuery(queryParams.query, queryParams.params, (err, userResponse) => {
      try {
        if (err) {
          console.error(new Date(), "[user] [UPDATE] UPDATE USER DB ERROR", "sqlMessage" in err ? err.sqlMessage : err);

          response.status(409).json({ code: 409, error: "DB ERROR" });
        } else {
          client.del('user_master');
          client.del('user_notify');

          if (userResponse[0][0].user_status === 'inactive') {

            app.eventEmitter.emit('revokeToken', userResponse[0][0].user_id);

          }

          if (request.previousRole == 'Driver' && userResponse[0][0].role == 'Driver') {

            queryParams = updateDriverDataQuery([request.first_name, request.last_name,
            request.driver_type_id, request.user_status === 'active' ? 0 : 1, request.user_id]);

            executeQuery(queryParams.query, queryParams.params, (updateErr, resp) => {
              if (updateErr) {
                console.error(new Date(), "[user] [UPDATE] DRIVER UPDATE DB ERROR",
                  "sqlMessage" in updateErr ? updateErr.sqlMessage : updateErr);
              } else {
                client.del('driverDetailsData');
              }
            });

          } else if (request.previousRole != 'Driver' && userResponse[0][0].role == 'Driver') {

            queryParams = postDriverDataQuery([request.first_name, request.last_name,
            userResponse[0][0].email, request.user_id, userResponse[0][0].driver_type_id]);

            executeQuery(queryParams.query, queryParams.params, (postErr, res) => {
              if (postErr) {
                console.error(new Date(), "[user] [POST] DRIVER POST DB ERROR",
                  "sqlMessage" in postErr ? postErr.sqlMessage : postErr);
              } else {
                client.del('driverDetailsData')
              }
            });

          }

          callback(null, { code: 200, data: userResponse[0][0] });
        }
      } catch (error) {
        response.status(400).json({ code: 400, error: "CODE ERROR" });
      }
    });
  }

  Usermaster.updateUserDetails = (request, response, options, cb) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        updateUserDetailsValidation(request, response, options, cb);
      }
    });

  }

  /** Native Mysql Query of User Delete  
   * Required Input @param params */
  const deleteUserDetailsQuery = (params) => {
    return {
      "query": "DELETE FROM user_master WHERE user_id=?",
      "params": params
    };
  }

  /** Perform delete on driver and user table */
  const deleteUserDetailsValidation = (userId, role, response, options, callback) => {

    try {

      if (role == 'Driver') {

        queryParams = deleteDriverDataQuery([userId]);

        executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
          if (err) {

            if (('code' in err) && err['code'] == 'ER_ROW_IS_REFERENCED_2') {
              callback(null, { code: 400, error: 'Driver is Referenced' });
            } else {
              console.error(new Date(), "[user] [DELETE] DELETE DRIVER DB ERROR", "sqlMessage" in err ? err.sqlMessage : err);

              response.status(409).json({ code: 409, error: "DB ERROR" });
            }

          } else {
            client.del('driverDetailsData');

            userDelete(userId, response, callback)
          }
        })

      } else {
        userDelete(userId, response, callback)
      }

    } catch (e) {
      console.error(new Date(), "[user] [DELETE] USER CODE ERROR", e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  /** to perform delete on user table */
  const userDelete = (userId, response, callback) => {

    executeQuery("DELETE FROM users_ui_settings WHERE user_id=?", [userId], (userUiErr, queryResponse) => {
      if (userUiErr) {
        console.error(new Date(), "[user] [DELETE] USER UI SETTINGS QUERY ERROR",
          "sqlMessage" in userUiErr ? userUiErr.sqlMessage : userUiErr);

        return response.status(409).json({ code: 409, error: "DB ERROR" });
      }

      queryParams = deleteUserDetailsQuery([userId]);

      executeQuery(queryParams.query, queryParams.params, (err, queryResp) => {

        if (err) {
          console.error(new Date(), "[user] [DELETE] USER DB ERROR", "sqlMessage" in err ? err.sqlMessage : err);

          response.status(409).json({ code: 409, error: "DB ERROR" });
        } else {
          client.del('user_master');
          client.del('user_notify');

          callback(null, { code: 200, data: 'Successfully Deleted' })
        }

      });
    });
  }


  Usermaster.deleteUserDetails = (user_id, role, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        deleteUserDetailsValidation(user_id, role, response, options, callback);
      }
    });

  }

  /* After the login set the user data in the response */
  Usermaster.afterRemote('login', (context, userInstance, next) => {

    Usermaster.findById(context.result.userId, async (err, user_master) => {
      if (err) {

        console.error(new Date(), "[user] [REMOTE] LOGIN AFTER REMOTE USER FETCH ERROR",
          "sqlMessage" in err ? err.sqlMessage : err);

        context.res.status(409).json({ code: 409, error: "DB ERROR" });
      } else {

        if (!user_master) {
          return context.res.status(401).json({ code: 401, error: "Invalid Email Id or Password" });
        }

        context.result.first_name = user_master.first_name;
        context.result.last_name = user_master.last_name;
        context.result.role = user_master.role;
        context.result.emailVerified = user_master.emailVerified;
        context.result.permissions = user_master.permissions;
        context.result.role_permissions = user_master.role_permissions;
        context.result.user_status = user_master.user_status;
        context.result.is_signed_up = user_master.is_signed_up;
        context.result.notification_toggle = user_master.notification_toggle;
        context.result.table_view = user_master.table_view;
        context.result.email = user_master.email;

        if (user_master.role === 'Driver') {
          context.result.driverId = await fetchDriverIdFromDb(context.result.userId, context);
        }

        if (context.result.user_status === 'active') {

          context.res.json({ code: 200, error: null, data: context.result });
        } else {

          app.eventEmitter.emit('revokeToken', context.result.userId);

          context.res.status(403).json({
            code: 403,
            error: "Your account has been de-activated. Please reach out to the Admin."
          });
        }
      }

    });

  });

  const fetchDriverIdFromDb = (userId, context) => {
    return new Promise((resolve, reject) => {
      return executeQuery("SELECT driver_id FROM driver_master where user_id = ?", [userId],
        (err, queryResponse) => {
          if (err) {
            console.error(new Date(), `[user] [FETCH] DRIVER ID QUERY ERROR`,
              "sqlMessage" in err ? err.sqlMessage : err);

            return context.res.json({ code: 200, error: "DB LOGIN ERROR" });
          }

          resolve(queryResponse[0].driver_id);
        })
    })
  }

  /* send password reset link when requested */
  Usermaster.on('resetPasswordRequest', (info) => {

    const url = `${config.ssl ? 'https' : 'http'}://${config.domain}/api/confirm-user`;
    // const url = `${config.ssl ? 'https' : 'http'}://localhost:3000/api/confirm-user`;

    const html = "<h1>Hi " + info.user.first_name + ' ' + info.user.last_name + "</h1> <p>You have requested to reset your password for Lotus TMS application, please click this <a href=" + url + "?access_token=" + info.accessToken.id + ">reset link</a> to change your password.</p> <p>Note: This link is valid for " + info.accessToken.ttl / 60 + " minutes</p> <br> <p>Thanks,<br />Team Lotus TMS</p>";

    Usermaster.app.models.Email.send({
      to: info.email,
      from: config.mail,
      subject: 'Password reset',
      html
    });

  });

  /* after password change to revoke token of logged in user with same id's */
  Usermaster.afterRemote('changePassword', (context, userInstance, next) => {
    app.eventEmitter.emit('revokeToken', context.args.id);
    context.res.json({ data: 'Password changed successfully' });
  });

  /* after set password send response */
  Usermaster.afterRemote('setPassword', (context, userInstance, next) => {

    Usermaster.findById(context.args.id, (err, user) => {
      if (!err && user) {
        !user.is_signed_up ?
          app.eventEmitter.emit('updateIsSignedUp', {
            userId: context.args.id,
            userName: `${user.first_name} ${user.last_name}`
          }) : null;
      }
    })
    app.eventEmitter.emit('revokeToken', context.args.id);
    context.res.json({ data: 'Password reset success' });
  });


  /** Native Mysql Query of Sparse Update  
   * Required Input @param params */
  const sparseUpdateUserQuery = (params) => {
    return {
      "query": "update user_master set ?? = ? where user_id = ?",
      "params": params
    };
  }

  /** to perform update on db with dynamic column and value */
  const performSparseUpdateOnDb = (request, response, options, callback) => {

    try {

      queryParams = sparseUpdateUserQuery([request.column, request.value, request.user_id]);

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), "[user] [UPDATE] SPARSE UPDATE DB ERROR",
            "sqlMessage" in err ? err.sqlMessage : err);

          response.status(409).json({ code: 409, error: "DB ERROR" });
        } else {
          callback(null, { code: 200, data: 'Successfully Updated' });
        }
      });

    } catch (e) {
      console.error(new Date(), "[user] [UPDATE] SPARSE UPDATE CODE ERROR", e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Usermaster.sparseUpdateUser = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        performSparseUpdateOnDb(request, response, options, callback);
      }
    });

  }

  /** resend verification function */
  const resendVerificationEmailToUser = (request, res, options, callback) => {

    Usermaster.findOne({ where: { email: request.email } }, (userError, user) => {
      if (userError) {

        console.error(new Date(), "[user] [FETCH] RESEND VERIFICATION USER FETCH ERROR",
          "sqlMessage" in userError ? userError.sqlMessage : userError);

        return res.status(409).json({ code: 409, error: 'DB ERROR' });
      }

      if (!user) {

        console.error(new Date(), "[user] [FETCH] RESEND VERIFICATION USER NOT FOUND ERROR", "EMAIL_NOT_FOUND");

        return res.status(404).json({ code: 404, error: 'EMAIL_NOT_FOUND' });
      }

      user.createAccessToken(DEFAULT_RESET_DAY_TTL, (accessTokenErr, accessToken) => {

        if (accessTokenErr) {
          console.error(new Date(), "[user] [FETCH] RESEND VERIFICATION CREATE ACCESS TOKEN ERROR",
            "sqlMessage" in accessTokenErr ? accessTokenErr.sqlMessage : accessTokenErr);

          return res.status(409).json({ code: 409, error: 'DB ERROR' });
        }

        Usermaster.passwordreset = false;

        let verifyOptions = {
          type: 'email',
          to: request.email,
          from: config.mail,
          subject: 'Thanks for registering.',
          template: path.resolve(__dirname, '../../server/views/verify.ejs'),
          host: config.domain,
          port: '80',
          redirect: `${config.ssl ? 'https' : 'http'}://${config.domain}/#/authentication/new?access_token=${accessToken.id}`,
          user: Usermaster,
        };

        user.verify(verifyOptions, (err, response, next) => {
          if (err) {
            console.error(new Date(), "[user] [VERIFY] RESEND VERIFICATION USER VERIFY ERROR", err);

            return res.status(409).json({ code: 409, error: 'USER VERIFY ERROR' });
          }

          callback(null, { code: 200, data: 'Successfully Resend Email' });
        });

      });

    });

  }

  /* to resend the verification email for the created user */
  Usermaster.resendVerificationEmail = (request, response, options, cb) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(500).json({ code: 500, error: 'ACCESS TOKEN FAILED' });
      } else {
        options.tokenModel = tokenModel;
        resendVerificationEmailToUser(request, response, options, cb);
      }
    });

  }

  /* to check the access token is valid or not */
  Usermaster.checkTokenExpired = (options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        callback(null, false);
      } else {
        callback(null, true);
      }
    });

  }

  /** Native Mysql Query of Sparse Update UI SETTINGS  
   * Required Input @param params */
  const updateUsersUiSettingsQuery = (params) => {
    return {
      "query": "update users_ui_settings set ?? = ? where user_id = ?",
      "params": params
    };
  }

  /** to perform update users ui settings on db with dynamic column and value */
  const performUsersUiSettingsUpdateOnDb = (request, response, options, callback) => {

    try {

      queryParams = updateUsersUiSettingsQuery([request.column, request.value, request.user_id]);

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), "[user] [UPDATE] SPARSE UPDATE USER UI SETTINGS DB ERROR",
            "sqlMessage" in err ? err.sqlMessage : err);

          response.status(409).json({ code: 409, error: "DB ERROR" });
        } else {
          callback(null, { code: 200, data: 'Successfully Updated' });
        }
      });

    } catch (e) {
      console.error(new Date(), "[user] [UPDATE] SPARSE UPDATE USER UI SETTINGS CODE ERROR", e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Usermaster.updateUiSettings = (request, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        performUsersUiSettingsUpdateOnDb(request, response, options, callback);
      }
    });

  }

  /** Native Mysql Query of fetch UI settings  
   * Required Input @param params */
  const fetchUsersUiSettingsQuery = (params) => {
    return {
      "query": "select orders_dispatch,orders_pending,orders_draft,dispatch_view,invoice_view,invoice_to_generated_view from users_ui_settings where user_id = ?",
      "params": params
    };
  }

  /** to fetch the ui settings of users */
  const fetchUsersUiSettingsFromDb = (userId, response, options, callback) => {

    try {

      queryParams = fetchUsersUiSettingsQuery([userId]);

      executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
        if (err) {
          console.error(new Date(), "[user] [FETCH] UI SETTINGS DB ERROR",
            "sqlMessage" in err ? err.sqlMessage : err);

          return response.status(409).json({ code: 409, error: "DB ERROR" });
        }

        callback(null, { code: 200, data: queryResponse.length !== 0 ? queryResponse[0] : {} });

      });

    } catch (e) {
      console.error(new Date(), "[user] [FETCH] UI SETTINGS CODE ERROR", e);

      response.status(400).json({ code: 400, error: "CODE ERROR" });
    }
  }

  Usermaster.getUsersUiSettings = (userId, response, options, callback) => {

    validateAccessToken(options.access_token, (err, tokenModel) => {
      if (err || !tokenModel) {
        response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
      } else {
        options.tokenModel = tokenModel;
        fetchUsersUiSettingsFromDb(userId, response, options, callback);
      }
    });

  }

  /* event listen on updateIsSignedUp trigger when the new user signed up successfully ang login to the app first time */
  app.eventEmitter.on('updateIsSignedUp', (request) => {

    Usermaster.updateAll({ user_id: request.userId }, { is_signed_up: true, user_status: 'active' }, (err, result) => {
      if (err) {
        console.error(new Date(), "[user] [UPDATE] USER SIGNED UP UPDATE ERROR", "sqlMessage" in err ? err.sqlMessage : err);
      } else {
        client.del('user_master');
        app.eventEmitter.emit('postNotification', {
          userId: request.userId,
          role: 'Admin',
          msg: `User ${request.userName} has signed up successfully`,
          hyperlink: null,
          icon: 'people',
          styleClass: 'mat-text-primary'
        })
      }
    });

  })

  /* event listen on revokeToken trigger when the user change password or deactivated in the app */
  app.eventEmitter.on('revokeToken', (userId) => {

    Usermaster.app.models.AccessToken.find({ where: { userId } }, (err, result) => {
      if (err) {
        console.error(new Date(), "[user] [FETCH] REVOKE TOKEN, TOKEN FETCH ERROR",
          "sqlMessage" in err ? err.sqlMessage : err);
      } else {
        if (result.length !== 0) {

          async.each(result, (temp, callback) => {

            Usermaster.relations.accessTokens.modelTo.destroyById(temp.__data.id, (delErr, info) => {
              if (delErr) {
                console.error(new Date(), "[user] [DELETE] TOKEN REVOKE ERROR", delErr);
              } else {
                console.log(new Date(), "[user] [DELETE] SUCCESSFULLY TOKEN REVOKED");
              }
            });

          }, (loopErr) => { // Called once every item has been iterated over

            if (loopErr) {
              console.error(new Date(), "[user] [DELETE] TOKEN REVOKE ERROR", loopErr);
            } else {
              console.log(new Date(), "[user] [DELETE] TOKEN REVOKED SUCCESSFULLY");
            }

          });

        }
      }
    })
  });

  /** Native Mysql Query of Quickbooks Credential Post  
   * Required Input @param params */
  const getQueryAndParams = (params) => {
    return {
      "query": "INSERT INTO quickbooks_token_master (id,realmId,token_type,access_token,refresh_token,expires_in,x_refresh_token_expires_in,id_token,latency,createdAt) VALUES(1,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE realmId = ?, token_type = ?, access_token = ?, refresh_token = ?, expires_in = ?, x_refresh_token_expires_in = ?, id_token = ?, latency = ?, createdAt = ?;",
      "params": params
    }
  }

  /* event listen on quickbooksTokenAdd trigger when the quickbooks newly get connected */
  app.eventEmitter.on('quickbooksTokenAdd', (response) => {

    queryParams = getQueryAndParams([response.realmId, response.token_type, response.access_token,
    response.refresh_token, response.expires_in, response.x_refresh_token_expires_in, response.id_token,
    response.latency, response.createdAt, response.realmId, response.token_type, response.access_token,
    response.refresh_token, response.expires_in, response.x_refresh_token_expires_in, response.id_token,
    response.latency, response.createdAt]);

    executeQuery(queryParams.query, queryParams.params, (err, result) => {
      if (err) {
        console.error(new Date(), "[user] [POST/UPDATE] ERROR WHILE SAVING QUICKBOOKS TOKEN",
          "sqlMessage" in err ? err.sqlMessage : null);
      } else {
        console.log(new Date(), '[user] [QUIKBOOKS] SUCCESSFULLY UPDATE OR INSERT QUICKBOOKS CREDS');
      }
    })
  })

  /* event listen on orderDeleteUsingPassword trigger when the order get delete to verify user password */
  app.eventEmitter.on('orderDeleteUsingPassword', (response) => {

    Usermaster.findOne({ where: { email: response.email } }, function (err, user) {

      if (err) {
        console.error(new Date(), "[user] [DELETE] [ORDER] ERROR ON DB", err);

        response.res.status(409).json({ code: 409, error: "DB ERROR" });

      } else if (user) {

        user.hasPassword(response.password, function (checkErr, isMatch) {
          if (checkErr) {
            console.error(new Date(), "[user] [DELETE] [ORDER] ERROR ON CODE", checkErr);

            response.res.status(400).json({ code: 400, error: "CODE ERROR" });
          } else if (isMatch) {

            executeQuery("call delete_order_data(?,?);", [response.order_id, response.orderContainerId],
              (deleteErr, queryResponse) => {
                if (deleteErr) {
                  if (('code' in deleteErr) && deleteErr['code'] == 'ER_ROW_IS_REFERENCED_2') {
                    response.cb(null, { code: 400, error: 'Order Referenced' });
                  } else {
                    console.error(new Date(), "[order_mgmt.js] [DELETE] ORDER QUERY ERROR",
                      "sqlMessage" in deleteErr ? deleteErr.sqlMessage : deleteErr);

                    response.res.status(409).json({ code: 409, error: "DB ERROR" });
                  }
                } else {
                  client.del(response.key);
                  client.del("advanced_search");
                  client.del('orderSearchData');
                  client.del('driverPayData');
                  client.del("driverDetailsData");
                  response.cb(null, { code: 200, data: "Succcessfully Deleted" });
                }
              })

          } else {
            console.error(new Date(), "[user] [DELETE] [ORDER] INVALID PASSWORD");

            response.res.status(401).json({ code: 401, error: "UNAUTHORIZED" });
          }
        });
      } else {
        console.error(new Date(), "[user] [DELETE] [ORDER] INVALID PASSWORD");

        response.res.status(404).json({ code: 404, error: "USER NOT FOUND" });
      }

    });

  })
};
