'use strict';

const multiparty = require('multiparty');
const { join, extname } = require('path');
const fs = require('fs');
const AWS = require('aws-sdk');
var config = require('./../../server/boot/load-config').getConfig();
const async = require('async');

AWS.config.update({
    secretAccessKey: config.awsSecretAccessKey,
    accessKeyId: config.awsAccessKeyId,
    region: config.awsRegion
});
const s3 = new AWS.S3({});

const app = require('../../server/server.js');

module.exports = function (Uploaddocumentlinks) {

    //database connector
    Uploaddocumentlinks.executeQuery = function (ds, query, params, callback) {
        ds.connector.query(query, params, callback);
    }
    //Access Token
    Uploaddocumentlinks.validateAccessToken = function (access_token, callback) {
        this.app.models.AccessToken.findById(access_token, callback)
    }
    //calling internally by loopback(overriden)
    Uploaddocumentlinks.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    //upload file in leg_mgmt
    const getFileFromRequest = (req) => new Promise((resolve, reject) => {
        const form = new multiparty.Form();
        form.parse(req, (err, fields, files) => {
            if (err) reject(err);
            if (!files) reject('File was not found in form data.');
            else resolve({ files, fields });
        });
    });

    const documentLinksUpdateOrInsertQuery = (params) => {
        return {
            query: `insert into upload_document_links 
                        (order_container_chassis_id, file_name, document_link)
                    values (?,?,?)
                    on duplicate key update
                        file_name = ?,
                        document_link = ?;`,
            params
        }
    }


    Uploaddocumentlinks.uploadFile = (req, response, options, cb) => {

        Uploaddocumentlinks.app.models.AccessToken.findById(options.access_token, async (err, token) => {
            if (err || !token) {

                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });

            } else {
                // extract the file from the request object
                const data = await getFileFromRequest(req);

                let files = data.files;

                let fields = JSON.parse(data.fields.fields[0]);

                let called = false;

                const q = async.queue((task, callback) => {
                    s3.upload({
                        Bucket: config.awsS3BucketName,
                        ACL: config.awsS3ACL,
                        Key: ['orders', fields.order_number + '-' + fields.sequence, task.originalFilename].join('/'),
                        ContentType: task.headers['content-type'],
                        Body: fs.createReadStream(task.path)
                    }, (s3Err, result) => {
                        if (s3Err) {
                            console.error(new Date(), `[order] [UPLOAD] AWS UPLOAD ERROR`, s3Err);
                            return callback(s3Err);
                        } else {

                            var ds = Uploaddocumentlinks.dataSource;
                            var queryParams;

                            queryParams = documentLinksUpdateOrInsertQuery([fields.id, result['key'], result['location'],
                            result['key'], result['location']]);

                            Uploaddocumentlinks.executeQuery(ds, queryParams.query, queryParams.params,
                                function (postErr, queryResponse) {
                                    if (postErr) {
                                        console.error(new Date(), `[order] [UPSERT] ${task.originalFilename} FILE LINK QUERY ERROR`,
                                            "sqlMessage" in postErr ? postErr.sqlMessage : postErr);

                                        return callback("sqlMessage" in postErr ? postErr.sqlMessage : postErr);
                                    } else {
                                        console.log(new Date(), `[order] [UPLOAD] FILE ${task.originalFilename} UPLOADED`);

                                        callback();
                                    }
                                });

                        } // return the values of the successful AWS S3 request
                    })
                }, 5);

                q.push(files.files, (pushErr) => {
                    if (pushErr && !called) {
                        console.error(new Date(), `[order] [UPLOAD] QUEUE ERROR`, pushErr);

                        q.kill();

                        called = true

                        for (const s of app.sockets) {
                            if (s.id === fields.userId) {
                                s.emit('fileUpload', { code: 409, error: "Some Files May Not be Uploaded" });
                            }
                        }
                    }
                })

                q.error(function (error, task) {
                    console.error(new Date(), '[order] [UPLOAD] Queue Error', error, task);

                    for (const s of app.sockets) {
                        if (s.id === fields.userId) {
                            s.emit('fileUpload', { code: 409, error: "Some Files May Not be Uploaded" });
                        }
                    }
                });

                if (process.env.NODE_ENV === 'local') {
                    q.drain = () => {
                        for (const s of app.sockets) {
                            if (s.id === fields.userId) {
                                s.emit('fileUpload', { code: 200, error: null });
                            }
                        }
                    }
                } else {
                    q.drain(() => {
                        for (const s of app.sockets) {
                            if (s.id === fields.userId) {
                                s.emit('fileUpload', { code: 200, error: null });
                            }
                        }
                    });
                }

                cb(null, "File Uploading...");

            }
        })
    };

    Uploaddocumentlinks.getOrderUploadedDocumentListQuery = function (params) {
        return {
            "query": "select file_name from upload_document_links where order_container_chassis_id = ?",
            "params": params
        };
    }

    Uploaddocumentlinks.getOrderUploadedDocumentListValidation = function (order_container_chassis_id,
        response, options, callback) {

        var ds = Uploaddocumentlinks.dataSource;
        var queryParams;
        try {
            queryParams = Uploaddocumentlinks.getOrderUploadedDocumentListQuery([order_container_chassis_id]);
            Uploaddocumentlinks.executeQuery(ds, queryParams.query, queryParams.params,
                function (err, queryResponse) {
                    if (err) {
                        console.error(new Date(), "[order] [FETCH] FILE LINK QUERY ERROR",
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: "DB ERROR" });
                    } else {
                        queryResponse = queryResponse.map(temp => temp.file_name);

                        callback(null, { code: 200, data: queryResponse });
                    }
                });
        } catch (e) {
            console.error(new Date(), "[order] [FETCH] FILE LINK CODE ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Uploaddocumentlinks.getOrderUploadedDocumentList = function (order_container_chassis_id, response,
        options, callback) {

        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getOrderUploadedDocumentListValidation(order_container_chassis_id, response,
                    options, callback);
            }
        });

    }

    const deleteFileToS3 = (fileName, options = [{}]) => {
        // return a promise
        return new Promise((resolve, reject) => {
            var params = {
                Bucket: config.awsS3BucketName,
                Key: fileName
            };
            s3.deleteObject(params, function (err, data) {
                if (err) {
                    console.error(new Date(), "[order] [DELETE] AWS DELETE ERROR", err)
                    return reject(err);
                }
                if (data) {
                    console.log(new Date(), "[order] FILE DELETED SUCCESSFULLY");
                    resolve(data);
                }
            });
        });
    };

    Uploaddocumentlinks.deleteFile = async (data, response, options) => {

        Uploaddocumentlinks.app.models.AccessToken.findById(options.access_token, async (err, token) => {
            if (err || !token) {
                return response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                // try {
                var updatedata = JSON.stringify(data);
                var parsedData = JSON.parse(updatedata);
                var fileName = parsedData.fileName;
                var order_container_chassis_id = parsedData.order_container_chassis_id;

                await deleteFileToS3(fileName);

                var originalFileName = fileName;

                await Uploaddocumentlinks.destroyAll(
                    {
                        order_container_chassis_id: order_container_chassis_id,
                        file_name: originalFileName
                    });
                // } catch (error) {
                //     return response.status(400).json({ code: 400, error: "CODE ERROR" });
                // }
            }
        })
    };

    Uploaddocumentlinks.getFileValidation = function (data, response, options, callback) {

        var updatedata = JSON.stringify(data);
        var parsedData = JSON.parse(updatedata);
        var fileName = parsedData.fileName;

        s3.getObject(
            {
                Bucket: config.awsS3BucketName,
                Key: fileName
            }, function (err, s3Response) {
                if (err) {
                    console.error(new Date(), "[order] [FETCH] AWS RETRIEVE FILE ERROR", err);

                    response.status(409).json({ code: 409, error: "S3 ERROR" });
                } else {
                    callback(null, {
                        ContentType: s3Response.ContentType,
                        buffer: s3Response.Body
                    })
                }
            });
    }

    Uploaddocumentlinks.getFile = function (data, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getFileValidation(data, response, options, callback);
            }
        });
    }


    Uploaddocumentlinks.getOrderFileSizeValidation = function (request, response, options, callback) {

        var updatedata = JSON.stringify(request);
        var parsedData = JSON.parse(updatedata);
        var fileName = parsedData.fileName;

        s3.listObjectsV2(
            {
                Bucket: config.awsS3BucketName,
                Prefix: fileName
            }, function (err, awsResponse) {
                if (err) {
                    console.error(new Date(), "[order] [FETCH] AWS RETRIEVE FILE ERROR", err);

                    response.status(409).json({ code: 409, error: "S3 ERROR" });
                } else {
                    let fileSizeObject = {};

                    awsResponse.Contents ? awsResponse.Contents.forEach(temp => {
                        fileSizeObject[temp.Key] = temp.Size;
                    }) : null;

                    callback(null, { code: 200, data: fileSizeObject });
                }
            });
    }

    Uploaddocumentlinks.getOrderFileSize = function (request, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getOrderFileSizeValidation(request, response, options, callback);
            }
        });
    }
};


