'use strict';

const async = require('async');
const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const _ = require('lodash');
const moment = require('moment');
const bing = require('../../server/boot/bingapi');
const app = require('../../server/server.js');

const schedule = require('node-schedule');

module.exports = function (Ordermgmt) {

    let queryParams;

    //Datasource connector
    const executeQuery = (query, params, callback) => {
        Ordermgmt.dataSource.connector.query(query, params, callback);
    }

    /** Mysql Native transaction initialization  */
    const transactionBegin = (callback) => {
        Ordermgmt.dataSource.connector.beginTransaction(Ordermgmt.Transaction.REPEATABLE_READ, callback);
    }

    /** To Execute Transaction Query  */
    const transactionQuery = (transaction, query, params, callback) => {
        transaction.query(query, params, callback);
    }

    /** To Execute query rollback when thing go wrong  */
    const transactionRollback = (transaction, response, error) => {
        transaction.rollback(() => {
            transaction.release();
            response.status(409).json({ code: 409, error });
        })
    }

    //Access Token
    const validateAccessToken = (access_token, callback) => {
        Ordermgmt.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Ordermgmt.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    const fetchOrdersNeedToMoveInvoice = () => {
        return {
            "query": `
        SELECT
            om.order_id,
            om.customer_id,
            om.order_type_id,
            occ.order_container_chassis_id,
            lm.not_delivered,
            JSON_ARRAYAGG(JSON_OBJECT('leg_number',
            lm3.leg_number,
            'leg_type_id',
            lm3.leg_type_id,
            'pu_time',
            UNIX_TIMESTAMP(lm3.pu_time) * 1000,
            'dl_time',
            UNIX_TIMESTAMP(lm3.dl_time) * 1000)) legs
        FROM
            order_mgmt om
        INNER JOIN order_container_chassis occ ON
            occ.order_id = om.order_id
        LEFT JOIN (
            SELECT
                COUNT(*) not_delivered,
                lm2.order_container_chassis_id
            FROM
                leg_mgmt lm2
            WHERE
                leg_status <> 'Delivered'
            GROUP BY
                lm2.order_container_chassis_id) lm ON
            lm.order_container_chassis_id = occ.order_container_chassis_id
        LEFT JOIN leg_mgmt lm3 ON
            lm3.order_container_chassis_id = occ.order_container_chassis_id
        WHERE
            occ.order_status = 'Dispatch'
            and occ.isReadyToInvoice = 1
            and set_time_to_invoice <= date_sub(now(),
            interval
            (CASE
                DAYNAME(set_time_to_invoice)
                WHEN 'Friday' THEN 72
                WHEN 'Saturday' THEN 48
                ELSE 24
            END) hour)
            and lm.not_delivered is null
        GROUP BY
            order_container_chassis_id;`
        };
    }

    const updateOrdersDelivered = (params) => {
        return {
            query: "UPDATE order_container_chassis SET order_status = 'Delivered',actual_time = CURRENT_TIMESTAMP() WHERE find_in_set(order_container_chassis_id,?)",
            params
        }
    }

    const postAccessorialCharges = (params) => {
        return {
            query: "call postChassisRentalCharge(?,?,?,?)",
            params
        }
    }

    const updateAccessorialCharges = (params) => {
        return {
            query: "call updateChassisRentalCharge(?,?,?,?)",
            params
        }
    }

    if (process.env.NODE_ENV && process.env.NODE_ENV !== 'local') {
        /* scheduler job runs in every weekdays midnight 12'o clock on cst refer https://crontab.guru/#0_0_*_*_1-5 for scheduler time */
        schedule.scheduleJob({ rule: "0 * * * 1-5", tz: "america/chicago" }, async () => {

            queryParams = fetchOrdersNeedToMoveInvoice();

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [SCHEDULER] [FETCH] SCHEDULER UPDATE QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return
                }
                if (queryResponse.length !== 0) {
                    let ids = queryResponse.map(temp => temp.order_container_chassis_id);

                    queryResponse.forEach(temp => {
                        temp.legs = temp.legs ? JSON.parse(temp.legs).sort((a, b) => a.leg_number - b.leg_number) : [];

                        temp.formatedLegs = temp.legs.filter(elem => elem.leg_type_id !== 1 && elem.chassis_number);

                        temp.formatedLegs.forEach(elem => {
                            elem.pickupDate = elem.pu_time && new Date(elem.pu_time).getFullYear() !== 1970 ? new Date(elem.pu_time).toDateString() : null;
                            elem.deliveryDate = elem.dl_time && new Date(elem.dl_time).getFullYear() !== 1970 ? new Date(elem.dl_time).toDateString() : null;
                        })

                        if (temp.formatedLegs.length !== 0 && temp.formatedLegs[temp.formatedLegs.length - 1].deliveryDate && temp.formatedLegs[0].pickupDate) {
                            temp.timeDifference = new Date(temp.formatedLegs[temp.formatedLegs.length - 1].deliveryDate).getTime() - new Date(temp.formatedLegs[0].pickupDate).getTime();

                            temp.days = Math.round(temp.timeDifference / (1000 * 3600 * 24)) + 1;
                        } else {
                            temp.days = 0;
                        }
                    })

                    let firstResponse = queryResponse;

                    queryParams = updateOrdersDelivered([ids.join(',')]);

                    executeQuery(queryParams.query, queryParams.params, async (updateErr, updateQueryResponse) => {
                        if (updateErr) {
                            console.error(new Date(), "[order_mgmt.js] [SCHEDULER] [UPDATE] SCHEDULER UPDATE QUERY ERROR",
                                "sqlMessage" in updateErr ? updateErr.sqlMessage : updateErr);

                            return
                        }
                        console.log(new Date(),
                            `[order_mgmt.js] [SCHEDULER] JOB FINSISHED SUCCESSFULLY ${'affectedRows' in updateQueryResponse ? updateQueryResponse.affectedRows : 0} UPDATED`);
                        client.del("ordersDispatchData");
                        client.del("dispatchData");
                        client.del("invoiceData");
                        client.del("advanced_search");
                        client.del('orderSearchData');
                        client.del("driverDetailsData");

                        for (let temp of firstResponse) {
                            if (temp.order_type_id !== 3) {
                                await postOrUpdateChassisRentalCharges(temp.order_container_chassis_id, temp.days, temp.customer_id);
                            }
                        }
                    })
                }
            });

        })
    }

    const postOrUpdateChassisRentalCharges = (id, days, customerId) => {
        return new Promise((resolve, reject) => {
            queryParams = checkCharges(['Chassis Rental', id]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [SCHEDULER] [CHECK CHARGES] SCHEDULER UPDATE QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return resolve("error");
                }
                if (days !== 0) {
                    if (queryResponse.length === 0) {

                        queryParams = postAccessorialCharges([customerId, 'Chassis Rental', id, days]);

                        return executeQuery(queryParams.query, queryParams.params, (postErr, queryRes) => {
                            if (postErr) {
                                console.error(new Date(), "[order_mgmt.js] [SCHEDULER] [POST CHARGES] SCHEDULER UPDATE QUERY ERROR",
                                    "sqlMessage" in postErr ? postErr.sqlMessage : postErr);

                                return resolve("error");
                            }

                            console.log(new Date(), `[order_mgmt.js] [SCHEDULER] [CHASSIS RENTAL CHARGE] SCHEDULER CHARGE POSTED`);

                            resolve("success");
                        })
                    } else {
                        queryParams = updateAccessorialCharges([customerId, 'Chassis Rental', id, days]);

                        return executeQuery(queryParams.query, queryParams.params, (updateErr, queryResp) => {
                            if (updateErr) {
                                console.error(new Date(), "[order_mgmt.js] [SCHEDULER] [UPDATE CHARGES] SCHEDULER UPDATE QUERY ERROR",
                                    "sqlMessage" in updateErr ? updateErr.sqlMessage : updateErr);

                                return resolve("error");
                            }

                            console.log(new Date(), `[order_mgmt.js] [SCHEDULER] [CHASSIS RENTAL CHARGE] SCHEDULER CHARGE UPDATED`);

                            resolve("success");
                        })
                    }
                } else {
                    console.log(new Date(), `[order_mgmt.js] [SCHEDULER] [CHASSIS RENTAL CHARGE] NOT POSTED OR UPDATED`);

                    resolve("nothing");
                }
            })

        })
    }

    //Post order management
    const createCustomerSpecficRoute = (params) => {
        return {
            "query": `insert
                          into
                          route_rate_master (customer_id,
                          start_loc,
                          des_loc,
                          rate,
                          distance,
                          order_type_id,
                          pu_name,
                          dl_name,
                          format_address)
                      values (?,?,?,?,?,?,UCASE(?),UCASE(?),?);`,
            "params": params
        };
    }

    const postOrderManagementQueryParams = (params) => {
        return {
            "query": "call postOrderData(?,?,?,?,?,?,?);",
            "params": params
        };
    }

    const postOrderTagQueryParams = (params) => {
        return {
            "query": "insert into order_tag(order_id,tag_id) values ?;",
            "params": params
        };
    }

    const getMilesRangeFlag = (params) => {
        return {
            "query": "SELECT order_flag_id from order_flag of2 WHERE ? BETWEEN range_from and ifnull(range_to,2147483647);",
            "params": params
        };
    }

    const postOrderContainerChassiQueryParams = (params) => {
        return {
            "query": "call postContainerData(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",
            "params": params
        };
    }

    const postLegDataInboundQuery = (params) => {
        return {
            "query": "call postLegDataInbound(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",
            "params": params
        };
    }

    const postLegDataOutboundQuery = (params) => {
        return {
            "query": "call postLegDataOutbound(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",
            "params": params
        };
    }

    const postLegDataTruckMoveQuery = (params) => {
        return {
            "query": `insert
                          into
                          leg_mgmt(chassis_number,
                            container_number,
                            pu_loc,
                            dl_loc,
                            pu_name,
                            dl_name,
                            pu_time,
                            dl_time,
                            est_miles,
                            rate,
                            created_by,
                            order_container_chassis_id,
                            order_id,
                            leg_number,
                            leg_status,
                            leg_type_id,
                            created_on,
                            is_auto_created,
                            pu_time_to,
                            dl_time_to,
                            is_brokered,
                            carrier_id)
                      values (UCASE(?),UCASE(?),?,?,UCASE(?),UCASE(?),?,?,?,?,?,?,?,1,?,?,current_timestamp,1,?,?,?,?);`,
            "params": params
        };
    }

    const postOrderAccessorialChargesQuery = function (params) {
        return {
            "query": "call postOrderAccessorialCharges(?,?,?);",
            "params": params
        };
    }

    const postFuelSurcharge = function (params) {
        return {
            "query": "call postFuelSurcharge(?,?,?,?);",
            "params": params
        };
    }

    const postNotes = (params) => {
        return {
            "query": "INSERT INTO notes_history(notes,notes_description,created_by,created_on,order_container_chassis_id,user_id,is_autocreated) VALUES(?,?,?,CURRENT_TIMESTAMP,?,?,1);",
            "params": params
        };
    }

    const postOrderManagementValidation = (request, response, options, cb) => {
        try {

            transactionBegin(async (transactionErr, transaction) => { // begin transaction

                if (transactionErr) {
                    console.error(new Date(), "[order_mgmt.js] [SETUP] TRANSACTION ERROR", transactionErr);

                    return response.status(409).json({ code: 409, error: "DB TRANSACTION ERROR" });
                }

                if (request.isRouteCreate) {

                    request.loadedLegMiles = await bing.getMiles(request.pu_formated_loc,
                        request.dl_formated_loc); // bing api to fetch miles

                    // request.loadedLegMiles = await bing.getMilesFromAzure(request.pu_formated_loc,
                    //     request.dl_formated_loc);

                    queryParams = createCustomerSpecficRoute([0, request.pu_formated_loc,
                        request.dl_formated_loc, 0, request.loadedLegMiles, request.order_type_id, request.pu_name,
                        request.dl_name, request.format_address]); // form create route query and params

                    await transactionQuery(transaction, queryParams.query,
                        queryParams.params, (err, queryResponse) => { // transaction query to perform insert
                            if (err) {
                                console.error(new Date(),
                                    "[order_mgmt.js] [POST] ROUTE RATE QUERY ERROR",
                                    "sqlMessage" in err ? err.sqlMessage : err);

                                client.del('route_rate');

                                /* if err then the rollback take place */
                                return transactionRollback(transaction, response, "ROUTE CREATE DB ERROR");
                            } else {
                                console.log(new Date(), "[order_mgmt.js] [POST] ROUTE RATE SUCCESSFULLY INSERTED");
                            }
                        });

                }

                request.loadedLegMiles = request.loadedLegMiles === 0 ?
                    await bing.getMiles(request.pu_loc, request.dl_loc) : request.loadedLegMiles;

                request.orderFlagId = await getOrderFlagId(request.loadedLegMiles); // to get order flag id to post on order table

                queryParams = postOrderManagementQueryParams([request.customer_id, request.order_notes, request.order_type_id,
                request.rail_cut, request.customer_reference, request.booking_number, request.format_address]); // form post order query and params

                transactionQuery(transaction, queryParams.query, queryParams.params,
                    (orderInsertErr, orderInsertResponse) => { // transaction query to perform insert order
                        if (orderInsertErr) {
                            console.error(new Date(), "[order_mgmt.js] [POST] ORDER TABLE QUERY ERROR",
                                "sqlMessage" in orderInsertErr ? orderInsertErr.sqlMessage : orderInsertErr);

                            return transactionRollback(transaction, response, "ORDER POST DB ERROR"); // rollback
                        } else {
                            request.order_id = orderInsertResponse[0][0].order_id;

                            request.order_number = orderInsertResponse[0][0].order_number;

                            if (request.tags && request.tags !== null && request.tags.length !== 0) {

                                request.tagArray = [];

                                request.tags.forEach(temp => {
                                    request.tagArray.push([request.order_id, temp]);
                                })

                                /* post order tag on order tag table */
                                queryParams = postOrderTagQueryParams([request.tagArray]);
                                transactionQuery(transaction, queryParams.query, queryParams.params,
                                    (tagInsertErr, tagInsertResponse) => {
                                        if (tagInsertErr) {
                                            console.error(new Date(),
                                                "[order_mgmt.js] [POST] ORDER TAG QUERY ERROR",
                                                "sqlMessage" in tagInsertErr ? tagInsertErr.sqlMessage : tagInsertErr);

                                            return transactionRollback(transaction, response, "ORDER TAG DB ERROR");
                                        }
                                    })

                            }

                            /* loop through the order container chassis to put all the data on container table */
                            async.mapSeries(request.order_container_chassis, (temp, callback) => {

                                if ((temp.container_size === '20' && temp.weight >= 39000) ||
                                    (temp.container_size !== '20' && temp.weight >= 44001)) {
                                    request.triaxle = "Y"
                                } else {
                                    request.triaxle = "N"
                                }

                                /* form container table insert query and param */
                                queryParams = postOrderContainerChassiQueryParams([
                                    temp.containerNameDetails.container_name, temp.containerNameDetails.chassis_name,
                                    temp.container_number, temp.container_type, temp.container_size,
                                    temp.weight, temp.pu_ref, temp.dl_ref, temp.lfd_date, temp.hazmat_req,
                                    temp.hazmatinfo.haz_classes, temp.hazmatinfo.haz_contact,
                                    temp.hazmatinfo.haz_name, temp.hazmatinfo.haz_page_ref,
                                    temp.hazmatinfo.haz_weight, temp.hazmatinfo.un_number, temp.is_brokered ? 'Assigned' : temp.order_status,
                                    temp.container_sequence, temp.is_dispatch,
                                    temp.containerNameDetails.hire_dehire_loc, temp.containerNameDetails.hire_name,
                                    temp.chassis_number, request.triaxle, temp.is_draft, temp.pieces, temp.seal_no, temp.temperature,
                                    request.pu_name, request.pu_loc, request.dl_name, request.dl_loc, request.loadedLegMiles, request.loggedInUser,
                                    request.p_city, request.p_zip, request.d_city, request.d_zip, request.est_pickup_to_time,
                                    request.est_pickup_from_time, request.est_delivery_to_time, request.est_delivery_from_time,
                                    request.orderFlagId, request.rateType, request.p_phone, request.p_email, request.d_phone,
                                    request.d_email, request.driver_notes, request.fetchedRate, temp.eta,
                                    temp.is_brokered, temp.carrier_rate, temp.carrier_id]);

                                transactionQuery(transaction, queryParams.query, queryParams.params,
                                    async (containerInsertErr, containerInsertResponse) => {
                                        if (containerInsertErr) {

                                            console.error(new Date(),
                                                "[order_mgmt.js] [POST] CONTAINER QUERY ERROR",
                                                "sqlMessage" in containerInsertErr ?
                                                    containerInsertErr.sqlMessage : containerInsertErr);

                                            return callback(containerInsertErr);

                                        }

                                        request.containerLevelId = containerInsertResponse[0][0].insertId;

                                        if (temp.is_draft === 0) {
                                            await legsAccessoriesAndNotesCreation(request, temp, transaction);

                                            if (temp.is_dispatch === 1) {
                                                request.key = 'ordersDispatchData';
                                                request.isDeleteDispatchCache = true;
                                            } else {
                                                request.key = 'ordersPendingData';
                                            }
                                        } else {

                                            request.key = 'ordersDraftData';

                                            console.log(new Date(),
                                                "[order_mgmt.js] [DRAFT] LEGS AND CHARGES NOT INSERTED");
                                        }

                                        callback();
                                    })
                            }, (err, result) => {
                                if (err) {
                                    console.error(new Date(), "[order_mgmt.js] [LOOP] ERROR",
                                        "sqlMessage" in err ? err.sqlMessage : err);
                                    return transactionRollback(transaction, response, "LOOP ERROR");
                                } else {
                                    transaction.commit((commitErr) => {
                                        if (commitErr) {
                                            console.error(new Date(), "[order_mgmt.js] [COMMIT] ERROR", commitErr);

                                            return transactionRollback(transaction, response, "COMMIT ERROR");
                                        } else {
                                            transaction.release();

                                            client.del(request.key);

                                            request.isDeleteDispatchCache ?
                                                client.del("dispatchData") : null;

                                            client.del("advanced_search");
                                            client.del('orderSearchData');
                                            cb(null, { code: 200, data: orderInsertResponse });
                                        }
                                    })
                                }
                            })

                        }
                    })
            })


        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    /** leg and charges and notes autocreation */
    const legsAccessoriesAndNotesCreation = async (request, temp, transaction) => {

        if (request.order_type.includes('Inbound')) {

            request.emptyLegMiles =
                (
                    temp.containerNameDetails.hire_dehire_loc !== request.hireDehireLocation &&
                    temp.containerNameDetails.hire_dehire_loc !== null && request.dl_loc !== null
                ) ? await bing.getMiles(request.dl_loc, temp.containerNameDetails.hire_dehire_loc) :
                    request.nextLegMiles;

            queryParams = postLegDataInboundQuery([temp.chassis_number, temp.container_number, request.pu_loc,
            request.dl_loc, request.pu_name, request.dl_name, temp.containerNameDetails.hire_dehire_loc,
            temp.containerNameDetails.hire_name, request.est_pickup_from_time, request.est_delivery_from_time,
            request.loadedLegMiles, request.emptyLegMiles, request.fetchedRate, request.loggedInUser,
            request.containerLevelId, request.order_id, request.est_pickup_to_time, request.est_delivery_to_time,
            temp.is_brokered, temp.carrier_id]);

            await transactionQuery(transaction, queryParams.query,
                queryParams.params, (legErr, legInboundInsert) => {
                    if (legErr) {
                        console.error(new Date(), `[order_mgmt.js] [POST] [INBOUND] ${request.order_number} LEGS QUERY ERROR`,
                            "sqlMessage" in legErr ? legErr.sqlMessage : legErr);
                    } else {
                        console.log(new Date(), `[order_mgmt.js] [POST] [INBOUND] ${request.order_number} LEGS ADDED SUCCESSFULLY`);
                    }
                });

            request.nextLegMiles = request.emptyLegMiles;

        } else if (request.order_type.includes('Outbound')) {

            request.emptyLegMiles =
                (
                    temp.containerNameDetails.hire_dehire_loc !== request.hireDehireLocation &&
                    temp.containerNameDetails.hire_dehire_loc !== null && request.pu_loc !== null
                ) ? await bing.getMiles(temp.containerNameDetails.hire_dehire_loc, request.pu_loc) :
                    request.nextLegMiles;


            queryParams = postLegDataOutboundQuery([temp.chassis_number, temp.container_number, request.pu_loc,
            request.dl_loc, request.pu_name, request.dl_name, temp.containerNameDetails.hire_dehire_loc,
            temp.containerNameDetails.hire_name, request.est_pickup_from_time, request.est_delivery_from_time,
            request.loadedLegMiles, request.emptyLegMiles, request.fetchedRate, request.loggedInUser,
            request.containerLevelId, request.order_id, request.est_pickup_to_time, request.est_delivery_to_time,
            temp.is_brokered, temp.carrier_id]);

            await transactionQuery(transaction, queryParams.query,
                queryParams.params, (legErr, respdata) => {
                    if (legErr) {
                        console.error(new Date(), `[order_mgmt.js] [POST] [OUTBOUND] ${request.order_number} LEGS QUERY ERROR`,
                            "sqlMessage" in legErr ? legErr.sqlMessage : legErr);
                    } else {
                        console.log(new Date(), `[order_mgmt.js] [POST] [OUTBOUND] ${request.order_number} LEGS ADDED SUCCESSFULLY`);
                    }
                });

            request.nextLegMiles = request.emptyLegMiles;

        } else if (request.order_type === 'Truck Move' || request.order_type === 'One Way Empty - Intermodal' ||
            request.order_type === 'One Way Loaded - Intermodal') {

            queryParams = postLegDataTruckMoveQuery([temp.chassis_number, temp.container_number, request.pu_loc,
            request.dl_loc, request.pu_name, request.dl_name, request.est_pickup_from_time,
            request.est_delivery_from_time, request.loadedLegMiles, request.fetchedRate, request.loggedInUser,
            request.containerLevelId, request.order_id, temp.is_brokered ? 'Assigned' : 'Open',
            request.order_type === 'One Way Empty - Intermodal' ? 3 : 4, request.est_pickup_to_time,
            request.est_delivery_to_time, temp.is_brokered, temp.carrier_id]);

            await transactionQuery(transaction, queryParams.query,
                queryParams.params, (legErr, respdata) => {
                    if (legErr) {
                        console.error(new Date(),
                            `[order_mgmt.js] [POST] [TM/One Way Empty - Intermodal/One Way Loaded - Intermodal] ${request.order_number} LEG QUERY ERROR`,
                            "sqlMessage" in legErr ? legErr.sqlMessage : legErr);
                    } else {
                        console.log(new Date(),
                            `[order_mgmt.js] [POST] [TM/One Way Empty - Intermodal/One Way Loaded - Intermodal] ${request.order_number} LEG ADDED SUCCESSFULLY`);
                    }
                });

        }

        request.hireDehireLocation = temp.containerNameDetails.hire_dehire_loc

        await postAccessories(request, 'Fuel Surcharge', transaction, true);

        if (temp.weight >= 44001) {

            await postAccessories(request, 'Overweight', transaction);
        }

        if (temp.hazmat_req == "1") {

            await postAccessories(request, 'HAZMAT Surcharge', transaction);
        }

        if ((temp.container_size === '20' && temp.weight >= 39000) ||
            (temp.container_size !== '20' && temp.weight >= 44001)) {

            await postAccessories(request, 'Triaxle Fee', transaction)
        }

        if (request.order_notes) {

            queryParams = postNotes([request.order_notes, 'order_notes', request.loggedInUser,
            request.containerLevelId, request.loggedInUserId]);

            await transactionQuery(transaction, queryParams.query,
                queryParams.params, (notesErr, respdata) => {
                    if (notesErr) {
                        console.error(new Date(), "[order_mgmt.js] [POST] NOTES QUERY ERROR",
                            "sqlMessage" in notesErr ? notesErr.sqlMessage : notesErr);
                    }
                });
        }
    }

    const postAccessories = async (request, charge, transaction, isFuelSurcharge) => {

        if (isFuelSurcharge) {
            queryParams = postFuelSurcharge([request.customer_id, charge, request.containerLevelId, request.fetchedRate]);
        } else {
            queryParams = postOrderAccessorialChargesQuery([request.customer_id, charge, request.containerLevelId]);
        }

        await transactionQuery(transaction, queryParams.query,
            queryParams.params, (chargesErr, AccessoriesRates) => {
                if (chargesErr) {
                    console.error(new Date(),
                        `[order_mgmt.js] [POST] [${charge.toUpperCase()}] ACCESSORIES QUERY ERROR`,
                        "sqlMessage" in chargesErr ? chargesErr.sqlMessage : chargesErr);
                }
            });
    }

    const getOrderFlagId = (miles) => {
        return new Promise((resolve, reject) => {

            queryParams = getMilesRangeFlag([miles]);

            return executeQuery(queryParams.query, queryParams.params, (err, mileRangeResponse) => {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [FETCH] ORDER FLAG QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);
                    return resolve(1);
                } else {
                    return resolve(mileRangeResponse && mileRangeResponse.length !== 0 ?
                        mileRangeResponse[0].order_flag_id : 1);
                }
            })
        })
    }

    Ordermgmt.postOrderManagement = (data, response, options, cb) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                postOrderManagementValidation(data, response, options, cb);
            }
        });

    }

    //Update data in order management
    const updateOrderManagementQueryParams = (params) => {
        return {
            "query": "call updateOrderData(?,?,?,?,?,?,?,?)",
            "params": params
        };
    }

    const deleteOrderTagQueryParams = (params) => {
        return {
            "query": "delete from order_tag where order_id = ?",
            "params": params
        };
    }

    const checkCharges = (params) => {
        return {
            "query": "select * from order_accessorial_charges WHERE accessories_name = ? and order_container_chassis_id = ?",
            "params": params
        };
    }

    const deleteOrderAccessorialChargesQuery = (params) => {
        return {
            "query": "delete from order_accessorial_charges where order_container_chassis_id = ? and accessories_name = ?",
            "params": params
        };
    }

    const updateOrderAccessorialChargesQuery = (params) => {
        return {
            "query": "call updateOrderAccessorialCharges(?,?,?)",
            "params": params
        };
    }

    const updateFuelSurcharge = (params) => {
        return {
            "query": "call updateFuelSurcharge(?,?,?,?)",
            "params": params
        };
    }

    const updateOrderContainerChassisQueryParams = (params) => {
        return {
            "query": "call updateContainerData(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",
            "params": params
        };
    }

    const updateLegDataInboundQuery = (params) => {
        return {
            "query": "call updateAllOrAutoLegDataInbound(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
            "params": params
        };
    }

    const updateLegDataOutboundQuery = (params) => {
        return {
            "query": "call updateAllOrAutoLegDataOutbound(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
            "params": params
        };
    }

    const updateLegDataTruckMoveQuery = (params) => {
        return {
            "query": `call updateAllOrAutoLegTruckMove(?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
            "params": params
        };
    }

    const updateNotes = (params) => {
        return {
            "query": "update notes_history set notes=?,notes_description=?,lastupdated_by=?,lastupdated_on = CURRENT_TIMESTAMP where order_container_chassis_id = ? and is_autocreated = 1;",
            "params": params
        };
    }

    const deleteNotes = (params) => {
        return {
            "query": "DELETE FROM notes_history WHERE order_container_chassis_id = ? and is_autocreated = 1;",
            "params": params
        };
    }

    const updateLegMgmt = (params) => {
        return {
            "query": `UPDATE leg_mgmt SET chassis_number = UCASE(?),container_number = UCASE(?),pu_name = ?,dl_name = ?,pu_loc = ?,dl_loc = ?,pu_time = ?, 
            dl_time = ?,est_miles = ?,pu_time_to = ?, dl_time_to = ?,leg_status = ?, is_brokered = ?, carrier_id = ?, driver_id = ? WHERE leg_id = ?`,
            params
        }
    }

    const updateOrderManagementValidation = async (request, response, options, cb) => {
        try {

            transactionBegin(async (transactionErr, transaction) => { // begin transaction

                if (transactionErr) {
                    console.error(new Date(), "[order_mgmt.js] [SETUP] TRANSACTION ERROR", transactionErr);

                    return response.status(409).json({ code: 409, error: "DB TRANSACTION ERROR" });
                }

                if (request.isRouteCreate) {

                    request.loadedLegMiles = await bing.getMiles(request.pu_formated_loc,
                        request.dl_formated_loc); // bing api to fetch miles

                    // request.loadedLegMiles = await bing.getMilesFromAzure(request.pu_formated_loc,
                    //     request.dl_formated_loc);

                    queryParams = createCustomerSpecficRoute([0, request.pu_formated_loc,
                        request.dl_formated_loc, 0, request.loadedLegMiles, request.order_type_id, request.pu_name,
                        request.dl_name, request.format_address]); // form create route query and params

                    await transactionQuery(transaction, queryParams.query,
                        queryParams.params, (err, queryResponse) => { // transaction query to perform insert
                            if (err) {
                                console.error(new Date(),
                                    "[order_mgmt.js] [POST] ROUTE RATE QUERY ERROR",
                                    "sqlMessage" in err ? err.sqlMessage : err);

                                /* if err then the rollback take place */
                                return transactionRollback(transaction, response, "ROUTE CREATE DB ERROR");
                            }

                            client.del('route_rate');

                            console.log(new Date(), "[order_mgmt.js] [POST] ROUTE RATE SUCCESSFULLY INSERTED");

                        });

                }

                request.loadedLegMiles = request.loadedLegMiles === 0 ?
                    await bing.getMiles(request.pu_loc, request.dl_loc) : request.loadedLegMiles;

                request.orderFlagId = await getOrderFlagId(request.loadedLegMiles);

                queryParams = updateOrderManagementQueryParams([request.customer_id,
                request.order_notes, request.order_type_id, request.rail_cut,
                request.customer_reference, request.booking_number, request.format_address,
                request.order_id]);

                transactionQuery(transaction, queryParams.query, queryParams.params, (orderUpdateErr, updatedata) => {
                    if (orderUpdateErr) {
                        console.error(new Date(), "[order_mgmt.js] [UPDATE] ORDER TABLE QUERY ERROR",
                            "sqlMessage" in orderUpdateErr ? orderUpdateErr.sqlMessage : orderUpdateErr);

                        return transactionRollback(transaction, response, "ORDER UPDATE DB ERROR"); // rollback
                    } else {

                        request.containerLevelId = request.order_container_chassis_id;

                        queryParams = deleteOrderTagQueryParams([request.order_id]);

                        transactionQuery(transaction, queryParams.query, queryParams.params,
                            (tagErr, tagDeleteResponse) => {
                                if (tagErr) {
                                    console.error(new Date(), "[order_mgmt.js] [DELETE] TAG TABLE QUERY ERROR",
                                        "sqlMessage" in tagErr ? tagErr.sqlMessage : tagErr);

                                    return transactionRollback(transaction, response, "ORDER TAG DB ERROR");
                                } else {

                                    if (request.tags && request.tags !== null && request.tags.length !== 0) {

                                        request.tagArray = [];

                                        request.tags.forEach(elem => {
                                            request.tagArray.push([request.order_id, elem]);
                                        })

                                        queryParams = postOrderTagQueryParams([request.tagArray]);

                                        transactionQuery(transaction, queryParams.query, queryParams.params,
                                            (tagInsertErr, tagInsertResponse) => {
                                                if (tagInsertErr) {
                                                    console.error(new Date(),
                                                        "[order_mgmt.js] [POST] TAG TABLE QUERY ERROR",
                                                        "sqlMessage" in tagInsertErr ?
                                                            tagInsertErr.sqlMessage : tagInsertErr);

                                                    return transactionRollback(transaction, response,
                                                        "ORDER TAG DB ERROR");
                                                }
                                            })
                                    }

                                }
                            });

                        let temp = request.order_container_chassis[0];

                        if (request.previousTriaxleValue === "Y" &&
                            !((temp.container_size === '20' && temp.weight >= 39000) ||
                                (temp.container_size !== '20' && temp.weight >= 44001))) {

                            request.triaxle = "N";

                            deleteAccessories(request, 'Triaxle Fee', transaction);

                        } else if (request.previousTriaxleValue === "N" && ((temp.container_size === '20' && temp.weight >= 39000) ||
                            (temp.container_size !== '20' && temp.weight >= 44001))) {

                            request.triaxle = "Y";

                            queryParams = checkCharges(['Triaxle Fee', request.order_container_chassis_id]);

                            transactionQuery(transaction, queryParams.query, queryParams.params,
                                (chargesCheckErr, checkCharge) => {
                                    if (chargesCheckErr) {

                                        console.error(new Date(),
                                            "[order_mgmt.js] [CHECK] [Triaxle Fee] CHARGES CHECK QUERY ERROR",
                                            "sqlMessage" in chargesCheckErr ? chargesCheckErr.sqlMessage :
                                                chargesCheckErr);

                                    } else {
                                        if (checkCharge.length === 0) {
                                            postAccessories(request, 'Triaxle Fee', transaction);
                                        }
                                    }
                                });

                        } else if (request.previousTriaxleValue === "Y" && ((temp.container_size === '20' && temp.weight >= 39000) ||
                            (temp.container_size !== '20' && temp.weight >= 44001))) {

                            request.triaxle = "Y";

                            updateAccessories(request, 'Triaxle Fee', transaction);

                        } else {
                            request.triaxle = "N";
                        }

                        if (!request.previousIsBrokered && !temp.is_brokered) {

                            request.isStatusEditable = 0;

                        } else if (!request.previousIsBrokered && temp.is_brokered) {

                            request.isStatusEditable = 1;
                            request.orderStatus = 'Assigned';

                        } else if (request.previousIsBrokered && !temp.is_brokered) {

                            request.isStatusEditable = 1;
                            request.orderStatus = 'Open';

                        } else if (request.previousIsBrokered && temp.is_brokered) {

                            request.isStatusEditable = 0;

                        }

                        queryParams = updateOrderContainerChassisQueryParams([
                            temp.containerNameDetails.container_name, temp.containerNameDetails.chassis_name,
                            temp.container_number, temp.container_type, temp.container_size,
                            temp.weight, temp.pu_ref, temp.dl_ref, temp.lfd_date, temp.hazmat_req,
                            temp.hazmatinfo.haz_classes, temp.hazmatinfo.haz_contact,
                            temp.hazmatinfo.haz_name, temp.hazmatinfo.haz_page_ref,
                            temp.hazmatinfo.haz_weight, temp.hazmatinfo.un_number, temp.is_dispatch,
                            temp.containerNameDetails.hire_dehire_loc, temp.containerNameDetails.hire_name,
                            temp.chassis_number, request.triaxle, temp.is_draft,
                            request.order_container_chassis_id, temp.pieces, temp.seal_no, temp.temperature,
                            request.pu_name, request.pu_loc, request.dl_name, request.dl_loc, request.est_pickup_from_time,
                            request.est_pickup_to_time, request.est_delivery_from_time, request.est_delivery_to_time,
                            request.loadedLegMiles, request.loggedInUser, request.p_city, request.p_zip, request.d_city,
                            request.d_zip, request.orderFlagId, request.rateType, request.p_phone, request.p_email, request.d_phone,
                            request.d_email, request.driver_notes, request.fetchedRate, temp.eta, temp.is_brokered,
                            temp.carrier_rate, temp.carrier_id, request.orderStatus, request.isStatusEditable]);

                        transactionQuery(transaction, queryParams.query, queryParams.params,
                            async (containerUpdateErr, queryResp) => {
                                if (containerUpdateErr) {

                                    console.error(new Date(),
                                        "[order_mgmt.js] [UPDATE] CONTAINER TABLE QUERY ERROR",
                                        "sqlMessage" in containerUpdateErr ? containerUpdateErr.sqlMessage :
                                            containerUpdateErr);

                                    return transactionRollback(transaction, response, "DB CONTAINER ERROR"); // rollback
                                } else {

                                    if (temp.previousIsDraft === 1 && temp.is_draft === 0) {
                                        console.log(new Date(),
                                            "[order_mgmt.js] [DRAFT] LEGS AND CHARGES GOING TO INSERT");

                                        await legsAccessoriesAndNotesCreation(request, temp, transaction);
                                    } else {

                                        console.log(new Date(),
                                            "[order_mgmt.js] [UPDATE] LEGS AND CHARGES GOING TO UPDATE");

                                        await legAccessoriesAndNotesUpdate(request, temp, transaction);

                                    }

                                    transaction.commit((commitErr) => {
                                        if (commitErr) {
                                            console.error(new Date(), "[order_mgmt.js] [COMMIT] ERROR", commitErr);

                                            return transactionRollback(transaction, response, "COMMIT ERROR");
                                        } else {
                                            transaction.release();
                                            temp.is_dispatch === 1 ? client.del("ordersDispatchData") : null;

                                            temp.is_dispatch === 0 && temp.is_draft === 0 ?
                                                client.del("ordersPendingData") : null;

                                            temp.is_draft === 1 ? client.del("ordersDraftData") : null;

                                            temp.is_dispatch === 1 ? client.del("dispatchData") : null;

                                            client.del("advanced_search");
                                            client.del('orderSearchData');
                                            cb(null, { code: 200, data: "successfully Updated" });
                                        }
                                    })
                                }
                            });
                    }
                })

            })

        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    /** update leg, accessories and notes */
    const legAccessoriesAndNotesUpdate = async (request, temp, transaction) => {

        return new Promise(async (resolve, reject) => {
            let allLegs = await getAllLegs(request.order_container_chassis_id);

            let previousLegs = JSON.stringify(allLegs);

            allLegs.forEach((element, i) => {
                element.index = i;

                if (request.order_type.includes('Inbound')) {

                    if (element.is_auto_created === 1 && element.leg_type_id === 4 && element.leg_status !== 'Delivered') {
                        element.pu_name = request.pu_name;
                        element.pu_loc = request.pu_loc;
                        element.pu_time = element.leg_status !== 'Picked Up' ? request.est_pickup_from_time : element.pu_time;
                        element.pu_time_to = element.leg_status !== 'Picked Up' ? request.est_pickup_to_time : element.pu_time_to;
                        element.dl_name = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_name :
                                (element.is_hub_inbetween === 0 ? request.dl_name : element.dl_name)) : request.dl_name;
                        element.dl_loc = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_loc :
                                (element.is_hub_inbetween === 0 ? request.dl_loc : element.dl_loc)) : request.dl_loc;
                        element.dl_time = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_time :
                                (element.is_hub_inbetween === 0 ? request.est_delivery_from_time : element.dl_time)) : request.est_delivery_from_time;
                        element.dl_time_to = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_time_to :
                                (element.is_hub_inbetween === 0 ? request.est_delivery_to_time : element.dl_time_to)) : request.est_delivery_to_time;

                        if (element.is_hub_triggered && element.is_hub_inbetween === 1 && allLegs[i + 1] && allLegs[i + 1].is_hub_created === 1) {
                            allLegs[i + 1].dl_time = request.est_delivery_from_time;
                            allLegs[i + 1].dl_time_to = request.est_delivery_to_time;
                            allLegs[i + 1].dl_loc = request.dl_loc;
                            allLegs[i + 1].dl_name = request.dl_name;
                        }
                    }

                    if (element.is_auto_created === 1 && element.leg_type_id === 3 && element.leg_status !== 'Delivered') {
                        element.pu_name = request.dl_name;
                        element.pu_loc = request.dl_loc;
                        element.dl_name = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_name :
                                (element.is_hub_inbetween === 0 ? temp.containerNameDetails.hire_name : element.dl_name))
                            : temp.containerNameDetails.hire_name;
                        element.dl_loc = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_loc :
                                (element.is_hub_inbetween === 0 ? temp.containerNameDetails.hire_dehire_loc : element.dl_loc))
                            : temp.containerNameDetails.hire_dehire_loc;

                        if (element.is_hub_triggered && element.is_hub_inbetween === 1 && allLegs[i + 1] && allLegs[i + 1].is_hub_created === 1) {
                            allLegs[i + 1].dl_loc = temp.containerNameDetails.hire_dehire_loc;
                            allLegs[i + 1].dl_name = temp.containerNameDetails.hire_name;
                        }
                    }

                } else if (request.order_type.includes('Outbound')) {

                    if (element.is_auto_created === 1 && element.leg_type_id === 3 && element.leg_status !== 'Delivered') {
                        element.pu_name = temp.containerNameDetails.hire_name;
                        element.pu_loc = temp.containerNameDetails.hire_dehire_loc;
                        element.dl_name = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_name :
                                (element.is_hub_inbetween === 0 ? request.pu_name : element.dl_name))
                            : request.pu_name;
                        element.dl_loc = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_loc :
                                (element.is_hub_inbetween === 0 ? request.pu_loc : element.dl_loc))
                            : request.pu_loc;

                        if (element.is_hub_triggered && element.is_hub_inbetween === 1 && allLegs[i + 1] && allLegs[i + 1].is_hub_created === 1) {
                            allLegs[i + 1].dl_loc = request.pu_loc;
                            allLegs[i + 1].dl_name = request.pu_name;
                        }
                    }

                    if (element.is_auto_created === 1 && element.leg_type_id === 4 && element.leg_status !== 'Delivered') {
                        element.pu_name = request.pu_name;
                        element.pu_loc = request.pu_loc;
                        element.pu_time = element.leg_status !== 'Picked Up' ? request.est_pickup_from_time : element.pu_time;
                        element.pu_time_to = element.leg_status !== 'Picked Up' ? request.est_pickup_to_time : element.pu_time_to;
                        element.dl_name = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_name :
                                (element.is_hub_inbetween === 0 ? request.dl_name : element.dl_name)) : request.dl_name;
                        element.dl_loc = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_loc :
                                (element.is_hub_inbetween === 0 ? request.dl_loc : element.dl_loc)) : request.dl_loc;
                        element.dl_time = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_time :
                                (element.is_hub_inbetween === 0 ? request.est_delivery_from_time : element.dl_time)) : request.est_delivery_from_time;
                        element.dl_time_to = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_time_to :
                                (element.is_hub_inbetween === 0 ? request.est_delivery_to_time : element.dl_time_to)) : request.est_delivery_to_time;

                        if (element.is_hub_triggered && element.is_hub_inbetween === 1 && allLegs[i + 1] && allLegs[i + 1].is_hub_created === 1) {
                            allLegs[i + 1].dl_time = request.est_delivery_from_time;
                            allLegs[i + 1].dl_time_to = request.est_delivery_to_time;
                            allLegs[i + 1].dl_loc = request.dl_loc;
                            allLegs[i + 1].dl_name = request.dl_name;
                        }
                    }

                } else if (request.order_type.includes('Truck Move') || request.order_type === 'One Way Loaded - Intermodal' ||
                    request.order_type === 'One Way Empty - Intermodal') {

                    if (request.order_type === 'One Way Empty - Intermodal' ?
                        element.is_auto_created === 1 && element.leg_type_id === 3 && element.leg_status !== 'Delivered' :
                        element.is_auto_created === 1 && element.leg_type_id === 4 && element.leg_status !== 'Delivered') {
                        element.pu_name = request.pu_name;
                        element.pu_loc = request.pu_loc;
                        element.pu_time = element.leg_status !== 'Picked Up' ? request.est_pickup_from_time : element.pu_time;
                        element.pu_time_to = element.leg_status !== 'Picked Up' ? request.est_pickup_to_time : element.pu_time_to;
                        element.dl_name = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_name :
                                (element.is_hub_inbetween === 0 ? request.dl_name : element.dl_name)) : request.dl_name;
                        element.dl_loc = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_loc :
                                (element.is_hub_inbetween === 0 ? request.dl_loc : element.dl_loc)) : request.dl_loc;
                        element.dl_time = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_time :
                                (element.is_hub_inbetween === 0 ? request.est_delivery_from_time : element.dl_time)) : request.est_delivery_from_time;
                        element.dl_time_to = element.is_hub_triggered ?
                            (element.is_hub_inbetween === 1 ? element.dl_time_to :
                                (element.is_hub_inbetween === 0 ? request.est_delivery_to_time : element.dl_time_to)) : request.est_delivery_to_time;

                        if (element.is_hub_triggered && element.is_hub_inbetween === 1 && allLegs[i + 1] && allLegs[i + 1].is_hub_created === 1) {
                            allLegs[i + 1].dl_time = request.est_delivery_from_time;
                            allLegs[i + 1].dl_time_to = request.est_delivery_to_time;
                            allLegs[i + 1].dl_loc = request.dl_loc;
                            allLegs[i + 1].dl_name = request.dl_name;
                        }
                    }

                }
            });

            allLegs.forEach((element, i) => {
                if (i !== 0) {
                    element.pu_name = element.bobtailed_by_leg ? element.pu_name : allLegs[i - 1].dl_name;
                    element.pu_loc = element.bobtailed_by_leg ? element.pu_loc : allLegs[i - 1].dl_loc;
                }
            });

            previousLegs = JSON.parse(previousLegs);

            let legMatchMap = new Map();

            previousLegs.forEach(element => {
                legMatchMap.set(element.leg_id, element);
            })

            for (let element of allLegs) {

                let previousLegData = legMatchMap.get(element.leg_id);

                if (previousLegData) {
                    if (request.previousPuLoc !== request.pu_loc || request.previousDlLoc !== request.dl_loc || request.previousPuFromTime !== request.est_pickup_from_time
                        || request.previousDlFromTime !== request.est_delivery_from_time || request.previousPuToTime !== request.est_pickup_to_time
                        || request.previousDlToTime !== request.est_delivery_to_time
                        || request.previousHireLoc !== temp.containerNameDetails.hire_dehire_loc
                        || request.previousCarrier !== temp.carrier_id
                        || request.isStatusEditable) {

                        if (previousLegData.pu_loc === element.pu_loc && previousLegData.dl_loc === element.dl_loc) {
                            element.miles = element.est_miles;
                        } else {
                            element.miles = element.leg_type_id === 4 ? request.loadedLegMiles : await bing.getMiles(element.pu_loc, element.dl_loc);
                        }

                        element.container_number = (element.leg_type_id === 1 || element.leg_type_id === 2) ? null : temp.container_number;

                        element.chassis_number = (request.order_type.includes('Truck Move') || element.leg_type_id === 1) ? null : temp.chassis_number;

                        if (request.isStatusEditable) {
                            element.leg_status = request.orderStatus;
                            element.is_brokered = temp.is_brokered;
                            element.driver_id = temp.is_brokered ? null : element.driver_id;
                        }

                        element.carrier_id = temp.is_brokered || request.isStatusEditable ?
                            temp.carrier_id : element.carrier_id;

                        if (element.leg_status !== 'Delivered') {
                            await updateAllLegs(element, transaction);
                        }
                    } else {
                        console.log(new Date(), `[order_mgmt] [UPDATE] ${element.leg_number} LEG NOT UPDATED`);
                    }
                }
            }

            if (request.order_type.includes('Inbound')) {

                // request.emptyLegMiles = (temp.containerNameDetails.hire_dehire_loc !== null &&
                //     request.dl_loc !== null) ?
                //     await bing.getMiles(request.dl_loc, temp.containerNameDetails.hire_dehire_loc) : 0;

                queryParams = updateLegDataInboundQuery([temp.chassis_number, temp.container_number, request.pu_loc,
                request.dl_loc, request.pu_name, request.dl_name, temp.containerNameDetails.hire_dehire_loc,
                temp.containerNameDetails.hire_name, request.est_pickup_from_time, request.est_delivery_to_time,
                request.loadedLegMiles, request.emptyLegMiles, request.fetchedRate, request.loggedInUser,
                request.order_container_chassis_id, request.order_id, request.isUpdateAll === 'Yes' ? 1 : 0]);

                transactionQuery(transaction, queryParams.query, queryParams.params, (legErr, respdata) => {
                    if (legErr) {
                        console.error(new Date(), "[order_mgmt.js] [UPDATE] [INBOUND] QUERY ERROR",
                            "sqlMessage" in legErr ? legErr.sqlMessage : legErr);
                    }
                });

            } else if (request.order_type.includes('Outbound')) {

                // request.emptyLegMiles = (temp.containerNameDetails.hire_dehire_loc !== null &&
                //     request.pu_loc !== null) ?
                //     await bing.getMiles(temp.containerNameDetails.hire_dehire_loc, request.pu_loc) : 0;

                queryParams = updateLegDataOutboundQuery([temp.chassis_number,
                temp.container_number, request.pu_loc, request.dl_loc,
                request.pu_name, request.dl_name, temp.containerNameDetails.hire_dehire_loc,
                temp.containerNameDetails.hire_name, request.est_pickup_from_time,
                request.est_delivery_to_time, request.loadedLegMiles,
                request.emptyLegMiles, request.fetchedRate, request.loggedInUser,
                request.order_container_chassis_id, request.order_id, request.isUpdateAll === 'Yes' ? 1 : 0]);

                transactionQuery(transaction, queryParams.query, queryParams.params, (legErr, respdata) => {
                    if (legErr) {
                        console.error(new Date(), "[order_mgmt.js] [UPDATE] [OUTBOUND] QUERY ERROR",
                            "sqlMessage" in legErr ? legErr.sqlMessage : legErr);
                    }
                });

            } else if (request.order_type.includes('Truck Move') || request.order_type === 'One Way Empty - Intermodal' ||
                request.order_type === 'One Way Loaded - Intermodal') {

                queryParams = updateLegDataTruckMoveQuery([temp.chassis_number, temp.container_number, request.pu_loc,
                request.dl_loc, request.pu_name, request.dl_name, request.est_pickup_from_time,
                request.est_delivery_to_time, request.loadedLegMiles, request.fetchedRate, request.loggedInUser,
                request.order_id, request.order_container_chassis_id, (request.order_type === 'One Way Empty - Intermodal' ||
                    request.order_type === 'One Way Loaded - Intermodal') ? 1 : 0]);

                transactionQuery(transaction, queryParams.query, queryParams.params, (legErr, respdata) => {
                    if (legErr) {
                        console.error(new Date(), "[order_mgmt.js] [UPDATE] [TM/One Way Empty - Intermodal/One Way Loaded - Intermodal] QUERY ERROR",
                            "sqlMessage" in legErr ? legErr.sqlMessage : legErr);
                    }
                });

            }

            updateAccessories(request, 'Fuel Surcharge', transaction, true);

            if (request.previousWeight < 44001 && temp.weight >= 44001) {

                queryParams = checkCharges(['Overweight', request.order_container_chassis_id]);

                transactionQuery(transaction, queryParams.query, queryParams.params, (chargesCheckErr, checkCharge) => {
                    if (chargesCheckErr) {

                        console.error(new Date(), "[order_mgmt.js] [CHECK] [Overweight] CHARGES CHECK QUERY ERROR",
                            "sqlMessage" in chargesCheckErr ? chargesCheckErr.sqlMessage : chargesCheckErr);

                    } else {

                        if (checkCharge.length === 0) {

                            postAccessories(request, 'Overweight', transaction);
                        }
                    }
                })

            } else if (request.previousWeight >= 44001 && temp.weight < 44001) {

                deleteAccessories(request, 'Overweight', transaction);

            } else if (
                (request.previousWeight >= 44001) &&
                (temp.weight >= 44001)
            ) {
                updateAccessories(request, 'Overweight', transaction);
            }

            if (request.previousHazmat == "1" && temp.hazmat_req == "0") {

                deleteAccessories(request, 'HAZMAT Surcharge', transaction);

            } else if (request.previousHazmat == "0" && temp.hazmat_req == "1") {

                queryParams = checkCharges(['HAZMAT Surcharge', request.order_container_chassis_id]);

                transactionQuery(transaction, queryParams.query, queryParams.params, (chargesCheckErr, checkCharge) => {
                    if (chargesCheckErr) {

                        console.error(new Date(), "[order_mgmt.js] [CHECK] [HAZMAT Surcharge] CHARGES CHECK QUERY ERROR",
                            "sqlMessage" in chargesCheckErr ? chargesCheckErr.sqlMessage : chargesCheckErr);

                    } else {
                        if (checkCharge.length === 0) {

                            postAccessories(request, 'HAZMAT Surcharge', transaction);
                        }
                    }
                })

            } else if (request.previousHazmat == "1" && temp.hazmat_req == "1") {

                updateAccessories(request, 'HAZMAT Surcharge', transaction);

            }

            request.notes = request.order_notes;
            request.notes_description = 'order_notes';

            if (request.previousNotes === null && request.notes !== null) {

                queryParams = postNotes([request.notes, request.notes_description, request.loggedInUser,
                request.containerLevelId, request.loggedInUserId]);

                await transactionQuery(transaction, queryParams.query,
                    queryParams.params, (notesErr, respdata) => {
                        if (notesErr) {
                            console.error(new Date(), "[order_mgmt.js] [POST] NOTES QUERY ERROR",
                                "sqlMessage" in notesErr ? notesErr.sqlMessage : notesErr);
                        }
                    });
            } else if (request.previousNotes !== null && request.notes !== null) {

                queryParams = updateNotes([request.notes, request.notes_description, request.loggedInUser,
                request.order_container_chassis_id]);

                transactionQuery(transaction, queryParams.query, queryParams.params, (notesErr, respdata) => {
                    if (notesErr) {
                        console.error(new Date(), "[order_mgmt.js] [UPDATE] NOTES QUERY ERROR",
                            "sqlMessage" in notesErr ? notesErr.sqlMessage : notesErr);
                    }
                });

            } else if (request.previousNotes !== null && request.notes === null) {

                queryParams = deleteNotes([request.order_container_chassis_id]);

                transactionQuery(transaction, queryParams.query, queryParams.params, (notesErr, respdata) => {
                    if (notesErr) {
                        console.error(new Date(), "[order_mgmt.js] [DELETE] NOTES QUERY ERROR",
                            "sqlMessage" in notesErr ? notesErr.sqlMessage : notesErr);
                    }
                });
            }

            resolve("success");
        })

    }

    const deleteAccessories = (request, charge, transaction) => {
        queryParams = deleteOrderAccessorialChargesQuery([request.order_container_chassis_id, charge]);

        transactionQuery(transaction, queryParams.query, queryParams.params, (chargesErr, AccessoriesRates) => {
            if (chargesErr) {
                console.error(new Date(),
                    `[order_mgmt.js] [DELETE] [${charge.toUpperCase()}] ACESSORIES CHARGE QUERY ERROR`,
                    "sqlMessage" in chargesErr ? chargesErr.sqlMessage : chargesErr);
            }
        });
    }

    const updateAccessories = (request, charge, transaction, isFuelSurcharge) => {
        if (isFuelSurcharge) {
            queryParams = updateFuelSurcharge([request.customer_id, charge,
            request.order_container_chassis_id, request.fetchedRate]);
        } else {
            queryParams = updateOrderAccessorialChargesQuery([request.customer_id, charge,
            request.order_container_chassis_id]);
        }


        transactionQuery(transaction, queryParams.query, queryParams.params, (chargesErr, AccessoriesRates) => {
            if (chargesErr) {
                console.error(new Date(),
                    `[order_mgmt.js] [UPDATE] [${charge.toUpperCase()}] ACESSORIES CHARGE QUERY ERROR`,
                    "sqlMessage" in chargesErr ? chargesErr.sqlMessage : chargesErr);
            }
        });
    }

    const getAllLegs = (id) => {
        return new Promise((resolve, reject) => {
            return executeQuery("SELECT * FROM leg_mgmt WHERE order_container_chassis_id = ?", [id],
                (err, queryResponse) => {
                    if (err) {
                        console.error(new Date(), "[order_mgmt.js] [FETCH] LEGS QUERY ERROR",
                            "sqlMessage" in err ? err.sqlMessage : err);

                        resolve([]);
                    } else {
                        queryResponse = queryResponse ? queryResponse.sort((a, b) => a.leg_number - b.leg_number) : [];

                        resolve(queryResponse);
                    }
                })
        })
    }

    const updateAllLegs = (temp, transaction) => {
        return new Promise((resolve, reject) => {
            queryParams = updateLegMgmt([temp.chassis_number, temp.container_number, temp.pu_name, temp.dl_name,
            temp.pu_loc, temp.dl_loc, temp.pu_time, temp.dl_time, temp.miles, temp.pu_time_to, temp.dl_time_to,
            temp.leg_status, temp.is_brokered, temp.carrier_id, temp.driver_id, temp.leg_id]);

            return transactionQuery(transaction, queryParams.query, queryParams.params, (chargesErr, AccessoriesRates) => {
                if (chargesErr) {
                    console.error(new Date(),
                        `[order_mgmt.js] [UPDATE] [${temp.leg_number}] LEG QUERY ERROR`,
                        "sqlMessage" in chargesErr ? chargesErr.sqlMessage : chargesErr);

                    resolve("error");
                }

                console.log(new Date(), `[order_mgmt.js] [UPDATE] [${temp.leg_number}] LEG UPDATED`);

                resolve("success");
            });
        })
    }

    Ordermgmt.updateOrderManagement = (request, response, options, cb) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                updateOrderManagementValidation(request, response, options, cb);
            }
        });

    }

    const performDeletionOnDatabase = (request, response, options, callback) => {
        try {
            request.cb = callback;

            request.res = response;

            app.eventEmitter.emit('orderDeleteUsingPassword', request);
        } catch (e) {
            console.error(new Date(), "[order_mgmt.js] [DELETE] ORDER CODE ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Ordermgmt.deleteOrder = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                performDeletionOnDatabase(request, response, options, callback);
            }
        });

    }

    /* get Order Data */
    const fetchOrdersDataFromDb = (request, response, options, callback) => {
        try {

            executeQuery(options.query, options.params, (err, queryResponse) => {
                try {

                    if (err) {

                        console.error(new Date(), "[order_mgmt.js] [FETCH] ALL ORDERS QUERY ERROR",
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: "DB ERROR" });
                    }

                    let orderData = orderResponseLoop(queryResponse[0], request, options, response);

                    client.setex(options.key, 3600, JSON.stringify(orderData));

                    options.progressFunction();

                } catch (error) {
                    console.error(new Date(), "[order_mgmt.js] [FETCH] ALL ORDERS CODE ERROR", error);

                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            })
        } catch (e) {
            console.error(new Date(), "[order_mgmt.js] [FETCH] ALL ORDERS QUERY ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    const getOrderDataCache = (request, response, options, callback) => {
        let redisResponse; // main response json
        let parsedData; // variable to store parsed order data

        /* to get the data from redis db if no data present, set the data to redis db*/
        client.get(options.key, function (err, cacheData) {
            try {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [FETCH] [ORDER] ALL ORDER REDIS ERROR", err);

                    return response.status(409).json({ code: 409, error: "REDIS ERROR" });
                }

                if (cacheData) {

                    parsedData = JSON.parse(cacheData);

                    if (request.table === 'draft') {

                        parsedData = parsedData.length !== 0 ?
                            parsedData.filter(temp => temp.is_draft === 1) : [];

                    } else if (request.table === 'pending' || request.table === 'dispatch') {

                        parsedData = getFilteredOrderData(parsedData, request.filterDate, response);

                    } else if (request.table === 'streetturn') {

                        parsedData = parsedData.length !== 0 ?
                            parsedData.filter(temp => temp.order_type.includes('Outbound') &&
                                temp.order_status == 'Open' &&
                                temp.container_size === request.container_size &&
                                temp.container_name === request.container_name) : [];

                    }

                    if (request.columnFilter) {
                        parsedData = advancedFilteredOrderData(request, response, parsedData)
                    }

                    if (request.table === 'streetturn' && request.searchFilter) {

                        parsedData = parsedData
                            .filter(temp => temp.order_sequence.includes(request.searchValue));

                    }

                    redisResponse = {
                        code: 200,
                        data: formatOrderData(request, parsedData)
                    }

                    return callback(null, redisResponse)
                }

                fetchOrdersDataFromDb(request, response, options, callback);

            } catch (error) {
                console.error(new Date(), "[order_mgmt.js] [FETCH] [ORDER] ALL ORDER CODE ERROR", error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        });
    }

    Ordermgmt.getOrderManagement = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                options.retrieve = 'order';

                if (request.table === 'dispatch' || request.table === 'streetturn') {
                    options.key = 'ordersDispatchData';
                    options.isDispatch = 1;
                    options.isDraft = 0;
                } else if (request.table === 'pending') {
                    options.key = 'ordersPendingData';
                    options.isDispatch = 0;
                    options.isDraft = 0;
                } else {
                    options.key = 'ordersDraftData';
                    options.isDispatch = 0;
                    options.isDraft = 1;
                }

                options.query = "CALL getOrdersData(?,?);"

                options.params = [options.isDispatch, options.isDraft];

                options.progressFunction = getOrderDataCache.bind(this, request, response, options, callback);

                getOrderDataCache(request, response, options, callback);
            }
        });

    }

    const getDispatchDataCache = (request, response, options, callback) => {
        let redisResponse; // main response json
        let parsedData; // variable to store parsed order data

        /* to get the data from redis db if no data present, set the data to redis db*/
        client.get(options.key, (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [FETCH] [DISPATCH] ALL ORDER REDIS ERROR", err);

                    return response.status(409).json({ code: 409, error: "REDIS ERROR" });
                }

                if (cacheData) {

                    parsedData = JSON.parse(cacheData);

                    parsedData = getFilteredOrderData(parsedData, request.filterDate, response)

                    parsedData.forEach(temp => {

                        temp.all_legs = temp.all_legs.filter(elem => request.filterDate ? (
                            elem.leg_status !== 'Delivered' && (
                                (
                                    elem.filterPuTime ?
                                        (new Date(request.filterDate).getTime() >= new Date(elem.filterPuTime).getTime()) :
                                        false
                                ) ||
                                (
                                    elem.filterDlToTime ?
                                        (new Date(request.filterDate).getTime() >= new Date(elem.filterDlToTime).getTime()) :
                                        false
                                ) ||
                                (
                                    (elem.filterPuTime && elem.filterDlToTime) ?
                                        (
                                            new Date(request.filterDate).getTime() >= new Date(elem.filterPuTime).getTime() &&
                                            new Date(request.filterDate).getTime() <= new Date(elem.filterDlToTime).getTime()
                                        ) :
                                        false
                                )
                            )
                        ) : true)
                    })

                    if (request.quickFilter) {
                        parsedData = quickFilterProcess(request.quickFilterValue, parsedData, request, response);
                    }

                    if (request.moreFilter) {
                        parsedData = dispatchAdvanceFilter(request, response, parsedData)
                    }

                    redisResponse = {
                        code: 200,
                        error: null,
                        data: formatOrderData(request, parsedData)
                    }

                    return callback(null, redisResponse)

                }

                fetchOrdersDataFromDb(request, response, options, callback)


            } catch (error) {
                console.error(new Date(), "[order_mgmt.js] [FETCH] [DISPATCH]  DISPATCH CODE ERROR", error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        });
    }

    const quickFilterProcess = (quickFilter, orderData, request, response) => {
        try {

            let parsedData;

            switch (quickFilter) {
                case 'empty':
                    parsedData = orderData.filter(temp => temp.leg_type === 'Empty' &&
                        temp.order_type.indexOf('Inbound') === 0);
                    break;

                case 'open':
                    parsedData = orderData.filter(temp => temp.leg_status ?
                        temp.leg_status === 'Open' : false);
                    break;

                case 'estimation':

                    orderData.forEach(temp => {
                        if (temp.leg_status &&
                            (temp.leg_status === 'Open' || temp.leg_status === 'Assigned') &&
                            temp.pu_time) {

                            temp.order_deadline = moment(temp.pu_time)
                                .diff(moment(request.currentDate), 'seconds', true) > 0 ?
                                moment(temp.pu_time)
                                    .diff(moment(request.currentDate), 'seconds', true) : null;

                        } else if (temp.leg_status &&
                            temp.leg_status === 'Picked Up' &&
                            temp.pu_time &&
                            temp.dl_time) {

                            temp.order_deadline = moment(temp.dl_time)
                                .diff(moment(temp.pu_time), 'seconds', true) > 0 ?
                                moment(temp.dl_time)
                                    .diff(moment(temp.pu_time), 'seconds', true) : null;

                        } else {
                            temp.order_deadline = null;
                        }
                    });

                    parsedData = orderData;

                    break;

                case 'moveInvoice':
                    parsedData = orderData.filter(temp => temp.order_status == 'Dispatch' &&
                        temp.isReadyToInvoice === 0);
                    break;

                case 'moveAutoInvoice':
                    parsedData = orderData.filter(temp => temp.order_status == 'Dispatch' &&
                        temp.isReadyToInvoice === 1);
                    break;

                case 'centrallot':
                    parsedData = truckLotQuickFilter(orderData, 'central_lot_pu', 'central_lot_dl', response);
                    break;

                case 'midcitieslot':
                    parsedData = truckLotQuickFilter(orderData, 'midcities_lot_pu', 'midcities_lot_dl', response);
                    break;

                default:
                    parsedData = [];
                    break;
            }

            return parsedData

        } catch (error) {
            console.error(new Date(), "[order_mgmt.js] [FETCH] [DISPATCH] QUICK FILTER CODE ERROR", error);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }

    }

    const truckLotQuickFilter = (parsedData, truckLotPu, truckLotDl, response) => {

        try {

            let data = parsedData.length ? JSON.stringify(parsedData) : JSON.stringify([]);

            let ordersAscending = JSON.parse(data);

            ordersAscending = ordersAscending && ordersAscending.length ?
                _.orderBy(ordersAscending, 'order_container_chassis_id', 'asc') : [];

            let filteredData = [];

            parsedData.forEach(temp => {
                if (temp[truckLotDl] && temp[truckLotDl].length) {

                    filteredData.push(temp);

                    let nextOrders = ordersAscending.filter(elem => new Date(temp.created_on).getTime() <= new Date(elem.created_on));

                    nextOrders.forEach((elem, i) => {

                        if (elem[truckLotPu] && elem[truckLotPu].length) {

                            if (elem[truckLotPu]
                                .some(element =>
                                    temp[truckLotDl]
                                        .some(item =>
                                        (((element.container_number && item.container_number) ?
                                            item.container_number === element.container_number : false) ||
                                            ((element.chassis_number && item.chassis_number) ?
                                                item.chassis_number === element.chassis_number : false))))) {

                                if (
                                    (i === 0 && elem[truckLotPu].length >= elem[truckLotDl].length) ||
                                    (i !== 0)
                                ) {
                                    filteredData.pop(temp);
                                }
                            }

                        }

                    })

                }
            });

            return filteredData;

        } catch (error) {
            console.error(new Date(), "[order_mgmt.js] [FETCH] [DISPATCH] TRUCK LOT FILTER CODE ERROR", error);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    const dispatchAdvanceFilter = (request, response, parsedData) => {

        try {

            Object.keys(request.selectedFilterData).forEach(element => {

                if (request.selectedFilterData[element] !== "") {

                    if ((element === 'est_pickup_from_time' ||
                        element === 'est_delivery_to_time') &&
                        request.selectedFilterData[element].length == 2) {

                        request.selectedFilterData[element][0] =
                            new Date(request.selectedFilterData[element][0]).getTime()

                        request.selectedFilterData[element][1] =
                            new Date(request.selectedFilterData[element][1]).getTime()

                    } else {

                        request.selectedFilterData[element] = (element !== 'leg_status' &&
                            element !== 'tags' && element !== 'order_flag') ?
                            request.selectedFilterData[element].toLowerCase().replace(/ +/g, "")
                            : request.selectedFilterData[element];

                    }

                    parsedData = parsedData.filter(temp => {
                        if (element == 'leg_status' || element == 'order_status'
                            || element == 'order_flag') {

                            return temp[element] ?
                                request.selectedFilterData[element].includes(temp[element])
                                : false

                        } else if (element == 'tags') {

                            return temp[element] ?
                                temp[element].split(',')
                                    .some(elem => request.selectedFilterData[element]
                                        .includes(elem))
                                : false

                        } else if (element == 'est_pickup_from_time' ||
                            element == 'est_delivery_to_time') {

                            return temp[element] ?
                                (new Date(temp[element]).getTime()
                                    >= request.selectedFilterData[element][0] &&
                                    new Date(temp[element]).getTime() <=
                                    request.selectedFilterData[element][1]) : false

                        } else {
                            return temp[element] && temp[element].trim() ?
                                temp[element].toString()
                                    .toLowerCase().replace(/ +/g, "")
                                    .indexOf(request.selectedFilterData[element]) === 0
                                : false
                        }
                    });
                }
            });

            return parsedData;

        } catch (error) {
            console.error(new Date(), "[order_mgmt.js] [FETCH] [DISPATCH]  ADVANCED FILTER CODE ERROR", error);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }


    }

    Ordermgmt.getDispatchData = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;

                options.retrieve = 'dispatch';

                options.key = 'dispatchData';

                options.query = "CALL getDispatchData();";

                options.params = null;

                options.progressFunction = getDispatchDataCache.bind(this, request, response, options, callback);

                getDispatchDataCache(request, response, options, callback);
            }
        });

    }

    const getFilteredOrderData = (orderData, filterDate, response) => {
        try {

            return orderData.filter(temp => {

                let legPickUp = temp.pu_time ? new Date(temp.pu_time).toLocaleDateString() : null;

                let legDeliveryTo = temp.dl_time_to ? new Date(temp.dl_time_to).toLocaleDateString() : null;

                return (
                    filterDate ?
                        (
                            temp.all_legs.some(elem => (
                                (
                                    elem.filterPuTime ?
                                        (new Date(filterDate).getTime() >= new Date(elem.filterPuTime).getTime() && elem.leg_status !== 'Delivered') :
                                        false
                                ) ||
                                (
                                    elem.filterDlToTime ?
                                        (new Date(filterDate).getTime() >= new Date(elem.filterDlToTime).getTime() && elem.leg_status !== 'Delivered') :
                                        false
                                ) ||
                                (
                                    (elem.filterPuTime && elem.filterDlToTime) ?
                                        (
                                            new Date(filterDate).getTime() >= new Date(elem.filterPuTime).getTime() &&
                                            new Date(filterDate).getTime() <= new Date(elem.filterDlToTime).getTime() &&
                                            elem.leg_status !== 'Delivered'
                                        ) :
                                        false
                                )
                            )) ||
                            (
                                (legPickUp && legDeliveryTo) ?
                                    (
                                        new Date(filterDate).getTime() >= new Date(legPickUp).getTime() &&
                                        new Date(filterDate).getTime() <= new Date(legDeliveryTo).getTime()
                                    ) :
                                    false
                            )
                        ) : true
                )
            });
        } catch (error) {
            console.error(new Date(), "[order_mgmt.js] [FETCH] [ORDER] ORDER FILTER CODE ERROR", error);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    const getInvoiceDataCache = (request, response, options, callback) => {
        let redisResponse; // main response json
        let parsedData; // variable to store parsed dispatch driver data

        /* to get the data from redis db if no data present, set the data to redis db*/
        client.get(options.key, function (err, cacheData) {
            try {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [FETCH] [INVOICE] ALL ORDER REDIS ERROR", err);

                    return response.status(409).json({ code: 409, error: 'REDIS ERROR' });
                }

                if (cacheData) {
                    /* slicing the array to keep the ui fast */
                    parsedData = JSON.parse(cacheData);

                    if (request.isUnique) {

                        parsedData = parsedData.length !== 0 ?
                            _.uniqBy(parsedData, 'order_number') : [];

                        if (request.columnFilter) {
                            parsedData = advancedFilteredOrderData(request, response, parsedData);
                        }

                        redisResponse = {
                            code: 200,
                            error: null,
                            data: formatOrderData(request, parsedData, true)
                        }

                    } else {

                        parsedData = parsedData.filter(temp => temp.order_number == request.orderNumber);

                        redisResponse = {
                            code: 200,
                            error: null,
                            data: formatOrderData(request, parsedData, true)
                        }
                    }

                    return callback(null, redisResponse);
                }

                fetchOrdersDataFromDb(request, response, options, callback);

            } catch (error) {
                console.error(new Date(), "[order_mgmt.js] [FETCH] [INVOICE] ALL ORDER CODE ERROR", error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        });
    }

    Ordermgmt.getInvoiceData = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;

                options.retrieve = 'invoice';

                options.key = 'invoiceData';

                options.query = "CALL getInvoiceData();"

                options.params = null;

                options.progressFunction = getInvoiceDataCache.bind(this, request, response, options, callback);

                getInvoiceDataCache(request, response, options, callback);
            }
        });

    }

    const setOrderSearchInRedis = (searchString, response, options, callback) => {
        executeQuery('CALL orderSearchData();', null, (err, queryResponse) => {
            try {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [FETCH] ORDER SEARCH QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {

                    client.setex('orderSearchData', 3600, JSON.stringify(queryResponse[0]));

                    searchOrder(searchString, response, options, callback);
                }
            } catch (error) {

                console.error(new Date(), "[order_mgmt.js] [FETCH] ORDER SEARCH CODE ERROR", error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        });
    }


    const searchOrder = (searchString, response, options, callback) => {
        let orderData = []

        client.get("orderSearchData", (err, cacheData) => {
            try {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [FETCH] ORDER SEARCH REDIS ERROR", err);

                    return response.status(409).json({ code: 409, error: "REDIS ERROR" })
                } else {
                    if (cacheData) {

                        orderData = JSON.parse(cacheData);

                        orderData = orderData.length !== 0 ?
                            orderData.filter(temp => {
                                return temp.order_sequence
                                    .toLowerCase()
                                    .replace(/[^a-zA-Z0-9/]+/g, '')
                                    .indexOf(searchString.toLowerCase().replace(/[^a-zA-Z0-9/]+/g, '')) === 0
                            }) : [];

                        orderData = orderData.length !== 0 ?
                            orderData.map(temp => {
                                return {
                                    searchValue: temp['order_sequence'],
                                    orderLevelId: temp['order_id'],
                                    containerLevelId: temp['order_container_chassis_id'],
                                    legToAssign: (temp['total_leg'] + 1),
                                    hazmat: temp['hazmat_req'],
                                    orderType: temp['order_type']
                                }
                            }) : [];

                        callback(null, orderData);
                    } else {
                        setOrderSearchInRedis(searchString, response, options, callback);
                    }
                }
            } catch (error) {
                console.error(new Date(), "[order_mgmt.js] [FETCH] ORDER SEARCH CODE ERROR", error);

                response.status(400).json({ code: 400, error: "CODE ERROR" });
            }
        })
    }

    Ordermgmt.orderAutoSuggest = (search, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                search = search ? decodeURIComponent(search) : null;
                searchOrder(search, response, options, callback);
            }
        });

    }

    const formatOrderData = (request, orderParsedData, isInvoice) => {
        let orderData;
        let orderPaginationLength;

        if (request.direction !== null && (request.direction === 'asc' || request.direction === 'desc')) {

            orderData = orderParsedData.length !== 0 ?
                _.orderBy(orderParsedData, request.column, request.direction).slice((request.offset - 1), request.limit) : [];

        } else if ('quickFilter' in request && request.quickFilter && request.quickFilterValue == 'estimation') {

            orderData = orderParsedData.length !== 0 ?
                _.orderBy(orderParsedData, 'order_deadline', 'asc').slice((request.offset - 1), request.limit) : [];

        } else if ('quickFilter' in request && request.quickFilter && request.quickFilterValue == 'pickup') {

            orderData = orderParsedData.length !== 0 ?
                _.orderBy(orderParsedData, ['pu_time'], ['asc']).slice((request.offset - 1), request.limit) : [];

        } else if ('quickFilter' in request && request.quickFilter && request.quickFilterValue == 'delivery') {

            orderData = orderParsedData.length !== 0 ?
                _.orderBy(orderParsedData, ["dl_time"], ['asc']).slice((request.offset - 1), request.limit) : [];

        } else {

            orderData = orderParsedData.length !== 0 ?
                _.orderBy(orderParsedData, isInvoice ? 'actual_time' : 'lastupdated_on', 'desc')
                    .slice((request.offset - 1), request.limit) : [];

        }

        orderPaginationLength = orderParsedData.length;

        return { orderData, orderPaginationLength }
    }

    const updateIsDispatchQueryParams = (params) => {
        return {
            "query": `UPDATE
                          order_container_chassis
                      SET
                          lastupdated_on = CURRENT_TIMESTAMP(),
                          is_dispatch = ?
                      WHERE
                          FIND_IN_SET(order_container_chassis_id,?);`,
            "params": params
        };
    }

    const updateIsDispatchValidation = async (request, response, options, cb) => {
        try {

            /* server side selection of all routes id for bulk update */
            if (request.isAllSelected) {
                let fetchedIds = await fetchAllOrderContainerId(request, response, options, cb);

                if (fetchedIds.code === 200) {
                    request.id = fetchedIds.data
                } else {
                    return cb(null, {
                        code: 103,
                        error: "One or few orders selected do not have Pickup/Delivery Date & Time set"
                    })
                }

            }

            queryParams = updateIsDispatchQueryParams([request.isDispatch, request.id]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {

                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [UPDATE] ORDER ISDISPATCH QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: 'DB ERROR' })
                }

                client.del('ordersDispatchData');
                client.del('ordersPendingData');
                client.del('orderSearchData');
                client.del("advanced_search");
                cb(null, { code: 200, data: "Successfully Moved" });
            });
        } catch (e) {
            console.error(new Date(), "[order_mgmt.js] [UPDATE] ORDER ISDISPATCH CODE ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    const fetchAllOrderContainerId = (request, response, options, callback) => {
        return new Promise((resolve, reject) => {

            return client.get(options.key, (err, cacheData) => {
                try {
                    if (err) {
                        console.error(new Date(), "[order_mgmt.js] [FETCH] ORDER ID REDIS ERROR", err);

                        return response.status(409).json({ code: 409, error: "REDIS ERROR" });
                    }

                    if (cacheData) {
                        let parsedData = JSON.parse(cacheData);

                        if (request.columnFilter) {
                            parsedData = advancedFilteredOrderData(request, response, parsedData)
                        }

                        let valid = true;

                        let containerIds = parsedData.map(temp => temp.order_container_chassis_id)
                            .filter(temp => !request.unSelectedId.includes(temp));

                        if (request.isDispatch) {
                            let isNotNull = [];

                            isNotNull = parsedData.map(temp => {
                                return containerIds
                                    .some(elem => elem === temp.order_container_chassis_id) ?
                                    (temp.est_pickup_from_time !== null && temp.est_pickup_to_time !== null &&
                                        temp.est_delivery_from_time !== null &&
                                        temp.est_delivery_to_time !== null)
                                    : null;
                            })

                            isNotNull = isNotNull.length !== 0 ? isNotNull.filter(temp => temp !== null) : [];

                            valid = isNotNull.length !== 0 ? !isNotNull.includes(false) : true;
                        }

                        if (valid) {
                            return resolve({ code: 200, data: containerIds.join(',') });
                        }

                        return resolve({ code: 103, error: "no date selected" });

                    }

                    fetchOrdersDataFromDb(request, response, options, callback);

                } catch (error) {
                    console.error(new Date(), "[order_mgmt.js] [FETCH] ORDER ID CODE ERROR", error);

                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            });
        })
    }

    Ordermgmt.updateIsDispatch = (request, response, options, cb) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;

                options.retrieve = 'order';

                options.key = request.isDispatch ? 'ordersPendingData' : 'ordersDispatchData';

                options.isDispatch = request.isDispatch ? 0 : 1;

                options.isDraft = 0;

                options.query = "CALL getOrdersData(?,?);"

                options.params = [options.isDispatch, options.isDraft];

                options.progressFunction = updateIsDispatchValidation.bind(this, request, response, options, cb);

                updateIsDispatchValidation(request, response, options, cb);
            }
        });

    }

    const advancedFilteredOrderData = (request, response, parsedData) => {
        try {
            for (var elem of request.filterData) {
                var column = elem.column;
                var values = elem.values;

                parsedData = parsedData.length !== 0 ?
                    parsedData.filter(temp => {
                        if (column === 'order_status' || column === 'leg_status' || column === 'order_flag') {

                            return temp[column] ? values.includes(temp[column]) : false

                        } else if (column === 'tags') {

                            return temp[column] ?
                                temp[column].split(',').some(item => values.includes(item)) : false

                        } else if (column === 'est_pickup_from_time' || column === 'est_delivery_to_time'
                            || column === 'rail_cut' || column === 'lfd_date' || column === 'eta'
                            || column === 'created_on') {

                            return temp[column] ?
                                new Date(temp[column])
                                    .toLocaleDateString().includes(new Date(values).toLocaleDateString())
                                : false

                        } else {

                            return temp[column] ? temp[column]
                                .toString().toLowerCase().replace(/ +/g, "")
                                .indexOf((values || "").toString().toLowerCase().replace(/ +/g, "")) === 0 : false

                        }
                    })
                    : [];
            }

            return parsedData

        } catch (error) {
            response.status(400).json({ code: 400, error: 'CODE ERROR' })
        }
    }

    // get selected order data
    const getSelectedOrderQuery = (params) => {
        return {
            "query": "call getSelectedOrderData(?,?);",
            "params": params
        };
    }

    const getSelectedOrdervalidation = (order_number, sequence, response, options, callback) => {
        try {
            queryParams = getSelectedOrderQuery([order_number, sequence]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [FETCH] SELECTED ORDER QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {
                    queryResponse[0].forEach(temp => {
                        temp.tag_color = temp.tag_color ? JSON.parse(temp.tag_color) : null;
                    })

                    callback(null, queryResponse[0]);
                }
            })
        } catch (e) {
            console.error(new Date(), "[order_mgmt.js] [FETCH] SELECTED ORDER CODE ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Ordermgmt.getSelectedOrder = (order_number, sequence, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                getSelectedOrdervalidation(order_number, sequence, response, options, callback);
            }
        });

    }

    // get selected order data
    const getOrderLegAccessoriesQuery = (params) => {
        return {
            "query": "call getMultipleSelectedOrder(?);",
            "params": params
        };
    }

    const getOrderLegAccessoriesvalidation = (orderchassis, response, options, callback) => {
        try {

            queryParams = getOrderLegAccessoriesQuery([orderchassis]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                try {

                    if (err) {
                        console.error(new Date(), "[order_mgmt.js] [FETCH] MULTIPLE SELECTED ORDER QUERY ERROR",
                            "sqlMessage" in err ? err.sqlMessage : err);

                        response.status(409).json({ code: 409, error: "DB ERROR" });
                    } else {

                        queryResponse[0].forEach(temp => {

                            temp.legs = temp.legs !== null ? JSON.parse(temp.legs) : [];

                            temp.accessories = temp.accessories !== null ? JSON.parse(temp.accessories) : [];
                        })

                        let responseData = {
                            invoice_number: queryResponse[0].map(temp => temp.invoice_number).join(),
                            order_number: queryResponse[0].map(temp => temp.order_number).join(),
                            order_container_chassis_id: queryResponse[0].map(temp => temp.order_container_chassis_id).join(),
                            date: queryResponse[0].length !== 0 ? queryResponse[0][0].date : null,
                            due_date: queryResponse[0].length !== 0 ? queryResponse[0][0].due_date : null,
                            total_amount: queryResponse[0].map(temp => +temp.total_amount).reduce((a, b) => a + b, 0),
                            business_name: queryResponse[0].length !== 0 ? queryResponse[0][0].business_name : null,
                            b_address: queryResponse[0].length !== 0 ? queryResponse[0][0].b_address : null,
                            b_street: queryResponse[0].length !== 0 ? queryResponse[0][0].b_street : null,
                            b_city: queryResponse[0].length !== 0 ? queryResponse[0][0].b_city : null,
                            b_state: queryResponse[0].length !== 0 ? queryResponse[0][0].b_state : null,
                            b_postal_code: queryResponse[0].length !== 0 ? queryResponse[0][0].b_postal_code : null,
                            business_phone: queryResponse[0].length !== 0 ? queryResponse[0][0].business_phone : null,
                            customer_reference: queryResponse[0].length !== 0 ? queryResponse[0][0].customer_reference : null,
                            booking_number: queryResponse[0].length !== 0 ? queryResponse[0][0].booking_number : null,
                            container_number: queryResponse[0].length !== 0 ? queryResponse[0][0].container_number : null,
                            chassis_number: queryResponse[0].length !== 0 ? queryResponse[0][0].chassis_number : null,
                            email: queryResponse[0].length !== 0 ? queryResponse[0][0].email : null,
                            preferred_invoice: queryResponse[0].length !== 0 ? queryResponse[0][0].preferred_invoice : null,
                            preferred_documents: queryResponse[0].length !== 0 ? queryResponse[0][0].preferred_documents : null,
                            orderLegAccessories: queryResponse[0].length !== 0 ? queryResponse[0] : [],
                            req: queryResponse[0].length !== 0 ? queryResponse[0].map(temp => {
                                return {
                                    order_container_chassis_id: temp.order_container_chassis_id,
                                    order_id: temp.order_id,
                                    invoice_quickbooks_id: temp.invoice_quickbooks_id,
                                    invoice_quickbooks_sync: temp.invoice_quickbooks_sync
                                }
                            }) : []
                        }

                        callback(null, responseData);
                    }
                } catch (error) {
                    console.error(new Date(), "[order_mgmt.js] [FETCH] MULTIPLE SELECTED ORDER CODE ERROR", error);

                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            })
        } catch (e) {
            console.error(new Date(), "[order_mgmt.js] [FETCH] MULTIPLE SELECTED ORDER QUERY ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Ordermgmt.getOrderLegAccessories = (orderchassis, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                getOrderLegAccessoriesvalidation(orderchassis, response, options, callback);
            }
        });
    }

    // get selected order data
    const moveOrderToInvoiceQuery = (params) => {
        return {
            "query": `update order_container_chassis set order_status = ?,isReadyToInvoice = ?,set_time_to_invoice = ?,actual_time = ?
                      where order_id = ? and order_container_chassis_id = ?`,
            "params": params
        };
    }

    const moveOrderToInvoiceValidation = (request, response, options, callback) => {

        try {
            switch (request.flag) {
                case "Yes, immediately":
                    request.order_status = "Delivered";
                    request.isReadyToInvoice = true;
                    request.time = null;
                    request.actual_time = new Date();
                    break;
                case "Yes, end of business day":
                    request.order_status = "Dispatch";
                    request.isReadyToInvoice = true;
                    request.time = new Date();
                    request.actual_time = null;
                    break;
                case "No":
                    request.order_status = "Dispatch";
                    request.isReadyToInvoice = false;
                    request.time = null;
                    request.actual_time = null;
                    break;
                default:
                    request.order_status = "Delivered";
                    request.isReadyToInvoice = true;
                    request.actual_time = new Date();
            }

            queryParams = moveOrderToInvoiceQuery([request.order_status, request.isReadyToInvoice, request.time, request.actual_time,
            request.order_id, request.order_container_chassis_id]);

            executeQuery(queryParams.query, queryParams.params, async (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [UPDATE] MOVE TO INVOICE QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {
                    if (request.order_status === 'Delivered' && request.order_type !== "Truck Move") {
                        await postOrUpdateChassisRentalCharges(request.order_container_chassis_id, request.days, request.customer_id);
                    }
                    client.del("ordersDispatchData");
                    client.del("dispatchData");
                    client.del("invoiceData");
                    client.del("advanced_search");
                    client.del("orderSearchData");
                    client.del("driverDetailsData");
                    callback(null, { code: 200, data: "Successfully Status Updated" });
                }
            })
        } catch (e) {
            console.error(new Date(), "[order_mgmt.js] [UPDATE] MOVE TO INVOICE QUERY ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Ordermgmt.moveOrderToInvoice = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                moveOrderToInvoiceValidation(request, response, options, callback);
            }
        });

    }


    /* cancelling of order */
    const checkOrderCancellable = (params) => {
        return {
            "query": `SELECT
                          COUNT(*) leg_count
                      FROM
                          leg_mgmt lm
                      WHERE
                          order_container_chassis_id = ?
                          and (leg_status = 'Picked Up'
                          or leg_status = 'Delivered');`,
            "params": params
        };
    }

    const updateOrderStatusCancel = function (params) {
        return {
            "query": `UPDATE
                          order_container_chassis
                      SET
                          order_status = "Cancelled"
                      WHERE
                          order_container_chassis_id = ?;`,
            "params": params
        };
    }

    const cancelOrderValidation = (request, response, options, callback) => {

        try {

            queryParams = checkOrderCancellable([request.order_container_chassis_id]);

            executeQuery(queryParams.query, queryParams.params, (checkErr, queryResponse) => {
                if (checkErr) {
                    console.error(new Date(), "[order_mgmt.js] [CHECK] IS ORDER CANCELLABLE QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: "CHECK DB ERROR" });
                } else {
                    if (queryResponse[0].leg_count === 0) {

                        queryParams = updateOrderStatusCancel([request.order_container_chassis_id]);

                        executeQuery(queryParams.query, queryParams.params, (updateErr, updateQueryResponse) => {
                            if (updateErr) {
                                console.error(new Date(), "[order_mgmt.js] [UPDATE] CANCEL ORDER QUERY ERROR",
                                    "sqlMessage" in err ? err.sqlMessage : err);

                                response.status(409).json({ code: 409, error: "UPDATE DB ERROR" });
                            } else {
                                if (request.isDispatch === 1) {
                                    client.del("ordersDispatchData");
                                } else {
                                    client.del("ordersPendingData");
                                }
                                client.del("advanced_search");
                                client.del("orderSearchData");
                                client.del("driverDetailsData");
                                client.del("archived_orders");
                                callback(null, { code: 200, data: "Successfully Status Updated" });
                            }
                        })
                    } else {
                        callback(null, { code: 422, data: "Cannot Cancel Order. Order Initiated" });
                    }
                }
            })
        } catch (e) {
            console.error(new Date(), "[order_mgmt.js] [CATCH] CANCEL ORDER CODE ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Ordermgmt.cancelOrder = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                cancelOrderValidation(request, response, options, callback);
            }
        });

    }


    const updateDriverNotesQuery = (params) => {
        return {
            "query": `UPDATE
                          order_container_chassis
                      SET
                          dispatch_driver_notes = ?
                      WHERE
                          order_container_chassis_id = ?;`,
            "params": params
        };
    }

    const updateInitDriverNotesQuery = (params) => {
        return {
            "query": `UPDATE
                          order_container_chassis
                      SET
                          dispatch_driver_notes = ?,
                          driver_notes_created_by = ?,
                          driver_notes_created_on = current_timestamp()
                      WHERE
                          order_container_chassis_id = ?;`,
            "params": params
        };
    }


    const performSparseUpdateOnDb = (request, response, options, callback) => {

        try {

            if (request.init) {
                queryParams = updateInitDriverNotesQuery([request.dispatch_driver_notes, request.loggedInUser,
                request.order_container_chassis_id]);
            } else {
                queryParams = updateDriverNotesQuery([request.dispatch_driver_notes,
                request.order_container_chassis_id]);
            }


            executeQuery(queryParams.query, queryParams.params, (checkErr, queryResponse) => {
                if (checkErr) {
                    console.error(new Date(), "[order_mgmt.js] [UPDATE] DRIVER NOTES QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {
                    callback(null, { code: 200, data: "Driver Notes Updated" });
                }
            })
        } catch (e) {
            console.error(new Date(), "[order_mgmt.js] [CATCH] DRIVER NOTES CODE ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Ordermgmt.updateDriverNotes = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                performSparseUpdateOnDb(request, response, options, callback);
            }
        });

    }

    const updateMilesAndRateQuery = (params) => {
        return {
            "query": `UPDATE
                          order_container_chassis
                      SET
                          miles = ?,
                          rate = ?
                      WHERE
                          order_container_chassis_id = ?;`,
            params
        };
    }

    const performMilesAndRateUpdateOnDb = (request, response, options, callback) => {

        try {

            queryParams = updateMilesAndRateQuery([request.miles, request.rate, request.order_container_chassis_id]);

            executeQuery(queryParams.query, queryParams.params, async (checkErr, queryResponse) => {
                if (checkErr) {
                    console.error(new Date(), "[order_mgmt.js] [UPDATE] ORDER MILES AND RATE QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: "DB ERROR" });
                }

                await postOrUpdateFuelSurcharge(request.order_container_chassis_id, request.rate, request.customer_id);

                callback(null, { code: 200, data: "Rates And Miles Updated" });

            })
        } catch (e) {
            console.error(new Date(), "[order_mgmt.js] [CATCH] DRIVER NOTES CODE ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    const postOrUpdateFuelSurcharge = (id, rate, customerId) => {
        return new Promise((resolve, reject) => {
            queryParams = checkCharges(['Fuel Surcharge', id]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [CHECK CHARGES] SCHEDULER UPDATE QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return resolve("error");
                }
                if (queryResponse.length === 0) {

                    queryParams = postFuelSurcharge([customerId, 'Fuel Surcharge', id, rate]);

                    return executeQuery(queryParams.query, queryParams.params, (postErr, queryResp) => {
                        if (postErr) {
                            console.error(new Date(), "[order_mgmt.js] [POST CHARGES] UPDATE QUERY ERROR",
                                "sqlMessage" in postErr ? postErr.sqlMessage : postErr);

                            return resolve("error");
                        }

                        console.log(new Date(), `[order_mgmt.js] [Fuel Surcharge CHARGE] CHARGE POSTED`);

                        resolve("success");
                    })
                } else {
                    queryParams = updateFuelSurcharge([customerId, 'Fuel Surcharge', id, rate]);

                    return executeQuery(queryParams.query, queryParams.params, (updateErr, queryRes) => {
                        if (updateErr) {
                            console.error(new Date(), "[order_mgmt.js] [UPDATE CHARGES] UPDATE QUERY ERROR",
                                "sqlMessage" in updateErr ? updateErr.sqlMessage : updateErr);

                            return resolve("error");
                        }

                        console.log(new Date(), `[order_mgmt.js] [Fuel Surcharge] CHARGE UPDATED`);

                        resolve("success");
                    })
                }
            })

        })
    }

    Ordermgmt.updateOrdersMiles = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                performMilesAndRateUpdateOnDb(request, response, options, callback);
            }
        });

    }


    // get selected order data
    const moveOrderBackToDispatchQuery = (params) => {
        return {
            "query": `update order_container_chassis set order_status = 'Dispatch',isReadyToInvoice = 0,set_time_to_invoice = null,actual_time = null
                      where ?? = ? and is_invoice_generated <> 1;`,
            "params": params
        };
    }

    const moveOrderBackToDispatchValidation = (request, response, options, callback) => {

        try {
            switch (request.flag) {
                case "invoice":
                    request.column = "order_id";
                    break;
                case "invoice_to_generated":
                    request.column = "order_container_chassis_id";
                    break;
            }

            queryParams = moveOrderBackToDispatchQuery([request.column, request.id]);

            executeQuery(queryParams.query, queryParams.params, async (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [UPDATE] MOVE TO INVOICE QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {
                    client.del("ordersDispatchData");
                    client.del("dispatchData");
                    client.del("invoiceData");
                    client.del("advanced_search");
                    client.del("orderSearchData");
                    client.del("driverDetailsData");
                    callback(null, { code: 200, data: "Successfully Status Updated" });
                }
            })
        } catch (e) {
            console.error(new Date(), "[order_mgmt.js] [UPDATE] MOVE TO INVOICE QUERY ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Ordermgmt.moveOrderBackToDispatch = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                moveOrderBackToDispatchValidation(request, response, options, callback);
            }
        });

    }


    // get selected order data
    const afterInvoiceEditQuery = (params) => {
        return {
            "query": `UPDATE
            order_mgmt
        JOIN order_container_chassis ON
            order_mgmt.order_id = order_container_chassis.order_id SET
            order_container_chassis.pieces = ?,
            order_container_chassis.seal_no = ?,
            order_container_chassis.pu_ref = ?,
            order_container_chassis.dl_ref = ?,
            order_mgmt.customer_reference = ?,
            order_mgmt.booking_number = ?
        WHERE
            order_container_chassis.order_container_chassis_id = ?;`,
            "params": params
        };
    }

    const afterInvoiceEditValidation = (request, response, options, callback) => {

        try {
            queryParams = afterInvoiceEditQuery([request.pieces, request.seal_no, request.pu_ref, request.dl_ref,
            request.customer_reference, request.booking_number, request.order_container_chassis_id]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [UPDATE] AFTER INVOICE QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {
                    client.del("invoiceData");
                    client.del("advanced_search");
                    callback(null, { code: 200, data: "Successfully Status Updated" });
                }
            })
        } catch (e) {
            console.error(new Date(), "[order_mgmt.js] [UPDATE] AFTER INVOICE QUERY ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Ordermgmt.afterInvoiceEdit = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                afterInvoiceEditValidation(request, response, options, callback);
            }
        });

    }


    const makeOrderAsNoInvoiceQuery = (params) => {
        return {
            "query": `UPDATE order_container_chassis SET order_status = ?, actual_time = current_timestamp() WHERE order_id = ?;`,
            "params": params
        };
    }

    const makeOrderAsNoInvoiceValidation = (request, response, options, callback) => {

        try {
            queryParams = makeOrderAsNoInvoiceQuery([request.order_status, request.order_id]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [UPDATE] STATUS QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {
                    client.del("invoiceData");
                    client.del("advanced_search");
                    client.del("archived_orders");
                    callback(null, { code: 200, data: "Successfully Status Updated" });
                }
            })
        } catch (e) {
            console.error(new Date(), "[order_mgmt.js] [UPDATE] STATUS QUERY ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Ordermgmt.makeOrderAsNoInvoice = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                makeOrderAsNoInvoiceValidation(request, response, options, callback);
            }
        });

    }


    const checkRouteRateValidation = async (request, response, options, callback) => {

        if (request.customer_reference && request.isCustomerRefCheck) {
            const customerRef = await checkCustomerRef(request.customer_reference);

            if (customerRef.code === 409) {
                return response.status(409).json({ code: 409, error: "DB ERROR" });
            }

            if (customerRef.data.length) {
                return checkRouteDbCall(request, response, callback, 103);
            } else {
                return checkRouteDbCall(request, response, callback, 200);
            }
        }

        return checkRouteDbCall(request, response, callback, 200);
    }

    const checkRouteDbCall = (request, response, callback, statusCode) => {

        executeQuery('call fetchRouteRate(?,?,?,?,?)',
            [request.pu_formated_loc, request.dl_formated_loc, request.format_address,
            request.customer_id, request.order_type_id], (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), "[order_mgmt.js] [FETCH] ROUTE RATE DETAILS QUERY ERROR",
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: "DB ERROR" });
                }
                console.log(new Date(), "[order_mgmt.js] [FETCH] ROUTE RATE DETAILS",
                    queryResponse[0]);

                return callback(null, { code: statusCode, data: queryResponse[0] });
            })

    }

    const checkCustomerRef = (customer_reference) => {
        return new Promise((resolve, reject) => {
            return executeQuery('SELECT * FROM order_mgmt om WHERE customer_reference = ?;',
                [customer_reference], (err, queryResponse) => {
                    if (err) {
                        console.error(new Date(), "[order_mgmt.js] [FETCH] CHECK CUSTOMER REF QUERY ERROR",
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return resolve({ code: 409, error: "DB ERROR" });
                    }
                    console.log(new Date(), "[order_mgmt.js] [FETCH] CHECK CUSTOMER REF",
                        queryResponse.length);

                    resolve({ code: 200, data: queryResponse });
                })
        })
    }

    Ordermgmt.checkRouteRate = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                checkRouteRateValidation(request, response, options, callback);
            }
        });

    }

    const railTraceHistoryFetchQuery = () => {
        return {
            "query": `SELECT
                          UPPER(railway) railway,
                          MAX(scheduler_end) lastUpdatedOn
                      FROM
                          railtracing_scheduler_history rsh
                      WHERE
                          scheduler_status = "Completed"
                      GROUP BY
                          railway;`
        };
    }

    const fetchLastUpdatedRailTraceHistory = (response, options, callback) => {

        queryParams = railTraceHistoryFetchQuery();

        executeQuery(queryParams.query, null, (err, queryResponse) => {
            try {

                if (err) {
                    console.error(new Date(), `[order_mgmt] [FETCH] RAILTRACE HISTORY QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: 'DB ERROR' });
                }

                callback(null, queryResponse);

            } catch (e) {
                console.error(new Date(), `[order_mgmt] [FETCH]  RAILTRACE HISTORY CODE ERROR`, e);

                response.status(400).json({ code: 400, error: 'CODE ERROR' });
            }
        })
    }

    Ordermgmt.getRailTraceHistory = (response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {

            if (err || !tokenModel) {
                return response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            }

            options.tokenModel = tokenModel;

            fetchLastUpdatedRailTraceHistory(response, options, callback);

        });

    }


    const orderResponseLoop = (orderData, request, options, response) => {

        try {

            orderData.forEach(temp => {

                if (options.retrieve !== 'invoice') {

                    temp.pu_time = (temp.pu_time &&
                        !isNaN(new Date(temp.pu_time).getFullYear()) &&
                        new Date(temp.pu_time).getFullYear() !== 1970) ?
                        temp.pu_time : null;

                    temp.dl_time = (temp.dl_time &&
                        !isNaN(new Date(temp.dl_time).getFullYear()) &&
                        new Date(temp.dl_time).getFullYear() !== 1970) ?
                        temp.dl_time : null;

                    temp.pu_time_to = (temp.pu_time_to &&
                        !isNaN(new Date(temp.pu_time_to).getFullYear()) &&
                        new Date(temp.pu_time_to).getFullYear() !== 1970) ?
                        temp.pu_time_to : null;

                    temp.dl_time_to = (temp.dl_time_to &&
                        !isNaN(new Date(temp.dl_time_to).getFullYear()) &&
                        new Date(temp.dl_time_to).getFullYear() !== 1970) ?
                        temp.dl_time_to : null;

                }

                temp.est_pickup_to_time = (temp.est_pickup_to_time &&
                    !isNaN(new Date(temp.est_pickup_to_time).getFullYear()) &&
                    new Date(temp.est_pickup_to_time).getFullYear() !== 1970) ?
                    temp.est_pickup_to_time : null;

                temp.est_pickup_from_time = (temp.est_pickup_from_time &&
                    !isNaN(new Date(temp.est_pickup_from_time).getFullYear()) &&
                    new Date(temp.est_pickup_from_time).getFullYear() !== 1970) ?
                    temp.est_pickup_from_time : null;

                temp.est_delivery_to_time = (temp.est_delivery_to_time &&
                    !isNaN(new Date(temp.est_delivery_to_time).getFullYear()) &&
                    new Date(temp.est_delivery_to_time).getFullYear() !== 1970) ?
                    temp.est_delivery_to_time : null;

                temp.est_delivery_from_time = (temp.est_delivery_from_time &&
                    !isNaN(new Date(temp.est_delivery_from_time).getFullYear()) &&
                    new Date(temp.est_delivery_from_time).getFullYear() !== 1970) ?
                    temp.est_delivery_from_time : null;

                if (options.retrieve === 'order') {

                    temp.formated_container = temp.container_number ?
                        temp.container_number.split('-') : [];

                    temp.formated_container = temp.formated_container.length &&
                        temp.formated_container.length > 1 ?
                        [
                            temp.formated_container[0],
                            temp.formated_container[1].replace(/^0+/, '')
                        ].join('') : temp.formated_container.join('');

                }

                if (options.retrieve !== 'invoice') {

                    temp.all_legs = temp.all_legs ?
                        JSON.parse(temp.all_legs).sort((a, b) => a.leg_number - b.leg_number) : [];

                    temp.all_legs.forEach(elem => {
                        elem.pu_time = elem.pu_time && new Date(elem.pu_time).getFullYear() !== 1970 ? elem.pu_time : null;

                        elem.dl_time = elem.dl_time && new Date(elem.dl_time).getFullYear() !== 1970 ? elem.dl_time : null;

                        elem.pu_time_to = elem.pu_time_to && new Date(elem.pu_time_to).getFullYear() !== 1970 ? elem.pu_time_to : null;

                        elem.dl_time_to = elem.dl_time_to && new Date(elem.dl_time_to).getFullYear() !== 1970 ? elem.dl_time_to : null;

                        if (request.timeZone) {
                            elem.filterPuTime = elem.pu_time ? moment(elem.pu_time).tz(request.timeZone).format('M/D/YYYY') : null;

                            elem.filterDlToTime = elem.dl_time_to ? moment(elem.dl_time_to).tz(request.timeZone).format('M/D/YYYY') : null;
                        } else {
                            elem.filterPuTime = elem.pu_time ? new Date(elem.pu_time).toLocaleDateString() : null;

                            elem.filterDlToTime = elem.dl_time_to ? new Date(elem.dl_time_to).toLocaleDateString() : null;
                        }


                        if (elem.pu_time === elem.pu_time_to) {
                            elem.pickup_time = elem.pu_time;
                        } else {
                            elem.pickup_time = null;
                        }


                        if (elem.dl_time === elem.dl_time_to) {
                            elem.delivery_time = elem.dl_time;
                        } else {
                            elem.delivery_time = null;
                        }

                    })

                }

                if (options.retrieve === 'dispatch') {

                    temp.midcities_lot_dl = temp.all_legs.filter(elem => {
                        return elem.dl_name === 'MID CITIES LOT' && elem.leg_type_id !== 1 &&
                            elem.leg_status === 'Delivered';
                    })

                    temp.central_lot_dl = temp.all_legs.filter(elem => {
                        return elem.dl_name === 'CENTRAL LOT' && elem.leg_type_id !== 1 &&
                            elem.leg_status === 'Delivered';
                    })

                    temp.midcities_lot_pu = temp.all_legs.filter(elem => {
                        return elem.pu_name === 'MID CITIES LOT' && elem.leg_type_id !== 1 &&
                            (elem.leg_status === 'Delivered' || elem.leg_status === 'Picked Up');
                    })

                    temp.central_lot_pu = temp.all_legs.filter(elem => {
                        return elem.pu_name === 'CENTRAL LOT' && elem.leg_type_id !== 1 &&
                            (elem.leg_status === 'Delivered' || elem.leg_status === 'Picked Up');
                    })

                }

                temp.tag_color = temp.tag_color !== null ? JSON.parse(temp.tag_color) : [];

                temp.tag_id = temp.tag_id !== null ? temp.tag_id.split(',') : [];

                if (temp.order_type === 'One Way Empty - Intermodal' ||
                    temp.order_type === 'One Way Loaded - Intermodal') {

                    temp.format_order_type = temp.order_type.split('-')[0]
                        .split(' ').map(elem => elem.charAt(0)).join('');

                }

                if (temp.est_pickup_from_time !== null && temp.est_pickup_to_time !== null) {
                    if (new Date(temp.est_pickup_from_time).toLocaleTimeString()
                        === new Date(temp.est_pickup_to_time).toLocaleTimeString()) {

                        temp.order_pickup_time = temp.est_pickup_from_time

                    } else {
                        temp.order_pickup_time = null;
                    }

                    if (new Date(temp.est_pickup_from_time).toLocaleDateString()
                        === new Date(temp.est_pickup_to_time).toLocaleDateString()) {
                        temp.order_pickup_date = temp.est_pickup_from_time
                    } else {
                        temp.order_pickup_date = null;
                    }
                }

                if (temp.est_delivery_from_time !== null &&
                    temp.est_delivery_to_time !== null) {
                    if (new Date(temp.est_delivery_from_time).toLocaleTimeString()
                        === new Date(temp.est_delivery_to_time).toLocaleTimeString()) {
                        temp.order_delivery_time = temp.est_delivery_from_time
                    } else {
                        temp.order_delivery_time = null;
                    }

                    if (new Date(temp.est_delivery_from_time).toLocaleDateString()
                        === new Date(temp.est_delivery_to_time).toLocaleDateString()) {
                        temp.order_delivery_date = temp.est_delivery_from_time
                    } else {
                        temp.order_delivery_date = null;
                    }
                }

                if (temp.pu_time && temp.pu_time_to) {

                    if (new Date(temp.pu_time).toLocaleTimeString()
                        === new Date(temp.pu_time_to).toLocaleTimeString()) {
                        temp.pickup_time = temp.pu_time;
                    } else {
                        temp.pickup_time = null;
                    }

                    if (new Date(temp.pu_time).toLocaleDateString()
                        === new Date(temp.pu_time_to).toLocaleDateString()) {
                        temp.pickup_date = temp.pu_time
                    } else {
                        temp.pickup_date = null;
                    }
                }

                if (temp.dl_time && temp.dl_time_to) {

                    if (new Date(temp.dl_time).toLocaleTimeString()
                        === new Date(temp.dl_time_to).toLocaleTimeString()) {
                        temp.delivery_time = temp.dl_time;
                    } else {
                        temp.delivery_time = null;
                    }

                    if (new Date(temp.dl_time).toLocaleDateString()
                        === new Date(temp.dl_time_to).toLocaleDateString()) {
                        temp.delivery_date = temp.dl_time
                    } else {
                        temp.delivery_date = null;
                    }
                }

            })

            return orderData;

        } catch (error) {
            console.error(new Date(), "[order_mgmt.js] [FETCH] ORDER LOOP ERROR", e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }
};
