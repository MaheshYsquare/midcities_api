'use strict';

var async = require('async');
const redis = require('redis');
const REDIS_PORT = process.env.REDIS_PORT;
const REDIS_HOST = process.env.REDIS_HOST;
const client = redis.createClient(REDIS_PORT, REDIS_HOST);
const fetchRequest = require('request');
const config = require('./../../server/boot/load-config').getConfig();

const app = require('../../server/server.js');
const OAuthClient = require('intuit-oauth');
const moment = require('moment');
const multiparty = require('multiparty');
const fs = require('fs');
const AWS = require('aws-sdk');

AWS.config.update({
    secretAccessKey: config.awsSecretAccessKey,
    accessKeyId: config.awsAccessKeyId,
    region: config.awsRegion
});
const s3 = new AWS.S3({});

module.exports = function (Manualinvoice) {

    let queryParams;

    let quickbooksUrl = app.oauthClient.environment == 'sandbox' ? OAuthClient.environment.sandbox : OAuthClient.environment.production;

    //database connector
    const executeQuery = (query, params, callback) => {
        Manualinvoice.dataSource.connector.query(query, params, callback);
    }

    //Access Token
    const validateAccessToken = (access_token, callback) => {
        Manualinvoice.app.models.AccessToken.findById(access_token, callback)
    }

    //calling internally by loopback(overriden)
    Manualinvoice.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    const postManualInvoiceQuery = (params) => {
        return {
            "query": "INSERT INTO manual_invoice(invoice_number,order_number,date,total_amount,due_date,container_number,customer_reference,booking_number,customer_id,random_notes,delivery_date,created_on,created_by,outstanding,consignee_address,consignee_phone,shipper_address,shipper_phone,consignee_name,shipper_name,manual_invoice_quickbooks_id) VALUES(?,?,?,?,?,UCASE(?),?,?,?,?,?,CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?)",
            "params": params
        };
    }

    const postManualInvoiceChargesQuery = (params) => {
        return {
            "query": "INSERT INTO manual_invoice_accessorial_charges(manual_invoice_id,accessories_name,description,rate) VALUES ?",
            "params": params
        };
    }

    const documentLinksUpdateOrInsertQuery = (params) => {
        return {
            query: `insert into manual_invoice_docs 
                        (manual_invoice_id, file_name)
                    values (?,?)
                    on duplicate key update
                        file_name = ?;`,
            params
        }
    }

    const authenticateQuickbooksConnection = async (request, response, options, section, callback) => {

        try {
            if (app.oauthClient.token.getToken().access_token) {

                if (app.oauthClient.isAccessTokenValid()) {
                    if (section == 'post') {
                        postDataInQuickbooks(request, response, callback)
                    } else if (section == 'delete') {
                        deleteInvoiceParsing(request, response, options, callback);
                    } else if (section == 'update') {
                        updateDataInQuickbooks(request, response, callback);
                    }

                } else {
                    app.oauthClient.refresh()
                        .then((authResponse) => {
                            app.eventEmitter.emit('quickbooksTokenAdd', authResponse.getToken());

                            if (section == 'post') {
                                postDataInQuickbooks(request, response, callback)
                            } else if (section == 'delete') {
                                deleteInvoiceParsing(request, response, options, callback);
                            } else if (section == 'update') {
                                updateDataInQuickbooks(request, response, callback);
                            }
                        })
                        .catch((e) => {
                            console.error(new Date(), `[invoice] [QUICKBOOKS] Refresh`, e);

                            callback(null, { code: 103, error: 'Please Connect To Quickbooks' });
                        });
                }

            } else {
                let quickBooksToken = await fetchQuickbooksToken(response);

                if (quickBooksToken.length !== 0) {
                    app.oauthClient.setToken(quickBooksToken[0]);

                    authenticateQuickbooksConnection(request, response, options, section, callback);
                } else {
                    callback(null, { code: 103, error: 'Please Connect To Quickbooks' });
                }
            }
        } catch (e) {
            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    const fetchQuickbooksToken = (response) => {
        return new Promise((resolve, reject) => {
            return executeQuery('select * from quickbooks_token_master where id = 1;', null,
                (err, queryResponse) => {
                    if (err) {
                        return response.status(409).json({ code: 409, error: "QUICKBOOKS TOKEN FETCH ERROR" });
                    } else {
                        resolve(queryResponse);
                    }
                })
        })
    }

    const postDataInQuickbooks = async (urlReq, res, callback) => {
        try {
            let itemId = await fetchItemIdFromQuickbooks();

            const data = await getFileFromRequest(urlReq);

            let files = data.files;

            let req = JSON.parse(data.fields.fields[0]);

            req.invoice_number = await fetchInvoiceNumber();

            if (itemId.code === 200) {
                req.quickbook_line.forEach(temp => {
                    temp.SalesItemLineDetail.ItemRef.value = itemId.Id;
                })
            } else if (itemId.code === 409) {
                return res.status(409).json(itemId);
            }

            let quickbooksInput = {
                "DocNumber": req.invoice_number,
                "Line": req.quickbook_line,
                "CustomerRef": {
                    "value": req.customer_quickbooks_id
                },
                "DueDate": req.due_date || moment().add(7, 'days').format('YYYY-MM-DD')
            }

            req.date = req.date ? req.date : new Date(new Date().toDateString());

            let invoiceOptions = {
                uri: `${quickbooksUrl}/v3/company/${app.oauthClient.getToken().realmId}/invoice?minorversion=38`,
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + app.oauthClient.getToken().access_token
                },
                json: quickbooksInput
            }

            fetchRequest(invoiceOptions, async (err, result) => {
                try {
                    if (err) {
                        return res.status(500).json({ code: 500, error: 'QUICKBOOK API CONNECTION ERROR' });
                    }

                    if (result.statusCode < 200 || result.statusCode > 299) {

                        if (result &&
                            ('body' in result)
                            && ('Fault' in result.body)
                            && result.body.Fault.Error.length !== 0
                            && (result.body.Fault.Error[0].Message == 'Invalid Customer'
                                || result.body.Fault.Error[0].Message == 'Invalid Reference Id')) {
                            return callback(null, {
                                code: 422,
                                error: 'Invoice cannot be created for inactive customer'
                            })
                        }

                        if (result &&
                            ('body' in result)
                            && ('Fault' in result.body)
                            && result.body.Fault.Error.length
                            && 'Message' in result.body.Fault.Error[0]
                            && result.body.Fault.Error[0].Message.includes("Duplicate Document Number Error")) {

                            console.error(new Date(), "[manual_invoice] [QUICKBOOKS] API ERROR",
                                result.body.Fault.Error[0]);

                            const updateSequence = await updateSequenceDocNumber();

                            if (updateSequence.code !== 200) {
                                return res.status(409).json(updateSequence);
                            }

                            postDataInQuickbooks(urlReq, res, callback);

                            return;
                        }

                        console.error(new Date(), `[manual_invoice] [API] QUICKBOOKS API ERROR`,
                            result && ('body' in result) && ('Fault' in result.body) &&
                                ('Error' in result.body.Fault) && result.body.Fault.Error &&
                                result.body.Fault.Error.length !== 0 ? result.body.Fault.Error[0]
                                : null);

                        return res.status(409).json({ code: 409, error: 'QUICKBOOKS ERROR' });

                    }

                    queryParams = postManualInvoiceQuery([req.invoice_number, req.order_number,
                    req.date, result.body.Invoice.TotalAmt, result.body.Invoice.DueDate, req.container_number,
                    req.customer_reference, req.booking_number, req.customer_id, req.random_notes,
                    req.delivery_date, req.created_by, result.body.Invoice.TotalAmt, req.consignee_address,
                    req.consignee_phone, req.shipper_address, req.shipper_phone, req.consignee,
                    req.shipper, result.body.Invoice.Id]);

                    executeQuery(queryParams.query, queryParams.params, (postErr, queryResponse) => {
                        if (postErr) {
                            console.error(new Date(), `[manual] [POST] MANUAL INVOICE QUERY ERROR`,
                                "sqlMessage" in postErr ? postErr.sqlMessage : postErr);

                            return res.status(409).json({ code: 409, error: 'DB ERROR' });
                        }

                        client.del('invoice_mgmt');
                        client.del('advanced_search');

                        let chargesBulkParams = [];

                        if (req.accessoriesCharge && req.accessoriesCharge.length) {

                            req.accessoriesCharge.forEach(temp => {
                                chargesBulkParams.push([queryResponse.insertId,
                                temp.accessories_name, temp.description, temp.rate]);
                            })

                            queryParams = postManualInvoiceChargesQuery([chargesBulkParams]);
                            executeQuery(queryParams.query, queryParams.params,
                                (postChargeErr, chargesQueryResponse) => {
                                    if (postChargeErr) {
                                        console.error(new Date(),
                                            `[manual] [POST] MANUAL INVOICE CHARGES QUERY ERROR`,
                                            "sqlMessage" in postChargeErr ? postChargeErr.sqlMessage : postChargeErr);

                                        callback(null, {
                                            code: 422,
                                            error: "Problem in adding charges"
                                        })
                                    } else {
                                        client.del('invoice_mgmt');
                                        client.del('advanced_search');

                                        uploadFiles(files, queryResponse.insertId, req.invoice_number, req.userId, null, null);

                                        callback(null, {
                                            code: 200,
                                            data: 'successfully invoice generated'
                                        })
                                    }
                                })

                        } else {
                            callback(null, {
                                code: 422,
                                error: "Problem in adding charges"
                            })
                        }

                    });

                } catch (error) {
                    console.error(new Date(), `[manual] [POST] MANUAL INVOICE CODE ERROR`, error);

                    res.status(400).json({ code: 400, error: 'CODE ERROR' });
                }

            })
        } catch (error) {
            console.error(new Date(), `[manual] [POST] MANUAL INVOICE CODE ERROR`, error);

            res.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    const updateSequenceDocNumber = () => {
        return new Promise((resolve, reject) => {
            return executeQuery("UPDATE sequences SET invoice_number = invoice_number + 1 WHERE sequence_name = 'orders';",
                null, (err, checkQueryResponse) => {
                    if (err) {
                        return resolve({ code: 409, error: "UPDATE SEQUENCE ERROR" });
                    }

                    resolve({ code: 200, data: "Successfully Updated" });

                })
        })
    }

    const fetchInvoiceNumber = () => {
        return new Promise((resolve, reject) => {
            return executeQuery("select invoice_number from sequences where sequence_name = 'orders';",
                null, (err, queryResponse) => {
                    if (err) {
                        console.error(new Date(), `[invoice_mgmt] [FETCH] INVOICE NUMBER QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        resolve(Math.floor(Math.random() * 100000) + 1);
                    } else {
                        resolve(queryResponse[0].length !== 0 ?
                            queryResponse[0].invoice_number : Math.floor(Math.random() * 100000) + 1);
                    }
                })
        })
    }

    const fetchInvoiceSyncTokenFromQuickbooks = (id) => {
        return new Promise((resolve, reject) => {
            return app.oauthClient.makeApiCall({ url: `${quickbooksUrl}v3/company/${app.oauthClient.getToken().realmId}/invoice/${id}?minorversion=38` })
                .then((result) => {
                    if (result.statusCode < 200 || result.statusCode > 299) {
                        console.error(new Date(), `[manual] [FETCH] QUICKBOOK SYNC TOKEN ERROR`,
                            result && ("body" in result) ? result.body : null);

                        resolve({ code: 409, error: 'QUICKBOOKS SYNC API ERROR' });
                    } else {
                        let syncToken = result.json.Invoice.SyncToken;

                        resolve({ code: 200, syncToken })
                    }
                }).catch(err => {
                    resolve({ code: 409, error: 'QUICKBOOKS SYNC CATCH ERROR' });
                })
        })
    }

    const fetchItemIdFromQuickbooks = () => {
        return new Promise((resolve, reject) => {
            return app.oauthClient.makeApiCall({ url: `${quickbooksUrl}v3/company/${app.oauthClient.getToken().realmId}/query?query=select * from Item where Name='Services'&minorversion=38` })
                .then((result) => {
                    if (result.statusCode < 200 || result.statusCode > 299) {
                        console.error(new Date(), `[manual] [FETCH] QUICKBOOKS ITEM ID ERROR`,
                            result && ("body" in result) ? result.body : null);

                        resolve({ code: 409, error: 'QUICKBOOKS ITEM API ERROR' });
                    } else {
                        let Id = result.json.QueryResponse.Item[0].Id;

                        resolve({ code: 200, Id })
                    }
                }).catch(err => {
                    resolve({ code: 409, error: 'QUICKBOOKS ITEM CATCH ERROR' });
                })
        })
    }

    const getFileFromRequest = (req) => new Promise((resolve, reject) => {
        const form = new multiparty.Form();
        form.parse(req, (err, fields, files) => {
            if (err) reject(err);
            resolve({ files, fields });
        });
    });

    const uploadFiles = (files, id, invoiceNumber, userId, cb, res) => {
        if (Object.keys(files).length === 0) {
            if (res) {
                return res.status(409).json({ code: 409, error: "NO FILES" });
            }
            return
        }

        let called = false;

        const q = async.queue((task, callback) => {
            s3.upload({
                Bucket: config.awsS3BucketName,
                ACL: config.awsS3ACL,
                Key: ['manual-invoice', invoiceNumber, task.originalFilename].join('/'),
                ContentType: task.headers['content-type'],
                Body: fs.createReadStream(task.path)
            }, (s3Err, result) => {
                if (s3Err) {
                    console.error(new Date(), `[invoice] [UPLOAD] AWS UPLOAD ERROR`, s3Err);
                    return callback(s3Err);
                } else {

                    queryParams = documentLinksUpdateOrInsertQuery([id, result['key'], result['key']]);

                    executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                        if (err) {
                            console.error(new Date(), `[invoice] [UPSERT] ${task.originalFilename} FILE LINK QUERY ERROR`,
                                "sqlMessage" in err ? err.sqlMessage : err);

                            return callback("sqlMessage" in err ? err.sqlMessage : err);
                        } else {
                            console.log(new Date(), `[invoice] [UPLOAD] FILE ${task.originalFilename} UPLOADED`);

                            callback();
                        }
                    });

                } // return the values of the successful AWS S3 request
            })
        }, 5);

        q.push(files.files, (pushErr) => {
            if (pushErr && !called) {
                console.error(new Date(), `[invoice] [UPLOAD] QUEUE ERROR`, pushErr);

                q.kill();

                called = true;

                for (const s of app.sockets) {
                    if (s.id === userId) {
                        s.emit('fileUpload', { code: 409, error: "Some Files May Not be Uploaded" });
                    }
                }
            }
        })

        if (process.env.NODE_ENV === 'local') {
            q.drain = () => {
                for (const s of app.sockets) {
                    if (s.id === userId) {
                        s.emit('fileUpload', { code: 200, error: null });
                    }
                }
            }
        } else {
            q.drain(() => {
                for (const s of app.sockets) {
                    if (s.id === userId) {
                        s.emit('fileUpload', { code: 200, error: null });
                    }
                }
            });
        }

        q.error(function (error, task) {
            console.error(new Date(), '[order] [UPLOAD] Queue Error', error, task);

            for (const s of app.sockets) {
                if (s.id === userId) {
                    s.emit('fileUpload', { code: 409, error: "Some Files May Not be Uploaded" });
                }
            }
        });

        if (cb) {
            cb(null, "UPLOADING...");
        }
    }

    Manualinvoice.postManualInvoice = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                authenticateQuickbooksConnection(request, response, options, 'post', callback);
            }
        });
    };

    const updateManualInvoiceQuery = (params) => {
        return {
            "query": "UPDATE manual_invoice set `date` = ?,total_amount = ?,due_date = ?,container_number = UCASE(?),customer_reference = ?,booking_number = ?,customer_id = ?,random_notes = ? ,delivery_date = ?,lastupdated_on = CURRENT_TIMESTAMP,lastupdated_by = ?,outstanding = ?,consignee_address = ?,consignee_phone = ?,shipper_address = ?,shipper_phone = ?,consignee_name = ?,shipper_name = ? WHERE manual_invoice_id = ?",
            "params": params
        };
    }

    const deleteManualInvoiceChargesQuery = (params) => {
        return {
            "query": "delete from manual_invoice_accessorial_charges where manual_invoice_id = ?",
            "params": params
        };
    }

    const updateDataInQuickbooks = async (req, res, callback) => {
        try {
            let itemId = await fetchItemIdFromQuickbooks();

            if (itemId.code === 200) {
                req.quickbook_line.forEach(temp => {
                    temp.SalesItemLineDetail.ItemRef.value = itemId.Id;
                })
            } else if (itemId.code === 409) {
                return res.status(409).json(itemId);
            }

            let quickbooksInput = {
                "Line": req.quickbook_line,
                "CustomerRef": {
                    "value": req.customer_quickbooks_id
                },
                "DueDate": req.due_date || moment().add(7, 'days').format('YYYY-MM-DD'),
                "Id": req.manual_invoice_quickbooks_id,
                "sparse": true
            }

            let syncToken = await fetchInvoiceSyncTokenFromQuickbooks(req.manual_invoice_quickbooks_id);

            if (syncToken.code === 200) {
                quickbooksInput['SyncToken'] = syncToken.syncToken;
            } else if (syncToken.code === 409) {
                return res.status(409).json(syncToken);
            }

            req.date = req.date ? req.date : new Date(new Date().toDateString());

            let invoiceOptions = {
                uri: `${quickbooksUrl}/v3/company/${app.oauthClient.getToken().realmId}/invoice?minorversion=38`,
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + app.oauthClient.getToken().access_token
                },
                json: quickbooksInput
            }

            fetchRequest(invoiceOptions, (err, result) => {
                try {
                    if (err) {
                        res.status(500).json({ code: 500, error: 'QUICKBOOK API CONNECTION ERROR' });
                    } else {
                        if (result.statusCode < 200 || result.statusCode > 299) {
                            if (result &&
                                ('body' in result)
                                && ('Fault' in result.body)
                                && result.body.Fault.Error.length !== 0
                                && (result.body.Fault.Error[0].Message == 'Invalid Customer'
                                    || result.body.Fault.Error[0].Message == 'Invalid Reference Id')) {
                                return callback(null, {
                                    code: 422,
                                    error: 'Invoice cannot be created for inactive customer'
                                })
                            } else {
                                console.error(new Date(), `[manual_invoice] [API] QUICKBOOKS API ERROR`,
                                    result && ('body' in result) && ('Fault' in result.body) &&
                                        ('Error' in result.body.Fault) && result.body.Fault.Error &&
                                        result.body.Fault.Error.length !== 0 ? result.body.Fault.Error[0]
                                        : null);

                                return res.status(409).json({ code: 409, error: 'QUICKBOOKS ERROR' });
                            }
                        } else {
                            queryParams = updateManualInvoiceQuery([req.date, result.body.Invoice.TotalAmt, result.body.Invoice.DueDate,
                            req.container_number, req.customer_reference, req.booking_number, req.customer_id, req.random_notes,
                            req.delivery_date, req.created_by, result.body.Invoice.TotalAmt, req.consignee_address,
                            req.consignee_phone, req.shipper_address, req.shipper_phone,
                            req.consignee, req.shipper, req.manual_invoice_id]);

                            executeQuery(queryParams.query, queryParams.params, (updateErr, queryResponse) => {
                                if (updateErr) {
                                    return res.status(409).json({ code: 409, error: 'DB ERROR' });
                                } else {
                                    let chargesBulkParams = [];

                                    if (req.accessoriesCharge && req.accessoriesCharge.length !== 0) {

                                        queryParams = deleteManualInvoiceChargesQuery([
                                            req.manual_invoice_id]);

                                        executeQuery(queryParams.query, queryParams.params,
                                            (deleteErr, deleteResponse) => {
                                                if (deleteErr) {
                                                    client.del('invoice_mgmt');
                                                    client.del('advanced_search');
                                                    return res.status(409).json({
                                                        code: 409,
                                                        error: 'DB ERROR CHARGE'
                                                    });
                                                } else {
                                                    req.accessoriesCharge.forEach(temp => {
                                                        chargesBulkParams.push([
                                                            req.manual_invoice_id,
                                                            temp.accessories_name, temp.description, temp.rate]);
                                                    })

                                                    queryParams = postManualInvoiceChargesQuery([
                                                        chargesBulkParams]);

                                                    executeQuery(queryParams.query, queryParams.params,
                                                        (updateChargeErr, chargesQueryResponse) => {
                                                            if (updateChargeErr) {
                                                                client.del('invoice_mgmt');
                                                                client.del('advanced_search');
                                                                callback(null, {
                                                                    code: 422,
                                                                    error: "Problem in Adding Charges"
                                                                })
                                                            } else {
                                                                client.del('invoice_mgmt');
                                                                client.del('advanced_search');
                                                                callback(null, {
                                                                    code: 200,
                                                                    data: 'successfully invoice updated'
                                                                })
                                                            }
                                                        })
                                                }
                                            })
                                    }
                                }
                            });
                        }
                    }
                } catch (error) {
                    console.error(new Date(), `[manual] [UPDATE] MANUAL INVOICE CODE ERROR`, error);

                    res.status(400).json({ code: 400, error: 'CODE ERROR' });
                }

            })
        } catch (error) {
            console.error(new Date(), `[manual] [UPDATE] MANUAL INVOICE CODE ERROR`, error);

            res.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Manualinvoice.updateManualInvoice = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                authenticateQuickbooksConnection(request, response, options, 'update', callback);
            }
        });
    };


    const fetchSelectedManualInvoiceFromDb = (invoiceNumber, response, options, callback) => {
        try {
            executeQuery("call getManualInvoice(?);", [invoiceNumber], (err, queryResponse) => {
                try {

                    if (err) {
                        console.error(new Date(), `[manual] [FETCH] MANAUAL INVOICE QUERY ERROR`,
                            "sqlMessage" in err ? err.sqlMessage : err);

                        return response.status(409).json({ code: 409, error: "DB ERROR" });
                    }

                    queryResponse[0].forEach(temp => {
                        temp.accessoriesCharge = temp.accessoriesCharge ? JSON.parse(temp.accessoriesCharge) : [];
                        temp.file_names = temp.file_names ? JSON.parse(temp.file_names) : [];
                    })

                    callback(null, queryResponse[0]);

                } catch (e) {
                    console.error(new Date(), `[manual] [FETCH] MANAUAL INVOICE CODE ERROR`, e);

                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }

            })
        } catch (e) {
            console.error(new Date(), `[manual] [FETCH] MANAUAL INVOICE CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Manualinvoice.getManualInvoice = (invoice_number, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                fetchSelectedManualInvoiceFromDb(invoice_number, response, options, callback);
            }
        });
    }


    //Get manual invoice
    Manualinvoice.getManualInvoiceSequenceQuery = function () {
        return {
            "query": "select invoice_number,order_number from sequences where sequence_name = 'orders';"
        };
    }

    Manualinvoice.getManualInvoiceSequenceValidation = function (response, options, callback) {
        try {
            queryParams = Manualinvoice.getManualInvoiceSequenceQuery();
            executeQuery(queryParams.query, null, function (err, queryResponse) {
                if (err) {
                    response.status(409).json({ code: 409, error: "DB ERROR" });
                } else {
                    callback(null, queryResponse);
                }
            })
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Manualinvoice.getManualInvoiceSequence = function (response, options, callback) {
        var self = this;
        validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getManualInvoiceSequenceValidation(response, options, callback);
            }
        });
    }

    Manualinvoice.getFileValidation = function (data, response, options, callback) {
        try {
            var updatedata = JSON.stringify(data);
            var parsedData = JSON.parse(updatedata);
            var fileName = parsedData.fileName;
            var senderAddress = parsedData.senderAddress;
            var invoiceData = parsedData.invoiceData;

            var reqOptions = {
                uri: `${config.reportingServer}manualInvoice`,
                method: "POST",
                encoding: null,
                json: invoiceData
            }

            fetchRequest.post(reqOptions, (err, result) => {
                try {
                    if (err) {
                        response.status(500).json({ code: 500, error: "REPORTING CONNECTION ERROR" });
                    } else {
                        if (result.statusCode < 200 || result.statusCode > 299) {
                            response.status(409).json({ code: 409, error: "REPORTING DOC ERROR" });
                        } else {
                            Manualinvoice.app.models.Email.send({
                                to: [senderAddress],
                                from: config.mail,
                                subject: `Mid Cities Invoice #${invoiceData.invoice_number}`,
                                attachments: [
                                    {   // binary buffer as an attachment
                                        filename: `${fileName}.pdf`,
                                        content: new Uint8Array(result.body)
                                    }
                                ]
                            })

                            callback(null, `Successfully mail sent to ${senderAddress}`);
                        }
                    }
                } catch (error) {
                    response.status(400).json({ code: 400, error: "CODE ERROR" });
                }
            })
        } catch (e) {
            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Manualinvoice.sendEmail = function (data, response, options, callback) {
        var self = this;
        validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getFileValidation(data, response, options, callback);
            }
        });
    }

    const deleteManualInvoiceQuery = (params) => {
        return {
            "query": "call deleteManualInvoice(?)",
            "params": params
        };
    }

    const deleteInvoiceParsing = async (req, response, options, callback) => {
        try {
            let quickbooksInput = {
                "Id": req.manual_invoice_quickbooks_id
            };

            let syncToken = await fetchInvoiceSyncTokenFromQuickbooks(req.manual_invoice_quickbooks_id)

            if (syncToken.code === 200) {
                quickbooksInput['SyncToken'] = syncToken.syncToken
            } else if (syncToken.code === 409) {
                return response.status(409).json(syncToken)
            }

            let invoiceOptions = {
                uri: `${quickbooksUrl}/v3/company/${app.oauthClient.getToken().realmId}/invoice?operation=delete&minorversion=38`,
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + app.oauthClient.getToken().access_token
                },
                json: quickbooksInput
            }

            fetchRequest(invoiceOptions, (err, result) => {
                try {
                    if (err) {
                        return response.status(500).json({ code: 500, error: 'QUICKBOOK API CONNECTION ERROR' });
                    } else {

                        if (result.statusCode < 200 || result.statusCode > 299) {
                            console.error(new Date(), `[manual_invoice] [API] QUICKBOOKS API ERROR`,
                                result && ('body' in result) && ('Fault' in result.body) &&
                                    ('Error' in result.body.Fault) && result.body.Fault.Error &&
                                    result.body.Fault.Error.length !== 0 ? result.body.Fault.Error[0]
                                    : null);

                            return response.status(409).json({ code: 409, error: 'QUICKBOOKS DELETE ERROR' });
                        } else {
                            queryParams = deleteManualInvoiceQuery([req.manual_invoice_id]);
                            executeQuery(queryParams.query, queryParams.params, (deleteErr, queryResponse) => {
                                if (deleteErr) {
                                    response.status(409).json({ code: 409, error: 'DB ERROR' });
                                } else {
                                    client.del('invoice_mgmt');
                                    client.del('advanced_search');
                                    deleteFileToS3({ fileName: req.fileName }, null, true, null);
                                    callback(null, { code: 200, data: "successfully deleted" });
                                }
                            });
                        }
                    }
                } catch (error) {
                    response.status(400).json({ code: 400, error: 'CODE ERROR' });
                }
            })
        } catch (e) {
            response.status(400).json({ code: 400, error: 'CODE ERROR' });
        }
    }

    Manualinvoice.deleteManualInvoice = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                res.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                authenticateQuickbooksConnection(request, response, options, 'delete', callback);
            }
        });
    };

    const deleteManualInvoiceDocsQuery = (params) => {
        return {
            query: "DELETE FROM manual_invoice_docs WHERE file_name = ? and manual_invoice_id = ?",
            params
        }
    }

    const deleteFileToS3 = (request, response, isMultiple, callback) => {
        let params;

        if (!isMultiple) {
            queryParams = deleteManualInvoiceDocsQuery([request.fileName, request.id]);

            executeQuery(queryParams.query, queryParams.params, (err, queryResponse) => {
                if (err) {
                    console.error(new Date(), `[manual] [DELETE] MANUAL INVOICE FILE QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: 'DB ERROR' });
                }

                params = {
                    Bucket: config.awsS3BucketName,
                    Key: request.fileName
                };

                s3.deleteObject(params, (s3Err, result) => {
                    if (s3Err) {
                        console.error(new Date(), "[invoice] [DELETE] AWS DELETE ERROR", s3Err)

                        return response.status(409).json({ code: 409, error: "AWS ERROR" });
                    }
                    if (result) {
                        console.log(new Date(), "[invoice] FILE DELETED SUCCESSFULLY");

                        callback(null, "Successfully Deleted");
                    }
                });
            });

        } else {
            params = {
                Bucket: config.awsS3BucketName,
                Prefix: request.fileName
            };
            s3.listObjects(params, (s3ListErr, data) => {
                if (s3ListErr) {
                    return console.error(new Date(), `[invoice] [DELETE] AWS DELETE ERROR`, s3ListErr);
                }

                if (data.Contents.length == 0) return;

                params = { Bucket: config.awsS3BucketName };
                params.Delete = { Objects: [] };

                data.Contents.forEach((content) => {
                    params.Delete.Objects.push({ Key: content.Key });
                });

                s3.deleteObjects(params, (s3DeleteErr, result) => {
                    if (s3DeleteErr) return console.error(new Date(), "[invoice] [DELETE] AWS DELETE ERROR", s3DeleteErr);
                    else console.log(new Date(), "[invoice] FILE DELETED SUCCESSFULLY");
                });
            });
        }
    };

    Manualinvoice.deleteManualInvoiceFiles = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                res.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;
                deleteFileToS3(request, response, false, callback);
            }
        });
    };


    Manualinvoice.uploadManualInvoiceFile = (request, response, options, callback) => {

        validateAccessToken(options.access_token, async (err, tokenModel) => {
            if (err || !tokenModel) {
                res.status(401).json({ code: 401, error: 'ACCESS TOKEN FAILED' });
            } else {
                options.tokenModel = tokenModel;

                const data = await getFileFromRequest(request);

                let files = data.files;

                let req = JSON.parse(data.fields.fields[0]);

                uploadFiles(files, req.id, req.invoice_number, req.userId, callback, response);
            }
        });
    };

    const fetchFileFromAWS = (request, response, options, callback) => {

        let params = {
            Bucket: config.awsS3BucketName,
            Key: request.fileName
        }

        s3.getObject(params, (err, data) => {
            if (err) {
                console.error(new Date(), "[invoice] [FETCH] AWS RETRIEVE FILE ERROR", err);

                response.status(409).json({ code: 409, error: "S3 ERROR" });
            } else {
                callback(null, {
                    ContentType: data.ContentType,
                    buffer: data.Body
                })
            }
        });
    }

    Manualinvoice.getManualInvoiceFile = (request, response, options, callback) => {

        validateAccessToken(options.access_token, (err, tokenModel) => {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                fetchFileFromAWS(request, response, options, callback);
            }
        });
    }

};
