'use strict';

const multiparty = require('multiparty');
const AWS = require('aws-sdk');
var config = require('./../../server/boot/load-config').getConfig();

AWS.config.update({
    secretAccessKey: config.awsSecretAccessKey,
    accessKeyId: config.awsAccessKeyId,
    region: config.awsRegion
});
const s3 = new AWS.S3({});

const fs = require('fs');
const async = require('async');
const app = require('../../server/server.js');

module.exports = function (Driverdocumentlinks) {

    //database connector
    Driverdocumentlinks.executeQuery = function (ds, query, params, callback) {
        ds.connector.query(query, params, callback);
    }
    //Access Token
    Driverdocumentlinks.validateAccessToken = function (access_token, callback) {
        this.app.models.AccessToken.findById(access_token, callback)
    }
    //calling internally by loopback(overriden)
    Driverdocumentlinks.createOptionsFromRemotingContext = function (ctx) {
        var base = this.base.createOptionsFromRemotingContext(ctx);
        base.access_token = ctx.req.headers.accesstoken || ctx.req.query.access_token || "";
        return base;
    }

    //upload file in leg_mgmt
    const getFileFromRequest = (req) => new Promise((resolve, reject) => {
        const form = new multiparty.Form();
        form.parse(req, (err, fields, files) => {
            if (err) reject(err); // get the file from the returned files object
            if (!files) reject('File was not found in form data.');
            else resolve({ files, fields });
        });
    });

    const documentLinksUpdateOrInsertQuery = (params) => {
        return {
            query: `insert into driver_document_links 
                        (driver_id, file_name, document_link)
                    values (?,?,?)
                    on duplicate key update
                        file_name = ?,
                        document_link = ?;`,
            params
        }
    }

    Driverdocumentlinks.uploadFile = (req, res, options, cb) => {

        Driverdocumentlinks.app.models.AccessToken.findById(options.access_token, async (err, token) => {
            if (err || !token) {

                return res.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });

            } else {
                // extract the file from the request object
                const data = await getFileFromRequest(req);

                let files = data.files;

                let fields = JSON.parse(data.fields.fields[0]);

                let called = false;

                const q = async.queue((task, callback) => {
                    s3.upload({
                        Bucket: config.awsS3BucketName,
                        ACL: config.awsS3ACL,
                        Key: ['drivers', fields.driver_name, task.originalFilename].join('/'),
                        ContentType: task.headers['content-type'],
                        Body: fs.createReadStream(task.path)
                    }, (s3Err, result) => {
                        if (s3Err) {
                            console.error(new Date(), `[driver] [UPLOAD] AWS UPLOAD ERROR`, s3Err);
                            return callback(s3Err);
                        } else {

                            var ds = Driverdocumentlinks.dataSource;
                            var queryParams;

                            queryParams = documentLinksUpdateOrInsertQuery([fields.driver_id, result['key'], result['location'],
                            result['key'], result['location']]);

                            Driverdocumentlinks.executeQuery(ds, queryParams.query, queryParams.params,
                                function (queryErr, queryResponse) {
                                    if (queryErr) {
                                        console.error(new Date(), `[driver] [UPSERT] ${task.originalFilename} FILE LINK QUERY ERROR`,
                                            "sqlMessage" in queryErr ? queryErr.sqlMessage : queryErr);

                                        return callback("sqlMessage" in queryErr ? queryErr.sqlMessage : queryErr);
                                    } else {
                                        console.log(new Date(), `[driver] [UPLOAD] FILE ${task.originalFilename} UPLOADED`);

                                        callback();
                                    }
                                });

                        } // return the values of the successful AWS S3 request
                    })
                }, 5);

                q.push(files.files, (pushErr) => {
                    if (pushErr && !called) {
                        console.error(new Date(), `[driver] [UPLOAD] QUEUE ERROR`, pushErr);

                        q.kill();

                        called = true

                        for (const s of app.sockets) {
                            if (s.id === fields.userId) {
                                s.emit('fileUpload', { code: 409, error: "Some Files May Not be Uploaded" });
                            }
                        }
                    }
                })

                q.error(function (error, task) {
                    console.error(new Date(), '[driver] [UPLOAD] Queue Error', error, task);

                    for (const s of app.sockets) {
                        if (s.id === fields.userId) {
                            s.emit('fileUpload', { code: 409, error: "Some Files May Not be Uploaded" });
                        }
                    }
                });

                if (process.env.NODE_ENV === 'local') {
                    q.drain = () => {
                        for (const s of app.sockets) {
                            if (s.id === fields.userId) {
                                s.emit('fileUpload', { code: 200, error: null });
                            }
                        }
                    }
                } else {
                    q.drain(() => {
                        for (const s of app.sockets) {
                            if (s.id === fields.userId) {
                                s.emit('fileUpload', { code: 200, error: null });
                            }
                        }
                    });
                }

                cb(null, "File Uploading...");

            }
        })

    };

    Driverdocumentlinks.getDocumentLinksQuery = function (params) {
        return {
            "query": "SELECT file_name FROM driver_document_links ddl WHERE driver_id = ?;",
            "params": params
        };
    }

    Driverdocumentlinks.getDocumentLinksValidation = function (driver_id, response, options, callback) {
        var ds = Driverdocumentlinks.dataSource;
        var queryParams;
        try {
            queryParams = Driverdocumentlinks.getDocumentLinksQuery([driver_id]);
            Driverdocumentlinks.executeQuery(ds, queryParams.query, queryParams.params, function (err, queryResponse) {
                if (err) {
                    console.error(new Date(), `[driver] [FETCH] FILE NAME QUERY ERROR`,
                        "sqlMessage" in err ? err.sqlMessage : err);

                    return response.status(409).json({ code: 409, error: "DB ERROR" });
                }
                queryResponse = queryResponse.map(temp => temp.file_name);

                callback(null, { code: 200, data: queryResponse });
            })
        } catch (e) {
            console.error(new Date(), `[driver] [FETCH] FILE NAME CODE ERROR`, e);

            response.status(400).json({ code: 400, error: "CODE ERROR" });
        }
    }

    Driverdocumentlinks.getDocumentLinks = function (driver_id, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getDocumentLinksValidation(driver_id, response, options, callback);
            }
        });
    }

    const deleteFileToS3 = (fileName, options = [{}]) => {
        // return a promise
        return new Promise((resolve, reject) => {
            var params = {
                Bucket: config.awsS3BucketName,
                Key: fileName
            };
            s3.deleteObject(params, function (err, data) {
                if (err) {
                    console.error(new Date(), `[driver] [DELETE] AWS DELETE FILE ERROR`, err);

                    return reject(err);
                }
                if (data) {
                    console.log(new Date(), "[driver] [DELETE] FILE DELETED SUCCESSFULLY");
                    resolve(data);
                }
            });
        });
    };

    Driverdocumentlinks.deleteFile = (data, res, options, cb) => {

        Driverdocumentlinks.app.models.AccessToken.findById(options.access_token, async (err, token) => {
            if (err || !token) {
                return res.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                // try {
                var updatedata = JSON.stringify(data);
                var parsedData = JSON.parse(updatedata);
                var fileName = parsedData.fileName;
                var driver_id = parsedData.driver_id;

                await deleteFileToS3(fileName);

                var originalFileName = fileName;

                await Driverdocumentlinks.destroyAll(
                    {
                        driver_id: driver_id,
                        file_name: originalFileName
                    });
                // } catch (error) {
                //     return res.status(400).json({ code: 400, error: "CODE ERROR" });
                // }

                cb(null, "success");
            }
        })

    };

    Driverdocumentlinks.getFileValidation = function (data, response, options, callback) {
        // try {
        var updatedata = JSON.stringify(data);
        var parsedData = JSON.parse(updatedata);
        var fileName = parsedData.fileName;
        s3.getObject(
            {
                Bucket: config.awsS3BucketName,
                Key: fileName
            }, function (err, s3Result) {
                if (err) {
                    console.error(new Date(), `[driver] [FETCH] AWS FILE RETREIVE ERROR`, err);

                    response.status(409).json({ code: 409, error: "S3 ERROR" });
                }
                else {
                    callback(null, {
                        ContentType: s3Result.ContentType,
                        buffer: s3Result.Body
                    });
                }
            });
        // } catch (error) {
        //     response.status(400).json({ code: 400, error: "CODE ERROR" });
        // }
    }

    Driverdocumentlinks.getFile = function (data, response, options, callback) {
        var self = this;
        this.validateAccessToken(options.access_token, function (err, tokenModel) {
            if (err || !tokenModel) {
                response.status(401).json({ code: 401, error: "ACCESS TOKEN FAILED" });
            } else {
                options.tokenModel = tokenModel;
                self.getFileValidation(data, response, options, callback);
            }
        });
    }

};
