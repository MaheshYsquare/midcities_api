console.log("NODE_ENV - ", process.env.NODE_ENV);

var loopback = require('loopback');

var app = require('./server/server.js');

var config = require('./server/boot/load-config').getConfig();

var OAuthClient = require('intuit-oauth');

app.oauthClient = new OAuthClient({
  clientId: config.quickBookClientId,
  clientSecret: config.quickBookClientSecret,
  environment: config.quickBookEnvironment,
  redirectUri: config.quickBookRedirectUri
});

app.sockets = new Set();

app.use(loopback.token({
  cookies: ['access_token'],
  headers: ['accesstoken', 'X-Access-Token']
}));

app.io = require('socket.io')(app.start());

app.use(function updateToken(req, res, next) {
  var token = req.accessToken; // get the accessToken from the request
  if (!token) return next(); // if there's no token we use next() to delegate handling back to loopback
  var now = new Date();
  // EDIT: to make sure we don't use a token that's already expired, we don't update it
  // this line is not really needed, because the framework will catch invalid tokens already
  if (token.created.getTime() + (token.ttl * 1000) < now.getTime()) return next();
  // performance optimization, we do not update the token more often than once per five minutes
  if (now.getTime() - token.created.getTime() < 300000) return next();
  token.updateAttribute('created', now, next); // save to db and move on
});

app.io.on('connection', socket => {

  socket.emit('addSocket', true);

  socket.on('login', data => {
    var AccessToken = app.models.AccessToken;
    //get credentials sent by the client
    if (data && data.userDetails) {
      AccessToken.find({
        where: {
          and: [{ userId: data.userDetails.userId }, { id: data.userDetails.id }]
        }
      }, function (err, tokenDetail) {
        if (err) throw err;
        if (tokenDetail.length) {
          if (!app.sockets.has(socket)) {
            socket.id = data.userDetails.userId;
            app.sockets.add(socket);
            console.log(new Date(), `[SOCKET] USER SOCKET ADDED ${socket.id}`, `TOTAL SOCKETS: ${app.sockets.size}`);
          }
        }
      });
    }
  });

  socket.on('logout', data => {
    app.sockets.delete(socket)
    console.log(new Date(), `[SOCKET] USER SOCKET DISCONNECTED ${socket.id}`, `REMAINING SOCKETS: ${app.sockets.size}`);
  })

  socket.on('changesInOrderDriver', data => {
    if (!app.sockets.has(socket)) {
      socket.id = data.userId;
      app.sockets.add(socket);
      console.log(new Date(), `[SOCKET] USER SOCKET ADDED ${socket.id}`, `TOTAL SOCKETS: ${app.sockets.size}`);
    }
    for (const s of app.sockets) {
      s.emit('data', { data: data.table });
    }
  })

  socket.on('sendNotification', data => {
    if (!app.sockets.has(socket)) {
      socket.id = data.userId;
      app.sockets.add(socket);
      console.log(new Date(), `[SOCKET] USER SOCKET ADDED ${socket.id}`, `TOTAL SOCKETS: ${app.sockets.size}`);
    }
    app.eventEmitter.emit('postNotification', data);
  })

  socket.on('disconnect', data => {
    app.sockets.delete(socket);
    console.log(new Date(), `[SOCKET] USER SOCKET DISCONNECTED ${socket.id}`, `REMAINING SOCKETS: ${app.sockets.size}`);
  });
});

process.on('uncaughtException', (error) => {

  console.log('Oh my god, something terrible happened: ', error);

})
